/**
 * \file      C:/Users/spanin/Portables/git/bin/git_irondoom/projects/ps2/RunTests/main.cpp
 * \brief     The Main class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   February  (the) 04(th), 2019, 17:15 MSK
 * \updated   February  (the) 04(th), 2019, 17:15 MSK
 * \TODO
**/

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "ps2/cpp/test/bitpack_test.h"
#include "ps2/cpp/test/base64pack_test.h"
#include "ps2/cpp/test/xformat_test.h"

int main(int argc, char* argv[]) {
  // global setup...

  int result = Catch::Session().run(argc, argv);

  // global clean-up...

  return result;
}
