#-------------------------------------------------
#
# Project created by QtCreator 2019-02-01T14:03:09
#
#-------------------------------------------------
PERFECT_PATH        = $$PWD/../perfect
PS2_PATH            = $$PWD/../
CATCH_PATH          = $$PWD/../../catch
OUTPUT_PATH         = $$PWD/../../../ps2_runtests/output


TEMPLATE            = app
TARGET              = RunTests
QT                 += sql core widgets xml network printsupport concurrent qml
QT                 -= gui
CONFIG             += console precompile_header c++1z
CONFIG             -= app_bundle
PRECOMPILED_HEADER  = main_test_pch.h
DEFINES            += QT _DEBUG "_TEST=0x0500" \
                      QT_DEPRECATED_WARNINGS

QMAKE_CXXFLAGS     += -DQT_NO_CAST_FROM_ASCII        \
                      -DQT_NO_CAST_TO_ASCII          \
                      -DQT_NO_CAST_FROM_BYTEARRAY    \
                      -DQT_NO_URL_CAST_FROM_STRING   \
                      -DQT_USE_QSTRINGBUILDER        \
                      -DQT_DISABLE_DEPRECATED_BEFORE
win32 {
    QMAKE_CXXFLAGS  += -std=c++17
#    QMAKE_CXXFLAGS  += -bigobj
#    QMAKE_CXXFLAGS  += -wd4146 \
#                       -wd4099
}

CONFIG(debug, debug|release) {
    LIBS    += -L$$PWD/../../bin/qmake/debug/ $$MY_LIBS

} else {
    CONFIG  -= console
    LIBS    += -L$$PWD/../../bin/qmake/release/ $$MY_LIBS
}

INCLUDEPATH += \
    $$PS2_PATH \
    $$CATCH_PATH

SOURCES += \
        main.cpp

HEADERS += \
    $$CATCH_PATH/catch.hpp \
    $$PS2_PATH/ps2/cpp/test/bitpack_test.h \
    $$PS2_PATH/ps2/cpp/test/base64pack_test.h \
    $$PS2_PATH/ps2/cpp/test/xformat_test.h

