``````
  _____   _____ ___  
 |  __ \ / ____|__ \ 
 | |__) | (___    ) |
 |  ___/ \___ \  / / 
 | |     ____) |/ /_ 
 |_|    |_____/|____|                 
                     
``````

What is it?
===========

The ps2 is a powerful and flexible library for c/c++/java/javascript.

Documentation
=============

The documentation available as of the date of this release is
included in HTML format in the docs/manual/ directory.

Platforms
=========

The following platforms and versions are tested and supported using

* Windows
* Linux
* FreeBSD

Installation
============

Please see the file called INSTALL.  
Platform specific notes can be found in README.platforms.

Licensing
=========

Please see the file called LICENSE.

Authors
=======
* Sergei Panin <dix75@mail.ru>

-------------------------------------------------------------------------------
Copyright (C) S.Panin, 2006 - 2016