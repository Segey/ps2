#!/usr/bin/python
# -*-coding: UTF-8 -*-
import os
import shutil

path="C:\\Qt\\"
files=["Qt5Widgets", 'Qt5Gui', 'Qt5Sql', 'Qt5Core', 'Qt5PrintSupport', 'Qt5QuickWidgets', 'Qt5WebChannel', 'Qt5Positioning'
       , 'Qt5Script', 'Qt5Xml', 'Qt5WebEngineWidgets', 'Qt5WebEngineCore', 'Qt5Quick', 'Qt5Network', 'Qt5Qml']
out="bin\\cmake\\Debug\\"


def updateFiles(path, debug=True, windows=True):
    ch = 'd' if debug else ''
    ext = '.dll' if windows else '.lib'
    for i, item in enumerate(files):
        files[i]= os.path.join(path, item + ch + '.dll')


def findPath(p, find='Qt5Widgetsd'):
    for root, dirs, files in os.walk(p):
        for file in files:
            if file.startswith(find):
                global path
                path=root
                return True
    print("=== Not found {} ===".format(find))
    return False


def copyFiles(out, files):
    for file in files:
        shutil.copy(file, out)
        print(" - {} copyied".format(file))


if __name__ == "__main__":
    if findPath(path, files[0]) is False:
        exit(1)

    updateFiles(path)
    copyFiles(out, files)
    print("copied {} files".format(len(files)))

