#!/usr/bin/python
# -*-coding: UTF-8 -*-
import os
import shutil

out="build/cmake-projects/ps2lib/"
h="ps2/qt/libs/"
cmake="cmake"
s="Visual Studio 15 2017 Win64"
#s="Visual Studio 14 2015 Win64"

if (__name__ == "__main__"):
    print("--- Begin ---")
    if(os.path.exists(out) is True):
        shutil.rmtree(out)
    os.makedirs(out)
    os.system("{} -G\"{}\" -B{} -H{}".format(cmake, s, out, h))
    print("--- End ---")

