/**
 * \file      ps2/ps2/visual/win_version.h
 * \brief     The main file "The OS Windows Algorithms"
 * \author    S.Panin <panin@ashmanov.com>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   May       (the) 01(th), 2012, 16:43 MSK
 * \updated   October   (the) 27(th), 2017, 11:06 MSK
 * \URL       http://msdn.microsoft.com/en-us/library/windows/desktop/ms724833(v=vs.85).aspx 
 * \TODO		
**/
#pragma once

typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);

#define WIN_UNDEFINED      0
#define WIN_2000           5
#define WIN_XP             6
#define WIN_XP_X64         7
#define WIN_SERVER_2003    8
#define WIN_HOME_SERVER    9
#define WIN_SERVER_2003_R2 10
#define WIN_VISTA          11
#define WIN_SERVER_2008    12
#define WIN_SERVER_2008_R2 13
#define WIN_7		       14
#define WIN_SERVER_2012    15
#define WIN_8			   16
#define WIN_8_1			   17
#define WIN_SERVER_2012_R2 18
#define WIN_10             19
#define WIN_SERVER_2016    20 

/** namespace ps2 */
namespace ps2 {
    
/**
 * \code
 *   const auto version = ps2::windowsVersion();
 *   if(WIN_XP == version || WIN_XP_X64 == version) return;
 * \endcode
**/
unsigned windowsVersion() {
	OSVERSIONINFOEX osvi;
	SYSTEM_INFO si;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	ZeroMemory(&si, sizeof(SYSTEM_INFO));

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if(NULL == GetVersionEx((OSVERSIONINFO*) &osvi)) return WIN_UNDEFINED;

	PGNSI pGNSI = (PGNSI) GetProcAddress(GetModuleHandle(_T("kernel32.dll")), "GetNativeSystemInfo");
	if(NULL != pGNSI) pGNSI(&si);
	else GetSystemInfo(&si);

	if (VER_PLATFORM_WIN32_NT != osvi.dwPlatformId) return WIN_UNDEFINED;
	if(5 == osvi.dwMajorVersion) { 
        if(NULL == osvi.dwMinorVersion) return WIN_2000;
        if(1 == osvi.dwMinorVersion) return WIN_XP;
        if(2 == osvi.dwMinorVersion) {
            if((osvi.wProductType == VER_NT_WORKSTATION) && (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)) return WIN_XP_X64;
            if(NULL == GetSystemMetrics(SM_SERVERR2)) return WIN_SERVER_2003;
            if(osvi.wSuiteMask & VER_SUITE_WH_SERVER) return WIN_HOME_SERVER;
            if(NULL != GetSystemMetrics(SM_SERVERR2)) return WIN_SERVER_2003_R2;
        }
    }
	if(6 == osvi.dwMajorVersion) { 
		if(0 == osvi.dwMinorVersion) {
			if(VER_NT_WORKSTATION == osvi.wProductType) return WIN_VISTA;
			else return WIN_SERVER_2008;
		}
		if(1 == osvi.dwMinorVersion) {
			if(VER_NT_WORKSTATION == osvi.wProductType) return WIN_7;
			else return WIN_SERVER_2008_R2;
		}
		if(2 == osvi.dwMinorVersion) {
			if(VER_NT_WORKSTATION == osvi.wProductType) return WIN_8;
			else return WIN_SERVER_2012;
		}
		if(3 == osvi.dwMinorVersion) {
			if(VER_NT_WORKSTATION == osvi.wProductType) return WIN_8_1;
			else return WIN_SERVER_2012_R2;
		}
    }
    if(10 == osvi.dwMajorVersion) { 
        if(0 == osvi.dwMinorVersion) {
            if(VER_NT_WORKSTATION == osvi.wProductType) return WIN_10;
            else return WIN_SERVER_2016;
        }
    }
	return WIN_UNDEFINED;
}

} // namespace ps2

