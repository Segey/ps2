/**
 * \file      c:/projects/irondoom/projects/irdObjects/classes/xformat.h
 * \brief     The Xformat class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.2.0
 * \created   February  (the) 07(th), 2019, 22:12 MSK
 * \updated   February  (the) 07(th), 2019, 22:12 MSK
 * \TODO
**/
#pragma once
#include <sstream>

/** \namespace ps2 */
namespace ps2 {

/**
 * \brief Creates a format string
 * \code
 *   std::wcout << ps2::xformat(L"444%s", L"bool");  // 444bool
 * \endcode
**/
static inline UnicodeString xformat(wchar_t const *s) {
    std::wstringstream out;
    while (*s) {
        if (*s == L'%' && *++s != L'%')
            throw std::runtime_error("invalid format string: missing arguments");
        out << *s++;
    }
    return out.str().c_str();
}
template<class T, class... Args>
static inline UnicodeString xformat(wchar_t const *s, T&& head, Args&&... args) {
    std::wstringstream out;
    while (*s) {
        if (*s == L'%' && *++s != L'%') {
            out << std::forward<T>(head) << xformat(++s, std::forward<Args>(args)...);
            return out.str().c_str();
        }
        out << *s++;
    }
    throw std::runtime_error("extra arguments provided to xformat");
}

}  // end namespace ps2
