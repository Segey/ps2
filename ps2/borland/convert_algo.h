/**
 * \file      /home/dix/projects/irondoom/projects/ps2/ps2/borland/convert_algo.h
 * \brief     The Convert_algo class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 23(th), 2019, 22:48 MSK
 * \updated   January   (the) 23(th), 2019, 22:48 MSK
 * \TODO
**/
#pragma once
#include <System.SysUtils.hpp>

/** \namespace ps2 */
namespace ps2 {
template<class T>
constexpr static inline bool isFloat(T&& val) noexcept {
    System::Extended val{};
    return TryStrToFloat(std::forward<T>(val), val);
}
template<class T>
constexpr static inline bool isInt(T&& val) noexcept {
    int val{};
    return  TryStrToInt(std::forward<T>(val), val);

}
}  // end namespace ps2
