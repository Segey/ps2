/**
 * \file      projects/ps2/ps2/borland/this.h
 * \brief     The Convert class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.1
 * \created   January   (the) 14(th), 2019, 10:16 MSK
 * \updated   January   (the) 14(th), 2019, 10:16 MSK
 * \TODO
**/
#pragma once
#include <string>

/** \namespace ps2::borland */
namespace ps2::borland {

/**
 * \code
 *      auto const val = ps2::borland::toString(this);
**/
template<class T>
static inline std::string toStdString(T* const form) noexcept {
	auto const val = reinterpret_cast<std::uint64_t>(form);
	return std::to_string(val);
}
template<class T>
static inline std::wstring toStdWString(T* const form) noexcept {
	auto const val = reinterpret_cast<std::uint64_t>(form);
    return std::to_wstring(val);
}
template<class T>
static inline UnicodeString toString(T* const form) noexcept {
	return UnicodeString(toStdString(form).c_str());
}
}  // end namespace ps2::borland

