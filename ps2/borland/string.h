/**
 * \file      projects/ps2/ps2/borland/string.h
 * \brief     The String class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 12(th), 2019, 01:09 MSK
 * \updated   January   (the) 12(th), 2019, 01:09 MSK
 * \TODO
**/
#pragma once
/** \namespace ps2::borland */
namespace ps2::borland {

static inline std::string toStdStr(AnsiString const& str) noexcept {
    return std::string(str.c_str());
}
//static inline std::string toStdStr(UnicodeString const& str) noexcept {
//    return std::string(str.w_str());
//}
//static inline std::wstring toStdWStr(AnsiString const& str) noexcept {
//    return std::wstring(str.c_str());
//}
static inline std::wstring toStdWStr(UnicodeString const& str) noexcept {
    return std::wstring(str.c_str());
}
static inline AnsiString toAString(std::string const& str) noexcept {
    return AnsiString(str.c_str(), str.length());
}
//static inline AnsiString toAString(std::wstring const& str) noexcept {
//    return AnsiString(str.w_str(), str.length());
//}
//static inline AnsiString toAString(AnsiString const& codepage, std::string const& str) noexcept {
//    AnsiString result;
//    if (!str.empty()) {
//        int len = UnicodeFromLocaleChars(codepage, 0, str.c_str(), str.length(), nullptr, 0);
//        if (len > 0) {
//            result.SetLength(len);
//            UnicodeFromLocaleChars(codepage, 0, str.c_str(), str.length(), result.c_str(), len);
//        }
//    }
//    return result;
//}
static inline UnicodeString toUString(std::string const& str) noexcept {
    return UnicodeString(str.c_str(), str.length());
}
static inline UnicodeString toUString(std::wstring const& str) noexcept {
    return UnicodeString(str.c_str(), str.length());
}
//static inline UnicodeString toUString(UnicodeString const& codepage, std::string const& str) noexcept {
//	UnicodeString result;
//	if (!str.empty()) {
//		int len = UnicodeFromLocaleChars(codepage, 0, str.c_str(), str.length(), nullptr, 0);
//		if (len > 0) {
//			result.SetLength(len);
//			UnicodeFromLocaleChars(codepage, 0, str.c_str(), str.length(), result.c_str(), len);
//		}
//	}
//	return result;
//}

} // end namespace ps2::borland

