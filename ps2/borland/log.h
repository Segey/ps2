/**
 * \file      c:/projects/irondoom/projects/ps2/ps2/borland/log.h
 * \brief     The Log class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   February  (the) 27(th), 2019, 12:41 MSK
 * \updated   February  (the) 27(th), 2019, 12:41 MSK
 * \TODO
**/
#pragma once
#include <iostream>
#include <fstream>
#include "xformat.h"

/** begin namespace ps2 */
namespace ps2 {

/**
 * \code
     LOG("Unsupported char", ch);
 * \endcode
 **/
class ProgramLog final {
public:
    using class_name = ProgramLog;

private:
    UnicodeString m_name;
    char const* m_filename = nullptr;
    char const* m_file_out = nullptr;
    unsigned m_line        = 0u;

public:
	ProgramLog() = default;
    ProgramLog(UnicodeString const& name, char const* file, char const* filename, unsigned line) noexcept
        : m_name(name)
        , m_file_out(file)
        , m_filename{filename}
        , m_line{line} {
    }
    template<class... Args>
    [[noreturn]]void operator()(UnicodeString const& msg, Args&&... args) const {
		auto const& val = xformat(msg, std::forward<Args>(args)...);
		std::wofstream out(m_filename, std::ios::app);
        out << xformat(L"%s(%s)[%s]- %s", m_filename, m_line, m_name, val) << std::endl;
	}
	template<class... Args>
    [[noreturn]]void operator()(std::string const& msg, Args&&... args) const {
        (*this)(UnicodeString(msg.c_str()), std::move(args)...);
    }
    template<class... Args>
    [[noreturn]]void operator()(std::wstring const& msg, Args&&... args) const {
        (*this)(UnicodeString(msg.c_str()), std::move(args)...);
    }
    template<class... Args>
    [[noreturn]]void operator()(char const* msg, Args&&... args) const {
        (*this)(UnicodeString(msg), std::move(args)...);
    }
    template<class... Args>
    [[noreturn]]void operator()(wchar_t const* msg, Args&&... args) const {
        (*this)(UnicodeString(msg), std::move(args)...);
    }
};

} // end namespace ps2

