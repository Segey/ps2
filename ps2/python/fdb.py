#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import kinterbasdb
import sqlite3

indb  = "/www/food.irf"
outdb = "/www/food.tfo"

class Script:
    def __init__(self):
        self.conn = sqlite3.connect(outdb)
        self.cursor = c = self.conn.cursor()
    def createCategories(self):
        self.cursor.execute("CREATE TABLE Categories("
           "cid           INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
           ",type         INTEGER"
           ",title        TEXT NOT NULL"
           ",description  TEXT"
           ",createdAt    DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"
           ",updatedAt    DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"
           ");")
        self.conn.commit()
        self.cursor.execute("CREATE UNIQUE INDEX CategoriesUniqueIndex ON Categories(cid);")
        self.conn.commit()
    def close(self):
        self.conn.close()
class Insert:
    def __init__(self):
        self.conn = sqlite3.connect(outdb)
        self.cursor = c = self.conn.cursor()
    def insertCategories(self, data):
        self.cursor.executemany("INSERT INTO Categories(cid, type, title) VALUES(?,1,?);", data)
        self.conn.commit()
    def close(self):
        self.conn.close()
class SqliteDb:
    def __init__(self):
        print "Sqlite db init"
        self.m_fdb = FirebirdDb()
    def createDb(self):
        if os.path.exists(outdb):
            os.unlink(outdb)
        open(outdb, 'a').close()
        script = Script()
        script.createCategories()
        script.close()
    def insertData(self):
        sdb = Insert()
        categories = self.m_fdb.selectCategoried()
        sdb.insertCategories(categories)
        sdb.close()
    def close(self):
        self.m_fdb.close()
class FirebirdDb:
    def __init__(self):
        print "Firebird db init"
        self.conn = kinterbasdb.connect(dsn='/www/food.irf', user='sysdba', password='masterkey')
        self.cursor = self.conn.cursor()
    def selectCategoried(self):
        self.cursor.execute("SELECT * FROM CATEGORIES;")
        return self.cursor.fetchall()
    def close(self):
        self.conn.close()

if  __name__ ==  "__main__":
    sdb = SqliteDb()
    sdb.createDb()
    sdb.insertData()
    sdb.close()
