#!/usr/bin/perl
# vim: ft=perl
# space2tab: ok
# #===============================================================================
#
#         FILE:  sendmail.pl
#
#        USAGE:  ./sendmail.pl  < email.txt
#         
#      OPTIONS:  ---
# REQUIREMENTS:  Net::SMTP::TLS
#        NOTES:  ---
#       AUTHOR:  Anatoliy Grishaev (), grian@cpan.org
#      VERSION:  1.0
#      CREATED:  08/16/2012 18:39:17
#===============================================================================
use strict;
use warnings;
use Data::Dumper;
#use Net::SMTP::TLS;
use Net::SMTP::SSL;
our $CRLF="\r\n";

=head1 config example
{
    user => '0body0@rambler.**',
    server => 'smtp.rambler.**',
    password => '*****',
    mailfrom => '0body0@rambler.**',
}
=cut

sub get_config{
    my $file = "$ENV{HOME}/.config/sendmail";
    open my $fh, "<", $file or die "open: $file - $!";
    my $e = do { local $/; my $e = "#line 1 '$file'\n" . <$fh>; };
    my $sendmail = eval $e or die "syntax: $file - $@";
}

sub get_smtp_server {
    my $sendmail = shift;
    my @d        = (
            $sendmail->{server},
            Hello => 'my.mail.ru',
            User     => $sendmail->{user},
            Port     => 465,
            Password => $sendmail->{password}
    );

    my $smtp = Net::SMTP::SSL->new(@d);
    print STDERR $smtp->domain, "\n";
    return $smtp;
}
sub main{
    my @argv  = @_;
    my $sendmail = get_config();

    my $m = get_smtp_server($sendmail);
    my $header;
    my @addr;
    while(<STDIN>){
	chomp;
	s/\r\z//;
	push @addr, $1 if m/\A(?:To|CC):\s*(.*)/i;
	if ( m/\ABCC:\s*(.*)/i){
	    push @addr, $1;
	}
	else {
	    $header .= $_ . $CRLF;
	}
	last if ! length;
    }
    for ( @addr ){
	if ( m/<([-a-z\d\.@]+)>/){
	    $_ = $1;
	}
	else {
	    s/\s+\z//;
	    $_ = undef if m/\s/;
	}
    }
    my %s;
    @addr = grep !$s{ $_ }++, @addr;
    $m->auth( $sendmail->{user}, $sendmail->{password} ) or warn ("auth");
    $m->mail( $sendmail->{mailfrom} ) or warn("mail");
    $m->to( @addr ) or warn("to");
    $m->data or warn("data");
    $m->datasend( $header ) or warn("datasend");
    while(<STDIN>){
	$m->datasend( $_ ) or warn("datasend");
    }
    $m->dataend or warn("dataend");
    $m->quit;
};

::main(@ARGV);
