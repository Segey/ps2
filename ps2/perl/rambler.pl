#!/usr/bin/perl
#===============================================================================
#
#         FILE:  send_sms.pl
#
#        USAGE:  ./send_sms.pl  
#
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Anatoliy Grishaev (), grian@cpan.org
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  08/09/2012 16:56:38
#     REVISION:  ---
#===============================================================================
use strict;
use warnings;
use bytes;


sub sms{
    my $to      = shift;
    my $subject = shift;
    my $message = shift;
    my $header = <<"A";
To: $to
Subject: $subject 
Content-type: text/plain; charset=UTF-8
A
    my $body = $header ."\n".$message;
    $body =~s/\r?\n/\n/g;
    open my $fh, "|-", "./sendmail.pl -t";
    print $fh $body;
}
sub main{
    my @argv  = @_;

#    sms( '0body0@rambler.ru', "subject", "Привет");
    sms( 'agrishaev@mail.ru', "subject", "Привет из скрипта");
    
};

::main(@ARGV);
