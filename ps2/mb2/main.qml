import QtQuick 2.4
import QtQuick.Controls 1.3
import mb 2.0

Item {
    Button {
        width:  200
        height: 200

        Chart {
            anchors.centerIn: parent
            anchors.fill: parent
            id: aPieChart
            title: "Cool ffffff"
            firstValue: 175
            secondValue: 100
            colorType: ColorType.Blue
            textType: TextType.None
            figureType: FigureType.SimplePie

            Menu {
                id: aChartMenu
                Menu {
                    title: qsTr("Type")
                    ExclusiveGroup {
                        id: aChartType
                    }
                    MenuItem {
                        text: qsTr("Simple Pie")
                        checkable: true
                        checked: true
                        exclusiveGroup: aChartType
                        onTriggered: aPieChart.figureType = FigureType.SimplePie
                    }
                    MenuItem {
                        text: qsTr("Fill Pie")
                        checkable: true
                         exclusiveGroup: aChartType
                        onTriggered: aPieChart.figureType = FigureType.FillPie
                    }
                    MenuItem {
                        text: qsTr("Simple Histogram")
                        checkable: true
                         exclusiveGroup: aChartType
                        onTriggered: aPieChart.figureType = FigureType.SimpleHistogram
                    }
                    MenuItem {
                        text: qsTr("Fill Histogram")
                        checkable: true
                        exclusiveGroup: aChartType
                        onTriggered: aPieChart.figureType = FigureType.FillHistogram
                    }
                }
                Menu {
                    title: qsTr("Text")
                    ExclusiveGroup {
                        id: aChartText
                    }
                    MenuItem {
                        text: qsTr("None")
                        checkable: true
                        checked: true
                        exclusiveGroup: aChartText
                        onTriggered: aPieChart.textType = TextType.None
                    }
                    MenuItem {
                        text: qsTr("Normal")
                        checkable: true
                        exclusiveGroup: aChartText
                        onTriggered: aPieChart.textType = TextType.Normal
                    }
                    MenuItem {
                        text: qsTr("Percent")
                        checkable: true
                        exclusiveGroup: aChartText
                        onTriggered: aPieChart.textType = TextType.Percent
                    }
                }
                Menu {
                    title: qsTr("Color")
                    ExclusiveGroup {
                        id: aChartColor
                    }
                    MenuItem {
                        text: qsTr("Red")
                        checkable: true
                        checked: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Red
                    }
                    MenuItem {
                        text: qsTr("Green")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Green
                    }
                    MenuItem {
                        text: qsTr("Blue")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Blue
                    }
                    MenuItem {
                        text: qsTr("Cyan")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Cyan
                    }
                    MenuItem {
                        text: qsTr("Magenta")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Magenta
                    }
                    MenuItem {
                        text: qsTr("Yellow")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Yellow
                    }
                    MenuItem {
                        text: qsTr("Gray")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Gray
                    }
                    MenuItem {
                        text: qsTr("White/Black")
                        checkable: true
                        exclusiveGroup: aChartColor
                        onTriggered: aPieChart.colorType = ColorType.Black
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                onClicked: {
                    if (mouse.button == Qt.RightButton)
                        aChartMenu.popup();
                }
            }
        }
    }
}
