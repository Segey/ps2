/**
 * \file      ps2/ps2/mb2/classes/type/painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 17:26 MSK
 * \updated   July (the) 28(th), 2015, 17:26 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QLinearGradient>

/** \namespace mb */
namespace mb {
    namespace {
        inline QLinearGradient createGradient(QRectF const& rect) {
            return QLinearGradient(0,0, rect.width(), rect.height());
        }
    }
inline QLinearGradient whiteGradient(QRectF const& rect) {
    auto gradient = createGradient(rect);
    gradient.setColorAt(0.0, Qt::white);
    gradient.setColorAt(0.8, Qt::transparent);
    return gradient;
}
inline QLinearGradient colorGradient(QRectF const& rect, Color const& color) {
    auto gradient = createGradient(rect);
    gradient.setColorAt(0.1, color.light());
    gradient.setColorAt(0.8, color.dark());
    gradient.setColorAt(1.0, color.dark());
    return gradient;
}
    
} // end namespace mb
