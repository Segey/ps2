/**
 * \file      ps2/ps2/mb2/classes/type/histogram/fill_histogram/fill_histogram.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 19:40 MSK
 * \updated   July (the) 30(th), 2015, 19:40 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include "../histogram.h"
#include "../determinator/first_determinator.h"
#include "../determinator/second_determinator.h"

/** \namespace mb */
namespace mb {

class FillHistogram: public Histogram {
public:
    typedef FillHistogram  class_name;
    typedef Histogram      inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::FillHistogram;
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE {
        painter->setPen(Qt::transparent);
        draw<histogram::FirstPainter>(this, painter, true);
        draw<histogram::SecondPainter>(this, painter, true);
    }
}; 

} // end namespace mb
