/**
 * \file      ps2/ps2/mb2/classes/type/histogram/station.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 29(th), 2015, 12:28 MSK
 * \updated   July (the) 29(th), 2015, 12:28 MSK
 * \TODO      
**/
#pragma once
#include <QApplication>
#include "classes/item.h"

/** \namespace mb::histogram */
namespace mb {
namespace histogram {

class Station: public Item {
public:
    typedef Station class_name;
    typedef Item   inherited;

public:
    double step() const {
        return inherited::rect().width() / 8;
    }
    double height() const {
        return inherited::rect().height() - inherited::textHeight();
    }
    double value(double val) const {
        return val * height() / inherited::values().max();
    }
public:
    Station(QRectF const& rect, UintValues const& values)
        :inherited(rect, values) {
    }
};
    
}} // end namespace mb::histogram
