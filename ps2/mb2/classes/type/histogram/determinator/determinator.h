/**
 * \file      ps2/ps2/mb2/classes/type/histogram/determinator/determinator.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 29(th), 2015, 12:25 MSK
 * \updated   July (the) 29(th), 2015, 12:25 MSK
 * \TODO      
**/
#pragma once
#include "../station.h"

/** \namespace mb::histogram */
namespace mb {
namespace histogram {

class Determinator: public Station {
public:
    typedef Determinator class_name;
    typedef Station      inherited;

private:
    StringsValues m_strings;

    double x() const {
        return rect().x() + step() * doOffset() - inherited::textWidth(doText())/ 2;
    }
    double y() const {
        return height() - inherited::value(doValue());
    }
protected:
    StringsValues const& strings() const {
        return m_strings;
    }
    virtual int doOffset()   const = 0;
    virtual QString doText() const = 0;
    virtual double doValue() const = 0;

public:
    Determinator(QRectF const& rect, UintValues const& values, StringsValues const& strings)
        :inherited(rect, values), m_strings(strings) {
    }
    QRectF evaluate() const {
        return QRectF(x(), y() + textHeight()*1.5, inherited::textWidth(doText()), inherited::textHeight());
    }
};
}} // end namespace mb::histogram
