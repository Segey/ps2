/**
 * \file      ps2/ps2/mb2/classes/type/histogram/determinator/second_determinator.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 29(th), 2015, 12:49 MSK
 * \updated   July (the) 29(th), 2015, 12:49 MSK
 * \TODO      
**/
#pragma once
#include "determinator.h"

/** \namespace mb::histogram */
namespace mb {
namespace histogram {

class SecondDeterminator: public Determinator {
public:
    typedef SecondDeterminator class_name;
    typedef Determinator       inherited;

private: 
    virtual int doOffset() const Q_DECL_OVERRIDE {
        return 6;
    }
    virtual QString doText() const Q_DECL_OVERRIDE {
        return strings().second();
    }
    virtual double doValue() const Q_DECL_OVERRIDE {
        return values().second();
    }
public:
    SecondDeterminator(QRectF const& rect, UintValues const& values, StringsValues const& strings)
        :inherited(rect,values, strings) {
    }
};
}} // end namespace mb::histogram
