/**
 * \file      ps2/ps2/mb2/classes/type/histogram/histogram.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 18:06 MSK
 * \updated   July (the) 28(th), 2015, 18:06 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include "../figure_type.h"
#include "../figure.h"
#include "../exec.h"
#include "painter/first_painter.h"
#include "painter/second_painter.h"
#include "determinator/first_determinator.h"
#include "determinator/second_determinator.h"

/** \namespace mb */
namespace mb {

class Histogram: public Figure {
public:
    typedef Histogram      class_name;
    typedef Figure         inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE = 0;
    virtual RectFValues doTextPlaces(StringsValues const& pair) const Q_DECL_OVERRIDE {
        return RectFValues(createRect<histogram::FirstDeterminator>(this, pair)
                         , createRect<histogram::SecondDeterminator>(this, pair));
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE = 0;
}; 

} // end namespace mb
