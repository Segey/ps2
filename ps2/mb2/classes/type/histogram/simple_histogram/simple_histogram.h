/**
 * \file      ps2/ps2/mb2/classes/type/histogram/simple_histogram/simple_histogram.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 20:04 MSK
 * \updated   July (the) 30(th), 2015, 20:04 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include "../histogram.h"
#include "../determinator/first_determinator.h"
#include "../determinator/second_determinator.h"

/** \namespace mb */
namespace mb {

class SimpleHistogram: public Histogram {
public:
    typedef SimpleHistogram class_name;
    typedef Histogram       inherited;
    typedef Figure::type_t  type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::SimpleHistogram;
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE {
        painter->setPen(color().pen());
        draw<histogram::FirstPainter>(this, painter, false);
        draw<histogram::SecondPainter>(this, painter, false);
    }
}; 

} // end namespace mb
