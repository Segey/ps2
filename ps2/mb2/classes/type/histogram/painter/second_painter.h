/**
 * \file      ps2/ps2/mb2/classes/type/histogram/painter/second_painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 29(th), 2015, 13:34 MSK
 * \updated   July (the) 29(th), 2015, 13:34 MSK
 * \TODO      
**/
#pragma once
#include "painter.h"

/** \namespace mb::histogram */
namespace mb {
namespace histogram {

class SecondPainter: public Painter { 
public:
    typedef SecondPainter class_name;
    typedef Painter       inherited;

protected: 
    virtual int doOffset() const Q_DECL_OVERRIDE {
        return 5;
    }
    virtual double doValue() const Q_DECL_OVERRIDE {
       return values().second(); 
    }
public:
    SecondPainter(QRectF const& rect, UintValues const& values, Color const& color)
        :inherited(rect, values, color) {
    }
};

}} // end namespace mb::histogram
