/**
 * \file      ps2/ps2/mb2/classes/type/histogram/painter/painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 18:48 MSK
 * \updated   July (the) 28(th), 2015, 18:48 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QLinearGradient>
#include "../station.h"
#include "classes/color.h"
#include "classes/type/painter.h"

/** \namespace mb::histogram */
namespace mb {
namespace histogram {

class Painter: public Station { 
public:
    typedef Painter class_name;
    typedef Station inherited;

private: 
    Color m_color;

protected:
    virtual int doOffset()   const = 0;
    virtual double doValue() const = 0;

public:
    Painter(QRectF const& rect, UintValues const& values, Color const& color)
        :inherited(rect, values), m_color(color) {
    }
    void paint(QPainter* painter, bool isShow) {
        if(isShow) painter->setBrush(mb::colorGradient(inherited::rect(), m_color));
        painter->drawRect(rect().x() + step() * doOffset()
                          , rect().y() + rect().height()
                          , step() * 2
                          , -value(doValue()));
    }
};
    
}} // end namespace mb::histogram
