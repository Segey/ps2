/**
 * \file      ps2/ps2/mb2/classes/type/figure_none.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 18:04 MSK
 * \updated   July (the) 25(th), 2015, 18:04 MSK
 * \TODO      
**/
#pragma once
#include <QPainter>
#include "figure.h"

/** \namespace mb */
namespace mb {

class FigureNone: public Figure {
public:
    typedef FigureNone     class_name;
    typedef Figure         inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::None;
    }
    virtual void doPaint(QPainter*) const Q_DECL_OVERRIDE {
    }
    RectFValues doTextPlaces(StringsValues const&) const Q_DECL_OVERRIDE {
        return RectFValues();
    }
}; 

} // end namespace mb
