/**
 * \file      ps2/ps2/mb2/classes/type/figure.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 18:02 MSK
 * \updated   July (the) 25(th), 2015, 18:02 MSK
 * \TODO      
**/
#pragma once
#include <QPainter>
#include "classes/item.h"
#include "classes/color.h"
#include "classes/values.h"
#include "figure_type.h"

/** \namespace mb */
namespace mb {

class Figure: public Item {
public:
    typedef Figure           class_name;
    typedef FigureType::Type type_t;

private:
    Color m_color;

protected:
    virtual type_t doType() const = 0;
    virtual void doPaint(QPainter* painter) const = 0;
    virtual RectFValues doTextPlaces(StringsValues const& tpair) const = 0;

public:
    Figure() = default;
    Color const& color() const {
        return m_color;
    }
    void setColor(Color const& color) {
        m_color = color;
    }
    void setColor(int id) {
        m_color.setColorType(static_cast<ColorType::Type>(id));
    }
    type_t type() const {
        return doType();
    }
    void paint(QPainter* painter) {
        doPaint(painter);
    }
    RectFValues textPlaces(StringsValues const& pair) const {
        return doTextPlaces(pair);
    }
};

} // end namespace mb
