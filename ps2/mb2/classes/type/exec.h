/**
 * \file      ps2/ps2/mb2/classes/type/exec.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 19:03 MSK
 * \updated   July (the) 30(th), 2015, 19:03 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>

/** \namespace mb */
namespace mb {

    template<typename U, typename T>
    QRectF createRect(T const* cls, StringsValues const& strings) {
        U item(cls->rect(), cls->values(), strings);
        return item.evaluate();
    }
    template<typename U, typename T>
    void draw(T const* cls, QPainter* painter, bool isShow) {
        U item(cls->rect(), cls->values(), cls->color());
        return item.paint(painter, isShow);
    }

} // end namespace mb
