/**
 * \file      ps2/ps2/mb2/classes/type/figure_type.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   July (the) 25(th), 2015, 18:00 MSK
 * \updated   July (the) 25(th), 2015, 18:00 MSK
 * \TODO      
**/
#pragma once
#include <QtQml>
#include <QObject>

/** \namespace mb */
namespace mb {

class FigureType : public QObject {
public:
    typedef FigureType class_name;
    typedef QObject    inherited;

Q_OBJECT

public:
    enum class Type: std::uint8_t {
        Invalid           = 0u
        , None            = 1u
        , SimpleHistogram = 2u
        , FillHistogram   = 4u
        , SimplePie       = 8u
        , FillPie         = 16u
    };
    Q_ENUM(Type)
};

} // end namespace mb

