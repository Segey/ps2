/**
 * \file      ps2/ps2/mb2/classes/type/pie/station.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 16:23 MSK
 * \updated   July (the) 30(th), 2015, 16:23 MSK
 * \TODO      
**/
#pragma once
#include "classes/item.h"

/** \namespace mb::pie */
namespace mb {

namespace pie {
class Station: public Item { 
public:
    typedef Station class_name;
    typedef Item    inherited;

private:
    double first() const {
        return values().first() % values().second();
    }
    double value() const {
        return static_cast<double>(values().second() - first());
    }
public:
    Station(QRectF const& rect, UintValues const& values) 
        : inherited(rect, values) {
    }
    double angle() const {
        return 360 * (value() / values().second());
    }
};

}} // end namespace mb::pie
