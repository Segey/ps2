/**
 * \file      ps2/ps2/mb2/classes/type/pie/simple_pie/simple_pie.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 22:13 MSK
 * \updated   July (the) 30(th), 2015, 22:13 MSK
 * \TODO      
**/
#pragma once
#include <QPainter>
#include "../pie.h"

/** \namespace mb */
namespace mb {

class SimplePie: public Pie {
public:
    typedef SimplePie   class_name;
    typedef Pie         inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::SimplePie;
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE {
        painter->setPen(color().pen());
        draw<pie::FirstPainter>(this, painter, false);
        draw<pie::SecondPainter>(this, painter, false);
    }
}; 

} // end namespace mb
