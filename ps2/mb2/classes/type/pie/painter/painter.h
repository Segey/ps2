/**
 * \file      ps2/ps2/mb2/classes/type/pie/painter/painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 18:41 MSK
 * \updated   July (the) 30(th), 2015, 18:41 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QLinearGradient>
#include "../station.h"
#include "classes/color.h"
#include "classes/type/painter.h"

/** \namespace mb::pie */
namespace mb {
namespace pie {

class Painter: public Station { 
public:
    typedef Painter class_name;
    typedef Station inherited;

private: 
    Color m_color;

protected:
    virtual double doAngle() const = 0;
    virtual QLinearGradient doGradient() const = 0;

    Color const& color() const {
        return m_color;
    }
public:
    Painter(QRectF const& rect, UintValues const& values, Color const& color)
        :inherited(rect, values), m_color(color) {
    }
    void paint(QPainter* painter, bool isShow) {
        if(isShow) painter->setBrush(doGradient());
        painter->drawPie(inherited::rect(), 0, doAngle() * 16);
    }
};
    
}} // end namespace mb::pie
