/**
 * \file      ps2/ps2/mb2/classes/type/pie/painter/first_painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 18:42 MSK
 * \updated   July (the) 30(th), 2015, 18:42 MSK
 * \TODO      
**/
#pragma once
#include "painter.h"

/** \namespace mb::pie */
namespace mb {
namespace pie {

class FirstPainter: public Painter { 
public:
    typedef FirstPainter class_name;
    typedef Painter inherited;

protected: 
    virtual double doAngle() const Q_DECL_OVERRIDE {
        return 360.0;
    }
    virtual QLinearGradient doGradient() const Q_DECL_OVERRIDE {
        return mb::colorGradient(inherited::rect(), inherited::color());
    }
public:
    FirstPainter(QRectF const& rect, UintValues const& values, Color const& color)
        :inherited(rect,values, color) {
    }
};

}} // end namespace mb::pie
