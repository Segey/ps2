/**
 * \file      ps2/ps2/mb2/classes/type/pie/painter/second_painter.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 18:42 MSK
 * \updated   July (the) 30(th), 2015, 18:42 MSK
 * \TODO      
**/
#pragma once
#include "painter.h"

/** \namespace mb::pie */
namespace mb {
namespace pie {

class SecondPainter: public Painter { 
public:
    typedef SecondPainter class_name;
    typedef Painter       inherited;

protected: 
    virtual double doAngle() const Q_DECL_OVERRIDE {
        return angle();
    }
    virtual QLinearGradient doGradient() const Q_DECL_OVERRIDE {
        return mb::whiteGradient(inherited::rect());
    }
public:
    SecondPainter(QRectF const& rect, UintValues const& values, Color const& color)
        :inherited(rect, values, color) {
    }
};

}} // end namespace mb::pie
