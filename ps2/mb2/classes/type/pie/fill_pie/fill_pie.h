/**
 * \file      ps2/ps2/mb2/classes/type/pie/fill_pie/fill_pie.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 30(th), 2015, 22:19 MSK
 * \updated   July (the) 30(th), 2015, 22:19 MSK
 * \TODO      
**/
#pragma once
#include <QPainter>
#include "../pie.h"

/** \namespace mb */
namespace mb {

class FillPie: public Pie {
public:
    typedef FillPie        class_name;
    typedef Pie            inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::FillPie;
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE {
        painter->setPen(Qt::transparent);
        draw<pie::FirstPainter>(this, painter, true);
        draw<pie::SecondPainter>(this, painter, true);
    }
}; 

} // end namespace mb
