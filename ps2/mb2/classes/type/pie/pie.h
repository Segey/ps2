/**
 * \file      ps2/ps2/mb2/classes/type/pie/pie.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 17:55 MSK
 * \updated   July (the) 25(th), 2015, 17:55 MSK
 * \TODO      
**/
#pragma once
#include <QPainter>
#include "../figure.h"
#include "../painter.h"
#include "../exec.h"
#include "painter/first_painter.h"
#include "painter/second_painter.h"
#include "determinator/first_determinator.h"
#include "determinator/second_determinator.h"

/** \namespace mb */
namespace mb {

class Pie: public Figure {
public:
    typedef Pie            class_name;
    typedef Figure         inherited;
    typedef Figure::type_t type_t;

private:
    virtual type_t doType() const Q_DECL_OVERRIDE = 0;
    RectFValues doTextPlaces(StringsValues const& pair) const Q_DECL_OVERRIDE {
        return RectFValues(createRect<pie::FirstDeterminator>(this, pair)
                         , createRect<pie::SecondDeterminator>(this, pair));
    }
    virtual void doPaint(QPainter* painter) const Q_DECL_OVERRIDE = 0; 
};

} // end namespace mb
