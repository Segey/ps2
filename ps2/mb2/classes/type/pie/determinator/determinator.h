/**
 * \file      ps2/ps2/mb2/classes/type/pie/determinator/determinator.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 17:02 MSK
 * \updated   July (the) 28(th), 2015, 17:02 MSK
 * \TODO      
**/
#pragma once
#include <QtMath>
#include <QRectF>
#include <QApplication>
#include <QFontMetrics>
#include "../station.h"

/** \namespace mb::pie */
namespace mb {
namespace pie {

class Determinator: public Station {
public:
    typedef Determinator class_name;
    typedef Station      inherited;

private:
    StringsValues m_strings;

protected:
    virtual QString doText() const = 0;
    virtual double updateValue(double val) const = 0;

    StringsValues strings() const {
        return m_strings;
    }
    double textWidth() const {
        return inherited::textWidth(doText());
    }
    double rx() const {
        return rect().width() / 2;
    }
    double ry() const {
        return rect().height() / 2;
    }
    template<typename F>
    double pos(F fun, double value) const {
        return fun(qDegreesToRadians(angle() / 2)) * (value/2);
    }
    double left() const {
        const double x = updateValue(pos(qFastCos, rx()));
        return rect().x() + rx() - x - textWidth() / 2;
    }
    double top() const {
        const double y = updateValue(pos(qFastSin,ry()));
        return rect().y() + ry() + y - textHeight() / 2;
    }
public:
    Determinator(QRectF const& rect, UintValues const& values, StringsValues const& strings)
        : inherited(rect, values), m_strings(strings) {
    }
    QRectF evaluate() const {
        return QRectF( left(), top(), textWidth(), textHeight());
    }
};


}} // end namespace mb::pie
