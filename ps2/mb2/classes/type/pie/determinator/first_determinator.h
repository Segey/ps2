/**
 * \file      ps2/ps2/mb2/classes/type/pie/determinator/first_determinator.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 29(th), 2015, 12:49 MSK
 * \updated   July (the) 29(th), 2015, 12:49 MSK
 * \TODO      
**/
#pragma once
#include "determinator.h"

/** \namespace mb::histogram */
namespace mb {
namespace pie {

class FirstDeterminator: public Determinator {
public:
    typedef FirstDeterminator class_name;
    typedef Determinator      inherited;

protected:
    virtual QString doText() const Q_DECL_OVERRIDE {
        return strings().first();
    }
    virtual double updateValue(double val) const Q_DECL_OVERRIDE {
        return val;
    }
public:
    FirstDeterminator(QRectF const& rect, UintValues const& values, StringsValues const& strings)
        :inherited(rect,values, strings) {
    }
};

}} // end namespace mb::histogram
