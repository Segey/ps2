/**
 * \file      ps2/ps2/mb2/classes/text/text_type.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 17:37 MSK
 * \updated   July (the) 25(th), 2015, 17:37 MSK
 * \TODO      
**/
#pragma once
#include <QtQml>
#include <QObject>

/** \namespace mb */
namespace mb {

class TextType : public QObject {
public:
    typedef TextType class_name;
    typedef QObject  inherited;

Q_OBJECT

public:
    enum class Type: std::uint8_t {
        Invalid     = 0u
        , None      = 1u
        , Normal    = 2u
        , Percent   = 4u
    };
Q_ENUM(Type)

};

} // end namespace mb
