/**
 * \file      ps2/ps2/mb2/classes/text/text_none.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 27(th), 2015, 23:07 MSK
 * \updated   July (the) 27(th), 2015, 23:07 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include "text.h"

/** \namespace mb */
namespace mb {

class TextNone:public Text {
public:
    typedef TextNone            class_name;
    typedef Text                inherited;
    typedef inherited::type_t   type_t;

protected:
    virtual type_t doType() const {
        return type_t::None;
    }
    virtual StringsValues doValues() const Q_DECL_OVERRIDE {
        return StringsValues();
    }
    virtual void doPaint(QPainter*, RectFValues const&) const Q_DECL_OVERRIDE {
    }
public:
};

} // end namespace mb
