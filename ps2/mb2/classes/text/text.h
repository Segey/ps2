/**
 * \file      ps2/ps2/mb2/classes/text/text.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 26(th), 2015, 05:57 MSK
 * \updated   July (the) 26(th), 2015, 05:57 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include "classes/item.h"
#include "text_type.h"
#include "classes/values.h"

/** \namespace mb */
namespace mb {

class Text: public Item {
public:
    typedef Text           class_name;
    typedef Item           inherited;
    typedef TextType::Type type_t;

private:
    QString m_title;

protected:
    virtual type_t doType() const = 0;
    virtual StringsValues doValues() const = 0;
    virtual void doPaint(QPainter* painter, RectFValues const& rects) const = 0;

public:
    Text() = default;
    type_t type() const {
        return doType();
    }
    StringsValues textValues() const {
        return doValues();
    }
    void paint(QPainter* painter, RectFValues const& rects) const {
        doPaint(painter, rects);
    }
    void paintTitle(QPainter* painter) {
        painter->drawText(rect(), Qt::AlignHCenter, m_title);
    }
    QString title() const {
        return m_title;
    }
    void setTitle(QString const& title) {
        m_title = title;
    }
};

} // end namespace mb
