/**
 * \file      ps2/ps2/mb2/classes/text/text_normal.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 14:53 MSK
 * \updated   July (the) 28(th), 2015, 14:53 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include <ps2/qt/convert/string.h>
#include "text.h"

/** \namespace mb */
namespace mb {

class TextNormal: public Text {
public:
    typedef TextNormal          class_name;
    typedef Text                inherited;
    typedef inherited::type_t   type_t;

private:
    static inline QString value(int value) noexcept {
        return ps2::toStr(value);
    }

protected:
    virtual type_t doType() const {
        return type_t::Normal;
    }
    virtual StringsValues doValues() const Q_DECL_OVERRIDE {
        return StringsValues(value(inherited::values().first()), value(inherited::values().second()));
    }
    virtual void doPaint(QPainter* painter, RectFValues const& rects) const Q_DECL_OVERRIDE {
        painter->drawText(rects.first(),  value(inherited::values().first()));
        painter->drawText(rects.second(), value(inherited::values().second()));
    }
public:
};

} // end namespace mb
