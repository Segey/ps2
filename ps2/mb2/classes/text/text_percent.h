/**
 * \file      ps2/ps2/mb2/classes/text/text_percent.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 15:23 MSK
 * \updated   July (the) 28(th), 2015, 15:23 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QPainter>
#include <ps2/qt/convert/string.h>
#include "text.h"

/** \namespace mb */
namespace mb {

class TextPercent:public Text {
public:
    typedef TextPercent       class_name;
    typedef Text              inherited;
    typedef inherited::type_t type_t;

private:
    QString value() const {
        auto const val = inherited::values().first() * 100.0/inherited::values().second();
        return QStringLiteral("%1%")
            .arg(ps2::to_qstr(val, QLocale(), 2));
    }
    static QString second() {
        return QStringLiteral("100%");
    }

protected:
    virtual type_t doType() const {
        return type_t::Percent;
    }
    virtual StringsValues doValues() const Q_DECL_OVERRIDE {
        return StringsValues(value(), second());
    }
    virtual void doPaint(QPainter* painter, RectFValues const& rects) const Q_DECL_OVERRIDE {
        painter->drawText(rects.first(),  value());
        painter->drawText(rects.second(), second());
    }
};

} // end namespace mb
