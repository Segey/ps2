/**
 * \file      ps2/ps2/mb2/classes/to_string.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   August (the) 04(th), 2015, 16:19 MSK
 * \updated   August (the) 04(th), 2015, 16:19 MSK
 * \TODO      
**/
#pragma once
#include <QDataStream>
#include <QByteArray>
#include "chart.h"

/** \namespace mb */
namespace mb {
    
class ToString {
public:
    typedef ToString class_name;

private: 
    static const qint8 version = 2;

public:
    void operator()(Chart* chart) {
        QByteArray byteArray;
        QDataStream out(&byteArray, QIODevice::WriteOnly);

        out << QDataStream::Qt_5_5
            << (qint8)version
            << chart->title()
            << (qint16)chart->figureType()
            << (qint16)chart->textType()
            << (qint16)chart->colorType()
            << (qint32)chart->firstValue()
            << (qint32)chart->secondValue();
    return byteArray.toBase64().data(); 
    }
};

} // end namespace mb
