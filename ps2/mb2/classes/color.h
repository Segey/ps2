/**
 * \file      ps2/ps2/mb2/classes/color.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 18:18 MSK
 * \updated   July (the) 25(th), 2015, 18:18 MSK
 * \TODO      
**/
#pragma once
#include <map>
#include <QPair>
#include <QColor>
#include "color_type.h"

/** \namespace mb */
namespace mb {

class Color {
public:
    typedef Color           class_name;
    typedef ColorType::Type type_t;

private:
    QColor m_light;
    QColor m_dark;
    type_t m_type;

    void init(QPair<QColor, QColor> const& val) {
        m_light = val.first;
        m_dark  = val.second;
    }
    void initColors() {
        std::map<type_t, QPair<QColor, QColor> > vec {
              {type_t::Red,     {Qt::red,     Qt::darkRed}}
            , {type_t::Green,   {Qt::green,   Qt::darkGreen}}
            , {type_t::Blue,    {Qt::blue,    Qt::darkBlue}}
            , {type_t::Cyan,    {Qt::cyan,    Qt::darkCyan}}
            , {type_t::Magenta, {Qt::magenta, Qt::darkMagenta}}
            , {type_t::Yellow,  {Qt::yellow,  Qt::darkYellow}}
            , {type_t::Gray,    {Qt::gray,    Qt::darkGray}}
            , {type_t::Black,   {Qt::white,   Qt::black}}
        };
        auto const& it = vec.find(m_type);
        if(it != vec.end()) init(it->second);
        else init({Qt::green, Qt::darkGreen});
    }

public:
    Color(type_t type = type_t::Red)
        :m_type(type) {
        initColors();
    }
    void setColorType(type_t type) {
        m_type = type;
        initColors();
    }
    type_t colorType() const {
        return m_type;
    }
    QColor const& light() const {
        return m_light;
    }
    QColor const& dark() const {
        return m_dark;
    }
    QPen pen() const {
        return QPen(dark(), 3);
    }
};

} // end namespace mb
