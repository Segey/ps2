/**
 * \file      ps2/ps2/mb2/classes/from_string.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   August (the) 04(th), 2015, 16:55 MSK
 * \updated   August (the) 04(th), 2015, 16:55 MSK
 * \TODO      
**/
#pragma once
#include <QDataStream>
#include <QByteArray>
#include "chart.h"

/** \namespace mb */
namespace mb {

class FromString {
public:
    typedef FromString class_name;

private: 
    template<class T>
    static void useVersion(Chart* chart, QString const& str) {
        T from;
        from.parse(str);
        from.import(chart);
    }
    static void versionChecker(Chart* chart, QString const& str, qint8 version) {
        if(version == 2) useVersion2(chart, str);
    }
public:
    void operator()(Chart* chart, QString const& str) {
        int ver;
        qint8 version;

        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        in >> ver;
        in >> version;
        
        versionChecker(chart, str, version);
    }
};
class FromString1 {
public:
    typedef FromString2 class_name;

private: 
    quint16 m_figure_type;
    quint16 m_color_type;

public:
    void import(Chart* chart) {
        chart->setTitle(m_title);
        chart->setFigureType(m_figure_type);
        chart->setTextType(m_text_type);
        chart->setColorType(m_color_type);
        chart->setFirstValue(m_first_value);
        chart->setSecondValue(m_second_value);
    }
    void parse(QString const& str) {
        int ver;
        qint8 version;

        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        in >> ver;
        in >> version;
        in >> m_title;
        in >> m_figure_type;
        in >> m_text_type;
        in >> m_color_type;
        in >> m_first_value;
        in >> m_second_value;
    }
};
class FromString2 {
public:
    typedef FromString2 class_name;

private: 
    QString m_title;
    quint16 m_figure_type;
    quint16 m_text_type;
    quint16 m_color_type;
    quint32 m_first_value;
    quint32 m_second_value;

public:
    void import(Chart* chart) {
        chart->setTitle(m_title);
        chart->setFigureType(m_figure_type);
        chart->setTextType(m_text_type);
        chart->setColorType(m_color_type);
        chart->setFirstValue(m_first_value);
        chart->setSecondValue(m_second_value);
    }
    void parse(QString const& str) {
        int ver;
        qint8 version;

        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        in >> ver;
        in >> version;
        in >> m_title;
        in >> m_figure_type;
        in >> m_text_type;
        in >> m_color_type;
        in >> m_first_value;
        in >> m_second_value;
    }
};
} // end namespace mb
