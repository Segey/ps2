/**
 * \file      ps2/ps2/mb2/classes/chart.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   July (the) 25(th), 2015, 17:40 MSK
 * \updated   July (the) 25(th), 2015, 17:40 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QPainter>
#include <QQuickItem>
#include <QSharedPointer>
#include <QQuickPaintedItem>
#include "classes/type/figure_none.h"
#include "classes/type/pie/fill_pie/fill_pie.h"
#include "classes/type/pie/simple_pie/simple_pie.h"
#include "classes/type/histogram/simple_histogram/simple_histogram.h"
#include "classes/type/histogram/fill_histogram/fill_histogram.h"
#include "classes/text/text_none.h"
#include "classes/text/text_normal.h"
#include "classes/text/text_percent.h"

/** \namespace mb */
namespace mb {

class Chart : public QQuickPaintedItem {
public:
    typedef Chart                  class_name;
    typedef QQuickPaintedItem      inherited;
    typedef QSharedPointer<Figure> figure_t;
    typedef QSharedPointer<Text>   text_t;
    typedef ColorType::Type        color_type_t;
    typedef FigureType::Type       figure_type_t;
    typedef TextType::Type         text_type_t;

    Q_OBJECT
    Q_PROPERTY(int colorType READ colorType WRITE setColorType)
    Q_PROPERTY(int textType READ textType WRITE setTextType)
    Q_PROPERTY(int figureType READ figureType WRITE setFigureType)
    Q_PROPERTY(int firstValue READ firstValue WRITE setFirstValue)
    Q_PROPERTY(int secondValue READ secondValue WRITE setSecondValue)
    Q_PROPERTY(QString title READ title WRITE setTitle)

    figure_t m_figure = figure_t(new FigureNone);
    text_t   m_text   = text_t(new TextNone);

private:
    template<typename T, typename U>
    void copy(T* type, U& obj) {
        *type = *(obj.data());
        obj.reset(type);
        inherited::update();
    }
    void updateTextType(text_type_t id) {
       Text* text = nullptr;
       if(id == text_type_t::Normal)       text = new TextNormal;
       else if(id == text_type_t::Percent) text = new TextPercent;
       else text = new TextNone;

        copy(text, m_text);
    }
    void updateFigureType(figure_type_t id) {
       Figure* fig = nullptr;

       if(id == figure_type_t::SimpleHistogram)    fig = new SimpleHistogram;
       else if(id == figure_type_t::FillHistogram) fig = new FillHistogram;
       else if(id == figure_type_t::FillPie)       fig = new FillPie;
       else if(id == figure_type_t::SimplePie)     fig = new SimplePie;
       else fig = new FigureNone;

       copy(fig, m_figure);
   }

public:
    Chart(QQuickItem *parent = 0)
     : QQuickPaintedItem(parent) {
    }
    QString title() const {
        return m_text->title();
    }
    void setTitle(QString const& title) {
        m_text->setTitle(title);
        inherited::update();
    }
    int textType() const {
        return static_cast<int>(m_text->type());
    }
    void setTextType(int text) {
        updateTextType(static_cast<text_type_t>(text));
    }
    int figureType() const {
        return static_cast<int>(m_figure->type());
    }
    void setFigureType(int id) {
        updateFigureType(static_cast<figure_type_t>(id));
    }
    int colorType() const {
        return static_cast<int>(m_figure->color().colorType());
    }
    void setColorType(int id) {
        m_figure->setColor(id);
        inherited::update();
    }
    int firstValue() {
        return m_figure->values().first();
    }
    void setFirstValue(int value) {
        m_figure->setFirstValue(value);
        m_text->setFirstValue(value);
    }
    int secondValue() {
        return m_figure->values().second();
    }
    void setSecondValue(int value) {
        m_figure->setSecondValue(value);
        m_text->setSecondValue(value);
    }
    void paint(QPainter *painter) {
        painter->setRenderHints(QPainter::Antialiasing, true);
        m_figure->setRect(boundingRect().adjusted(10,QApplication::fontMetrics().height() + 8,-10,-10));
        m_figure->paint(painter);

        m_text->setRect(boundingRect().adjusted(0,5,0,0));
        auto const& vals = m_figure->textPlaces(m_text->textValues());

        painter->setPen(Qt::black);
        m_text->paint(painter, vals);
        m_text->paintTitle(painter);
    }
};

} // end namespace mb
