/**
 * \file      ps2/ps2/mb2/classes/plugin.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   August (the) 07(th), 2015, 17:59 MSK
 * \updated   August (the) 07(th), 2015, 17:59 MSK
 * \TODO      
**/
#pragma once
#include <QQmlExtensionPlugin>
#include <qqml.h>
#include "chart.h"

class MbPlugin : public QQmlExtensionPlugin {
public:
    typedef MbPlugin            class_name;
    typedef QQmlExtensionPlugin inherited;

private: 
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void ChartsPlugin::registerTypes(const char *uri) {
        qmlRegisterType<mb::Chart>(uri,      2, 0, "Chart");
        qmlRegisterType<mb::ColorType>(uri,  2, 0, "ColorType");
        qmlRegisterType<mb::TextType>(uri,   2, 0, "TextType");
        qmlRegisterType<mb::FigureType>(uri, 2, 0, "FigureType");
    } 
};
