/**
 * \file      ps2/ps2/mb2/classes/color_type.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 14:59 MSK
 * \updated   July (the) 25(th), 2015, 14:59 MSK
 * \TODO      
**/
#pragma once
#include <QObject>

/** \namespace mb */
namespace mb {

class ColorType : public QObject {
public:
    typedef ColorType class_name;
    typedef QObject   inherited;

Q_OBJECT

public:
    enum class Type: std::uint8_t {
          Invalid  = 0u
        , Red      = 1u
        , Green    = 2u
        , Blue     = 4u
        , Cyan     = 8u
        , Magenta  = 16u
        , Yellow   = 32u
        , Gray     = 64u
        , Black    = 128u
    };
    Q_ENUM(Type)
};

} // end namespace mb
