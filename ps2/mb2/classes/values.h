/**
 * \file      ps2/ps2/mb2/classes/values.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 25(th), 2015, 18:22 MSK
 * \updated   July (the) 25(th), 2015, 18:22 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include <QString>
#include <QRectF>

/** namespace mb */
namespace  mb {

template<class T>
class Values {
public:
    typedef T                  typename_t;
    typedef Values<typename_t> class_name;
    typedef T                  value_t;

private:
    value_t m_first;
    value_t m_second;

public:
    Q_DECL_CONSTEXPR Values(value_t const& first = value_t(), value_t const& second = value_t()) Q_DECL_NOTHROW
       : m_first(first), m_second(second) {
    }
    Q_DECL_CONSTEXPR value_t first() const {
        return m_first;
    }
    Q_DECL_CONSTEXPR void setFirst(value_t const& value) {
        m_first = value;
    }
    void setFirst(value_t&& value){
        m_first = std::move(value);
    }
    Q_DECL_CONSTEXPR value_t second() const {
        return m_second;
    }
    Q_DECL_CONSTEXPR void setSecond(value_t const& value) {
        m_second = value;
    }
    void setSecond(value_t&& value) {
        m_second = std::move(value);
    }
    value_t max() const;
    value_t min() const;
}; 
template<>
Q_DECL_CONSTEXPR inline uint Values<uint>::second() const {
    return m_second == 0u ? 1u : m_second;
}
template<>
inline uint Values<uint>::max() const {
    return std::max(m_first, m_second);
}
template<>
inline uint Values<uint>::min() const {
    return std::min(m_first, m_second);
}

typedef Values<uint>    UintValues;
typedef Values<double>  DoubleValues;
typedef Values<QString> StringsValues;
typedef Values<QRectF>  RectFValues;

} // end namespace mb
