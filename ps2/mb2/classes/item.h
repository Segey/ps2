/**
 * \file      ps2/ps2/mb2/classes/item.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   July (the) 28(th), 2015, 17:42 MSK
 * \updated   July (the) 28(th), 2015, 17:42 MSK
 * \TODO      
**/
#pragma once
#include <QRectF>
#include <QFont>
#include <QFontMetrics>
#include <QApplication>
#include "values.h"

/** \namespace mb */
namespace mb {

class Item {
public:
    typedef Item class_name;

private: 
    QRectF     m_rect;
    UintValues m_values;

public:
    Item(QRectF const& rect = QRectF(), UintValues const& values = UintValues()) 
        :m_rect(rect), m_values(values) {
    }
    virtual ~Item(){
    }
    QRectF const& rect() const {
        return m_rect;
    }
    void setRect(QRectF const& rect) {
        m_rect = rect;
    }
    UintValues const& values() const {
        return m_values;
    }
    void setValues(UintValues const& values) {
        m_values = values;
    }
    void setFirstValue(int val) {
        m_values.setFirst(val);
    }
    void setSecondValue(int val) {
        m_values.setSecond(val);
    }
    int textHeight() const {
        return QApplication::fontMetrics().height();
    }
    int textWidth(QString const& s) const {
        return QApplication::fontMetrics().width(s);
    }
};

} // end namespace mb
