TEMPLATE        = lib
CONFIG         += plugin
QT             += qml quick widgets
QMAKE_CXXFLAGS += -std=c++1y

DESTDIR         = ../mb
TARGET = $$qtLibraryTarget(mb_plugin)
DESTPATH=../mb

HEADERS += \
    classes/chart.h \
    classes/color.h \
    classes/color_type.h \
    classes/values.h \
    classes/type/figure_type.h \
    classes/text/text_type.h \
    classes/text/text.h \
    classes/text/text_none.h \
    classes/type/figure.h \
    classes/type/figure_none.h \
    classes/text/text_normal.h \
    classes/text/text_percent.h \
    classes/type/pie/pie.h \
    classes/item.h \
    classes/type/histogram/histogram.h \
    classes/type/histogram/determinator.h \
    classes/type/histogram/painter.h \
    classes/type/histogram/station.h \
    classes/type/histogram/first_determinator.h \
    classes/type/histogram/second_determinator.h \
    classes/type/histogram/painter/first_painter.h \
    classes/type/histogram/painter/painter.h \
    classes/type/histogram/painter/second_painter.h \
    classes/type/painter.h \
    classes/type/histogram/determinator/determinator.h \
    classes/type/histogram/determinator/first_determinator.h \
    classes/type/histogram/determinator/second_determinator.h \
    classes/type/pie/determinator/determinator.h \
    classes/type/pie/determinator/first_determinator.h \
    classes/type/pie/determinator/second_determinator.h \
    classes/type/pie/station.h \
    classes/type/pie/painter/first_painter.h \
    classes/type/pie/painter/painter.h \
    classes/type/pie/painter/second_painter.h \
    classes/type/exec.h \
    classes/type/histogram/histogram/fill_histogram.h \
    classes/type/histogram/simple_histogram/simple_histogram.h \
    classes/type/histogram/fill_histogram/fill_histogram.h \
    classes/type/pie/fill_pie/fill_pie.h \
    classes/type/pie/simple_pie/simple_pie.h \
    classes/from_string.h \
    classes/to_string.h
    classes/plugin.h

#RESOURCES += qml.qrc

target.path=$$DESTPATH
qmldir.files=$$PWD/qmldir
qmldir.path=$$DESTPATH
INSTALLS += target
 qmldir

OTHER_FILES += qmldir

# Copy the qmldir file to the same folder as the plugin binary
QMAKE_POST_LINK += $$QMAKE_COPY $$replace($$list($$quote($$PWD/qmldir) $$DESTDIR), /, $$QMAKE_DIR_SEP)
