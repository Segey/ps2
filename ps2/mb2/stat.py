#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
# @file      stats.py
# @brief     The main file creates statistics from current project
# @author    S.Panin <dix75@mail.ru>
# @copyright S.Panin, 2015 - 2015
# @version   v.1.2
# @created   September (the) 15(th), 2012, 19:47 MSK
# @updated   April (the) 15(th), 2015, 15:45 MSK
# @TODO
#
import os
import time
import re

out_folder = "stats"   # folder to output file
all_lines = 0          # all lines in the project
all_symbols = 0        # all symbols in the project
all_files = 0          # all files in the project
all_files_h = 0        # all files *.h in the project
all_files_cpp = 0      # all files *.cpp in the project
all_files_tests = 0    # all files with tests in the project

for root, dirs, files in os.walk("."):
    for name in files:
        file = os.path.join(root, name)
        if(None == re.search(r"^(?!.*moc_).*\.(cpp|h)$", file)):
            continue
        all_files += 1
        if(re.search(r"^.*\.h$", file)):
            all_files_h += 1
        if(re.search(r"^.*\.cpp$", file)):
            all_files_cpp += 1
        if(re.search(r"^.*_test\.h$", file)):
            all_files_tests += 1

        with open(file) as f:
            for line in f:
                all_lines += 1
                all_symbols += len(line)

str = ("{0:-^42}\n".format(""))
str += "Current Day is   {0}\n".format(time.ctime())
str += "{0:<32}-{1:>8}\n".format("Max header files in the Project", all_files_h)
str += "{0:<32}-{1:>8}\n".format("Max cpp files in the Project", all_files_cpp)
str += "{0:<32}-{1:>8}\n".format("Max tests files in the Project",
                                 all_files_tests - 1)
str += "{0:<32}-{1:>8}\n".format("Max files in the Project", all_files)
str += "{0:<32}-{1:>8}\n".format("Max lines in the Project", all_lines)
str += "{0:<32}-{1:>8}\n".format("Max symbols in the Project", all_symbols)
print (str)

tm = time.localtime()
file_out = "{0}/{1}_{2}.st".format(out_folder, tm.tm_mon, tm.tm_year)
print(file_out)

with open(file_out, "a+") as f:
    f.write(str)
