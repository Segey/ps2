/**
 * \file      ps2/ps2/thetest/test.h
 * \brief     Test
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.6
 * \created   April    (the) 14(th), 2015, 20:06 MSK
* \updated    November (the) 28(th), 2016, 12:41 MSK
 * \TODO
**/
#pragma once
#include <iostream>
#include <iomanip>
#include <chrono>
#include "test_output.h"

#define TPS_EPSILON 1.0e-3
extern bool g_log_maybe_exception;

namespace {
    using namespace std;
    using namespace std::chrono;

struct Compare final {
    using class_name = Compare;

    static int m_classes;
    static int m_nsclasses;
    static int m_pass;
    static int m_failed;
    static high_resolution_clock::time_point m_start;

    private:
        string m_file;
        string m_fun;
        string m_name;
        int    m_line;

    private:
        ostream& init(string const& title) noexcept {
            cout << boolalpha << "\n  - " << title.c_str() << " "
                 << m_file.c_str() << "(" << m_line << ") : " << m_fun.c_str()  << "\n    ";
            return cout;
        }
        bool done() noexcept {
            ++m_pass;
            printf(". ");
            return true;
        }

    public:
        explicit Compare(string&& file, string&& fun, int line)
            : m_file(std::move(file))
            , m_fun(std::move(fun))
            , m_line(line) {
        }
        template<class T>
        void message(T&& s) noexcept {
            using namespace thetest;
            init("FAILED with message: ") << s << endl;
        }
        template<class T>
        void errorMessage(T&& s) noexcept {
            using namespace thetest;
            init("FAILED with message: ") << s << endl;
            ++m_failed;
        }
        template<class T, class U>
        void trace(T const& a, U const& t) noexcept {
            using namespace thetest;
            init("TRACE") << a << "  =>  " << (t) << endl;
        }
        template<class A, class B>
        bool compare(A const& a, B const& b1, B const& b2) {
            using namespace thetest;

            bool result = false;
            if((b1) == (b2))
                result = done();
            else {
                ++m_failed;
                init("FAILED") << TestOutput("==").print(a, b1, b2);
            }
            return result;
        }
        template<class A, class B>
        bool not_compare(A&& a, B const& b, B const& _c) noexcept {
            using namespace thetest;

            bool result = false;
            if((b) != (_c))
                result = done();
            else {
                ++m_failed;
                init("FAILED") << "[" << std::forward<A>(a) << "]   =>   " << (b) << " != " << (_c) << endl;
            }
            return result;
        }
        template<class A, class B>
        bool compare_nullptr(A&& a, B const& b) noexcept {
            using namespace thetest;

            bool result = false;
            if((b) == nullptr)
                result = done();
            else {
                ++m_failed;
                init("FAILED") << "[" << std::forward<A>(a) << " not nullptr]" << endl;
            }
            return result;
        }
        template<class A, class B>
        bool compare_not_nullptr(A&& a, B const& b) noexcept {
            using namespace thetest;

            bool result = false;
            if((b) != nullptr)
                result = done();
            else {
                ++m_failed;
                init("FAILED") << "[" << (a) << " is nullptr]" << endl;
            }
            return result;
        }
        template<class A, class B>
        void verify(A&& a, B&& b) noexcept {
            compare(std::forward<A>(a),std::forward<B>(b),true);
        }
        template<class S>
        bool verify_double(S&& s, double a, double _c, double epsilon = TPS_EPSILON) noexcept {
            bool result = false;
            if((_c - epsilon) <= (a) && (a) <= (_c + epsilon)) {
                result = done();
            }
            else {
               ++m_failed;
               init("FAILED") << std::setprecision(6) << s << "   "<< double(_c - epsilon) << " <= " << a << " <= " << double(_c + epsilon) << endl;
            }
            return result;
        }
        void valid() {
            done();
        }
        void invalid() {
            ++m_failed;
            init("No try ocurred!") << endl;
        }
        template<class S>
        void invalidException(S&& e) noexcept {
            ++m_failed;
            init("") << "An exception [" << e  << "] hasn't been raised!" << endl;
        }
        static auto now() noexcept {
            return high_resolution_clock::now();
        }
        static auto timeResult(high_resolution_clock::time_point start, high_resolution_clock::time_point end) {
            return duration_cast<duration<float>>(end - start).count();
        }
        static void start() noexcept {
            m_start = now();
        }
        static void end() noexcept {
            printf("\n\n ---  \n");
            printf("  Passed:  %d\n", m_pass);
            printf("  Failed:  %d\n", m_failed);
            printf("  Classes: %d\n", m_classes);
            printf("  Elapsed: %.8f s \n", timeResult(m_start, now()));
        }
    };

    int Compare::m_classes = 0;
    int Compare::m_nsclasses = 0;
    int Compare::m_failed = 0;
    int Compare::m_pass = 0;
    decltype(Compare::m_start) Compare::m_start = Compare::now();
}

#define assertMessage(s) {                                 \
    Compare(__FILE__, __FUNCTION__, __LINE__).message(s);  \
}
#define assertErrorMessage(s) {                            \
    Compare(__FILE__, __FUNCTION__, __LINE__)              \
        .errorMessage(s);                                  \
}
#define assertInvalid() {                                  \
    Compare(__FILE__, __FUNCTION__, __LINE__).invalid();   \
}
#define assertDoubleEqualsEpsilon(a,b,_c) {                \
    Compare d(__FILE__, __FUNCTION__, __LINE__);           \
    if(!d.verify_double(#a, a, b, _c)) return;             \
}
#define assertEquals(a,b) {                                \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.compare(#a" == "#b, a, b)) return;              \
}
#define assertEqualsPass(a,b) {                            \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    _c.compare(#a" == "#b, a, b);                          \
}
#define assertEquals2(a,b,_c) {                            \
    Compare d(__FILE__, __FUNCTION__, __LINE__);           \
    if((a) != (b) && ((a) != (_c))) {                      \
        d.compare(#a" == "#b, a, b);                       \
        printf("    - OR -");                              \
        d.compare(#a" == "#_c, a, _c);                     \
        return;                                            \
    }                                                      \
}
#define assertEquals3(_a,_b,_c,_d) {                       \
    Compare f(__FILE__, __FUNCTION__, __LINE__);           \
    if((_a) != (_b) && ((_a) != (_c)) && ((_a) != (_d))) { \
        f.compare(#_a" == "#_b, _a, _b);                   \
        printf("    - OR -");                              \
        f.compare(#_a" == "#_c, _a, _c);                   \
        printf("    - OR -");                              \
        f.compare(#_a" == "#_d, _a, _d);                   \
        return;                                            \
    }                                                      \
}
#define assertDoubleEquals(a,b) {                          \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.verify_double(#a, double(a), double(b)))        \
        return;                                            \
}
#define assertDoubleZero(a) {                              \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.verify_double(#a, a, .0)) return;               \
}
#define assertDoubleEqualsPass(a,b) {                      \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    _c.verify_double(#a, a, b);                            \
}
#define assertNotEquals(a,b) {\
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.not_compare(#a" != "#b, a, b)) return;          \
}
#define assertTrue(a) {                                    \
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.compare(#a" == true", a, true)) return;         \
}
#define assertFalse(a) {\
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.compare(#a" == false", a, false)) return;       \
}
#define assertNullptr(a) {\
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.compare_nullptr(#a, a)) return;                 \
}
#define assertNotNullptr(a) {\
    Compare _c(__FILE__, __FUNCTION__, __LINE__);          \
    if(!_c.compare_not_nullptr(#a, a)) return;             \
}
#define assertMaybeException(a, b) {                                          \
    g_log_maybe_exception = true;                                             \
    Compare(__FILE__, __FUNCTION__, __LINE__).compare(#a" == "#b, a, b);      \
    g_log_maybe_exception = false;                                            \
}
#define assertMaybeExceptionTrue(a) {                                         \
    g_log_maybe_exception = true;                                             \
    Compare(__FILE__, __FUNCTION__, __LINE__).compate(#a" == true", a, true); \
    g_log_maybe_exception = false;                                            \
}

/**
 * \code
    assertMaybeExceptionFalse(
            creator.check()
        );
 * \endcode
**/
#define assertMaybeExceptionFalse(a) {                                          \
    g_log_maybe_exception = true;                                               \
    Compare(__FILE__, __FUNCTION__, __LINE__).compare(#a" == false", a, false); \
    g_log_maybe_exception = false;                                              \
}
#define assertAnyException(a)                                                   \
    g_log_maybe_exception = true;                                               \
        try {                                                                   \
            a;                                                                  \
            Compare(__FILE__, __FUNCTION__, __LINE__).invalid();                \
        } catch(...) {                                                          \
            Compare(__FILE__, __FUNCTION__, __LINE__).valid();                  \
        }                                                                       \
    g_log_maybe_exception = false;

#define assertException(a,exp)                                                  \
    g_log_maybe_exception = true;                                               \
        try {                                                                   \
            a;                                                                  \
            Compare(__FILE__, __FUNCTION__, __LINE__).invalid();                \
        } catch(exp const&) {                                                 \
            Compare(__FILE__, __FUNCTION__, __LINE__).valid();                  \
        } catch(...) {                                                          \
            Compare(__FILE__, __FUNCTION__, __LINE__).invalidException(#exp);   \
        }                                                                       \
    g_log_maybe_exception = false;


/**
  \code
        TPS_TICK(100,
            tested::fromString(QStringLiteral("10.5"));
        );
  \endcode
**/
#define TPS_TICK(a, fun) {                                                     \
    auto const start  = Compare::now();                                        \
    for(decltype(a) i = 0; i != a; ++i) {                                      \
        fun;                                                                   \
    }                                                                          \
   printf("\n  Elapsed time: %.8f ms \n"                                       \
        , Compare::timeResult(start, Compare::now()) * 1000 );                 \
}

/**
  \code
        TPS_TICK_ONE(
            tested::fromString(QStringLiteral("10.5"));
        );
  \endcode
 **/
#define TPS_TICK_ONE(a, b) TPS_TICK(1,b);
#define TEST(cls)           {                   \
    printf("\n%s", #cls);                       \
    cls()();                                    \
    ++Compare::m_classes;                       \
}
#define TEST_START()       { Compare::start(); }
#define TEST_END()         { Compare::end(); }
#define TEST_CLASSES(cls)  { cls()(); }
#define TEST_SCLASSES(cls) {                    \
    printf("\n\n=== BEGIN %i.%s ==="            \
            , ++Compare::m_nsclasses,  #cls);   \
    cls()();                                    \
}
