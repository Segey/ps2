/**
 * \file      ps2/ps2/thetest/qt/helpers.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   September (the) 14(th), 2016, 12:09 MSK
 * \updated   September (the) 14(th), 2016, 12:09 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <QUrl>
#include <QVector>
#include <QVariant>
#include <QModelIndex>
#include <QDateTime>
#include <ps2/qt/out.h>
#include <ps2/cpp/algorithm/concat.h>

std::ostream& operator<<(std::ostream& out, std::type_info const& ti) noexcept
{
   return out << "\"typeid\" : { \"name\": \"" << ti.name()
              << "\", \"hash\": " << ti.hash_code() << "}";
}
std::ostream& operator<<(std::ostream& out, QChar const& ch) noexcept
{
   return out << ch.toLatin1();
}
std::ostream& operator<<(std::ostream& out, QString const& str) noexcept
{
   return out << str.toStdString();
}
std::ostream& operator<<(std::ostream& out, QDate const& date) noexcept
{
   return out << (date.isValid() ? date.toString(Qt::ISODate)
                                 : QStringLiteral("\"invalid date\""));
}
std::ostream& operator<<(std::ostream& out, QModelIndex const& index) noexcept
{
    return ps2::cout(out, index);
}
std::ostream& operator<<(std::ostream& out, QLocale::Language lang) noexcept
{
    return out << QLocale::languageToString(lang).toStdString();
}
std::ostream& operator<<(std::ostream& out, QVariant const& index) noexcept
{
    return ps2::cout(out, index);
}
std::ostream& operator<<(std::ostream& out, QUrl const& url) noexcept
{
   return out << url.toString();
}
template<class T, class U>
std::ostream& operator<<(std::ostream& out, QPair<T,U> const& pair) noexcept
{
    return ps2::cout(out, pair);
}
std::ostream& operator<<(std::ostream& out, QVector<std::string> const& vec) noexcept
{
    return out << ps2::concat(vec, std::string(", ")
               , std::string("QVector<std::string>("), std::string(")"));
}
std::ostream& operator<<(std::ostream& out, QVector<QStringList> const& vec) noexcept
{
    return out << ps2::concat(vec, QStringLiteral(", "), QStringLiteral("QVector<QStringLiteral>(")
                              , QStringLiteral(")"), [](auto const& str, auto val){
        return str + val.join(QStringLiteral(", "));
    });
}
std::ostream& operator<<(std::ostream& out, std::vector<char> const& vec) noexcept
{
    return out << ps2::concat(vec, std::string(", "), std::string("vector<char>(")
                              , std::string(")"), [](auto const& str, auto val){
        return str + val;
    });
}

/** namespace thetest */
namespace thetest {

template<class T>
static inline int countUnique(T values) noexcept
{
    return std::distance(values.begin()
                     ,std::unique(values.begin(), values.end()));
}

template<class R, class... Args>
static inline R toSmt(Args&&... args) noexcept {
    return R{std::forward<Args>(args)...};
}
template<class R, class T>
static inline R toOp(T const& t) noexcept {
    return t;
}

} // end namespace thetest

