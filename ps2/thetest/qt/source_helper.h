/**
 * \file      ps2/ps2/thetest/qt/source_helper.h
 * \brief     SourceHelper db class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 06(th), 2015, 20:13 MSK
 * \updated   May (the) 06(th), 2015, 20:13 MSK
 * \TODO
**/
#pragma once
#include <QFile>
#include <QString>
#include <QStringList>
#include <ps2/qt/objects/file_name.h>
#include "db_faked.h"

/** namespace thetest */
namespace thetest {

/**
 * \code
 *     auto const& helper = SourceHelper(fileName(), dbName(), source());
 *     helper.tablesNumber() == 3u;
 *     helper.columnsNumber(QStringLiteral("Users")) == 22u;
 *     helper.columnIndexName(QStringLiteral("UsersUniqueIndex")) == QStringLiteral("uid");
 * \endcode
**/
class SourceHelper {
public:
    using class_name = SourceHelper;

private:
    QString       m_programName;
    ps2::FileName m_file;
    QString       m_connectionName;
    QStringList   m_source;
    FakedDb       m_db;

private:
    void removeDb() noexcept {
       QFile::remove(m_file.name());
    }

public:
    SourceHelper() Q_DECL_EQ_DEFAULT;
    SourceHelper(QString const& programName, QString const& file, QString const& dbName, QStringList const& source)
        : m_programName(programName)
        , m_file(ps2::FileName::temp(file))
        , m_connectionName(dbName)
        , m_source(source)  {
    }
    SourceHelper(QString const& programName, QString const& file, QString const& dbName, QString const& source)
        : m_programName(programName)
        , m_file(ps2::FileName::temp(file))
        , m_connectionName(dbName) {
        m_source.append(source);
    }
    FakedDb const* fakedDb() const noexcept {
        return &m_db;
    }
    QSqlDatabase const& db() const noexcept {
        return m_db.db();
    }
    virtual ~SourceHelper() noexcept {
        removeDb();
    }
    uint tablesNumber() const noexcept {
        QSqlQuery query(m_db.db());
        query.exec(QStringLiteral("SELECT COUNT(*) FROM sqlite_master WHERE type='table';"));
        query.next();
        return query.value(0).toUInt();
    }
    uint columnsNumber(QString const& tableName) const noexcept {
        QSqlQuery query(m_db.db());
        query.exec(QStringLiteral("PRAGMA table_info ('%1');").arg(tableName));
        query.last();
        return query.value(0).toUInt() + 1;
    }
    uint lastInsertRowId() noexcept {
        QSqlQuery query(m_db.db());
        query.exec(QStringLiteral("SELECT last_insert_rowid();"));
        return query.next() ? query.value(0).toUInt() : 0u;
    }
    int rowsNumber(QString const& tableName) const noexcept {
        QSqlQuery query(m_db.db());
        query.exec(QStringLiteral("SELECT COUNT(*) FROM %1;").arg(tableName));
        if(!query.next()) return 0u;
        return query.value(0).toInt();
    }
    bool isTableEmpty(QString const& tableName) const noexcept {
        return rowsNumber(tableName) == 0u;
    }
    QString columnIndexName(QString const& idxName) noexcept {
        QSqlQuery query(m_db.db());
        query.exec(QStringLiteral("PRAGMA index_info('%1');").arg(idxName));
        query.next();
        return query.value(2).toString();
    }
    bool createAndOpenDb() noexcept {
        removeDb();
        const bool done = FakedDb().createDatabase(m_programName, m_source, m_file.name(), m_connectionName);
        if(done == false) return false;
        return m_db.connect(m_programName, m_file.name(), m_connectionName);
    }
};

} // end namespace thetest
