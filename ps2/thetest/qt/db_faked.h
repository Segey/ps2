/**
 * \file      ps2/ps2/thetest/qt/db_faked.h
 * \brief     Faked db class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 06(th), 2015, 15:59 MSK
 * \updated   May (the) 06(th), 2015, 15:59 MSK
 * \TODO
**/
#pragma once
#include <ps2/qt/db/sqlite_db.h>

/**
 * \code
 *     const bool done = thetest::FakedDb::createDatabase(m_source, m_file.name(), m_connectionName);
 *     FakedDb db;
 *     db.connect(m_file.name(), m_connectionName);
 * \endcode
**/

/** namespace thetest */
namespace thetest {

class FakedDb: public ps2::SqliteDb {
public:
    using class_name = FakedDb;
    using inherited  = ps2::Db;

public:
    explicit FakedDb() Q_DECL_EQ_DEFAULT;
    ~FakedDb() Q_DECL_EQ_DEFAULT;
    FakedDb(QString const& programName, QString const& file, QString const& connectionName) noexcept {
        connect(programName, file, connectionName);
    }
    bool connect(QString const& programName, QString const& file, QString const& connectionName) noexcept {
        return inherited::connect(programName, file, connectionName);
    }
    bool reConnect(QString const& programName, QString const& file, QString const& connectionName) noexcept {
        return inherited::connect(programName, file, connectionName);
    }
    void disconnect() noexcept {
        inherited::disconnect();
    }
};

} // end namespace thetest
