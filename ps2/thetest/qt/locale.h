/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/thetest/qt/lib/locale.h 
 * \brief     The Locale class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:38 MSK
 * \updated   November  (the) 03(th), 2017, 15:38 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>

/** \namespace thetest */
namespace thetest {

class Locale {
public:
    using class_name = Locale;

public:
    static inline QLocale ru() noexcept {
        return QLocale(QLocale::Russian);
    }
    static inline QLocale en() noexcept {
        return QLocale(QLocale::English);
    }
    static inline QLocale de() noexcept {
        return QLocale(QLocale::German);
    }
};

} // end namespace thetest
