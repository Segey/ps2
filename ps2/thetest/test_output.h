/**
 * \file      ps2/ps2/thetest/test_output.h
 * \brief     The Test_output class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.0
 * \created   December (the) 15(th), 2016, 12:57 MSK
 * \updated   January  (the) 09(th), 2017, 13:35 MSK
 * \TODO
**/
#pragma once
#include <string>
#include <sstream>
#include <ps2/thetest/qt/helpers.h>
#include "test_json_output.h"
#ifdef QT
#include <QJsonDocument>
#include <QJsonObject>
#endif

/** \namespace thetest */
namespace thetest {

class TestOutput {
public:
    using class_name = TestOutput;
    using uint       = unsigned;
    using str_t      = std::string;

private:
    str_t m_sign;

private:
    template<class T>
    static inline str_t toString(T const& val, bool is = true) noexcept {
        std::stringstream out;
        if(is) out << '{' << val << '}';
        else out << val;
        return out.str();
    }
    template<class T, class E>
    static void errorMessage(std::stringstream& out, T&& obj, E&& error) noexcept {
#ifdef QT
        decltype(obj.size()) const pos = error.offset - 1;
        out << "[INVALID JSON]   =>  " << qPrintable(error.errorString())
            << "(" << error.error << ") [position: " << pos
            << ", char: '" << obj[pos] << '\'';
            if(pos + 5 < obj.size()) out << " chars: '" << obj.substr(pos - 1, 5) << '\'';
            out << ']' << std::endl << obj << std::endl;
#endif
    }
    template<class T>
    static auto toJson(std::stringstream& out, T&& obj) noexcept {
        QJsonParseError error;
        auto&& byte = QByteArray::fromRawData(obj.c_str(), static_cast<int>(obj.size()));
        auto&& doc = QJsonDocument::fromJson(std::move(byte), &error);
        if(error.error != QJsonParseError::NoError) {
            errorMessage(out, std::forward<T>(obj), std::forward<QJsonParseError>(error));
            return QJsonDocument{};
        }
        return doc;
    }

private:
    inline str_t simplePrint(str_t const& a, str_t const& b1
                            , str_t const& b2) noexcept {
        std::stringstream out;
        out << "[" << a << "]   =>   " << (b1) << " " << m_sign << " " << (b2) << std::endl;
        return out.str();
    }
    inline str_t strongPrint(str_t const& a, str_t const& b1
                            , str_t const& b2) noexcept {
        std::stringstream out;
        auto&& j1 = toJson(out, b1);
        auto&& j2 = toJson(out, b2);

        out << "[" << a << "]   =>   \n" << TestJsonOutput(j1)() << "\n" <<  m_sign
            << "\n" << TestJsonOutput(j2)() << std::endl;
        return out.str();
    }

public:
    explicit TestOutput(str_t&& sign) noexcept
        : m_sign(sign) {
    }
    template<class A, class B>
    str_t print(A const& a, B const& b1, B const& b2) noexcept {
        auto str = simplePrint(str_t(a), toString(b1, false), toString(b2, false));
        if(str.find(':') != str_t::npos && str.find('{') != str_t::npos && str.find('}') != str_t::npos && str.size() > 128)
            return strongPrint(str_t(a), toString(b1), toString(b2));
        return str;
    }
};

} // end namespace thetest
