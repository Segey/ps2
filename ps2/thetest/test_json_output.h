/**
 * \file      ps2/ps2/thetest/test_json_output.h
 * \brief     The Test_json_output class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 09(th), 2017, 17:41 MSK
 * \updated   January (the) 09(th), 2017, 17:41 MSK
 * \TODO      
**/
#pragma once
#include <string>
#ifdef QT
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#endif

/** \namespace thetest */
namespace thetest {

class TestJsonOutput {
public:
    using class_name = TestJsonOutput;
    using str_t      = std::string;

private: 
    QJsonDocument m_doc;

private:
    str_t printOffset(int pos) noexcept {
        return str_t(3 * pos, ' ');
    }
    str_t printComma(int pos, bool is) noexcept {
        return printOffset(pos) + (is ? ',' : ' ');
    }
    str_t printKey(int pos, bool is, QString const& key) noexcept {
        if(key.isEmpty()) return printOffset(pos) + qPrintable(key);
        return printComma(pos, is) + str_t("\"") + qPrintable(key) + "\"";
    }
    bool isSimpleObject(QJsonObject const& obj) noexcept {
        for(auto const& item: obj)
           if(item.isObject() || item.isArray()) return false;
        return true;
    }
    str_t printSimple(QJsonValue const& val) noexcept {
        std::stringstream s;
        if(val.isBool()) s << val.toBool();
        else if(val.isDouble()) s << val.toDouble();
        else if(val.isString()) s << "\"" << qPrintable(val.toString()) << "\"";
        else if(val.isNull()) s << "null";
        else s << "\"invalid\"";
        return s.str();
    }
    str_t printSimple(int pos, bool is, QJsonValue const& val) noexcept {
        return printComma(pos, is) + printSimple(val);
    }
    str_t printSimple(QString const& key
                            , QJsonValue const& val) noexcept {
        return str_t("\"") + qPrintable(key) + "\": " + printSimple(val);
    }
    str_t printSimple(int pos, bool is, QString const& key
                            , QJsonValue const& val) noexcept {
        return printComma(pos, is) + printSimple(key, val);
    }

private:
    str_t printSimpleObject(int pos, bool is, QJsonObject const& obj) noexcept {
        str_t s = printComma(pos, is) + "{";
        for(auto const& str: obj.keys()) {
            auto const& item = obj.value(str);
            s += printSimple(str, item) + ", ";
        }
        if(s.size() > 2 && s[s.size() - 2] == ',') {
            s[s.size() - 2] = '}';
            return s + '\n';
        }
        return s + "}\n";
    }
    str_t printSimpleObject(int pos, bool is, QString const& key, QJsonObject const& obj) noexcept {
        str_t s = printComma(pos, is);
        s += str_t("\"") + qPrintable(key) + str_t("\" : {");
        for(auto const& str: obj.keys()) {
            auto const& item = obj.value(str);
            s += printSimple(str, item) + ", ";
        }
        if(s.size() > 2 && s[s.size() - 2] == ',') {
            s[s.size() - 2] = '}';
            return s + '\n';
        }
        return s + "}\n";
    }
    str_t traversObject(int pos, bool is, QString const& key
                        , QJsonObject const& obj) noexcept {
        if(obj.isEmpty()) return printKey(pos++, is, key) + ": {}\n";

        int cx = 0;
        str_t s;
        if(!key.isNull()) s = printKey(pos++, is, key) + ": {\n";
        else s += printOffset(pos) + "{\n";
        for(auto const& str: obj.keys()) {
            auto const& item = obj.value(str);
            if(item.isObject()) {
                if(isSimpleObject(item.toObject()))
                    s += printSimpleObject(pos + 1, cx > 0, str, item.toObject());
                else
                    s += traversObject(pos + 1, cx > 0, str, item.toObject());
            }
            else if(item.isArray()) {
                s += traverseArray(pos + 1, cx > 0, str, item.toArray());
            }
            else
                s += printSimple(pos + 1, cx > 0, str, item) + "\n";
            ++cx;
        }
        if(pos == 0) return s + printOffset(1) + "}";
        return s + printOffset(pos) + "}\n";
    }
    str_t traverseArray(int pos, bool is, QString const& key, QJsonArray const& arr) noexcept {
        if(arr.isEmpty()) return printKey(pos++, is, key) + ": []\n";

        int cx = 0;
        str_t s = printKey(pos++, is, key) + ": [\n";
        if(arr.isEmpty()) return s + "]\n";
        for(auto const& item: arr) {
            if(item.isObject()) {
                if(isSimpleObject(item.toObject()))
                    s += printSimpleObject(pos + 1, cx > 0, item.toObject());
                else
                    s += traversObject(pos + 1, cx > 0, QString(), item.toObject());
            }
            else if(item.isArray()) {
                s += traverseArray(pos + 1, cx > 0,  QString(), item.toArray());
            }
            else
                s += printSimple(pos + 1, cx > 0, item) + "\n";
            ++cx;
        }
        return s + printOffset(pos) + "]\n";
    }

public:
    explicit TestJsonOutput(QJsonDocument const& doc) noexcept
        : m_doc(doc){
    }
    str_t operator()() noexcept {
        //return m_doc.toJson(QJsonDocument::JsonFormat::Indented).data();
        if(isSimpleObject(m_doc.object()))
            return printSimple(QString(), m_doc.object());

        return traversObject(0, false, QString(), m_doc.object());
    }
};

} // end namespace thetest
