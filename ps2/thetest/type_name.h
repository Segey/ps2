/**
 * \file      ps2/ps2/thetest/type_name.h
 * \brief     The Type_name class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 11(th), 2017, 17:03 MSK
 * \updated   January (the) 11(th), 2017, 17:03 MSK
 * \TODO      
**/
#pragma once
#include <string>

/** \namespace thetest */
namespace thetest {
    namespace {
        template<class T>
        std::string type() noexcept {
            std::string str = typeid(T).name();
            auto pos = str.find("class"); 
            return pos == std::string::npos ?  str :
                   str.substr(0,pos) + str.substr(pos+5);
        }
    }
    template<class T>
    std::string typeName() noexcept {
        std::string result = type<T>();
        result.erase(std::remove(result.begin(), result.end(), ' '),
               result.end());
        return result;
    }
    
} // end namespace thetest
