/**
 * \file      ps2/ps2/qt/convert/string.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   September (the) 20(th), 2015, 01:46 MSK
 * \updated   March     (the) 03(th), 2016, 16:50 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QRegExp>
#include <QLocale>
#include <ps2/qt/algorithm.h>

/** \namespace ps2 */
namespace ps2 {

/**
* \code
*       auto const& str = ps2::rstrip(str, QChar('*'));
* \endcode
**/
inline QString rstrip(QString const& str, QChar ch = QChar::fromLatin1(' ')) noexcept {
    auto n = str.size() - 1;

    for (; n >= 0; --n) 
        if (str.at(n) != ch) return str.left(n + 1);
    
    return QString();
}
/**
* \code
*      auto const& str = ps2::truncate(str, size);
* \endcode
**/
inline QString truncate(QString s, int size) noexcept {
    if (s.size() < size) return s;

    s.truncate(size - 3);
    s += QStringLiteral("...");
    return s;
}
inline QString toPlainText(QString s) noexcept {
    s.remove(QRegExp(QStringLiteral("<[^>]*>")));
    return s;
}
inline QString toPlainText(QString&& s) noexcept {
    s.remove(QRegExp(QStringLiteral("<[^>]*>")));
    return qMove(s);
}

inline QString capitalize(QString const& str,  QLocale const loc = {}) noexcept {
    if(str.isEmpty()) return str;

    auto result = loc.toLower(str);
    result[0] = result[0].toUpper();
    return result;
}
inline QString rightColon(QString str) noexcept {
    return str + QChar::fromLatin1(':');
}

/** \namespace html */
namespace html {

inline QString extractBody(QString const& str) noexcept {
    QRegExp rx(QStringLiteral(R"(<body[^>]*>(.*)</body>)"));
    return rx.indexIn(str, 0) == -1 ?
             str : QStringLiteral("<body>%1</body>").arg(rx.cap(1));
}
inline QString extractNoEmptyBody(QString const& str) noexcept {
    auto&& body = extractBody(str);
    auto&& text = toPlainText(body);
    if(text.isEmpty()) return text;
    return text.indexOf(QRegExp(QStringLiteral(R"(\S)"))) == -1 ?
             QString() : body;

}

}} // end namespace ps2::html
