/**
 * \file      ps2/ps2/qt/convert/enum.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 25(th), 2016, 11:56 MSK
 * \updated   March (the) 25(th), 2016, 11:56 MSK
 * \TODO
**/
#pragma once
#include <type_traits>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *    auto val = std::get<ps2::to_utype(UserInfoFields::uiEmail)>(uInfo);
 * \endcode
**/
template<class E>
constexpr auto to_utype(E enumerator) noexcept {
    return static_cast<std::underlying_type_t<E>>(enumerator);
}

} // end namespace ps2
