/**
 * \file      ps2/ps2/qt/convert/integer.h
 * \brief     The File is used to work with integer
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.3
 * \created   May   (the) 22(th), 2015, 15:17 MSK
*  \updated   March (the) 01(th), 2016, 15:09 MSK
 * \TODO
**/
#pragma once
#include <memory>
#include <QLocale>
#include <QString>
#include <QVariant>

/** namespace ps2 */
namespace ps2 {
/**
 * \brief Can convert String to unsigned int
 * \code
 *    uint num = ps2::toUint(QStringLiteral("4"), 0u);
 * \endcode
**/
template<class T>
static inline bool isUint(T&& val) noexcept {
   bool is = false;
   QLocale().toUInt(std::forward<T>(val), &is);
   return is;
}
template<class T>
static inline bool isInt(T&& val) noexcept {
   bool is = false;
   QLocale().toInt(std::forward<T>(val), &is);
   return is;
}
/**
 * \brief String to unsigned int
 * \code
 *    uint num = ps2::toUint(QStringLiteral("4"), 0u);
 * \endcode
**/
template<class T>
static inline unsigned int toUint(T const& val, unsigned int defValue = 0u
        , QLocale const& loc = {}) {
   auto is = false;
   auto const value = loc.toUInt(val, &is);
   return is ? value : defValue;
}
static inline int toUint(QVariant&& val, uint defValue = 0u) {
   return toUint<>(val.toString(), defValue);
}
/**
 * \brief String to unsigned int
 * \code
 *    uint num = ps2::toInt(QStringLiteral("4"), INT_MAX);
 * \endcode
**/
template<class T>
static inline int toInt(T val, int defValue = 0, QLocale const& loc = {}) {
   auto is = false;
   auto const value = loc.toInt(val, &is);
   return is ? value : defValue;
}
static inline int toInt(QVariant&& val, int defValue = 0) {
   return toInt<>(val.toString(), defValue);
}
template<class T>
static inline long long toLong(T val, long long defValue = 0, QLocale const loc = {}) {
   auto is = false;
   auto const value = loc.toLongLong(val, &is);
   return is ? value : defValue;
}
static inline long long toLong(QVariant&& val, long long defValue = 0) {
   return toLong<>(val.toString(), defValue);
}

} // end namespace ps
