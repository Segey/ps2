/**
 * \file      ps2/ps2/qt/convert/serialization.h
 * \brief     The file for serialization simple data
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.4
 * \created   April     (the) 04(th), 2012, 23:42 MSK
 * \updated   September (the) 09(th), 2015, 01:28 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include <QIODevice>
#include <QByteArray>
#include <QDataStream>

/** \namespace ps2  */
namespace ps2 {

/** 
 * \code
 * 	    QString to 		= ps2::Serialization55::to(QStringLiteral("ggg"));
 * 	    QString from 	= ps2::Serialization55::from<QString>(s22);
 * 	    uint version 	= ps2::Serialization55::version(s22);
 * 	    const bool is 	= ps2::Serialization55::is(s22);
 * \endcode
 **/
template<int VERSION = QDataStream::Qt_5_5>
struct Serialization {
    /**
     * \brief Checks versions
     * \param {in: QString const&} val Serialization
     **/
   static bool is(QString const& val) {
        auto byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        return in.version() == VERSION;
    }
   /**
    * \brief The version serialization
    * \param {in: QString const& } val Сериал
   **/
   static uint version(QString const& val) {
        auto byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        return in.version();
    }
    /**
     * \brief Getting data from serialization
     * \param {in: QString const& } val - the serialization
     * \param {in: template T} defaultValue - возращаемое значение если версия другая
     * \result  T   данные из сериала или дефолтовая строка
    **/
    template<class T>
    static T from(QString const& val, T const& defaultValue = T()) {
        T result = T();
        int ver;
        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        in >> ver;
        in >> result;
        return ver == VERSION ? result : defaultValue;
    }

    /**
     * \brief Setting data to serialization
     * \param {in: QString const& } val - the data
     * \result  T   данные из сериала или дефолтовая строка
    **/
    template<class T>
    static QString to(T const& val) {
        QByteArray byteArray;
        QDataStream out(&byteArray, QIODevice::WriteOnly);
        out  << VERSION << val;
        return byteArray.toBase64().data();
    }
};

using Serialization55 = Serialization<QDataStream::Qt_5_5>;

} // end namespace ps2

