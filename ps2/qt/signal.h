/**
 * \file      ps2/ps2/qt/signal.h
 * \brief     The Signal class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 16(th), 2017, 12:49 MSK
 * \updated   March     (the) 16(th), 2017, 12:49 MSK
 * \TODO      
**/
#pragma once
#include <initializer_list>
#include <QObject>

/** \namespace ps2::signal */
namespace ps2 {
namespace signal2 {

/**
 * \code
        ps2::signal2::block( [this]() {
            display(createItem());
        }, {m_waist->widget(), m_hip->widget()});

        ps2::signal2::block( std::bind(&WHR::display, this, createItem())
            , {m_waist->widget(), m_hip->widget()});

 * \endcode
**/
template<class F>
static inline void block(F fun, std::initializer_list<QObject*> objs) noexcept {
    for(auto obj : objs)
        if(obj) obj->blockSignals(true);

    fun();

    for(auto obj : objs)
        if(obj) obj->blockSignals(false);
}
    
}} // end namespace ps2::signal
