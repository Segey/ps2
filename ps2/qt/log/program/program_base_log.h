/**
 * \file      ps2/ps2/qt/log/program/program_base_log.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   August (the) 14(th), 2015, 16:56 MSK
 * \updated   August (the) 14(th), 2015, 16:56 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QString>
#include <QtSql/QSqlError>
#include <QDateTime>
#include <QByteArray>
#include <QFileDevice>
#include <QJsonParseError>

class ProgramLog {
public:
    using class_name = ProgramLog;

private:
    QString m_file;
    QString m_fun;
    uint    m_line;

protected:
    virtual void doWrite(QByteArray const& msg) const noexcept = 0;

private:
    QByteArray format(QString const& msg) const noexcept {
        auto&& str = QStringLiteral("[%1] %2:%3 | %5")
            .arg(QDateTime::currentDateTime().toString(QStringLiteral("dd.MM.yyyy:hh:mm:ss")))
            .arg(m_file).arg(m_line).arg(msg);

        return str.toLatin1();
    }

public:
    explicit ProgramLog(QString const& file, QString const& fun, uint line)
        : m_file(file)
        , m_fun(fun)
        , m_line(line) {
    }
    explicit ProgramLog(QString&& file, QString&& fun, uint line)
        : m_file(qMove(file))
        , m_fun(qMove(fun))
        , m_line(line) {
    }
    virtual ~ProgramLog() Q_DECL_EQ_DEFAULT;
    QString const& file() const noexcept {
        return m_file;
    }
    void setFile(QString const& file) noexcept {
        m_file = file;
    }
    void setFile(QString&& file) noexcept {
        m_file = qMove(file);
    }
    QString const& fun() const noexcept {
        return m_fun;
    }
    void setFun(QString const& fun) noexcept {
        m_fun = fun;
    }
    void setFun(QString&& fun) noexcept {
        m_fun = qMove(fun);
    }
    uint line() const noexcept {
        return m_line;
    }
    void setLine(uint line) noexcept {
        m_line = line;
    }
    void write(QString const& msg) noexcept {
        doWrite(format(msg));
    }
    void write(QString&& msg) noexcept {
        doWrite(format(qMove(msg)));
    }
};
