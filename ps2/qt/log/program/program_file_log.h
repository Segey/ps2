/**
 * \file      ps2/ps2/qt/log/program/program_file_log.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   August  (the) 14(th), 2015, 16:56 MSK
 * \updated   January (the) 19(th), 2017, 15:49 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QFileInfo>
#include <ps2/qt/literal/literals.h>
#include "program_base_log.h"

class ProgramFileLog : public ProgramLog {
public:
    using class_name = ProgramFileLog;
    using inherited  = ProgramLog;

private:
    QString m_log_file;
    quint64 m_max_file_size = 100_Mb;

protected:
    void update() const noexcept {
        if(m_max_file_size == 0) return;

        QFileInfo info(m_log_file);
        if(quint64(info.size()) > m_max_file_size) QFile::remove(m_log_file);
    }
    virtual void doWrite(QByteArray const& msg) const noexcept Q_DECL_OVERRIDE {
        QFile file(m_log_file);
        if(!file.open(QFileDevice::Append)) return;
        file.write(msg);
        file.write("\n");
    }

public:
    explicit ProgramFileLog(QString const& log_file, QString const& fun, QString const& file, uint line)
        : inherited(file, fun, line)
        , m_log_file(log_file) {
        update();
    }
    explicit ProgramFileLog(QString const& log_file, const char* file, const char* fun, uint line)
        : ProgramFileLog(log_file, QString::fromLatin1(file), QString::fromLatin1(fun), line) {
        update();
    }
    void setMaxFileSize(quint64 size_in_bytes = 100_Mb) noexcept {
        m_max_file_size = size_in_bytes;
    }
    QString const& logFile() const noexcept {
        return m_log_file;
    }
    void setLogFile(QString const& log_file) noexcept {
        m_log_file = log_file;
    }
    void setLogFile(QString&& log_file) noexcept {
        m_log_file = qMove(log_file);
    }
};
