/**
 * \file      ps2/ps2/qt/log/program/program_log.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   August (the) 14(th), 2015, 17:10 MSK
 * \updated   August (the) 14(th), 2015, 17:10 MSK
 * \TODO      
**/
#pragma once
#include "program_base_log.h"
#include "program_file_log.h"
