/**
 * \file      ps2/ps2/qt/resource.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 10(th), 2015, 16:41 MSK
 * \updated   October (the) 10(th), 2015, 16:41 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QString>

/** \namespace ps2 */
namespace ps2 {

class Resource {
public:
    using class_name = Resource;

private: 
    QString m_file;

public:
    explicit Resource(QString const& file)  noexcept
        : m_file(file){
    }
    QString const& file() const noexcept {
        return m_file;
    }
    void setFile(QString const& file) noexcept {
        m_file = file;
    }
    static QString toStr(char* s) noexcept {
        QFile file(QString::fromLatin1(s));
        file.open(QIODevice::ReadOnly);
        return QString::fromLatin1(file.readAll());
    }
    static QString toStr(QString const& s) noexcept {
        QFile file(s);
        file.open(QIODevice::ReadOnly);
        return QString::fromLatin1(file.readAll());
    }
}; 

} // end namespace ps2

