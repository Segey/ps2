/**
 * \file      ps2/ps2/qt/tick.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 16(th), 2016, 17:08 MSK
 * \updated   May (the) 16(th), 2016, 17:08 MSK
 * \TODO      
**/
#pragma once
#include <QDateTIme>

/** \namespace ps2 */
namespace ps2 {

template<class F>
qint64 tick(F f) noexcept {
   auto const start = QDateTime::currentMSecsSinceEpoch();
   f();
   auto const end = QDateTime::currentMSecsSinceEpoch();
   return end - start;
}

template<class F>
double tick(F f, uint count) noexcept {
    qint64 cx = 0;
    for(int i = 0; i != count; ++i)
        cx += tick(f);
    return double(cx) / count;
}

} // end namespace ps2
