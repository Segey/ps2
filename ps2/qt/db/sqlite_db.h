/**
 * \file      ps2/ps2/qt/db/sqlite_db.h
 * \brief     Base db class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.2
 * \created   April (the) 28(th), 2015, 17:21 MSK
 * \updated   May (the)   07(th), 2015, 16:10 MSK
 * \TODO
**/
#pragma once
#include <QDir>
#include <QFile>
#include <QString>
#include <QFileInfo>
#include <QStringList>
#include <QSqlQuery>
#include <QVariant>
#include <QSqlDatabase>
#include "db.h"

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *    ps2::SqliteDb::createDatabase(Program::name(), QStringList, "target", "mainDb");
 *    ps2::SqliteDb::checkConnect(Program::name(), file, "mainDb");
 * \endcode
**/
class SqliteDb: public Db {
public:
    using class_name = SqliteDb;
    using inherited  = Db;

private:
    static bool isValidDb(QString const& programName, QSqlDatabase const& db, QString const& name) noexcept {
        QSqlQuery query(db);
        query.exec(QStringLiteral("SELECT program, database FROM program_info"));
        query.next();
        return query.value(0).toString() == programName
                 && (query.value(1).toString() == name);
    }

protected:
    virtual QString doGetDbType() const Q_DECL_OVERRIDE {
        return dbType();
    }
    virtual QString doCreateDatabaseName(QString const& file) const Q_DECL_OVERRIDE {
        return file;
    }
    virtual bool doIsValidDb(QString const& programName, QSqlDatabase const& db, QString const& name) Q_DECL_OVERRIDE {
        return isValidDb(programName, db, name);
    }
    virtual bool doFillDatabase(QSqlDatabase const& db) Q_DECL_OVERRIDE {
        Q_UNUSED(db);
        return true;
    }
    SqliteDb(QString const& file, QString const& connectionName)
        :inherited(file, connectionName){
    }
    SqliteDb() Q_DECL_EQ_DEFAULT;
    SqliteDb(SqliteDb const& ) Q_DECL_EQ_DELETE;
    SqliteDb& operator=(SqliteDb const& ) Q_DECL_EQ_DELETE;
    virtual ~SqliteDb() Q_DECL_EQ_DEFAULT;
    static inline QString dbType() noexcept {
        return QStringLiteral("QSQLITE");
    }

public:
    //bool checkConnect(QString const& programName, QString const& file, QString const& connectionName) {
        //return SqliteDb().inherited::checkConnect(programName, file, connectionName);
    //}
    bool createDatabase(QString const& programName, QStringList const& list, QString const& file, QString const& connectionName) {
        return inherited::createDatabase(programName, list, file, connectionName);
    }
    bool createAndFillDatabase(QString const& programName, QStringList const& list, QString const& file, QString const& connectionName) {
        return inherited::createAndFillDatabase(programName, list, file, connectionName);
    }
};

} // end namespace ps2
