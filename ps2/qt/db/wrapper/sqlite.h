/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/db/wrapper/sqlite.h 
 * \brief     The Sqlite class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 13(th), 2017, 23:45 MSK
 * \updated   September (the) 13(th), 2017, 23:45 MSK
 * \TODO      
**/
#pragma once
#include "sqldatabase.h"

/** \namespace ps2::wrapper */
namespace ps2 {
namespace wrapper {

class Sqlite final : public SqlDatabase {
public:
    using class_name = Sqlite;
    using inherited  = SqlDatabase;

public:
    Sqlite() noexcept
        : inherited(QStringLiteral("QSQLITE")) {
    }
};

}} // end namespace ps2::wrapper
