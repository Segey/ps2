/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/wrapper/db.h 
 * \brief     The Db class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 13(th), 2017, 18:50 MSK
 * \updated   September (the) 13(th), 2017, 18:50 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QSqlQuery>
#include <QSqlDatabase>

/** \namespace ps2::wrapper */
namespace ps2 {
namespace wrapper {

/**
 * \code
        m_db.addDatabase(QStringLiteral("SqliteBackup"));
        m_db.setDatabaseName(m_file);
        return m_db.open();
 * \endcode
**/
class SqlDatabase {
public:
    using class_name = SqlDatabase;

private:
    QString      m_driver;
    QString      m_connectionName;
    QSqlDatabase m_db;

private:
    SqlDatabase(SqlDatabase const& ) Q_DECL_EQ_DELETE;
    SqlDatabase& operator=(SqlDatabase const& ) Q_DECL_EQ_DELETE;

protected:
    SqlDatabase(QString const& driver) noexcept
        : m_driver(driver) {
    }
    void addDatabase(QString const& driver, QString const& connectionName) noexcept {
        m_driver = driver;
        addDatabase(connectionName);
    }

public:
    SqlDatabase() Q_DECL_EQ_DEFAULT;
    void addDatabase(QString const& connectionName) noexcept {
        m_connectionName = connectionName;
        if(m_driver.isEmpty() || m_connectionName.isEmpty())
            return;

        m_db = QSqlDatabase::addDatabase(m_driver, m_connectionName);
    }
    void setDatabaseName(QString const& file) noexcept {
        m_db.setDatabaseName(file);
    }
    bool open() {
        return m_db.open();
    }
    QSqlDatabase const& db() const {
        return m_db;
    }
    bool isOpen() const {
        return m_db.isOpen();
    }
    QSqlQuery exec(QString const& query = QString{}) const {
        return m_db.exec(query);
    }
    virtual ~SqlDatabase() {
        if(m_connectionName.isEmpty())
            return;

        if(m_db.isOpen())
            m_db.close();

        m_db = QSqlDatabase::database(QStringLiteral("sqldatabase_disconnect"));
        QSqlDatabase::removeDatabase(m_connectionName);
    }
};

}} // end namespace ps2::wrapper
