/**
 * \file      ps2/ps2/qt/db/source_program_info.h
 * \brief     SourceProgramInfo source class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.3
 * \created   May       (the) 29(th), 2015, 16:29 MSK
 * \updated   September (the) 13(th), 2017, 18:26 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include <QStringList>

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *   SourceProgramInfo::get(Program::name(), Program::mainDbName(), Program::mainDbVersion());
 * \endcode
**/
class SourceProgramInfo final {
public:
    using class_name = SourceProgramInfo;

private:
    static QString info() noexcept {
        return QStringLiteral("CREATE TABLE program_info( "
               "program TEXT NOT NULL UNIQUE,"
               "database TEXT NOT NULL UNIQUE,"
               "version INTEGER NOT NULL UNIQUE"
            ")");
    }
    static QString insertInfo(QString const& programName, QString const& dbName
            , QString const& version) noexcept {
        return QStringLiteral("INSERT INTO program_info(program, database, version) VALUES('%1', '%2', '%3');")
            .arg(programName).arg(dbName).arg(version);
    }

public:
    static QStringList get(QString const& programName, QString const& dbName
            , QString const& version) noexcept {
        QStringList list{{
              info()
            , insertInfo(programName, dbName, version)
        }};
        return list;
    }
};

} // end namespace ps2
