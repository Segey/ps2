/**
 * \file      ps2/ps2/qt/db/db.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.3
 * \created   March (the) 05(th), 2016, 02:07 MSK
 * \updated   April (the) 25(th), 2016, 16:42 MSK
 * \TODO
**/
#pragma once
#include <QDir>
#include <QFile>
#include <QString>
#include <QFileInfo>
#include <QStringList>
#include <QSqlQuery>
#include <QVariant>
#include <QSqlDatabase>

/** namespace ps2 */
namespace ps2 {

class Db {
public:
    using class_name = Db;

private:
    QString      m_file;
    QString      m_connectionName;
    QSqlDatabase m_db;

private:
    static inline void prepareTargetFile(QString const& file) noexcept {
        if(QFile::exists(file) == false)
            QDir().mkpath(QFileInfo(file).dir().path());
        else QFile::remove(file);
    }
    static void execInstructions(QStringList const& list, QSqlDatabase const& db) {
        QSqlQuery query(db);

        for(auto const& line: list)
            query.exec(line);
    }
    bool createDatabase(bool isFill, QString const& programName, QStringList const& list, QString const& file, QString const& connectionName) {
        prepareTargetFile(file);
        bool result = false;
        {
            auto db = QSqlDatabase::addDatabase(doGetDbType(), QStringLiteral("Creating"));
            db.setDatabaseName(doCreateDatabaseName(file));
            if(true == db.open()) {
                execInstructions(list, db);
                if(isFill) doFillDatabase(db);
                result = checkConnect(programName, file, connectionName);
            }
        }
        QSqlDatabase::removeDatabase(QStringLiteral("Creating"));
        return result;
    }
    bool isEmpty() noexcept {
        return m_connectionName.isEmpty()
            && m_file.isEmpty()
            && !m_db.isOpen();
    }
    void updateDbData(QString const& connectionName, QString const file) noexcept {
        m_connectionName = connectionName;
        m_file = file;
        m_db = QSqlDatabase::database(connectionName);
    }

protected:
    virtual QString doGetDbType() const = 0;
    virtual QString doCreateDatabaseName(QString const& file) const = 0;
    virtual bool doIsValidDb(QString const& programName, QSqlDatabase const& db, QString const& name) = 0;
    virtual bool doFillDatabase(QSqlDatabase const& db) = 0;

    Db(QString const& file, QString const& connectionName)
        : m_file(file)
        , m_connectionName(connectionName){
    }
    Db() Q_DECL_EQ_DEFAULT;
    Db(Db const& ) Q_DECL_EQ_DELETE;
    Db& operator=(Db const& ) Q_DECL_EQ_DELETE;

    bool checkConnect(QString const& programName, QString const& file, QString const& connectionName) {
        if(QFile::exists(file) == false) return false;

        bool b = false;
        {
            auto db = QSqlDatabase::addDatabase(doGetDbType(), QStringLiteral("checkConnect"));
            db.setDatabaseName(doCreateDatabaseName(file));
            b = !db.open() ? false : doIsValidDb(programName, db, connectionName);
        }
        QSqlDatabase::removeDatabase(QStringLiteral("checkConnect"));
        return b;
    }
    bool connect(QString const& programName, QString const& file
                 , QString const& connectionName) {
        if(isEmpty() && QSqlDatabase::contains(connectionName)) {
            updateDbData(connectionName, file);
            return true;
        }
        if(false == checkConnect(programName, file, connectionName)) return false;

        disconnect();
        m_db = QSqlDatabase::addDatabase(doGetDbType(),connectionName);
        m_db.setDatabaseName(doCreateDatabaseName(file));
        if(!m_db.open()) return false;

        m_connectionName = connectionName;
        m_file = file;
        return true;
    }
    bool createDatabase(QString const& programName, QStringList const& list
                        , QString const& file, QString const& connectionName) {
        return createDatabase(false, programName, list, file, connectionName);
    }
    bool createAndFillDatabase(QString const& programName, QStringList const& list
                               , QString const& file, QString const& connectionName) {
        return createDatabase(true, programName, list, file, connectionName);
    }

public:
    void disconnect() noexcept {
        if(m_connectionName.isEmpty()) return;
        m_db.close();
        m_db = QSqlDatabase::database(QStringLiteral("db_disconnect"));
        QSqlDatabase::removeDatabase(m_connectionName);
    }
    QString const& file() const noexcept {
        return m_file;
    }
    QString const& connectionName() const noexcept {
        return m_connectionName;
    }
    QSqlDatabase const& db() const noexcept {
        return m_db;
    }
    QSqlDatabase& db() noexcept {
        return m_db;
    }
    bool isConnected() const noexcept {
        return m_db.isOpen();
    }
    virtual ~Db() {
        disconnect();
    }
};

} // end namespace ps2
