/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/db/dump/sqlite_filetyp.h 
 * \brief     The Sqlite_filetyp class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 17(th), 2017, 00:42 MSK
 * \updated   September (the) 17(th), 2017, 00:42 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QString>
#include <QLocale>
#include <ps2/qt/out.h>
#include <main/log.h>
#include "ps2/qt/algorithm.h"

/** \namespace ps2::db */
namespace ps2 {
namespace db {

enum class BackupRestoreType : quint8 {
     Invalid = 0
    , Sql    = 1
    , BinSql = 2
    , Csv    = 4
};
static inline QString to_qstr(BackupRestoreType type) noexcept {
    if (type == BackupRestoreType::Invalid) return QStringLiteral("Invalid");
    if (type == BackupRestoreType::Sql)     return QStringLiteral("Sql");
    if (type == BackupRestoreType::BinSql)  return QStringLiteral("BinSql");
    if (type == BackupRestoreType::Csv)     return QStringLiteral("Csv");

    LOG_ERROR(0, "Unknown type of BackupRestoreType (%1)", ps2::to_utype(type));
    return QStringLiteral("Error");
}
static inline QString to_sqstr(BackupRestoreType type) noexcept {
    return to_qstr(type);
}
static inline bool from_qstr(QString const& str, BackupRestoreType& val
                                  , QLocale const& = {}) noexcept {
    if (str == QStringLiteral("Invalid"))     val = BackupRestoreType::Invalid;
    else if (str == QStringLiteral("Sql"))    val = BackupRestoreType::Sql;
    else if (str == QStringLiteral("BinSql")) val = BackupRestoreType::BinSql;
    else if (str == QStringLiteral("Csv"))    val = BackupRestoreType::Csv;
    else {
        LOG_ERROR(0, "Invalid BackupRestoreType value (%1)", str);
        val = BackupRestoreType::Invalid;
        return false;
    }
    return true;
}
static inline BackupRestoreType toSqliteFileType(quint8 value) Q_DECL_NOEXCEPT {
    if (value == 1) return BackupRestoreType::Sql;
    if (value == 2) return BackupRestoreType::BinSql;
    if (value == 4) return BackupRestoreType::Csv;

    LOG_ERROR(0, "Invalid BackupRestoreType value (%1)", value);
    return BackupRestoreType::Invalid;
}
static inline bool from_sqstr(QString const& str, BackupRestoreType& val) noexcept {
    return from_qstr(str, val);
}
inline void from_value(quint8 id, BackupRestoreType& val) noexcept {
    if (id == 1)      val = BackupRestoreType::Sql;
    else if (id == 2) val = BackupRestoreType::BinSql;
    else if (id == 4) val = BackupRestoreType::Csv;
    else {
        LOG_ERROR(0, "Invalid BackupRestoreType value (%1)", val);
        val = BackupRestoreType::Invalid;
    }
}
inline QDebug operator<<(QDebug dbg, BackupRestoreType type) Q_DECL_NOEXCEPT {
    return dbg.nospace() << to_qstr(type);
}
inline std::ostream& operator<<(std::ostream& out, BackupRestoreType type) Q_DECL_NOEXCEPT {
    return ps2::cout(out, type);
}

}} // end namespace ps2::db

Q_DECLARE_METATYPE(ps2::db::BackupRestoreType)
