/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/dump/helpers/db_helpers.h 
 * \brief     The Db_helpers class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 15(th), 2017, 16:07 MSK
 * \updated   September (the) 15(th), 2017, 16:07 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QStringList>
#include <ps2/cpp/algorithm/concat.h>

/** \namespace ps2::dump */
namespace ps2 {
namespace dump {

class DbHelpers final { 
public:
    using class_name = DbHelpers;
        
public:
    static inline QStringList getColumnNames(wrapper::Sqlite& db, QString const& name) noexcept {
        auto query = db.exec(QStringLiteral("PRAGMA table_info(\"%1\");")
                           .arg(name));
        auto const nameid = query.record()
                        .indexOf(QStringLiteral("name"));
        QStringList list;
        while (query.next())
            list << query.value(nameid).toString();
        return list;
    }
    static inline QString getSqlQuery(wrapper::Sqlite& db, QString const& name) noexcept {
        auto query = db.exec(QStringLiteral("SELECT sql FROM sqlite_master WHERE name='%1';")
                           .arg(name));
        if(!query.next())
            return QString();

        return query.value(0).toString();
    }
    static inline QString createQuery(QString const& name, QStringList const& list) noexcept {
        auto const& end = QStringLiteral(" FROM %1;").arg(name);
        return ps2::concat(list, QStringLiteral(", ")
                           , QStringLiteral("SELECT "), end);
    }
};

}} // end namespace ps2::dump
