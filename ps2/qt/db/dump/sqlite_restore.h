/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/db/dump/sqlite_restore.h 
 * \brief     The Sqlite_restore class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 17(th), 2017, 00:22 MSK
 * \updated   September (the) 17(th), 2017, 00:22 MSK
 * \TODO      
**/
#pragma once
#include <memory>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QStringList>
#include <QSqlDatabase>
#include <ps2/qt/shim.h>
#include <ps2/qt/db/wrapper/sqlite.h>
#include "sqlite_filetype.h"
#include "handler/head.h"
#include "helpers/db_helpers.h"
#include "handler/sql_backup_handler.h"
#include "handler/csv_backup_handler.h"
#include "handler/sql_restore_handler.h"

/** \namespace ps2::db */
namespace ps2 {
namespace db {

/**
 * \brief version 1
 * \code
        ps2::db::SqliteRestore("name.ird2", ps2::SqliteFileType::BinSql)
            .toFile("new.sqlite");
        ps2::db::SqliteRestore("name.sql", ps2::SqliteFileType::Sql)
            .toFile("new.sqlite");
        ps2::db::SqliteRestore(arr, ps2::SqliteFileType::Sql)
            .toByteArray();
 * \endcode
**/
class SqliteRestore final {
public:
    using class_name = SqliteRestore;
    using handler_t  = BaseBackupHandler;
    using type_t     = BackupRestoreType;

private:
    QString         m_file;
    type_t          m_type;
    wrapper::Sqlite m_db;
    QByteArray      m_array;

private:
    template<class T>
    static bool toRollUp(T& db, QStringList const& list) noexcept {
        if(!db->open())
            return false;

        for(auto s : list)  {
            auto exec = db->exec(s);
            if(exec.lastError().isValid())
                return false;
        }
        return true;
    }
    static bool saveToDb(QStringList const& list, QString const& file) noexcept {
        auto db = std::make_unique<wrapper::Sqlite>();
        db->addDatabase(QStringLiteral("SqliteRestore"));
        db->setDatabaseName(file);
        return toRollUp(db, list);
    }
    static std::unique_ptr<wrapper::Sqlite>
            putToMemory(QStringList const& list, QString const&) noexcept {
        auto db = std::make_unique<wrapper::Sqlite>();
        db->addDatabase(QStringLiteral("SqliteMemoryRestore"));
        db->setDatabaseName(QStringLiteral(":memory:"));
        if(toRollUp(db, list))
            return db;
        return nullptr;
    }

private:
    template<class F>
    inline auto fileHandler(QString const& file, QStringList(* fun)(QString const&), F fn) noexcept {
        auto const& list = fun(ps2::shim::to_qstr(m_file));
        return fn(list, file);
    }
    template<class F>
    inline auto arrayHandler(QString const& file
         , QByteArray const& array, QStringList(* fun)(QByteArray const&), F fn) noexcept  {
        auto const& list = fun(array);
        return fn(list, file);
    }

public:
    template<class S>
    inline explicit SqliteRestore(S const& str, type_t type) noexcept
       : m_file(ps2::shim::to_qstr(str))
       , m_type(type) {
    }
    inline explicit SqliteRestore(QByteArray const& array, type_t type) noexcept
       : m_type(type)
       , m_array(array) {
    }
    template<class T>
    inline bool toFile(T const& file) noexcept {
        if(!m_file.isEmpty()) {
            if(m_type == BackupRestoreType::BinSql)
                return fileHandler(file, &SqlRestoreHandler::extractBinFromFile, &saveToDb);
            if(m_type == BackupRestoreType::Sql)
                return fileHandler(file, &SqlRestoreHandler::extractSqlFromFile, saveToDb);
        }
        if(m_type == BackupRestoreType::BinSql)
            return arrayHandler(file, m_array, &SqlRestoreHandler::extractBinFromArray, saveToDb);
        if(m_type == BackupRestoreType::Sql)
            return arrayHandler(file, m_array, &SqlRestoreHandler::extractSqlFromArray, saveToDb);
        return false;
    }
    inline std::unique_ptr<wrapper::Sqlite> toMemory() noexcept {
        QString placeholder;
        if(!m_file.isEmpty()) {
            if(m_type == BackupRestoreType::BinSql)
                return fileHandler(placeholder, &SqlRestoreHandler::extractBinFromFile, &putToMemory);
            if(m_type == BackupRestoreType::Sql)
                return fileHandler(placeholder, &SqlRestoreHandler::extractSqlFromFile, &putToMemory);
        }
        if(m_type == BackupRestoreType::BinSql)
            return arrayHandler(placeholder, m_array, &SqlRestoreHandler::extractBinFromArray, &putToMemory);
        if(m_type == BackupRestoreType::Sql)
            return arrayHandler(placeholder, m_array, &SqlRestoreHandler::extractSqlFromArray, &putToMemory);
        return nullptr;
    }
};

}} // end namespace ps2::db
