/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/db/dump/handler/base_sql_backup_handler.h 
 * \brief     The Base_sql_backup_handler class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 19(th), 2017, 00:29 MSK
 * \updated   September (the) 19(th), 2017, 00:29 MSK
 * \TODO      
**/
#pragma once
#include <QTextStream> 
#include <QDataStream>
#include "base_backup_handler.h"
#include <ps2/cpp/algorithm/concat.h>

/** \namespace ps2::db */
namespace ps2 {
namespace db {

template<class T>
class SqlBackupHandler: public BaseBackupHandler {
public:
    using class_name = SqlBackupHandler<T>;
    using inherited  = BaseBackupHandler;
     
private:
    T& m_out;

private:
     void doUpdateText(QTextStream& out) noexcept {
        out << endl;
    }
    void doUpdateText(QDataStream&) noexcept {
    }
    void writeInit(QTextStream& out) noexcept {
        out << "BEGIN TRANSACTION;";
        doUpdateText(out);
    }
    static void writeFinish(QTextStream& out) noexcept {
        out << QStringLiteral("COMMIT;");
        out.flush();
    }
    void writeInit(QDataStream& out) noexcept {
        out.setVersion(Head::qtVersion());
        out << quint32(Head::magic());
        out << qint32(Head::qtVersion());
        out << quint8(Head::version());
        out << QStringLiteral("BEGIN TRANSACTION;");
    }
    static void writeFinish(QDataStream& out) noexcept {
        out << QStringLiteral("COMMIT;");
    }

private:
    QString insert(QString const& db, QStringList const& list) noexcept {
       auto const& beg = QStringLiteral("INSERT INTO \"%1\" VALUES(").arg(db);
       return ps2::concat(list, QStringLiteral(",")
                      , beg, QStringLiteral(");")
                      , [](auto const& str, auto val){
            return QString::fromLatin1("%1\"%2\"").arg(str, val);
        });
    }
    static QString createLine(QString const& sql) noexcept {
        return QStringLiteral("%1;").arg(sql);
    }


protected: 
    virtual void doWriteTableInit(QString const& name
      , QString const& sql, QStringList const&) noexcept override final {
        m_out << QStringLiteral("DROP TABLE IF EXISTS \"%1\";")
                 .arg(name);
        doUpdateText(m_out);
        m_out << createLine(sql);
        doUpdateText(m_out);
    }
    virtual void doWriteTableRow(QString const& name, QStringList const& list) noexcept override final {
        m_out << insert(name, list);
        doUpdateText(m_out);
    }
    virtual void doWriteTableFinish() noexcept override final {
    }
    void doWriteTableIndex(QString const& name, QString const& sql) noexcept override final {
        m_out << QStringLiteral("DROP INDEX IF EXISTS \"%1\";")
                 .arg(name);
        doUpdateText(m_out);
        m_out << createLine(sql);
        doUpdateText(m_out);
    }
    virtual void doInit() noexcept override final {
        writeInit(m_out);
    }
    virtual void doFinish() noexcept override final {
        writeFinish(m_out);
    }

public:
    SqlBackupHandler(T& out) noexcept
        : m_out(out) {
    }
};

}} // end namespace ps2::db

