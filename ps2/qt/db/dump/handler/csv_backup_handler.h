/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/dump/handler/binary_backup_handler.h 
 * \brief     The Binary_backup_handler class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 15(th), 2017, 16:34 MSK
 * \updated   September (the) 15(th), 2017, 16:34 MSK
 * \TODO      
**/
#pragma once
#include <QTextStream> 
#include "base_backup_handler.h"
#include <ps2/cpp/algorithm/concat.h>

/** \namespace ps2::db */
namespace ps2 {
namespace db {

class CsvBackupHandler final: public BaseBackupHandler {
public:
    using class_name = CsvBackupHandler;
    using inherited  = BaseBackupHandler;
     
private:
    QTextStream& m_out;

    void writeList(QStringList const& list) noexcept {
        m_out << ps2::concat(list, QStringLiteral(",")) << endl;
    }

protected:
    virtual void doWriteTableInit(QString const& name
      , QString const&, QStringList const& list) noexcept override final {
        m_out << name << endl;
        writeList(list);
    }
    virtual void doWriteTableFinish() noexcept override final {
        m_out << endl;
    }
    virtual void doWriteTableRow(QString const&, QStringList const& list) noexcept override final {
        writeList(list);
    }
    virtual void doWriteTableIndex(QString const&, QString const&) noexcept override final{
    }
    virtual void doInit() noexcept override final {
    }
    virtual void doFinish() noexcept override final {
    }

public:
    CsvBackupHandler(QTextStream& out) noexcept
        : m_out(out) {
    }
};

}} // end namespace ps2::db

