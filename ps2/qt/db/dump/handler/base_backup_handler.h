/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/dump/handler/base_backup_handler.h 
 * \brief     The Base_backup_handler class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 15(th), 2017, 16:28 MSK
 * \updated   September (the) 15(th), 2017, 16:28 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include "head.h"

/** \namespace ps2::db */
namespace ps2 {
namespace db {

class BaseBackupHandler {
public:
    using class_name = BaseBackupHandler;
     
protected: 
    virtual void doWriteTableIndex(QString const& name, QString const& sql) noexcept = 0;
    virtual void doWriteTableInit(QString const& name, QString const& sql, QStringList const& list) noexcept = 0;
    virtual void doWriteTableRow(QString const& name, QStringList const& list) noexcept = 0;
    virtual void doWriteTableFinish() noexcept = 0;
    virtual void doInit() noexcept = 0;
    virtual void doFinish() noexcept = 0;

public:
    virtual ~BaseBackupHandler() Q_DECL_EQ_DEFAULT;
    void writeTableIndex(QString const& name, QString const& sql) noexcept {
        doWriteTableIndex(name, sql);
    }
    void writeTableInit(QString const& name, QString const& sql, QStringList const& list) noexcept {
        doWriteTableInit(name, sql, list);
    }
    void writeTableRow(QString const& name, QStringList const& list) noexcept {
        doWriteTableRow(name, list);
    }
    void writeTableFinish() noexcept {
        doWriteTableFinish();
    }
    void init() noexcept {
        doInit();
    }
    void finish() noexcept {
        doFinish();
    }
};

}} // end namespace ps2::db

