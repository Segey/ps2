/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/db/serializator.h 
 * \brief     The Serializator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 14(th), 2017, 16:14 MSK
 * \updated   September (the) 14(th), 2017, 16:14 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QDataStream>

/** \namespace ps2::db */
namespace ps2 {
namespace db {
    
class Head final {
public:
    using class_name = Head;
     
public:
    constexpr static quint32 magic() noexcept {
        return 1'923'948'022;
    }
    constexpr static qint32 qtVersion() noexcept {
        return QDataStream::Qt_5_9;
    }
    constexpr static quint8 version() noexcept {
        return 1;
    }
};

}} // end namespace ps2::db
