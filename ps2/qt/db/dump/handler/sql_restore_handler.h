/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/db/dump/handler/sql_restore_handler.h 
 * \brief     The Sql_restore_handler class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 23(th), 2017, 23:22 MSK
 * \updated   September (the) 23(th), 2017, 23:22 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QTextStream>
#include <QDataStream>
#include <ps2/cpp/algorithm/concat.h>

/** \namespace ps2::db */
namespace ps2 {
namespace db {

class SqlRestoreHandler {
public:
    using class_name = SqlRestoreHandler;
     
public:
    static bool check(QDataStream& in) noexcept {
        quint32 val;
        qint32 qver;
        quint8 ver;
        in.setVersion(Head::qtVersion());
        in >> val;
        if(val != Head::magic())
            return false;
        in >> qver;
        if(qver != Head::qtVersion())
            return false;
        in >> ver;
        return ver == Head::version();
    }
    static QStringList getData(QDataStream& in) noexcept {
        QStringList result;
        QString tmp;

        do {
            in >> tmp;
            result.push_back(tmp);
        }
        while(!tmp.isEmpty());

        return result;
    }
    static QStringList getData(QTextStream& in) noexcept {
        QStringList result;

        do {
            result.push_back(in.readLine());
        }
        while(!(--result.cend())->isEmpty());

        return result;
    }

public:
    static QStringList extractBinFromFile(QString const& file) noexcept {
        QFile f(file);
        if(!f.open(QIODevice::ReadOnly))
            return QStringList{};

        QDataStream in(&f);
        if(!check(in))
            return QStringList{};
        return getData(in);
    }
    static QStringList extractBinFromArray(QByteArray const& array) noexcept {
        QDataStream in(array);
        if(!check(in))
            return QStringList{};
        return getData(in);
    }
    static QStringList extractSqlFromFile(QString const& file) noexcept {
        QFile f(file);
        if(!f.open(QIODevice::ReadOnly))
            return QStringList{};

        QTextStream in(&f);
        return getData(in);
    }
    static QStringList extractSqlFromArray(QByteArray const& array) noexcept {
        QTextStream in(array);
        return getData(in);
    }
};

}} // end namespace ps2::db

