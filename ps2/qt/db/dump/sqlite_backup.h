/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/db/dump/sqlite_backup.h 
 * \brief     The Sqlite_backup class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 12(th), 2017, 18:35 MSK
 * \updated   September (the) 12(th), 2017, 18:35 MSK
 * \TODO      
**/
#pragma once
#include <QSet>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QStringList>
#include <QSqlDatabase>
#include <ps2/qt/shim.h>
#include <ps2/qt/db/wrapper/sqlite.h>
#include "handler/head.h"
#include "helpers/db_helpers.h"
#include "handler/csv_backup_handler.h"
#include "handler/sql_backup_handler.h"

/** \namespace ps2::db */
namespace ps2 {
namespace db {

/**
 * \brief version 1
 * \code
        db::SqliteBackupT("name.sqlite").all().toCsv();
        db::SqliteBackupT("name.sqlite").all().except({"info"})
            .toSql();
        db::SqliteBackupT("name.sqlite").all().toCsvFile("name.csv");
        db::SqliteBackupT("name.sqlite")
            .tables({"workouts", "days"}).toBinSqlFile("name.ird");
        auto _1 = ps2::db::SqliteBackup(m_file).all()
                .except("sqlite_sequence").toBinSql();
 * \endcode
**/
class SqliteBackup final {
public:
    using class_name = SqliteBackup;
    using handler_t  = BaseBackupHandler;

private:
    QString         m_file;
    QSet<QString>   m_tables;
    QSet<QString>   m_indexes;
    wrapper::Sqlite m_db;

private:
    bool openDb() noexcept {
        m_db.addDatabase(QStringLiteral("SqliteBackup"));
        m_db.setDatabaseName(m_file);
        return m_db.open();
    }
    void fill(QSet<QString>& set, QString const& sql) noexcept {
        if(!m_db.isOpen())
            return;

        auto query = m_db.exec(sql);
        while (query.next())
            set << query.value(0).toString();
    }
    void fillTables() noexcept {
        fill(m_tables, QStringLiteral("SELECT name FROM sqlite_master WHERE name <> 'sqlite_sequence' AND type='table' ORDER BY name;"));
    }
    void fillIndexes() noexcept {
        fill(m_indexes, QStringLiteral("SELECT name FROM sqlite_master WHERE type='index' AND name NOT LIKE 'sqlite_autoindex_program_info__' ORDER BY name;"));
    }
    inline void substractExcept() noexcept {
    }
    template<class T, class... Args>
    inline void substractExcept(T&& head, Args&&... tail) noexcept {
        m_tables.remove(ps2::shim::to_qstr(std::forward<T>(head)));
        substractExcept(std::forward<Args>(tail)...);
    }
    inline void addTables() noexcept {
    }
    template<class T, class... Args>
    inline void addTables(T&& head, Args&&... tail) noexcept {
        m_tables << shim::to_qstr(std::forward<T>(head));
        addTables(std::forward<Args>(tail)...);
    }
    void writeTableDump(handler_t& handler, QString const& name
                        , QStringList const& list) noexcept {
        auto query = m_db.exec(dump::DbHelpers::createQuery(name, list));

        while (query.next()) {
            QStringList items;
            for(auto i = 0; i != list.length(); ++i)
                items << query.value(i).toString();
            handler.writeTableRow(name, items);
        }
        handler.writeTableFinish();
    }
    void writeTablesDump(handler_t& handler) noexcept {
        for(auto name : m_tables) {
            auto const& sql = dump::DbHelpers::getSqlQuery(m_db, name);
            auto const& list = dump::DbHelpers::getColumnNames(m_db, name);
            handler.writeTableInit(name, sql, list);
            writeTableDump(handler, name, list);
        }
    }
    void writeIndexesDump(handler_t& handler) noexcept {
        for(auto name : m_indexes)
            handler.writeTableIndex(name, dump::DbHelpers::getSqlQuery(m_db, name));
    }
    template<class H>
    void handlerExec(H& handler) noexcept {
        handler.init();
        writeTablesDump(handler);
        writeIndexesDump(handler);
        handler.finish();
    }

private:
    template<class T, class H>
    inline QByteArray byteHandler() noexcept {
        if(!m_db.isOpen() || m_tables.isEmpty())
            return QByteArray{};

        QByteArray arr;
        T out(&arr, QIODevice::WriteOnly);

        H handler(out);
        handlerExec(handler);
        return arr;
    }
    template<class T, class S, class H>
    inline void fileHandler(T const& file) noexcept {
        if(!m_db.isOpen() || m_tables.isEmpty())
            return;

        QFile f(ps2::shim::to_qstr(file));
        f.open(QIODevice::WriteOnly);

        S out(&f);
        H handler(out);
        handlerExec(handler);
        f.close();
    }

public:
    template<class S>
    inline explicit SqliteBackup(S const& str) noexcept
       : m_file(ps2::shim::to_qstr(str)) {
        openDb();
    }
    inline class_name&& all()&& noexcept {
        fillTables();
        fillIndexes();
        return qMove(*this);
    }
    template<class... Args>
    inline class_name&& tables(Args&&... args)&& noexcept {
        static_assert(sizeof...(args), " Count of arguments cannot be 0");
        addTables(std::forward<Args>(args)...);
        return qMove(*this);
    }
    template<class... Args>
    inline class_name&& except(Args&&... args) && noexcept {
        static_assert(sizeof...(args), " Count of arguments cannot be 0");
        substractExcept(std::forward<Args>(args)...);
        return qMove(*this);
    }
    inline QByteArray toCsv() noexcept {
        return byteHandler<QTextStream, CsvBackupHandler>();
    }
    template<class T>
    inline void toCsvFile(T const& file) noexcept {
        fileHandler<T, QTextStream, CsvBackupHandler>(file);
    }
    inline QByteArray toBinSql() noexcept {
        return byteHandler<QDataStream, SqlBackupHandler<QDataStream>>();
    }
    template<class T>
    inline void toBinSqlFile(T const& file) noexcept {
        fileHandler<T, QDataStream, SqlBackupHandler<QDataStream>>(file);
    }
    inline QByteArray toSql() noexcept {
        return byteHandler<QTextStream, SqlBackupHandler<QTextStream>>();
    }
    template<class T>
    inline void toSqlFile(T const& file) noexcept {
        fileHandler<T, QTextStream, SqlBackupHandler<QTextStream>>(file);
    }
};

}} // end namespace ps2::db
