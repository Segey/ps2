/**
 * \file      ps2/ps2/qt/db/access_db.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 05(th), 2016, 02:25 MSK
 * \updated   March (the) 05(th), 2016, 02:25 MSK
 * \TODO
**/
#pragma once
#include "db.h"

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *    ps2::AccessDb::checkConnect(Program::name(), file, "mainDb");
 *    ps2::AccessDb::createDatabase(Program::name(), QStringList, "target", "mainDb");
 * \endcode
**/
class AccessDb: public Db {
public:
    typedef AccessDb class_name;
    typedef Db       inherited;

protected:
    virtual QString doGetDbType() const Q_DECL_OVERRIDE {
        return QStringLiteral("QODBC");
    }
    virtual QString doCreateDatabaseName(QString const& file) const Q_DECL_OVERRIDE {
        return QStringLiteral("DRIVER={Microsoft Access Driver (*.mdb)};FIL={MS Access};DBQ=%1").arg(file);
    }
    virtual bool doIsValidDb(QString const& , QSqlDatabase const& , QString const& ) Q_DECL_OVERRIDE {
        return true;
    }
    AccessDb(QString const& file, QString const& connectionName) noexcept
        :inherited(file, connectionName){
    }
    AccessDb() Q_DECL_EQ_DEFAULT;
    AccessDb(AccessDb const& ) Q_DECL_EQ_DELETE;
    AccessDb& operator=(AccessDb const& ) Q_DECL_EQ_DELETE;
    virtual ~AccessDb() Q_DECL_EQ_DEFAULT;

public:
    bool createDatabase(QString const& programName, QStringList const& list, QString const& file, QString const& connectionName) noexcept {
        return inherited::createDatabase(programName, list, file, connectionName);
    }
};
} // end namespace ps2

