/**
 * \file      ps2/ps2/qt/db/sqlite_reader.h
 * \brief     SqlQuery source class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.1
 * \created   May  (the) 06(th), 2015, 11:46 MSK
 * \updated   Март (the) 06(th), 2016, 02:55 MSK
 * \TODO
**/
#pragma once
#include <QLocale>
#include <QString>
#include <QDateTime>
#include "sql_reader.h"

/** namespace ps2 */
namespace ps2 {

class SqliteReader: public SqlReader {
public:
    using class_name = SqliteReader;
    using inherited  = SqlReader;

public:
    virtual ~SqliteReader() Q_DECL_EQ_DEFAULT;
    template<class T>
    static T toType(QSqlQuery const& query, int index) noexcept {
        auto const val = query.value(index).toUInt();
        return static_cast<T>(val);
    }
};

} // end namespace ps2
