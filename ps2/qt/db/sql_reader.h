/**
 * \file      ps2/ps2/qt/db/sql_reader.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March (the) 06(th), 2016, 02:52 MSK
 * \updated   March (the) 06(th), 2016, 02:52 MSK
 * \TODO
**/
#pragma once
#include <QLocale>
#include <QString>
#include <QDateTime>
#include <QSqlQuery>
#include <QByteArray>
#include <ps2/qt/objects/rate.h>

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *    class UserDbReader: public ps2::SqlReader
 *
 *    user.setCreatedAt(toTimeStamp(query,20));
 * \endcode
**/
class SqlReader {
public:
    using class_name = SqlReader;
    using query_t    = QSqlQuery;

public:
    SqlReader() Q_DECL_EQ_DEFAULT;
    virtual ~SqlReader() Q_DECL_EQ_DEFAULT;
    template<class T>
    static uint toUint(T const& query, int index) noexcept {
        return query.value(index).toUInt();
    }
    template<class T>
    static int toInt(T const& query, int index) noexcept {
        return query.value(index).toInt();
    }
    template<class T>
    static QString toStr(T const& query, int index) noexcept {
        return query.value(index).toString();
    }
    template<class T>
    static QDate toDate(T const& query, int index) noexcept {
        return query.value(index).toDate();
    }
    template<class T>
    static QByteArray toByteArray(T const& query, int index) noexcept {
        return query.value(index).toByteArray();
    }
    template<class T>
    static ps2::Rate10 toRate10(T const& query, int index) noexcept {
        const uint val = query.value(index).toUInt();
        return ps2::Rate10(val);
    }
    template<class T>
    static QTime toTime(T const& query, int index) noexcept {
        return query.value(index).toTime();
    }
    template<class T>
    static double toDouble(T const& query, double index) noexcept {
        return query.value(index).toDouble();
    }
    template<class T>
    static QDateTime toDateTime(T const& query, int index) noexcept {
        return query.value(index).toDateTime();
    }
    template<class T>
    static QDateTime toTimeStamp(T const& query, int index) noexcept {
        return toDateTime(query, index);
    }
    template<class T>
    static QLocale::Country toCountry(T const& query, int index) noexcept {
        const uint val = query.value(index).toUInt();
        return static_cast<QLocale::Country>(val);
    }
    template<class T>
    static T toType(query_t const& query, int index) noexcept {
        auto const val = query.value(index).toUInt();
        return static_cast<T>(val);
    }
};
} // end namespace ps2
