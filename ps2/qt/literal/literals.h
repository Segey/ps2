/**
 * \file      ps2/ps2/qt/literal/literals.h
 * \brief     The Literals class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 15:03 MSK
 * \updated   January (the) 19(th), 2017, 15:03 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/literal/qstring.h>
#include <ps2/cpp/literal/literals.h>
