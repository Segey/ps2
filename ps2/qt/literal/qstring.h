/**
 * \file      ps2/ps2/qt/literal/qstring.h
 * \brief     The Udl class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 31(th), 2016, 12:18 MSK
 * \updated   October (the) 31(th), 2016, 12:18 MSK
 * \TODO
**/
#pragma once
#include <QString>

static inline QString operator "" _utf8(char const* str, size_t len) {
    return QString::fromUtf8(str, static_cast<int>(len));
}
static inline QString operator "" _latin1(char const* str, size_t len) {
    return QString::fromLatin1(str, static_cast<int>(len));
}

