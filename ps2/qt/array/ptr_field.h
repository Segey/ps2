/**
 * \file      ps2/ps2/qt/array/ptr_field.h
 * \brief     The Pointer PtrField Class. A class is a pointer container that uses an underlying ps2::Field<T*> to store the pointers.
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 25(th), 2017, 12:39 MSK
 * \updated   January (the) 25(th), 2017, 12:39 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QSharedPointer>
#include "field.h"

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *  ColumnArray<int> arr;
 *  arr.add(new int (5));
 *  arr.remove(0u, 44);
 *  arr.remove(0u);
 *  arr.edit(0u, 5);
 *  arr.available_size();
 *
 *  std::distance(arr.unavailable_begin(), arr.unavailable_end())
 * \endcode
**/
template<class T>
class PtrField {
public:
    using class_name       = PtrField;
    using item_t           = T;
    using shared_t         = QSharedPointer<T>;
    using field_t          = Field<shared_t>;
    using status_t         = typename field_t::status_t;
    using value_t          = typename field_t::value_t;
    using iterator         = typename field_t::iterator;
    using const_iterator   = typename field_t::const_iterator;
    using const_iterator_t = ConstIterator<const_iterator>;

protected:
    field_t m_field;

public:
    explicit PtrField() Q_DECL_EQ_DEFAULT;
    virtual ~PtrField() Q_DECL_EQ_DEFAULT;
    void init(std::initializer_list<StatusPair<item_t*>> items) noexcept {
        foreach(auto const& item, items)
            m_field.init(item.status, shared_t(item.value));
    }
    PtrField(class_name const& rhs) noexcept
        : m_field(rhs.m_field) {
    }
    class_name& operator=(class_name const& rhs) noexcept {
        if(this != &rhs) 
            m_field = rhs.m_field;
        return *this;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_field == rhs.m_field;
    }
    friend bool operator != (class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    void addPure(item_t* item) noexcept {
        m_field.addPure(shared_t(item));
    }
    void addPure(shared_t const& item) noexcept {
        m_field.addPure(item);
    }
    void addPure(shared_t&& item) noexcept {
        m_field.addPure(qMove(item));
    }
    void add(item_t* item) noexcept {
        m_field.add(shared_t(item));
    }
    void add(shared_t const& item) noexcept {
        m_field.add(item);
    }
    void add(shared_t&& item) noexcept {
        m_field.add(qMove(item));
    }
    void edit(int index,  item_t* item) noexcept {
        m_field.edit(index, shared_t(item));
    }
    void edit(int index,  shared_t const& item) noexcept {
        m_field.edit(index, item);
    }
    void edit(int index,  shared_t&& item) noexcept {
        m_field.edit(index, qMove(item));
    }
    auto const& operator[](int index) const noexcept {
        return m_field[index];
    }
    auto& operator[](int index) noexcept {
        return m_field[index];
    }
    auto& at(int index) {
        return m_field[index].value;
    }
    auto const& at(int index) const {
        return m_field[index].value;
    }
    void remove(int index) noexcept {
        m_field.remove(index);
    }
    void remove(int index, int count) noexcept {
        m_field.remove(index, count);
    }
    void removeAll() noexcept {
        m_field.removeAll();
    }
    const_iterator_t makeAvailableIterator() const noexcept {
        return m_field.makeAvailableIterator();
    }
    const_iterator_t makeUnavailableIterator() const noexcept {
        return m_field.makeUnavailableIterator();
    }
    iterator begin() noexcept {
        return m_field.begin();
    }
    iterator end() noexcept {
        return m_field.end();
    }
    const_iterator begin() const noexcept {
        return m_field.begin();
    }
    const_iterator end() const noexcept {
        return m_field.end();
    }
    item_t& front() {
        return m_field.front();
    }
    item_t const& front() const {
        return m_field.front();
    }
    item_t last() {
        return m_field.last();
    }
    item_t const& last() const {
        return m_field.last();
    }
    bool isEmpty() const noexcept {
        return m_field.isEmpty();
    }
    int available_size() const noexcept {
        return m_field.available_size();
    }
    int unavailable_size() const noexcept {
        return m_field.unavailable_size();
    }
    int size() const noexcept {
        return m_field.size();
    }
    void clear() noexcept {
        return m_field.clear();
    }
    void reserve(int size) noexcept {
        m_field.reserve(size);
    }
    void swap(class_name& arr) noexcept {
        m_field.swap(arr.m_field);
    }
};

template<class T>
static inline void swap(PtrField<T>& lhs, PtrField<T>& rhs) noexcept {
    lhs.swap(rhs);
}

template<class T>
inline QDebug operator<<(QDebug dbg, PtrField<T> const& arr) noexcept
{
    dbg.nospace() << "\"PtrField<" << thetest::typeName<T>().c_str() << ">\": [";
    for(auto it = arr.begin(); it != arr.end(); ++it) {
        dbg.nospace() << "{\"status\": \"" << it->status << "\", " << it->value << "}";
        if(it != arr.end() -1) dbg.nospace() << ", ";
    }
    dbg.nospace() << ']';
    return dbg.maybeSpace();
}

template<class T>
static auto toSharedVector(PtrField<T> const& vec) noexcept {
    QVector<QSharedPointer<T>> items;
    if(!vec.isEmpty()) items.reserve(vec.size());

    std::transform(vec.begin(), vec.end(), std::back_inserter(items), [](auto const& item) {
        return QSharedPointer<T>(item.value);
    });
    return items;
}

} // end namespace
