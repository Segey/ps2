/**
 * \file      ps2/ps2/qt/array/field.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.3
 * \created   January   (the) 27(th), 2016, 19:27 MSK
 * \updated   September (the) 30(th), 2016, 14:47 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <QVector>
#include <ps2/qt/out.h>
#include <ps2/thetest/type_name.h>
#include <ps2/qt/pair/status.h>
#include <ps2/cpp/iterator/const_iterator.h>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *  ColumnArray<int> arr;
 *  arr.add({1,2,3,4});
 *  arr.remove(0u, 44);
 *  arr.remove(0u);
 *  arr.edit(0u, 5);
 *  arr.available_size();
 *
 *  std::distance(arr.unavailable_begin(), arr.unavailable_end())
 * \endcode
**/
template<class T>
class Field {

public:
    using class_name       = Field;
    using status_t         = FieldStatus;
    using item_t           = T;
    using value_type       = item_t;
    using value_t          = StatusPair<T>;
    using items_t          = QVector<value_t>;
    using iterator         = typename items_t::iterator;
    using const_iterator   = typename items_t::const_iterator;
    using const_iterator_t = ConstIterator<const_iterator>;

protected:
    items_t m_items;
    int     m_removed = 0;

public:
    template<class U = T>
    void init(status_t status, U&& item) noexcept {
        if(status != status_t::Removed) {
            if (size() == 0) m_items.push_back(value_t(status, std::forward<U>(item)));
            else m_items.insert(size() - m_removed, value_t(status, std::forward<U>(item)));
            return;
        }
        ++m_removed;
        m_items.push_back(value_t(status, std::forward<U>(item)));
    }
    void init(value_t const& val) noexcept {
        init(val.status, val.value);
    }

private:
    template<class U = T>
    void updateItem(int index, status_t status, U&& item) noexcept {
        m_items[index].status  = status;
        m_items[index].value = std::forward<U>(item);
    }
    status_t isEdited(int index) const noexcept {
        return m_items[index].status == status_t::Added
                                      ? status_t::Added
                                      : status_t::Edited;
    }

public:
    explicit Field() = default;
    virtual ~Field() = default;
    Field(std::initializer_list<value_t> items) noexcept {
        init(items);
    }
    Field(class_name const& rhs) noexcept
        : m_items(rhs.m_items)
        , m_removed(rhs.m_removed) {
    }
    class_name& operator=(class_name const& rhs) noexcept {
        if(this != &rhs) {
            m_items   = rhs.m_items;
            m_removed = rhs.m_removed;
        }
        return *this;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_items.size() == rhs.m_items.size()
            && lhs.m_removed == rhs.m_removed
            && lhs.m_items == rhs.m_items
        ;
    }
    friend bool operator != (class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    void init(std::initializer_list<value_t> items) noexcept {
        foreach(auto const& item, items)
            init(item.status, item.value);
    }
    template<class U = T>
    void addPure(U&& item) noexcept {
        init(status_t::Pure, std::forward<U>(item));
    }
    template<class U = T>
    void add(U&& item) noexcept {
        init(status_t::Added, std::forward<U>(item));
    }
    template<class U = T>
    void edit(int index, U&& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        if(index < m_items.size() - m_removed)
            updateItem(index, isEdited(index), std::forward<U>(item));
    }
    value_t const& operator[](int index) const noexcept {
        return m_items[index];
    }
    value_t& operator[](int index) noexcept {
        return m_items[index];
    }
    item_t& at(int index) {
        return m_items[index].value;
    }
    item_t const& at(int index) const {
        return m_items[index].value;
    }
    void remove(int index) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size()
                   , "indexing", "out of range");

        remove(index, 1);
    }
    void remove(int index, int count) noexcept {
        Q_ASSERT_X(index + count >=0 && index + count <= m_items.size() , "indexing", "out of range");

        auto begin = m_items.begin() + index;
        auto end = m_items.begin() + index + count;

        items_t items;
        std::copy(begin, end, std::back_inserter(items));
        m_items.erase(begin, end);

        foreach(auto const& item, items) {
            if(item.status == status_t::Added)
                continue;

            m_items.push_back(item);
            m_items.last().status = status_t::Removed;
            ++m_removed;
        }
    }
    void removeAll() noexcept {
        remove(0,available_size());
    }
    const_iterator_t makeAvailableIterator() const noexcept {
        return const_iterator_t(m_items.begin(), m_items.end() - m_removed);
    }
    const_iterator_t makeUnavailableIterator() const noexcept {
        return const_iterator_t(m_items.end() - m_removed, m_items.end());
    }
    iterator begin() noexcept {
        return m_items.begin();
    }
    iterator end() noexcept {
        return m_items.end();
    }
    const_iterator begin() const noexcept {
        return m_items.begin();
    }
    const_iterator end() const noexcept {
        return m_items.end();
    }
    item_t& front() {
        return m_items[0].value;
    }
    item_t const& front() const {
        return m_items[0].value;
    }
    item_t last() {
        return m_items[size() - 1].value;
    }
    item_t const& last() const {
        return m_items[size() - 1].value;
    }
    bool isEmpty() const noexcept {
        return m_items.empty();
    }
    int available_size() const noexcept {
        return size() - m_removed;
    }
    int unavailable_size() const noexcept {
        return m_removed;
    }
    int size() const noexcept {
        return m_items.size();
    }
    void clear() noexcept {
        return m_items.clear();
    }
    void reserve(int size) noexcept {
        m_items.reserve(size);
    }
    void swap(class_name& arr) noexcept {
        m_items.swap(arr.m_items);
        qSwap(m_removed, arr.m_removed);
    }
};

template<class T>
static inline void swap(Field<T>& lhs, Field<T>& rhs) noexcept {
    lhs.swap(rhs);
}
inline QDebug operator<<(QDebug dbg, FieldStatus status) noexcept {
    if(status == FieldStatus::Pure ) dbg.nospace() << "Pure";
    else if(status == FieldStatus::Added ) dbg.nospace() << "Added";
    else if(status == FieldStatus::Edited ) dbg.nospace() << "Edited";
    else if(status == FieldStatus::Removed ) dbg.nospace() << "Removed";
    else dbg.nospace() << "Invalid";
    return dbg.maybeSpace();
}
inline std::ostream& operator<<(std::ostream& out, FieldStatus status) noexcept
{
    return ps2::cout(out, status);
}

template<class T>
inline QDebug operator<<(QDebug dbg, Field<T> const& arr) noexcept
{
    dbg.nospace() << "\"Fields<" << thetest::typeName<T>().c_str() << ">\": [";
    for(auto it = arr.begin(); it != arr.end(); ++it) {
        dbg.nospace() << "{\"status\": \"" << it->status << "\", " << it->value << "}";
        if(it != arr.end() -1) dbg.nospace() << ", ";
    }
    dbg.nospace() << ']';
    return dbg.maybeSpace();
}

template<class T>
inline std::ostream& operator<<(std::ostream& out, Field<T> const& arr) noexcept
{
    return ps2::cout(out, arr);
}

inline QString to_qstr(FieldStatus status, QLocale const = {}) noexcept {
    return ps2::cout(status);
}
inline QString to_sqstr(FieldStatus status) noexcept {
    return ps2::cout(status);
}

template<class T>
static QVector<T> toVector(Field<T> const& vec) noexcept {
    QVector<T> items;
    if(!vec.isEmpty())
        items.reserve(vec.size());

    std::transform(vec.begin(), vec.end(), std::back_inserter(items)
                   , [](auto const& item) {
        return item.value;
    });
    return items;
}

} // end namespace ps2
