/**
 * \file      ps2/ps2/qt/array/category.h
 * \brief     The CategoryArray class provides a true/false behaviour for category classes.
 *            Class have 2 layers. 1 Layer Category, 2 layer items which are placed under an category.
 * \copyright S.Panin, 2006 - 2017
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   October (the) 03(th), 2016, 15:14 MSK
 * \updated   January (the) 25(th), 2017, 12:28 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <ps2/qt/pair/bool.h>
#include <ps2/thetest/type_name.h>
#include <ps2/qt/out.h>
#include <ps2/cpp/iterator/const_iterator.h>
#include "toggle.h"

/** \namespace ps2 */
namespace ps2 {

template<class T>
class CategoryArray {
public:
    using class_name       = CategoryArray;
    using value_t          = T;
    using item_t           = BoolPair<T>;
    using items_t          = QVector<item_t>;
    using iterator         = typename items_t::iterator;
    using const_iterator   = typename items_t::const_iterator;
    using const_iterator_t = ConstIterator<const_iterator>;

private:
    item_t  m_category;
    items_t m_items;

private:
    void switcher(bool status, int index, value_t const& value) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        m_items[index].status = status;
        m_items[index].value  = value;
    }

public:
    explicit CategoryArray() = default;
    virtual ~CategoryArray() = default;
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_items   == rhs.m_items
            && lhs.m_category == rhs.m_category
        ;
    }
    friend bool operator != (class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    void setCategory(item_t const& item) noexcept {
        m_category = item;
    }
    void setCategory(item_t&& item) noexcept {
        m_category = qMove(item);
    }
    void setCategory(value_t const& item) noexcept {
        m_category = {false, item};
    }
    item_t const& category() const noexcept {
        return m_category;
    }
    item_t& category() noexcept {
        return m_category;
    }
    void addItem(item_t const& item) noexcept {
        m_items.push_back(item);
    }
    void addItem(item_t&& item) noexcept {
        m_items.push_back(qMove(item));
    }
    void addItems(std::initializer_list<item_t> items) noexcept {
        foreach(auto&& item, items)
            addItem(item);
    }
    void editItem(int index, item_t const& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");
        m_items[index] = item;
    }
    void editItem(int index, item_t&& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");
        m_items[index] = qMove(item);
    }
    void toggle(int index, value_t const& value) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        m_items[index].status ^= 1;
        m_items[index].value = value;
    }
    bool isSwitchedOn(int index) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");
        return m_items[index].status == true;
    }
    void switchOn(int index, value_t const& value) noexcept {
        switcher(true, index, value);
    }
    void switchOff(int index, value_t const& value) noexcept {
        switcher(false, index, value);
    }
    item_t const& operator[](int index) const noexcept {
        return m_items[index];
    }
    item_t& operator[](int index) noexcept {
        return m_items[index];
    }
    void remove(int index) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        remove(index, 1);
    }
    void remove(int index, int count) noexcept {
        Q_ASSERT_X(index + count >=0 && index + count <= m_items.size() , "indexing", "out of range");

        auto begin = m_items.begin() + index;
        auto end = m_items.begin() + index + count;
        m_items.erase(begin, end);
    }
    const_iterator_t makeItemsIter() const noexcept {
        return const_iterator_t(m_items.begin(), m_items.end());
    }
    iterator begin() noexcept {
        return m_items.begin();
    }
    iterator end() noexcept {
        return m_items.end();
    }
    const_iterator begin() const noexcept {
        return m_items.begin();
    }
    const_iterator end() const noexcept {
        return m_items.end();
    }
    bool empty() const noexcept {
        return m_items.empty();
    }
    int size() const noexcept {
        return m_items.size();
    }
    void clear() noexcept {
        return m_items.clear();
    }
    void reserve(int size) noexcept {
        m_items.reserve(size);
    }
    void swap(class_name& arr) noexcept {
        m_category.swap(arr.m_category);
        m_items.swap(arr.m_items);
    }
};

template<class T>
static inline void swap(CategoryArray<T>& lhs, CategoryArray<T>& rhs) noexcept {
    lhs.swap(rhs);
}

template<class T>
inline QDebug operator<<(QDebug dbg, CategoryArray<T> const& arr) noexcept
{
    dbg.nospace() << "\"CategoryArray<" << thetest::typeName<T>().c_str() << ">\": [";
    for(auto it = arr.begin(); it != arr.end(); ++it) {
        dbg.nospace() << "{\"status\": \"" << it->status << "\", " << it->value << "}";
        if(it != arr.end() -1) dbg.nospace() << ", ";
    }
    dbg.nospace() << ']';
    return dbg.maybeSpace();
}
template<class T>
inline std::ostream& operator<<(std::ostream& out, CategoryArray<T> const& e) noexcept {
    return ps2::cout(out, e);
}

} // end namespace
