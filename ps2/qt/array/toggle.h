/**
 * \file      ps2/ps2/qt/array/toggle.h
 * \brief     The ToggleArray class provides a true/false behaviour for an array
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 30(th), 2016, 15:56 MSK
 * \updated   September (the) 30(th), 2016, 15:56 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <ps2/qt/pair/bool.h>
#include <ps2/thetest/type_name.h>
#include <ps2/qt/out.h>
#include <ps2/cpp/iterator/const_iterator.h>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *  ToggleArray<int> arr;
 *  arr.add({1,2,3,4});
 *  arr.remove(0u, 44);
 *  arr.remove(0u);
 *  arr.edit(0u, 5);
 *  arr.available_size();
 *
 *  std::distance(arr.unavailable_begin(), arr.unavailable_end())
 * \endcode
**/
template<class T>
class ToggleArray {
public:
    using class_name       = ToggleArray;
    using value_t          = T;
    using item_t           = BoolPair<T>;
    using items_t          = QVector<item_t>;
    using iterator         = typename items_t::iterator;
    using const_iterator   = typename items_t::const_iterator;
    using const_iterator_t = ConstIterator<const_iterator>;

private:
    items_t m_items;

public:
    explicit ToggleArray() = default;
    virtual ~ToggleArray() = default;
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_items == rhs.m_items
        ;
    }
    friend bool operator != (class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    void add(bool status, std::initializer_list<value_t> items) noexcept {
        foreach(auto&& item, items)
            add(status, item);
    }
    void add(bool status, value_t const& item) noexcept {
        m_items.push_back(item_t(status, item));
    }
    void add(bool status, value_t&& item) noexcept {
        m_items.push_back(item_t(status, qMove(item)));
    }
    void add(value_t const& item) noexcept {
        add(false, item);
    }
    void add(value_t&& item) noexcept {
        add(false, qMove(item));
    }
    void edit(int index, bool status, value_t const& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        auto& cur = m_items[index];
        cur.status = status;
        cur.value = item;
    }
    void edit(int index, bool status, value_t&& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        auto& cur = m_items[index];
        cur.status = status;
        cur.value = qMove(item);
    }
    void edit(int index, item_t const& item) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        edit(index, item.status, item.value);
    }
    void toggle(int index) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        m_items[index].status ^= 1;
    }
    item_t const& operator[](int index) const noexcept {
        return m_items[index];
    }
    item_t& operator[](int index) noexcept {
        return m_items[index];
    }
    void remove(int index) noexcept {
        Q_ASSERT_X(index >=0 && index < m_items.size(), "indexing", "out of range");

        remove(index, 1);
    }
    void remove(int index, int count) noexcept {
        Q_ASSERT_X(index + count >=0 && index + count <= m_items.size() , "indexing", "out of range");

        auto begin = m_items.begin() + index;
        auto end = m_items.begin() + index + count;
        m_items.erase(begin, end);
    }
    const_iterator_t makeIter() const noexcept {
        return const_iterator_t(m_items.begin(), m_items.end());
    }
    iterator begin() noexcept {
        return m_items.begin();
    }
    iterator end() noexcept {
        return m_items.end();
    }
    const_iterator begin() const noexcept {
        return m_items.begin();
    }
    const_iterator end() const noexcept {
        return m_items.end();
    }
    bool empty() const noexcept {
        return m_items.empty();
    }
    int size() const noexcept {
        return m_items.size();
    }
    void clear() noexcept {
        return m_items.clear();
    }
    void reserve(int size) noexcept {
        m_items.reserve(size);
    }
    void swap(class_name& arr) noexcept {
        m_items.swap(arr.m_items);
    }
};

template<class T>
static inline void swap(ToggleArray<T>& lhs, ToggleArray<T>& rhs) noexcept {
    lhs.swap(rhs);
}
template<class T>
inline QDebug operator<<(QDebug dbg, ToggleArray<T> const& arr) noexcept
{
    dbg.nospace() << "\"ToggleArray<" << thetest::typeName<T>().c_str() << ">\": [";
    for(auto it = arr.begin(); it != arr.end(); ++it) {
        dbg.nospace() << "{\"status\": \"" << it->status << "\", " << it->value << "}";
        if(it != arr.end() -1) dbg.nospace() << ", ";
    }
    dbg.nospace() << ']';
    return dbg.maybeSpace();
}
template<class T>
inline std::ostream& operator<<(std::ostream& out, ToggleArray<T> const& e) noexcept {
    return ps2::cout(out, e);
}

} // end namespace
