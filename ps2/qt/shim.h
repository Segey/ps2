/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/shim.h 
 * \brief     The Shim class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 13(th), 2017, 00:33 MSK
 * \updated   September (the) 13(th), 2017, 00:33 MSK
 * \TODO      
**/
#pragma once
#include <string>
#include <QString>
#include <ps2/qt/objects/file_name.h>

/** \namespace ps2::shim */
namespace ps2::shim {

static inline std::string to_str(char const* str) noexcept {
    return std::string(str);
}
static inline std::string to_str(std::string const& str) noexcept {
    return str;
}
static inline std::string to_str(QString const& str) noexcept {
    return str.toStdString();
}
static inline QString to_qstr(char const* str) noexcept {
    return QString::fromLatin1(str);
}
static inline QString to_qstr(std::string const& str) noexcept {
    return QString::fromStdString(str);
}
static inline QString to_qstr(QString const& str) noexcept {
    return str;
}
static inline QString to_qstr(QString&& str) noexcept {
    return std::move(str);
}
static inline QString to_qstr(FileName const& name) noexcept {
    return name.name();
}

} // end namespace ps2::shim
