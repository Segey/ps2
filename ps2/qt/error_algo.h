/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/error_algo.h 
 * \brief     The Error_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 15(th), 2017, 13:45 MSK
 * \updated   November  (the) 15(th), 2017, 13:45 MSK
 * \TODO      
**/
#pragma once
#include <QJsonParseError>

/** begin namespace ps2 */
namespace ps2 {
    /**
     * \code
     *      throw ps2::make_error(QJsonParseError::MissingObject);
     * \endcode
    **/
    static inline QJsonParseError make_error(QJsonParseError::ParseError error, int offset = 0) noexcept {
        QJsonParseError o;
        o.error = error;
        o.offset = offset;
        return o;
    }
} // end namespace ps2
