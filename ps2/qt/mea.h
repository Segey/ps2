/**
 * \file      ps2/ps2/qt/mea.h
 * \brief     The class provides Measurement System
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   April (the) 22(th), 2016, 00:16 MSK
 * \updated   April (the) 22(th), 2016, 00:16 MSK
 * \TODO
**/
#pragma once
#include <QLocale>

/** \namespace ps2::locale */
namespace ps2 {
namespace locale {

static inline QLocale ru() noexcept {
    return QLocale(QLocale::Russian);
}
static inline QLocale en() noexcept {
    return QLocale(QLocale::English);
}

}} // end namespace ps2::locale
