/**
 * \file      ps2/ps2/qt/regex/regex_match.h
 * \brief     The Regex_match class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 12:35 MSK
 * \updated   January (the) 19(th), 2017, 12:35 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QRegExp>
#include <QRegularExpression>
#include "regex_expression.h"
#include "regex_validator.h"

/** \namespace ps2::regex::match */
namespace ps2 {
namespace regexp {
namespace match {

static inline bool email(QString const& mail) noexcept {
    return validator::email().exactMatch(mail);
}

}}} // end namespace ps2::regexp::match
