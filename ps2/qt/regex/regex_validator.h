/**
 * \file      ps2/ps2/qt/regex/regex_validator.h
 * \brief     The Regex_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 12:42 MSK
 * \updated   January (the) 19(th), 2017, 12:42 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>
#include <QRegularExpression>
#include "regex_expression.h"

/** \namespace ps2::regexp::validator */
namespace ps2 {
namespace regexp {
namespace validator {

static inline QRegExp email() noexcept {
    QRegExp reg(ps2::regexp::email());
    reg.setCaseSensitivity(Qt::CaseInsensitive);
    reg.setPatternSyntax(QRegExp::RegExp);
    return reg;
}

}}} // end namespace ps2::regexp::validator
