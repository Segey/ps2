/**
 * \file      ps2/ps2/qt/regex/regex_expression.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.1
 * \created   April   (the) 13 (th), 2016, 19:27 MSK
 * \updated   January (the) 18(th), 2017, 12:22 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace ps2::regexp */
namespace ps2 {
namespace regexp {

static inline QString email() noexcept {
    return QStringLiteral("^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$");
}

/** \namespace tag */
namespace tag {
    
static inline QString A() noexcept {
    return QStringLiteral(R"(<a\s+href\s*=\s*\"([^\"]*)\"\s*>([^<]*)</a>)");
}

} // end namespace tag

}} // end ps2::regexp namespace 
