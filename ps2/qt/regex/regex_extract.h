/**
 * \file      ps2/ps2/qt/regex/regex_extract.h
 * \brief     The Regex_extract class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 12:47 MSK
 * \updated   January (the) 19(th), 2017, 12:47 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QRegExp>
#include <QRegularExpression>
#include "regex_expression.h"
#include "regex_validator.h"
#include <ps2/qt/objects/link.h>

/** \namespace ps2::regex::extract */
namespace ps2 {
namespace regexp {
namespace extract {

/** \namespace tag */
namespace tag {
    
/**
 * \code
    auto&& re = ps2::regexp::extract::tag::A();
 * \endcode
**/
static inline auto A(QString const& str) noexcept {
    QRegularExpression re(qMove(ps2::regexp::tag::A()));
    auto m = re.match(str);
    if(!m.isValid() || !m.hasMatch()) return QPair<QString, QString>{};
    return qMakePair(m.captured(1),m.captured(2));
}
static inline auto A(QString&& str) noexcept {
    QRegularExpression re(qMove(ps2::regexp::tag::A()));
    auto m = re.match(qMove(str));
    if(!m.isValid() || !m.hasMatch()) return QPair<QString, QString>{};
    return qMakePair(m.captured(1),m.captured(2));
}

} // end namespace tag

}}} // end namespace ps2::regexp::extract
