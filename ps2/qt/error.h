/**
 * \file      ps2/ps2/qt/error.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   June (the) 01(th), 2016, 00:29 MSK
 * \updated   June (the) 01(th), 2016, 00:29 MSK
 * \TODO      Add ps2::cpp::Trace behaviour
**/
#pragma once
#include <QString>
#include "shim.h"

/** begin namespace ps2 */
namespace ps2 {

/**
 * \code
     THROW_LOGIC_ERROR("Unsupported char", ch);
 * \endcode
 **/
template<class T>
class ErrorThrower final {
public:
    using class_name = ErrorThrower;
    using error_t    = T;

private:
    char const* m_filename = nullptr;
    unsigned m_line        = 0u;

private:
    inline QString createStr() const noexcept {
        if(m_filename)
            return QStringLiteral("%1(%2) - ")
                .arg(QString::fromLatin1(m_filename)).arg(m_line);
        return QString();
    }
    template<class U>
    static inline QString one(U&& t) noexcept {
        return QStringLiteral("%1").arg(t);
    }

public:
    ErrorThrower() noexcept = default;
    ErrorThrower(char const* filename, unsigned line) noexcept
        : m_filename{filename}
        , m_line{line} {
    }
    template<class... Args>
    [[noreturn]]void operator()(QString const& msg, Args&&... args) const {
        auto const& val = QStringLiteral("%1%2").arg(createStr(), msg);
        if constexpr(sizeof...(args) == 0)
            throw error_t(qPrintable(val));
        if constexpr(sizeof...(args) == 1) {
            throw error_t(qPrintable(val + one(std::forward<Args>(args)...)));
        }
    }
    template<class... Args>
    [[noreturn]]void operator()(std::string const& msg, Args&&... args) const {
        (*this)(shim::to_qstr(msg), std::move(args)...);
    }
    template<class... Args>
    [[noreturn]]void operator()(char const* msg, Args&&... args) const {
        (*this)(shim::to_qstr(msg), std::move(args)...);
    }
};

} // end namespace ps2

#ifdef _DEBUG
    #define THROW_LOGIC_ERROR ps2::ErrorThrower<std::logic_error>(__FILE__, __LINE__)
    #define THROW_RANGE_ERROR ps2::ErrorThrower<std::range_error>(__FILE__, __LINE__)
    // #define THROW_LOGIC_ERROR //
    // #define THROW_RANGE_ERROR //
#else
    #define THROW_LOGIC_ERROR ps2::ErrorThrower<std::logic_error>()
    #define THROW_RANGE_ERROR ps2::ErrorThrower<std::range_error>()
#endif

