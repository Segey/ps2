/**
 * \file      ps2/ps2/qt/delegates/bool_delegate.h
* \brief      The Bool delegate 
* \author     S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
* \version    v.2.00
* \date       Created on 07 September 2012 y., 01:38
* \TODO
**/
#pragma once
/** namespace go::delegate */
namespace go {
namespace delegate {
class Bool : public QItemDelegate {
public:
    typedef QItemDelegate	inherited;
    typedef Bool 		    class_name;

    static const uint bool_role = Qt::UserRole +1;              //!< The bool role for 3 column

public:
    explicit Bool(QObject* parent = 0) : inherited(parent) {
    }
    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        QComboBox* box = new QComboBox(parent);
        box->addItem(iWord::tr("false"));
        box->addItem(iWord::tr("true"));
        return box;
    }
    void setEditorData(QWidget* widget, const QModelIndex& index) const {
        const bool value = index.model()->data(index, bool_role).toBool();

        QComboBox* box = static_cast<QComboBox*>(widget);
        box->setCurrentIndex(value == false ? 0 : 1);
    }
    void setModelData(QWidget* widget, QAbstractItemModel* model, const QModelIndex& index) const {
        QComboBox* box = static_cast<QComboBox*>(widget);
        const bool value = box->currentIndex() == 1 ? true : false;
        model->setData(index, value, bool_role);
        model->setData(index, value, Qt::EditRole);
    }
    void updateEditorGeometry(QWidget* widget, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        widget->setGeometry(option.rect);
    }
};
}} // end namespace go::delegate

