/**
 * \file      ps2/ps2/qt/qml_window.h
 * \brief     Base class to work with qml 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   June (the) 22(th), 2015, 14:30 MSK
 * \updated   June (the) 22(th), 2015, 14:30 MSK
 * \TODO
**/
#pragma once
#include <QUrl>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QQuickWindow>
#include <QCoreApplication>
#include <QQmlApplicationEngine>

namespace ps2 {

/**
 * \code
 *      class Calculator : public QObject, public ps2::QmlWindow<ps2::qmlwindow::None> {
 *      protected:
 *          virtual void doDisconnect(QQuickWindow* window) override { }
 *          virtual void doConnect(QQuickWindow* window) override { }
 *      }
**/
namespace qmlwindow {
    class None {
    public:
        typedef None class_name;
        void operator()() {
        }
    };
    class Terminate {
    public:
        typedef Terminate class_name;
        void operator()() {
            QCoreApplication::instance()->quit();
        }
    };
}


template<typename S = qmlwindow::Terminate>
class QmlWindow {
public:
    typedef S            terminate_s;
    typedef QmlWindow<S> class_name;

private:
    QQuickWindow* m_window;

protected:
    virtual void doDisconnect(QQuickWindow* window) = 0;
    virtual void doConnect(QQuickWindow* window) = 0;

    QQuickWindow* window() {
        return m_window;
    }

public:
    QmlWindow(QQuickWindow* window = nullptr)
        : m_window(window){
    }
    bool setProperty(const char *name, QVariant const& value) {
        return m_window ? m_window->setProperty(name, value) : false;
    }
    bool setProperty(const char *name, QVariant&& value) {
        return m_window ? m_window->setProperty(name, std::move(value)) : false;
    }
    QVariant property(const char *name) const {
        return m_window ? m_window->property(name) : QVariant();
    }
    void setWindow(QQmlApplicationEngine&& engine, uint index = 0u) {
        setWindow(&engine, index);
    }
    void setWindow(QQmlApplicationEngine& engine, uint index = 0u) {
        setWindow(&engine, index);
    }
    void setWindow(QQmlApplicationEngine* engine, uint index = 0u) {
        if(!engine || engine->rootObjects().size() <= static_cast<int>(index)) return terminate_s()();
        QObject* root = engine->rootObjects().at(index);
        QQuickWindow* window = qobject_cast<QQuickWindow*>(root);
        if (!window) return terminate_s()();
        setWindow(window);
    }
    void setWindow(QQuickWindow* window) {
        if (m_window != nullptr) doDisconnect(m_window);
        m_window = window;
        if (m_window != nullptr) doConnect(m_window);
    }
};
} // end namespace ps2
