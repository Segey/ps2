/**
 * \file      ps2/ps2/qt/objects/clause_type.h
 * \brief     The Clause_type class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 28(th), 2017, 00:05 MSK
 * \updated   February  (the) 28(th), 2017, 00:05 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QString>
#include <ps2/qt/out.h>
#include <ps2/qt/convert/enum.h>
#include <main/tr.h>

/** \namespace ps2 */
namespace ps2 {

enum class ClauseType : quint8 {
     Invalid = 0
    , Text   = 1
    , Html   = 2
};

static inline QString to_qstr(ClauseType type) noexcept {
    if (type == ClauseType::Invalid) return QStringLiteral("Invalid");
    if (type == ClauseType::Text) return QStringLiteral("Text");
    if (type == ClauseType::Html) return QStringLiteral("Html");

    Q_ASSERT_X(0, "ClauseType::to_qstr", "Unknown type");
    return QStringLiteral("Error");
}
static inline QString to_tr(ClauseType type) noexcept {
	if (type == ClauseType::Invalid) return iError::tr("Invalid");
	if (type == ClauseType::Text) return iText::tr("Text");
	if (type == ClauseType::Html) return iText::tr("Html");

	Q_ASSERT_X(0, "ClauseType::to_qstr", "Unknown type");
	return iError::tr("Error");
}
static inline QString to_sqstr(ClauseType type) noexcept {
    return to_tr(type);
}
inline bool from_qstr(QString const& s, ClauseType& val
                                  , QLocale const& = {}) noexcept {
    auto const str = s.toLower();
    if (str == QStringLiteral("text"))     val = ClauseType::Text;
    else if (str == QStringLiteral("html")) val = ClauseType::Html;
    else {
        Q_ASSERT_X(0, "ClauseType::from_qstr", "Invalid type");
        val = ClauseType::Invalid;
        return false;
    }
    return true;
}
inline bool from_sqstr(QString const& str, ClauseType& val) noexcept {
    return from_qstr(str, val);
}
inline bool from_value(quint8 id, ClauseType& val) noexcept {
    if (id == 1)      val = ClauseType::Text;
    else if (id == 2) val = ClauseType::Html;
    else {
        Q_ASSERT_X(0, "ClauseType::from_value", "Invalid value");
        val = ClauseType::Invalid;
        return false;
    }
    return true;
}
inline QDebug operator<<(QDebug dbg, ClauseType type) noexcept {
    return dbg.nospace() << ps2::to_qstr(type);
}
inline std::ostream& operator<<(std::ostream& out, ClauseType type) noexcept {
    return ps2::cout(out, type);
}

} // end namespace eg

Q_DECLARE_METATYPE(ps2::ClauseType)
