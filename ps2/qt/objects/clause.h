/**
 * \file      ps2/ps2/qt/objects/clause.h
 * \brief     The Clause class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 27(th), 2017, 23:50 MSK
 * \updated   February  (the) 27(th), 2017, 23:50 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/out.h>
#include "clause_type.h"

/** \namespace ps2 */
namespace ps2 {

class Clause {
public:
    using class_name = Clause;
    using type_t     = ClauseType;

private:
    type_t m_type = type_t::Invalid;
    QString m_title;
    QString m_text;

public:
    Clause() Q_DECL_EQ_DEFAULT;
    Clause(type_t type, QString const& title, QString const& text)
        : m_type(type)
        , m_title(title)
        , m_text(text)
    {}
    Clause(type_t type, QString&& title, QString&& text)
        : m_type(type)
        , m_title(qMove(title))
        , m_text(qMove(text))
    {}
    static class_name null() noexcept {
        return class_name{};
    }
    bool isNull() const noexcept {
        return !isValid();
    }
    bool isValid() const noexcept {
        return m_type != type_t::Invalid
            && !m_title.isEmpty()
            && !m_text.isEmpty()
        ;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_type  == rhs.m_type
            && lhs.m_title == rhs.m_title
            && lhs.m_text  == rhs.m_text
        ;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    type_t type() const noexcept {
        return m_type;
    }
    void setType(type_t type) noexcept {
        m_type = type;
    }
    QString const& title() const noexcept {
        return m_title;
    }
    void setTitle(QString const& title) noexcept {
        m_title = title;
    }
    void setTitle(QString&& title) noexcept {
        m_title = qMove(title);
    }
    QString const& text() const noexcept {
        return m_text;
    }
    void setText(QString const& text) noexcept {
        m_text = text;
    }
    void setText(QString&& text) noexcept {
        m_text = qMove(text);
    }
    void swap(class_name& rhs) noexcept {
        qSwap(m_type, rhs.m_type);
        m_title.swap(rhs.m_title);
        m_text.swap(rhs.m_text);
    }
};

static inline void swap(ps2::Clause& lhs, ps2::Clause& rhs) noexcept {
    lhs.swap(rhs);
}

static inline QDebug operator<<(QDebug dbg, ps2::Clause const& clause) {
    return dbg.nospace()
        << "\"Clause\": {"
        << "\"type\": " << clause.type()
        << ", \"text\": " << clause.text()
        << ", \"title\": " << clause.title()
        << "}";
}

static inline std::ostream& operator<<(std::ostream& out, ps2::Clause const& clause) noexcept {
    return ps2::cout(out, clause);
}

} // end namespace ps2

Q_DECLARE_METATYPE(ps2::Clause)
