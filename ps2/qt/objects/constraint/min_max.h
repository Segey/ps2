/**
 * \file      ps2/ps2/qt/objects/constraint/min_max.h
 * \brief     The Min_max class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 18:31 MSK
 * \updated   March     (the) 30(th), 2017, 18:31 MSK
 * \TODO      
**/
#pragma once
#include "base.h"
#include <numeric>

/** \namespace ps2::constraint */
namespace ps2 {
namespace constraint {

template<class T>
class MinMax : public Base {
public:
    using class_name = MinMax<T>;
    using inherited  = Base;
     
private: 
    T m_bottom = std::numeric_limits<T>::min();
    T m_top    = std::numeric_limits<T>::max();

public:
    Q_DECL_CONSTEXPR explicit MinMax() Q_DECL_EQ_DEFAULT;
    Q_DECL_CONSTEXPR explicit MinMax(T bottom, T top) noexcept
        : m_bottom(bottom)
        , m_top(top) {
    }
    T top() const noexcept {
        return m_top;
    }
    void setTop(T top) noexcept {
        m_top = top;
    }
    T bottom() const noexcept {
        return m_bottom;
    }
    void setBottom(T bottom) noexcept {
        m_bottom = bottom;
    }
    virtual void swap(class_name& rhs) noexcept {
        qSwap(m_bottom, rhs.m_bottom);
        qSwap(m_top, rhs.m_top);
        inherited::swap(rhs);
    }
};

}} // end namespace ps2::constraint

template<class T>
static inline void swap(ps2::constraint::MinMax<T>& lhs, ps2::constraint::MinMax<T>& rhs) noexcept {
    lhs.swap(rhs);
}

template<class T>
inline QDebug operator<<(QDebug dbg, ps2::constraint::MinMax<T> const& kggr) {
    return dbg.nospace()
        << "\"ps2::constraint::MinMax\": {"
        << "\"bottom\":" << kggr.bottom()
        << ", \"top\":" << kggr.top()
        << "," << static_cast<ps2::constraint::Base>(kggr)
        << "}";
}

template<class T>
inline std::ostream& operator<<(std::ostream& out, ps2::constraint::MinMax<T> const& e) noexcept {
    return ps2::cout(out, e);
}

