/**
 * \file      ps2/ps2/qt/objects/constraint/double.h
 * \brief     The Double class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 23(th), 2017, 16:52 MSK
 * \updated   March     (the) 23(th), 2017, 16:52 MSK
 * \TODO      
**/
#pragma once
#include "min_max.h"

/** \namespace ps2::constraint */
namespace ps2 {
namespace constraint {

class Double: public MinMax<double> {
public:
    using class_name = Double;
    using inherited  = MinMax<double>;
     
private: 
    int m_decimals = 3;

public:
    explicit Double() Q_DECL_EQ_DEFAULT;
    explicit Double(double bottom, double top, int decimals = 3) noexcept
        : inherited(bottom, top)
        , m_decimals(decimals) {
    }
    int decimals() const noexcept {
        return m_decimals;
    }
    void setDecimals(int decimals) noexcept {
        m_decimals = decimals;
    }
    void swap(class_name& rhs) noexcept {
        qSwap(m_decimals, rhs.m_decimals);
        inherited::swap(rhs);
    }
};

}} // end namespace ps2::constraint

static inline void swap(ps2::constraint::Double& lhs, ps2::constraint::Double& rhs) noexcept {
    lhs.swap(rhs);
}
inline QDebug operator<<(QDebug dbg, ps2::constraint::Double const& dbl) {
    return dbg.nospace()
        << "\"ps2::constraint::Double\": {"
        << "\"decimals\":" << dbl.decimals()
        << ","  << static_cast<ps2::constraint::MinMax<double>>(dbl)
        << "}";
}
inline std::ostream& operator<<(std::ostream& out, ps2::constraint::Double const& e) noexcept {
    return ps2::cout(out, e);
}

Q_DECLARE_METATYPE(ps2::constraint::Double);
