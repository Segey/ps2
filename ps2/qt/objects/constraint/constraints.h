/**
 * \file      ps2/ps2/qt/objects/constraint/constraints.h
 * \brief     The Constraints class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 18:45 MSK
 * \updated   March     (the) 30(th), 2017, 18:45 MSK
 * \TODO      
**/
#pragma once
#include "min_max.h"
#include "double.h"

/** \namespace ps2::constraint */
namespace ps2 {
namespace constraint {

using KgGrDouble = MinMax<double>;
using LbOzDouble = MinMax<double>;
using KgInt      = MinMax<int>;
using GrInt      = MinMax<int>;

}} // end namespace ps2::constraint
