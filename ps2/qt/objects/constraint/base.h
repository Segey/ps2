/**
 * \file      ps2/ps2/qt/objects/constraint/base.h
 * \brief     The Base class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 29(th), 2017, 15:07 MSK
 * \updated   March     (the) 29(th), 2017, 15:07 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/cpp/typedefs.h>

/** \namespace ps2::constraint */
namespace ps2 {
namespace constraint {

class Base {
public:
    using class_name = Base;

public:
    explicit Base() Q_DECL_EQ_DEFAULT;
    virtual ~Base() Q_DECL_EQ_DEFAULT;
    virtual void swap(class_name&) noexcept { }
};

}} // end namespace ps2::constraint

static inline void swap(ps2::constraint::Base& lhs, ps2::constraint::Base& rhs) noexcept {
    lhs.swap(rhs);
}
inline QDebug operator<<(QDebug dbg, ps2::constraint::Base const&) noexcept {
    return dbg.nospace() << "\"ps2::constraint::Base\": {}";
}
inline std::ostream& operator<<(std::ostream& out, ps2::constraint::Base const& e) noexcept {
    return ps2::cout(out, e);
}

Q_DECLARE_METATYPE(ps2::constraint::Base);
