/**
 * \file      ps2/ps2/qt/objects/rate.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April (the) 12(th), 2016, 18:28 MSK
 * \updated   April (the) 12(th), 2016, 18:28 MSK
 * \TODO      
**/
#pragma once
#include <QtNumeric>

/** \namespace ps2 */
namespace ps2 {

template<uint T1, uint T2>
class Rate {
public:
    using class_name = Rate<T1, T2>;
         
private: 
    uint m_value = UINT_MAX;

public:
    Q_DECL_CONSTEXPR explicit Rate(uint value = UINT_MAX) noexcept
        : m_value(value){
    }
    ~Rate() = default;
    Q_DECL_CONSTEXPR friend bool operator== (class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_value == rhs.m_value;
    }
    Q_DECL_CONSTEXPR friend bool operator != (class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_value != rhs.m_value;
    }
    Q_DECL_CONSTEXPR friend bool operator == (class_name const& lhs, uint rhs) noexcept {
        return lhs.m_value == rhs;
    }
    Q_DECL_CONSTEXPR friend bool operator != (class_name const& lhs, uint rhs) noexcept {
        return lhs.m_value != rhs;
    }
    Q_DECL_CONSTEXPR friend bool operator == (uint lhs, class_name const& rhs) noexcept {
        return lhs == rhs.m_value;
    }
    Q_DECL_CONSTEXPR friend bool operator != (uint lhs, class_name const& rhs) noexcept {
        return lhs != rhs.m_value;
    }
    Q_DECL_CONSTEXPR bool isNull() const noexcept {
        return m_value == UINT_MAX;
    }
    Q_DECL_CONSTEXPR bool isValid() const noexcept {
        return T1 <= m_value && m_value >= T2;
    }
    Q_DECL_CONSTEXPR uint value() const noexcept {
        return m_value;
    }
    Q_DECL_CONSTEXPR void setValue(uint value) noexcept {
        m_value = value;
    }
    Q_DECL_CONSTEXPR uint toUint() const noexcept {
        return m_value;
    }
    void swap(class_name& rhs) noexcept {
        std::swap(m_value, rhs.m_value);
    }
};

template<uint T1, uint T2>
static inline void swap(Rate<T1, T2>& lhs, Rate<T1, T2>& rhs) noexcept {
    lhs.swap(rhs);
}

using Rate10 = Rate<0,10>;
using Rate5 = Rate<0,5>;

} // end namespace ps2

#ifdef QT
#include <QDebug>

Q_DECLARE_METATYPE(ps2::Rate5)
Q_DECLARE_METATYPE(ps2::Rate10)

Q_DECLARE_TYPEINFO(ps2::Rate5, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(ps2::Rate10, Q_MOVABLE_TYPE);

template<uint T1, uint T2>
inline QDebug operator<<(QDebug dbg, ps2::Rate<T1, T2> const& item) {
    return dbg.nospace()
        << "Rate< " << T1 << ", " << T2 << "> {"
        << "value:" << item.value()
        << "}";
}
#endif
