/**
 * \file      ps2/ps2/qt/objects/registry/registry.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 03(th), 2016, 06:55 MSK
 * \updated   January (the) 03(th), 2016, 06:55 MSK
 * \TODO
**/
#pragma once
#include <QVector>
#include <QSettings>
#include "registry_item.h"

/** \namespace ps2 */
namespace ps2 {
    
class Registry: public QSettings {
public:
    typedef Registry        class_name;
    typedef QSettings       inherited;
    typedef RegistryItem    item_t;
    typedef QVector<item_t> items_t;

public:
    explicit Registry() = default;
    explicit Registry(QString const& hkey)
        : inherited(hkey, QSettings::NativeFormat) {
    }
    items_t readBranch(QString const& name) {
        inherited::beginGroup(name);

        items_t items;
        for(QString const& item: inherited::allKeys())
            items.push_back(item_t(item, inherited::value(item)));

        inherited::endGroup();
        return items;
    }
    void writeBranch(QString const& name, items_t const& items) {
        inherited::beginGroup(name);

        for(item_t const& item: items)
            inherited::setValue(item.key(), item.value());

        inherited::endGroup();
    }
    /**
     *  \code
     *   Registry::moveBranch(hkeyFrom(), hkeyTo(), QStringLiteral("from"));
     *   Registry::moveBranch(QStringLiteral("HKEY_CURRENT_USER\\SOFTWARE\\IRoNDooM"),
     *                        QStringLiteral("HKEY_CURRENT_USER\\SOFTWARE\\FiveSoft\\IRoNDooM2"),
     *                        QStringLiteral("from"));
     *  \endcode
    **/
    static void moveBranch(QString const& from, QString const& to, QString const& name) {
        class_name cls_from(from);
        auto const& items = cls_from.readBranch(name);

        class_name cls_to(to);
        cls_to.writeBranch(name, items);
    }
    /**
     *  \code
     *      auto const is = Registry::branchExists(hkeySoftware(),  QStringLiteral("IRoNDooM"));
     *  \endcode
    **/
    static bool branchExists(QString const& key, QString const& name) {
        class_name reg(key);
        return reg.childGroups().contains(name);
    } 
};

} // end namespace ps2
