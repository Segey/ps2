/**
 * \file      ps2/ps2/qt/objects/registry/registry_item.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 03(th), 2016, 05:40 MSK
 * \updated   January (the) 03(th), 2016, 05:40 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QVariant>

/** \namespace ps2 */
namespace ps2 {

class RegistryItem {
public:
    typedef RegistryItem class_name;
     
private: 
    QString  m_key;
    QVariant m_value;

public:
    RegistryItem() = default;
    explicit RegistryItem(QString const& key, QVariant const& value) 
        : m_key(key), m_value(value){
    }
    QString const& key() const {
        return m_key;
    }
    void setKey(QString const& key) {
        m_key = key;
    }
    QVariant const& value() const {
        return m_value;
    }
    void setItem(QVariant const& value) {
        m_value = value;
    }
};

} // end namespace ps2

