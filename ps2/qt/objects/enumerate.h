/**
 * \file      ps2/ps2/qt/objects/enumerate.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 13(th), 2015, 03:12 MSK
 * \updated   September (the) 13(th), 2015, 03:12 MSK
 * \TODO      
**/
#pragma once

/** \namespace ps2 */
namespace ps2 {
    
/**
 * \code
        std::vector<double> v = { 1., 2., 3., 4., 5. };
        for (auto&& a: enumerate(v)) {
            auto index = a.first;
            auto val   = a.second;
        }
 * \endcode
**/
template<class T>
class enumerate_object {
public:
    using type_t     = T;
    using class_name = enumerate_object<type_t>;
    using size_t     = std::size_t;

private:
    const T&                         m_iter;
    size_t                           m_size;
    decltype(std::begin(m_iter))     m_begin;
    const decltype(std::end(m_iter)) m_end;

public:
    enumerate_object(T const& iter)
        : m_iter(iter)
        , m_size(0)
        , m_begin(std::begin(iter))
        , m_end(std::end(iter))
    {}
    enumerate_object const& begin() const {
        return *this; 
    }
    enumerate_object const& end() const {
        return *this; 
    }
    bool operator!=(enumerate_object const&) const {
        return m_begin != m_end;
    }
    void operator++() {
        ++m_begin;
        ++m_size;
    }
    auto operator*() const -> std::pair<size_t, decltype(*m_begin)> {
        return { m_size, *m_begin };
    }
};
template<class Iterable>
inline enumerate_object<Iterable> enumerate(Iterable const& iter)
{
    return { iter };
}

} // end namespace ps2
