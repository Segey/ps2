/**
 * \file      ps2/ps2/qt/objects/email_validator.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   Сентябрь (the) 09(th), 2015, 03:02 MSK
 * \updated   Сентябрь (the) 09(th), 2015, 03:02 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>
#include <QValidator>
#include <QString>

/** \namespace ps2 */
namespace ps2 {

class EmailValidator : public QValidator {
    Q_OBJECT

private:
    const QRegExp valid;
    const QRegExp m_intermediate;

public:
    using class_name = EmailValidator;
    using inherited  = QValidator;

public:
    explicit EmailValidator(QObject* parent = nullptr)
        : QValidator(parent)
        , m_valid("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")
        , m_intermediate("[a-z0-9._%+-]*@?[a-z0-9.-]*\\.?[a-z]*") {
    }
    State validate(QString &text, int& pos) const Q_DECL_OVERRIDE {
        Q_UNUSED(pos)

        fixup(text);

        if (valid.exactMatch(text))
            return Acceptable;
        if (m_intermediate.exactMatch(text))
            return Intermediate;

        return Invalid; 
    }
    void fixup(QString &text) const Q_DECL_OVERRIDE {
        text = text.trimmed().toLower(); 
    }
}; 
} // end namespace ps2
