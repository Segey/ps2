/**
 * \file      ps2/ps2/qt/objects/link.h
 * \brief     The Link class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 17(th), 2017, 13:25 MSK
 * \updated   January (the) 17(th), 2017, 13:25 MSK
 * \TODO      
**/
#pragma once
#include <QUrl>
#include <QString>
#include <ps2/qt/out.h>
#include <ps2/qt/regex/regex_extract.h>

/** \namespace ps2 */
namespace ps2 {
    
class Link {
public:
    using class_name = Link;

private: 
    QUrl    m_url;
    QString m_text;

protected:
    QString createLink() const noexcept {
        return QStringLiteral("<a href=\"%1\">%2</a>").arg(m_url.url(), m_text);
    }

public:
    explicit Link(QUrl const& url = QUrl(), QString const& text = QString()) noexcept 
        : m_url(url)
        , m_text(text) {
    }
    explicit Link(QString const& url, QString const& text) noexcept
        : m_url(QUrl{url})
        , m_text(text) {
    }
    explicit Link(QUrl&& url, QString&& text) noexcept
        : m_url(qMove(url))
        , m_text(qMove(text)) {
    }
    explicit Link(QString&& url, QString&& text) noexcept
        : m_url(QUrl(qMove(url)))
        , m_text(qMove(text)) {
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_url == rhs.m_url
            && lhs.m_text == rhs.m_text
        ;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    QUrl const& url() const noexcept {
        return m_url;
    }
    void setUrl(QUrl const& url) noexcept {
        m_url = url;
    }
    void setUrl(QUrl&& url) noexcept {
        m_url = std::move(url);
    }
    void setUrl(QString const& url) noexcept {
        m_url = QUrl(url);
    }
    QString const& text() const noexcept {
        return m_text;
    }
    void setText(QString const& text) noexcept {
        m_text = text;
    }
    void setText(QString&& text) noexcept {
        m_text = qMove(text);
    }
    QString toHtml() const noexcept {
        return createLink();
    }
    static QString toHtml(QString const& url, QString const& text) noexcept {
        class_name cls(url, text);
        return cls.toHtml();
    }
    static QString toHtml(QString && url, QString&& text) noexcept {
        class_name cls(qMove(url), qMove(text));
        return cls.toHtml();
    }
    bool isValid() const noexcept {
        return !isEmpty();
    }
    bool isEmpty() const noexcept {
        return m_url.isEmpty() || m_text.isEmpty();
    }
    static class_name fromString(QString const& str) noexcept {
        auto&& pair = ps2::regexp::extract::tag::A(str);
        return class_name{pair.first, pair.second};
    }
    static class_name fromString(QString&& str) noexcept {
        auto&& pair = ps2::regexp::extract::tag::A(qMove(str));
        return class_name{pair.first, pair.second};
    }
    void swap(class_name& rhs) noexcept {
        m_url.swap(rhs.m_url);
        m_text.swap(rhs.m_text);
    }
};

static inline void swap(ps2::Link& lhs, ps2::Link& rhs) noexcept {
    lhs.swap(rhs);
}
static inline QDebug operator<<(QDebug dbg, ps2::Link const& link) {
    return dbg.nospace()
        << "\"ps2::Link\": {"
        << "\"url\":" << link.url().toString()
        << ", \"text\": " << link.text()
        << "}";
}
static inline std::ostream& operator<<(std::ostream& out, ps2::Link const& link) noexcept {
    return ps2::cout(out, link);
}

} // end namespace ps2

Q_DECLARE_METATYPE(ps2::Link)
