/**
 * \file      ps2/ps2/qt/objects/date_range.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   Февраль (the) 29(th), 2016, 22:21 MSK
 * \updated   Февраль (the) 29(th), 2016, 22:21 MSK
 * \TODO
**/
#pragma once
#include <QPair>
#include <QDate>
#include <QVariant>
#include <QByteArray>
#include <QDataStream>

namespace ps2 {

class DateRange final {
public:
    using class_name = DateRange;

private:
    QDate m_start;
    QDate m_end;

public:
    DateRange() Q_DECL_EQ_DEFAULT;
    DateRange(QDate const& start, QDate const& end) noexcept
        : m_start(start)
        , m_end(end) {
    }
    DateRange(QDate&& start, QDate&& end) noexcept
        : m_start(qMove(start))
        , m_end(qMove(end)) {
    }
    QDate const& start() const noexcept {
        return m_start;
    }
    void setStart(QDate const& start) noexcept {
        m_start = start;
    }
    void setStart(QDate&& start) noexcept {
        m_start = qMove(start);
    }
    QDate const& end() const noexcept {
        return m_end;
    }
    void setEnd(QDate const& end) noexcept {
        m_end = end;
    }
    void setEnd(QDate&& end) noexcept {
        m_end = qMove(end);
    }
    bool isValid() const noexcept {
        return m_start.isValid() && m_end.isValid();
    }
    QVariant toVariant() const noexcept {
        QByteArray result;
        QDataStream out(&result, QIODevice::WriteOnly);
        out  << m_start << m_end;
        return QVariant(result);
    }
    void fromByteArray(QByteArray& arr) noexcept {
        if(arr.isEmpty())
            return;

        QDataStream in(&arr, QIODevice::ReadOnly);
        in >> m_start;
        in >> m_end;
    }
    static inline class_name today(QDate const& date = QDate::currentDate()) {
        return class_name(date, date);
    }
    static inline class_name yesterday(QDate const& date = QDate::currentDate()) {
        return class_name(date.addDays(-1), date);
    }
    static inline class_name currentWeek(QDate const& date = QDate::currentDate()) {
        return class_name(date.addDays( - date.dayOfWeek() + 1), date);
    }
    static inline class_name currentMonth(QDate const& date = QDate::currentDate()) {
        return class_name({date.year(), date.month(), 1}, date);
    }
    static inline class_name priorWeek(QDate const& date = QDate::currentDate()) {
        return class_name(date.addDays( - date.dayOfWeek() - 6), date);
    }
    static inline class_name priorMonth(QDate const& date = QDate::currentDate()) {
        auto&& day = date.addMonths( - 1).addDays( - date.day() + 1);
        return class_name(qMove(day), date);
    }
};

}
