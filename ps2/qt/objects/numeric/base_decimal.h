/**
 * \file      ps2/ps2/qt/objects/numeric/base_decimal.h
 * \brief     The Base_decimal class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 00:56 MSK
 * \updated   March     (the) 30(th), 2017, 00:56 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include <QString>
#include <ps2/qt/out.h>
#include <ps2/qt/algorithm.h>
#include <ps2/qt/convert/integer.h>

/** \namespace ps2 */
namespace ps2 {

template<class T, int Precision>
class BaseDecimal {
public:
    using class_name = BaseDecimal<T, Precision>;
    using integral_t = T;
    using fraction_t = uint;

private: 
    T m_integral = std::numeric_limits<T>::max();
    uint m_fraction = UINT_MAX;

protected:
    static inline decltype(auto) pow() noexcept {
        return std::pow(10, Precision);
    }

protected:
    virtual QString doToString(QLocale const& loc) const noexcept = 0;
    virtual double doToDouble() const noexcept = 0;
    virtual uint doMinFraction() const noexcept = 0;
    virtual uint doMaxFraction() const noexcept = 0;

public:
    explicit BaseDecimal() Q_DECL_EQ_DEFAULT;
    explicit BaseDecimal(T integral) noexcept
        : m_integral(integral)
        , m_fraction(0) {
    }
    explicit BaseDecimal(T integral, uint fraction) noexcept
        : m_integral(integral)
        , m_fraction(fraction) {
    }
    virtual ~BaseDecimal() Q_DECL_EQ_DEFAULT;
    static inline int precision() noexcept {
        return Precision;
    }
    bool isValid() const noexcept {
        return m_integral != std::numeric_limits<T>::max()
            && (minFraction() <= m_fraction && m_fraction < maxFraction())
        ;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_integral == rhs.m_integral
            && lhs.m_fraction == rhs.m_fraction
        ;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    friend bool operator > (class_name const& lhs, class_name const& rhs) noexcept {
        if( lhs.integral() > rhs.integral()) return true;
        return lhs.integral() == rhs.integral()
                ? lhs.fraction() > rhs.fraction() : false;
    }
    T integral() const noexcept {
        return m_integral;
    }
    void setIntegral(T integral) noexcept {
        m_integral = integral;
    }
    uint fraction() const noexcept {
        return m_fraction;
    }
    void setFraction(uint fraction) noexcept {
        m_fraction = fraction;
    }
    uint minFraction() const noexcept {
        return doMinFraction();
    }
    uint maxFraction() const noexcept {
        return doMaxFraction();
    }
    QString toString(QLocale const& loc = {}) const noexcept {
        return doToString(loc);
    }
    double toDouble() const noexcept {
        return doToDouble();
    }
    virtual void swap(class_name& rhs) noexcept {
        qSwap(m_integral, rhs.m_integral);
        qSwap(m_fraction, rhs.m_fraction);
    }
};

template<class T, int Precision>
static inline void swap(ps2::BaseDecimal<T,Precision>& lhs
                        , ps2::BaseDecimal<T,Precision>& rhs) noexcept {
    lhs.swap(rhs);
}
template<class T, int Precision>
static inline QDebug operator<<(QDebug dbg, ps2::BaseDecimal<T,Precision> const& decimal) {
    return dbg.nospace()
        << "\"BaseDecimal\": {"
        << "\"integral\": " << decimal.integral()
        << ", \"fraction\": " << decimal.fraction()
        << "}";
}
template<class T, int Precision>
static inline std::ostream& operator<<(std::ostream& out
                           , ps2::BaseDecimal<T,Precision> const& decimal) noexcept {
    return ps2::cout(out, decimal);
}

} // end namespace ps2
