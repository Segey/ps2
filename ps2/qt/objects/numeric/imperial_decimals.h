/**
 * \file      ps2/ps2/qt/objects/numeric/imperial_decimals.h
 * \brief     The Imperial_decimals class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 18:09 MSK
 * \updated   April     (the) 02(th), 2017, 18:09 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include <ps2/cpp/convertor/length.h>
#include <ps2/cpp/convertor/weight.h>
#include "base_decimal.h"
#include "imperial_decimal.h"
#include "mile_imperial_decimal.h"

/** \namespace ps2 */
namespace ps2 {

using StLbDecimal  = ImperialDecimal<ps2::WeightConvertor::st_lb>;
using LbOzDecimal  = ImperialDecimal<ps2::WeightConvertor::lb_oz>;
using MiYdDecimal  = MileImperialDecimal;
using YdFtDecimal  = ImperialDecimal<ps2::LengthConvertor::yd_ft>;
using FtInDecimal  = ImperialDecimal<ps2::LengthConvertor::ft_in>;
using InSubDecimal = ImperialDecimal<ps2::LengthConvertor::in_subin>;

} // end namespace ps2
