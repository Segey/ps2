/**
 * \file      ps2/ps2/qt/objects/numeric/metric_decimals.h
 * \brief     The Kggr_decimal class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 01:40 MSK
 * \updated   March     (the) 30(th), 2017, 01:40 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include "ps2/cpp/wide_int.h"
#include "ps2/cpp/decimal.h"
#include "decimal.h"

/** \namespace ps2 */
namespace ps2 {

using TKgDecimal   = ps2::decimal<long long int,3>;
using KgGrDecimal  = ps2::decimal<long long int,3>;
using KmMDecimal   = ps2::decimal<long long int,3>;
using MCmDecimal   = ps2::decimal<long long int,2>;
using CmMmDecimal  = ps2::decimal<long long int,1>;
using NanoDecimal  = ps2::decimal<long long int,3>;
using MicroDecimal = ps2::decimal<long long int,6>;
using MilliDecimal = ps2::decimal<long long int,3>;

} // end namespace ps2
