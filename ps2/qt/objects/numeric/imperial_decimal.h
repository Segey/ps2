/**
 * \file      ps2/ps2/qt/objects/numeric/imperial_decimal.h
 * \brief     The Imperial_decimal class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 17:46 MSK
 * \updated   April     (the) 02(th), 2017, 17:46 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include "base_decimal.h"

/** \namespace ps2 */
namespace ps2 {

template<int VALUE>
class ImperialDecimal final : public BaseDecimal<int, 2> {
public:
    using class_name = ImperialDecimal<VALUE>;
    using inherited  = BaseDecimal<int, 2>;

private:
    static inline uint fractionValue(QStringRef const& str, QLocale const& loc = {}) noexcept {
        auto const val = toUint(str, UINT_MAX, loc);
        if(val >= VALUE || str.size() > 2) return UINT_MAX;
        return val == 1 && str.size() == 1 ? 10 : val;
    }
    virtual double doToDouble() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        auto const part = static_cast<double>(inherited::fraction()) / 100;
        return static_cast<double>(inherited::integral()) + part;
    }
    virtual QString doToString(QLocale const& loc = {}) const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        QString str = loc.toString(inherited::integral()) + loc.decimalPoint();
        if(inherited::fraction() < 10) str += QChar::fromLatin1('0');
        return str + QString::number(inherited::fraction());
    }
    virtual uint doMinFraction() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        return 0u;
    }
    virtual uint doMaxFraction() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        return VALUE;
    }

public:
    using BaseDecimal<int, 2>::BaseDecimal;
    static inline class_name fromString(QString const& str, QLocale const& loc = {}) noexcept {
        if(!ps2::is_double(str, loc))
            return class_name{};

        auto const inx = str.indexOf(loc.decimalPoint());
        auto const val = toLong(QStringRef(&str,0,(inx == -1 ? str.size() : inx)), UINT_MAX, loc);
        if(inx == -1)
            return class_name{static_cast<int>(val), 0};

        auto const frc = fractionValue(QStringRef(&str,inx + 1,str.size() - inx - 1), loc);
        return class_name
            { static_cast<int>((inx == 0 ? 0 : val)), frc};
    }
    static class_name zero() noexcept {
        return class_name{0, 0u};
    }
    static inline class_name null() noexcept {
        return class_name{INT_MAX, UINT_MAX};
    }
};

template<int VALUE>
static inline void swap(ps2::ImperialDecimal<VALUE>& lhs
                        , ps2::ImperialDecimal<VALUE>& rhs) noexcept {
    lhs.swap(rhs);
}
template<int VALUE>
static inline QDebug operator<<(QDebug dbg, ps2::ImperialDecimal<VALUE> const& decimal) noexcept {
    return dbg.nospace()
        << "\"ImperialDecimal<" << VALUE << ">\": {"
        << "\"integral\": " << decimal.integral()
        << ", \"fraction\": " << decimal.fraction()
        << "}";
}
template<int VALUE>
static inline std::ostream& operator<<(std::ostream& out
                           , ps2::ImperialDecimal<VALUE> const& decimal) noexcept {
    return ps2::cout(out, decimal);
}

template<int VALUE>
static bool from_qstr(QString s, ps2::ImperialDecimal<VALUE>& val
                      , QLocale const& loc = {}) noexcept {
    val = ImperialDecimal<VALUE>::fromString(s, loc);
    return val.isValid();
}

template<int VALUE>
static QString to_qstr(ps2::ImperialDecimal<VALUE> const& n, QLocale const& loc = {}) {
    QString str = loc.toString(n.integral()) + loc.decimalPoint();
    if(n.fraction() < 10) str += QChar::fromLatin1('0');
    return str + QString::number(n.fraction());
}


} // end namespace ps2
