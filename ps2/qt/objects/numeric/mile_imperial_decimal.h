/**
 * \file      ps2/ps2/qt/objects/numeric/mile_imperial_decimal.h
 * \brief     The Mile_imperial_decimal class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May       (the) 03(th), 2017, 16:31 MSK
 * \updated   May       (the) 03(th), 2017, 16:31 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include "base_decimal.h"

/** \namespace ps2 */
namespace ps2 {

class MileImperialDecimal final : public BaseDecimal<int, 4> {
public:
    using class_name = MileImperialDecimal;
    using inherited  = BaseDecimal<int, 4>;

private:
    static inline uint fractionValue(QStringRef const& str, QLocale const& loc = {}) noexcept {
        auto const val = toUint(str, UINT_MAX, loc);
        if(val >= ps2::LengthConvertor::mi_yd || str.size() > 4) return UINT_MAX;
        auto const v = val * std::pow(10, 4 - str.size());
        return v < ps2::LengthConvertor::mi_yd ? v : UINT_MAX;
    }
    virtual double doToDouble() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        auto const part = static_cast<double>(inherited::fraction()) / 100;
        return static_cast<double>(inherited::integral()) + part;
    }
    virtual QString doToString(QLocale const& loc = {}) const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        QString str = loc.toString(inherited::integral()) + loc.decimalPoint();
        if(inherited::fraction() < 10) str += QChar::fromLatin1('0');
        if(inherited::fraction() < 100) str += QChar::fromLatin1('0');
        if(inherited::fraction() < 1000) str += QChar::fromLatin1('0');
        return str + QString::number(inherited::fraction());
    }
    virtual uint doMinFraction() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        return 0u;
    }
    virtual uint doMaxFraction() const noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {
        return ps2::LengthConvertor::mi_yd;
    }

public:
    using BaseDecimal<int, 4>::BaseDecimal;
    static inline class_name fromString(QString const& str, QLocale const& loc = {}) noexcept {
        if(!ps2::is_double(str, loc))
            return class_name{};

        auto const inx = str.indexOf(loc.decimalPoint());
        auto const val = toLong(QStringRef(&str,0,(inx == -1 ? str.size() : inx)), UINT_MAX, loc);
        if(inx == -1)
            return class_name{static_cast<int>(val), 0};

        auto const frc = fractionValue(QStringRef(&str,inx + 1,str.size() - inx - 1), loc);
        return class_name
            { static_cast<int>((inx == 0 ? 0 : val)), frc};
    }
    static class_name zero() noexcept {
        return class_name{0, 0u};
    }
    static inline class_name null() noexcept {
        return class_name{INT_MAX, UINT_MAX};
    }
};

static inline void swap(ps2::MileImperialDecimal& lhs
                        , ps2::MileImperialDecimal& rhs) noexcept {
    lhs.swap(rhs);
}
static inline QString to_qstr(ps2::MileImperialDecimal const& n
                       , QLocale const& loc = {}) noexcept {
        QString str = loc.toString(n.integral()) + loc.decimalPoint();
        if(n.fraction() < 10) str += QChar::fromLatin1('0');
        if(n.fraction() < 100) str += QChar::fromLatin1('0');
        if(n.fraction() < 1000) str += QChar::fromLatin1('0');
        return str + QString::number(n.fraction());
}
static inline bool from_qstr(QString const& s, ps2::MileImperialDecimal& val, QLocale const& loc = {}) {
    val = ps2::MileImperialDecimal::fromString(s, loc);
    return true;
}
static inline QDebug operator<<(QDebug dbg, ps2::MileImperialDecimal const& decimal) noexcept {
    return dbg.nospace()
        << "\"MileImperialDecimal\": {"
        << "\"integral\": " << decimal.integral()
        << ", \"fraction\": " << decimal.fraction()
        << "}";
}
static inline std::ostream& operator<<(std::ostream& out
                           , ps2::MileImperialDecimal const& decimal) noexcept {
    return ps2::cout(out, decimal);
}

} // end namespace ps2
