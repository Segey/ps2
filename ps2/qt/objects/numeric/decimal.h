/**
 * \file      ps2/ps2/qt/objects/numeric/decimal.h
 * \brief     The Decimal class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 25(th), 2017, 01:49 MSK
 * \updated   March     (the) 25(th), 2017, 01:49 MSK
 * \TODO      
**/
#pragma once
#include <cmath>
#include <QString>
#include <ps2/qt/out.h>
#include <ps2/qt/algorithm.h>
#include <ps2/qt/convert/string.h>
#include <ps2/qt/convert/integer.h>

/** \namespace ps2 */
namespace ps2 {

template<class T, int Precision>
class Decimal {
public:
    using class_name = Decimal<T, Precision>;
    using integral_t = T;
    using fraction_t = quint32;
    using int64_t    = qint64;

private:
    T m_integral    = std::numeric_limits<T>::max();
    uint m_fraction = UINT_MAX;

private:
    static inline auto normalize(quint32 val) noexcept {
        return val * ipow();
    }
    static inline decltype(auto) pow() noexcept {
        return std::pow(10, Precision);
    }
    static inline int64_t ipow() noexcept {
        return static_cast<int64_t>(std::pow(10, Precision));
    }
    Q_DECL_CONSTEXPR static inline class_name minus(class_name lhs
                                                , class_name const& rhs) noexcept {
        lhs.m_integral -= rhs.m_integral;
        if(lhs.m_fraction < rhs.m_fraction) {
            --lhs.m_integral;
            lhs.m_fraction += ipow();
        }
        lhs.m_fraction -= rhs.m_fraction;
        return lhs;
    }

private:
    static inline uint fractionValue(QStringRef const& str, QLocale const& loc = {}) noexcept {
        auto const val = ps2::toUint(str, UINT_MAX, loc);
        if(val == UINT_MAX || str.size() > Precision) return UINT_MAX;
        return val * std::pow(10, Precision - str.size());
    }

public:
    Q_DECL_CONSTEXPR explicit Decimal() Q_DECL_EQ_DEFAULT;
    Q_DECL_CONSTEXPR explicit Decimal(T integral) noexcept
        : m_integral(integral)
        , m_fraction(0) {
    }
    Q_DECL_CONSTEXPR explicit Decimal(T integral, uint fraction) noexcept
        : m_integral(integral)
        , m_fraction(fraction) {
    }
    Q_DECL_CONSTEXPR static inline int precision() noexcept {
        return Precision;
    }
    Q_DECL_CONSTEXPR bool isValid() const noexcept {
        return m_integral != std::numeric_limits<T>::max()
            && (minFraction() <= m_fraction && m_fraction < maxFraction())
        ;
    }
    Q_DECL_CONSTEXPR friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_integral == rhs.m_integral
            && lhs.m_fraction == rhs.m_fraction
        ;
    }
    Q_DECL_CONSTEXPR friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    Q_DECL_CONSTEXPR friend bool operator > (class_name const& lhs, class_name const& rhs) noexcept {
        if( lhs.m_integral > rhs.m_integral) return true;
        return lhs.m_integral == rhs.m_integral
                ? lhs.m_fraction > rhs.m_fraction : false;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator>(class_name const& lhs, U val) noexcept {
        return lhs.m_integral > val;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator>(U val, class_name const& lhs) noexcept {
        return val > lhs.m_integral;
    }
    Q_DECL_CONSTEXPR friend bool operator>(class_name const& lhs, double val) noexcept {
        return lhs.m_integral > fromDouble(val);
    }
    Q_DECL_CONSTEXPR friend bool operator>(double val, class_name const& lhs) noexcept {
        return fromDouble(val) > lhs.m_integral;
    }
    Q_DECL_CONSTEXPR friend bool operator >= (class_name const& lhs, class_name const& rhs) noexcept {
        if( lhs.m_integral > rhs.m_integral) return true;
        return lhs.m_integral == rhs.m_integral
             ? lhs.m_fraction >= rhs.m_fraction : false;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator>=(class_name const& lhs, U val) noexcept {
        return lhs.m_integral >= val;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator>=(U val, class_name const& lhs) noexcept {
        return val >= lhs.m_integral;
    }
    Q_DECL_CONSTEXPR friend bool operator<(class_name const& lhs
                                           , class_name const& rhs) noexcept {
        return !(lhs >= rhs);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator<(class_name const& lhs, U val) noexcept {
        return lhs.m_integral < val;
    }
    Q_DECL_CONSTEXPR friend bool operator<(class_name const& lhs, double val) noexcept {
        return lhs < fromDouble(val);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator<(double val, class_name const& lhs) noexcept {
        return fromDouble(val) < lhs;
    }
    Q_DECL_CONSTEXPR friend bool operator<=(class_name const& lhs
                                            , class_name const& rhs) noexcept {
        return !(lhs > rhs);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator<=(class_name const& lhs, U val) noexcept {
        return lhs.m_integral <= val;
    }
    Q_DECL_CONSTEXPR friend bool operator<=(class_name const& lhs, double val) noexcept {
        return lhs <= fromDouble(val);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend bool operator<=(U val, class_name const& lhs) noexcept {
        return val <= lhs.m_integral;
    }
    Q_DECL_CONSTEXPR friend bool operator<=(double val, class_name const& lhs) noexcept {
        return fromDouble(val) <= lhs;
    }
    Q_DECL_CONSTEXPR friend class_name operator+(class_name lhs
                                                 , class_name const& rhs) noexcept {
        if((lhs < zero() && rhs < zero()) || (lhs > zero() && rhs > zero())) {
            lhs.m_integral += rhs.m_integral;
            lhs.m_fraction += rhs.m_fraction;
            if(lhs > zero())
                lhs.m_integral += lhs.m_fraction / ipow();
            else
                lhs.m_integral -= lhs.m_fraction / ipow();
            lhs.m_fraction %= ipow();
            return lhs;
        }
        return lhs < zero() ? rhs - -lhs
                            : lhs - -rhs;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend class_name operator+(class_name lhs, U val) noexcept {
        lhs.m_integral += val;
        return lhs;
    }
    Q_DECL_CONSTEXPR friend class_name operator+(class_name const& lhs, double val) noexcept {
        return lhs + fromDouble(val);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend class_name operator+(U val, class_name rhs) noexcept {
        return rhs + val;
    }
    Q_DECL_CONSTEXPR friend class_name operator+(double val, class_name rhs) noexcept {
        return fromDouble(val) + rhs;
    }
    Q_DECL_CONSTEXPR friend class_name operator-(class_name cls) noexcept {
        cls.m_integral = -cls.m_integral;
        return cls;
    }
    Q_DECL_CONSTEXPR friend class_name operator-(class_name const& lhs, class_name const& rhs) noexcept {
        if(lhs < zero() && rhs > zero())
            return lhs + -rhs;

        if(rhs < zero())
            return lhs + class_name(std::abs(rhs.m_integral), rhs.m_fraction);

        return rhs >= lhs ? -minus(rhs, lhs)
                          :  minus(lhs, rhs);
    }
    template<class U>
    Q_DECL_CONSTEXPR friend class_name operator-(class_name lhs, U val) noexcept {
        lhs.m_integral -= val;
        return lhs;
    }
    Q_DECL_CONSTEXPR friend class_name operator-(class_name const& lhs, double val) noexcept {
        return lhs - fromDouble(val);
    }
    Q_DECL_CONSTEXPR friend class_name operator-(double val, class_name rhs) noexcept {
        return fromDouble(val) - rhs;
    }
    template<class U>
    Q_DECL_CONSTEXPR friend class_name operator-(U val, class_name const& rhs) noexcept {
        return class_name(val, 0) - rhs;
    }
    Q_DECL_CONSTEXPR friend class_name operator*(class_name lhs
                                     , class_name const& rhs) noexcept {
        auto const ix = ipow() * ipow();
        auto const integral = lhs.m_integral * rhs.m_integral;
        auto const fraction = static_cast<int64_t>(lhs.m_integral * rhs.m_fraction + rhs.m_integral * lhs.m_fraction) * ipow()
              + (lhs.m_fraction * rhs.m_fraction);
        lhs.m_integral = integral + fraction / ix;
        lhs.m_fraction = (fraction % ix) / ipow();
        return lhs;
    }
    Q_DECL_CONSTEXPR T integral() const noexcept {
        return m_integral;
    }
    Q_DECL_CONSTEXPR void setIntegral(T integral) noexcept {
        m_integral = integral;
    }
    Q_DECL_CONSTEXPR uint fraction() const noexcept {
        return m_fraction;
    }
    Q_DECL_CONSTEXPR void setFraction(uint fraction) noexcept {
        m_fraction = fraction;
    }
    static inline class_name fromString(QString const& str, QLocale const& loc = {}) noexcept {
        if(!ps2::is_double(str, loc))
            return class_name{};

        auto const inx = str.indexOf(loc.decimalPoint());
        auto const val = toLong(QStringRef(&str,0,(inx == -1 ? str.size() : inx)), UINT_MAX, loc);
        if(inx == -1)
            return class_name{static_cast<T>(val), 0};

        auto const frc = fractionValue(QStringRef(&str,inx + 1,str.size() - inx - 1), loc);
        return class_name
            { static_cast<T>((inx == 0 ? 0 : val)), frc};
    }
    Q_DECL_CONSTEXPR static inline class_name zero() noexcept {
        return class_name{T{}, 0u};
    }
    Q_DECL_CONSTEXPR bool isZero() const noexcept {
        return zero() == *this;
    }
    Q_DECL_CONSTEXPR static inline class_name null() noexcept {
        return class_name{std::numeric_limits<T>::max(), UINT_MAX};
    }
    Q_DECL_CONSTEXPR bool isNull() const noexcept {
        return null() == *this;
    }
    Q_DECL_CONSTEXPR uint minFraction() const noexcept {
        return 0u;
    }
    Q_DECL_CONSTEXPR uint maxFraction() const noexcept {
        return std::pow(10, Precision);
    }
    Q_DECL_CONSTEXPR static inline class_name min() noexcept {
        return class_name{std::numeric_limits<T>::min(), std::numeric_limits<uint>::min()};
    }
    Q_DECL_CONSTEXPR bool isMin() const noexcept {
        return min() == *this;
    }
    Q_DECL_CONSTEXPR static inline class_name max() noexcept {
        return null();
    }
    Q_DECL_CONSTEXPR bool isMax() const noexcept {
        return max() == *this;
    }
    QString toString(QLocale const& loc = {}) const noexcept {
        return loc.toString(toDouble(), 'f', Precision);
    }
    Q_DECL_CONSTEXPR static inline class_name fromDouble(double value) noexcept {
        auto const ip = ipow();
        long long int const val =  value * ip;
        return class_name((val / ip), std::abs(val % ip) );
    }
    Q_DECL_CONSTEXPR double toDouble() const noexcept {
        auto const part = static_cast<double>(m_fraction) / pow();
        return static_cast<double>(m_integral) + part;
    }
    Q_DECL_CONSTEXPR operator double() const noexcept {
        return toDouble();
    }
    QString save() const noexcept {
        return QStringLiteral("%1|%2")
                .arg(ps2::to_sqstr(m_integral), ps2::to_sqstr(m_fraction));
    }
    static inline QString save(class_name const& d) noexcept {
        return d.save();
    }
    static inline class_name restore(QString const& str) noexcept {
        auto const& pos = str.indexOf(QChar::fromLatin1('|'));
        if(pos == -1) return class_name::null();

        return class_name(str.mid(0, pos).toLongLong()
                          , str.mid(pos + 1, str.length() - pos).toLongLong());
    }
    Q_DECL_CONSTEXPR void swap(class_name& rhs) noexcept {
        qSwap(m_integral, rhs.m_integral);
        qSwap(m_fraction, rhs.m_fraction);
    }
};

template<class T, int Precision>
Q_DECL_CONSTEXPR static inline void swap(ps2::Decimal<T,Precision>& lhs
                        , ps2::Decimal<T,Precision>& rhs) noexcept {
    lhs.swap(rhs);
}
template<class T, int Precision>
static inline QDebug operator<<(QDebug dbg, ps2::Decimal<T,Precision> const& decimal) {
    return dbg.nospace()
        << "\"ps2::Decimal\": {"
        << "\"integral\": " << decimal.integral()
        << ", \"fraction\": " << decimal.fraction()
        << "}";
}
template<class T, int Precision>
static inline std::ostream& operator<<(std::ostream& out
                           , ps2::Decimal<T,Precision> const& decimal) noexcept {
    return ps2::cout(out, decimal);
}

} // end namespace ps2
