/**
 * \file      ps2/ps2/qt/objects/text_link.h
 * \brief     The Text_link class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 26(th), 2017, 17:34 MSK
 * \updated   February  (the) 26(th), 2017, 17:34 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QStringList>
#include "link.h"
#include <ps2/qt/out.h>

/** \namespace ps2 */
namespace ps2 {

class TextLink {
public:
    using class_name = TextLink;
    using link_t     = Link;

private:
    Link    m_link;
    QString m_text;

public:
    TextLink() Q_DECL_EQ_DEFAULT;
    TextLink(Link const& link, QString const& text)
        : m_link(link)
        , m_text(text)
    {}
    TextLink(Link&& link, QString&& text)
        : m_link(qMove(link))
        , m_text(qMove(text))
    {}
    static class_name null() noexcept {
        return class_name{};
    }
    bool isNull() const noexcept {
        return !isValid();
    }
    bool isValid() const noexcept {
        return !m_link.isEmpty()
            && !m_text.isEmpty()
        ;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_link == rhs.m_link
            && lhs.m_text == rhs.m_text
        ;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    Link const& link() const noexcept {
        return m_link;
    }
    void setLink(Link const& link) noexcept {
        m_link = link;
    }
    void setLink(Link&& link) noexcept {
        m_link = qMove(link);
    }
    QString const& text() const noexcept {
        return m_text;
    }
    void setText(QString const& text) noexcept {
        m_text = text;
    }
    void setText(QString&& text) noexcept {
        m_text = qMove(text);
    }
    void swap(class_name& rhs) noexcept {
        m_link.swap(rhs.m_link);
        m_text.swap(rhs.m_text);
    }
};

static inline void swap(ps2::TextLink& lhs, ps2::TextLink& rhs) noexcept {
    lhs.swap(rhs);
}
static inline QDebug operator<<(QDebug dbg, ps2::TextLink const& link) {
    return dbg.nospace()
        << "\"TextLink\": {"
        << "\"text\": " << link.text()
        << ", \"link\": { " << link.link() << "}"
        << "}";
}
static inline std::ostream& operator<<(std::ostream& out, ps2::TextLink const& link) noexcept {
    return ps2::cout(out, link);
}

} // end namespace ps2

Q_DECLARE_METATYPE(ps2::TextLink)
