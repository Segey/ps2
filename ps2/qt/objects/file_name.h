/**
 * \file      ps2/ps2/qt/objects/file_name.h
 * \brief     FileName class contains FileName
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   May       (the) 06(th), 2015, 15:10 MSK
 * \updated   September (the) 09(th), 2015, 18:10 MSK
 * \TODO
**/
#pragma once
#include <QDir>
#include <QFile>
#include <QString>
#include <ps2/qt/rand.h>
#include <stdio.h>

namespace ps2 {
/**
 * \code
 *      auto const& file = ps2::FileName::temp();                      // "C:/Users/spanin/AppData/Local/Temp/si88.1"
 *      auto const& file = tested::temp(QStringLiteral("cool.db"));    // "C:/Users/spanin/AppData/Local/Temp/cool.db"
 *
 * \endcode
**/
class FileName final {
public:
    using class_name = FileName;

private:
    QString m_file;

private:
    static QString getName(QString const& fileName = {}) noexcept {
#ifdef Q_OS_WIN
        auto result = _tempnam(QDir::tempPath().toLocal8Bit().data()
                              , fileName.toLocal8Bit().data());
#else
		auto result = tempnam(QDir::tempPath().toLocal8Bit().data()
			, fileName.toLocal8Bit().data());
#endif
        return QString::fromLocal8Bit(result);
    }

public:
    FileName() = default;
    ~FileName() = default;
    FileName(QString const& name) noexcept
        : m_file(name) {
    }
    FileName(QString&& name) noexcept
        : m_file(qMove(name)) {
    }
    QString const& name() const noexcept {
        return m_file;
    }
    void setName(QString const& val) noexcept {
        m_file = val;
    }
    void setName(QString&& val) noexcept {
        m_file = qMove(val);
    }
    operator QString() noexcept {
        return m_file;
    }
    static inline class_name temp(QString const& fileName = QString()) noexcept {
        if(fileName.isEmpty())
            return class_name(getName(fileName));
        return class_name(QDir::tempPath() + QDir::separator() + fileName);
    }
    static inline class_name temp(std::string const& fileName) noexcept {
        return temp(QString::fromStdString(fileName));
    }
    /**
     * \code
        auto const& file = ps2::FileName::temp("bpbr.tmp");
     * \endcode
     * @return
    **/
    static inline class_name temp(char const* const fileName) noexcept {
        return temp(QString::fromLocal8Bit(fileName));
    }
};

} // end namespace ps2
