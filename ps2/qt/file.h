/**
 * \file      ps2/ps2/qt/file.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 21(th), 2015, 01:25 MSK
 * \updated   September (the) 21(th), 2015, 01:25 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QString>
#include <QPrinter>
#include <QFileInfo>
#include <QTextCodec>
#include <QTextStream>
#include <QMessageBox>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *      auto const& ext = QFileInfo(ps2::checkUpdateExt(file)).suffix();
 * \endcode
**/
inline QString checkUpdateExt(QString const& file, QString const& def_ext = QStringLiteral(".txt")) {
    return QFileInfo(file).suffix().isEmpty() ? file + def_ext : file;
}
/**
 * \code
 *      ps2::saveAsPdf(file_name, text_edit_->document());
 * \endcode
**/
template<class T>
inline void saveAsPdf(QString const& fileName, T* document)
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    document->print(&printer);
} 
/**
 * \code
 *      ps2::saveAsText(file, ui->m_text, this, program::fullName(), iButton::tr("Cannot write file %1:\n%2."));
 * \endcode
**/
template<class T, class U>
inline bool saveAsText(QString const& fileName, U* data, T* parent, QString const& name, QString const& err_str)
{
    QFile file(fileName);
    if(!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(parent, name, err_str.arg(fileName) .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");
    out << data->toPlainText();
    return true;
}
/**
 * \code
 *      ps2::saveAsHtml(file, ui->m_text, this, program::fullName(), iButton::tr("Cannot write file %1:\n%2."));
 * \endcode
**/
template<class T, class U>
inline bool saveAsHtml(QString const& fileName, U* data, T* parent, QString const& name, QString const& err_str)
{
    QFile file(fileName);
    if(!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(parent, name, err_str.arg(fileName) .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");
    out << data->toHtml();
    return true;
}

/**
 * \code
 *      ps2::openAsHtml(file, ui->m_text, this, program::fullName(), iButton::tr("Cannot open file %1:\n%2."));
 * \endcode
**/
template<class T, class U>
inline bool openAsHtml(QString const& fileName, U* obj, T* parent, QString const& name, QString const& err_str)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::warning(parent, name, err_str.arg(fileName) .arg(file.errorString()));
        return false;
    }
    obj->setHtml(QString::fromUtf8(file.readAll()));
    return true;
} 

/**
 * \code
 *      ps2::openAsText(file, ui->m_text, this, program::fullName(), iButton::tr("Cannot open file %1:\n%2."));
 * \endcode
**/
template<class T, class U>
inline bool openAsText(QString const& fileName, U* obj, T* parent, QString const& name, QString const& err_str)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::warning(parent, name, err_str.arg(fileName) .arg(file.errorString()));
        return false;
    }
    obj->setPlainText(QString::fromUtf8(file.readAll()));
    return true;
} 

} // end namespace ps2
