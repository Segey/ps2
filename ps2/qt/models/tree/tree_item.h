/**
 * \file      ps2/ps2/qt/models/tree/tree_item.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   August (the) 11(th), 2015, 20:35 MSK
 * \updated   August (the) 11(th), 2015, 20:35 MSK
 * \TODO      
**/
#pragma once
#include <QList>
#include <QVariant>

/** namespace ps2 */
namespace ps2 {

/**
 * \code
 *      typedef TreeItem<QList<QVariant> > item_t;
 *      typedef TreeItem<QVariant>         item_t;
 * \endcode
**/
template<class T =  QVariant>
class TreeItem {
public:
    using data_t     = T;
    using class_name = TreeItem;
    using items_t    = QList<TreeItem*>;

private:
    data_t    m_data;
    items_t   m_items;
    TreeItem* m_parent = nullptr;

public:
    explicit TreeItem() = default;
    explicit TreeItem(class_name* parent, data_t const& data)
        : m_data(data)
        , m_parent(parent) {
     }
    explicit TreeItem(class_name* parent, data_t&& data)
        : m_data(qMove(data))
        , m_parent(parent) {
     }
    virtual ~TreeItem() {
        qDeleteAll(m_items);
    }
    void addChild(TreeItem* child) {
        m_items.append(child);
    }
    TreeItem* child(int row) {
        return m_items.value(row);
    }
    TreeItem* last() {
        return m_items.value(m_items.size() - 1);
    }
    int childCount() const noexcept {
        return m_items.count();
    }
    data_t& data() noexcept {
        return m_data;
    }
    data_t const& data() const noexcept {
        return m_data;
    }
    void setData(data_t const& data) noexcept {
        m_data = data;
    }
    void setData(data_t&& data) noexcept {
        m_data = std::move(data);
    }
    virtual QVariant columnData(uint column) const {
        Q_UNUSED(column)
        return QVariant{};
    }
    virtual void addColumnData(QVariant const&) {
    }
    int row() const {
        if (!m_parent) return 0;
        return m_parent->m_items.indexOf(const_cast<TreeItem*>(this));
    }
    TreeItem* parent() const noexcept {
         return m_parent;
    }
};

} // end namespace ps2

Q_DECLARE_METATYPE(ps2::TreeItem<QVariant>)
Q_DECLARE_METATYPE(ps2::TreeItem<QVariant>*)
