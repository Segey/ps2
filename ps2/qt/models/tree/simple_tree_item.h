/**
 * \file      ps2/ps2/qt/models/tree/tree_item.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.2.0
 * \created   August    (the) 11(th), 2015, 20:35 MSK
 * \updated   October   (the) 14(th), 2019, 10:07 MSK
 * \TODO      
**/
#pragma once
#include <QList>
#include <QVariant>

/** namespace ps2 */
namespace ps2 {

/**
 * \code
 *      using item_t = SimpleTreeItem<QList<QVariant>>;
 *      using item_t = SimpleTreeItem<QVariant>;
 * \endcode
**/
template<class T =  QVariant>
class SimpleTreeItem final {
public:
    using data_t         = T;
    using class_name     = SimpleTreeItem<data_t>;
    using class_name_ptr = class_name*;
    using items_t        = QList<class_name_ptr>;

private:
    items_t        m_items;
    data_t         m_data;
    class_name_ptr m_parent = Q_NULLPTR;

public:
    explicit SimpleTreeItem(class_name_ptr parent = Q_NULLPTR, data_t const& data = {}) Q_DECL_NOEXCEPT
        : m_parent(parent)
        , m_data(data) {
     }
    ~SimpleTreeItem() Q_DECL_NOEXCEPT {
        qDeleteAll(m_items);
    }
    void addChild(class_name_ptr child) Q_DECL_NOEXCEPT {
        m_items.append(child);
    }
    class_name_ptr child(int row) {
        return m_items.value(row);
    }
    class_name_ptr last() {
        return m_items.value(m_items.size() - 1);
    }
    int childCount() const {
        return m_items.count();
    }
    data_t const& data() const {
        return m_data;
    }
    void setData(data_t const& data) {
        m_data = data;
    }
    int row() const {
        if (!m_parent)
            return 0;
        return m_parent->m_items.indexOf(const_cast<class_name_ptr>(this));
    }
    class_name_ptr parent() const Q_DECL_NOEXCEPT {
         return m_parent;
    }
};

} // end namespace ps2

Q_DECLARE_METATYPE(ps2::SimpleTreeItem<QVariant>)
Q_DECLARE_METATYPE(ps2::SimpleTreeItem<QString>)
Q_DECLARE_METATYPE(ps2::SimpleTreeItem<QVariant>*)
