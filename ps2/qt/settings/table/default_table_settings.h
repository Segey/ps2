/**
 * \file      ps2/ps2/qt/settings/table/default_table_settings.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 23(th), 2016, 16:19 MSK
 * \updated   March (the) 23(th), 2016, 16:19 MSK
 * \TODO      
**/
#pragma once
#include <QTableView>
#include <QHeaderView>
#include "../default_settings_item.h"
#include "default_table_settings_type.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class Table: public Item {
public:
    using class_name = Table;
    using type_t     = TableType;
    using inherited  = Item;
     
private: 
    QTableView* m_view = nullptr;
    type_t      m_type = type_t::None;

    bool init() const {
        Q_ASSERT_X(m_type != type_t::Invalid, "TableType::init", "TableType is Invalid");
        if(m_type == type_t::None) return true;
        if(m_type == type_t::Size) return true;
        if(m_type == type_t::ColsContext) return initColumnsToContext();
        if(m_type == type_t::ColsContextAndLastFull) return initColumnsContextAndLastFull();

        Q_ASSERT_X(0, "TableType::init", "unknown type");
        return true;
    }
    bool initColumnsToContext() const {
        m_view->resizeColumnsToContents();
        return true;
    }
    bool initColumnsContextAndLastFull() const {
        m_view->resizeColumnsToContents();
        m_view->horizontalHeader()->setStretchLastSection(true);
        return true;
    }
    bool read(QVariant const& var) const {
        return m_view->horizontalHeader()->restoreState(var.toByteArray());
    }

protected: 
    virtual bool doRead(QVariant const& var) const Q_DECL_OVERRIDE {
        Q_ASSERT_X(m_view && m_view->model(), "Table::doRead", "table view is empty");
        if(!m_view || !m_view->model()) return false;

        return var.isValid() ? read(var) : init();
    }
    virtual QVariant doWrite() const Q_DECL_OVERRIDE {
        Q_ASSERT_X(m_view && m_view->model(), "Table::doWrite", "table view is empty");
        if(!m_view || !m_view->model()) return QVariant();

        return m_view->horizontalHeader()->saveState();
    }

public:
    explicit Table(QTableView* view, TableType type)
        : m_view(view), m_type(type){
    }
    QTableView* view() const {
        return m_view;
    }
    void setView(QTableView* view) {
        m_view = view;
    }
};
}} // end namespace ps2::default_settings
