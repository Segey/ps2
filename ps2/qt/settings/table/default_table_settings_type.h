/**
 * \file      ps2/ps2/qt/settings/table/default_table_settings_type.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 23(th), 2016, 19:29 MSK
 * \updated   March (the) 23(th), 2016, 19:29 MSK
 * \TODO      
**/
#pragma once
#include <QTableView>
#include "../default_settings_item.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

enum class TableType: std::uint8_t {
    Invalid                  = 0u
    , None                   = 1u
    , Size                   = 2u
    , ColsContext            = 4u
    , ColsContextAndLastFull = 8u
};

}} // end namespace ps2::default_settings
