/**
 * \file      ps2/ps2/qt/settings/widget/default_settings_widget_geometry_type.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 24(th), 2016, 19:12 MSK
 * \updated   March (the) 24(th), 2016, 19:12 MSK
 * \TODO
**/
#pragma once

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

enum class WidgetGeometryType: quint8 {
      Invalid         = 0u
    , None            = 1u
    , Min             = 2u
    , Adjusted        = 3u
    , Max             = 4u
    , Calculated      = 5u
    , MinFixed        = 6u
    , AdjustedFixed   = 7u
    , MaxFixed        = 8u
    , CalculatedFixed = 9u
};

}} // end namespace ps2::default_settings
