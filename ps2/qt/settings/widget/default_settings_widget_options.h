/**
 * \file      ps2/ps2/qt/settings/widget/default_settings_widget_options.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 24(th), 2016, 19:48 MSK
 * \updated   March (the) 24(th), 2016, 19:48 MSK
 * \TODO
**/
#pragma once
#include <QRect>
#include <QPair>
#include <QApplication>
#include <ps2/qt/macros.h>
#include <ps2/qt/convert/enum.h>
#include <ps2/cpp/fraction.h>
#include "default_settings_widget_geometry_type.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class WidgetGeometryOptions {
public:
    using class_name = WidgetGeometryOptions;
    using pair_t     = QPair<QRect, QPoint>;
    using type_t     = WidgetGeometryType;

private:
    QRect  m_rect;
    type_t m_type = type_t::Calculated;

    static inline pair_t desktopSize() noexcept {
        auto const& result = QApplication::desktop()->screenGeometry();
        return pair_t(result, result.center());
    }

public:
    /**
     * \code
     *    ps2::WindowOptions(ps2::default_settings::WindowOptions::Maximum);
     *    ps2::WindowOptions();
     * \endcode
    **/
    WidgetGeometryOptions(type_t type = type_t::Min) noexcept
        : m_type(type) {
    }
    /**
     * \code
     *    ps2::WindowOptions(300,400);
     * \endcode
    **/
    WidgetGeometryOptions(uint width, uint height) noexcept {
        auto&& result = desktopSize();
        result.first.setWidth(width);
        result.first.setHeight(height);
        result.first.moveCenter(result.second);
        m_rect = result.first;
    }
    /**
     * \code
     *    ps2::WindowOptions(size);
     * \endcode
    **/
    WidgetGeometryOptions(QSize const& size) noexcept
        : class_name(size.width(), size.height()) {
    }
    /**
     * \code
     *    ps2::WindowOptions(300);
     * \endcode
    **/
    WidgetGeometryOptions(uint width) noexcept
        : class_name(width, (width / GOLDEN_RULE)){
    }
    /**
     * \code
     *    ps2::WindowOptions(Fraction{3,4});
     * \endcode
    **/
    template<int Denominator>
    WidgetGeometryOptions(ps2::Fraction<Denominator> const& fraction)
        : class_name(desktopSize().first.width() * fraction.toDouble()) {
    }
    type_t type() const noexcept {
        return m_type;
    }
    QRect const& rect() const noexcept {
        return m_rect;
    }
};

}} // end namespace ps2::default_settings
