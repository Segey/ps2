/**
 * \file      ps2/ps2/qt/settings/widget/default_settings_widget.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March (the) 24(th), 2016, 19:06 MSK
 * \updated   March (the) 24(th), 2016, 19:06 MSK
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QWindow>
#include <QRect>
#include <QMainWindow>
#include "../default_settings_item.h"
#include "default_settings_widget_options.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class WidgetGeometry: public Item {
public:
    using widget_t   = QWidget;
    using class_name = WidgetGeometry;
    using inherited  = Item;
    using options_t  = WidgetGeometryOptions;
    using type_t     = options_t::type_t;
    using pair_t     = QPair<QRect, QPoint>;

    static inline pair_t desktopSize() {
        auto const& result = QApplication::desktop()->screenGeometry();
        return pair_t(result, result.center());
    }
    inline void setFixed(bool isFixed) const noexcept {
        if(isFixed) m_widget->setFixedSize(m_widget->size());
    }

    bool initSizeable(QSize const& size, bool isFixed) const noexcept {
        auto&& result = desktopSize();
        result.first.setSize(size);
        result.first.moveCenter(result.second);
        m_widget->setGeometry(result.first);
        setFixed(isFixed);
        return true;
    }

private:
    widget_t* m_widget = Q_NULLPTR;
    options_t m_options;

    bool init() const noexcept {
        auto const type = m_options.type();
        Q_ASSERT_X(type != type_t::Invalid, "WindowType::init", "WindowType is Invalid");
        if(type == type_t::None) return true;
        if(type == type_t::Min) return initMinimum(false);
        if(type == type_t::MinFixed) return initMinimum(true);
        if(type == type_t::Max) return initMinimum(false);
        if(type == type_t::MaxFixed) return initMinimum(true);
        if(type == type_t::Adjusted) return initAdjusted(false);
        if(type == type_t::AdjustedFixed) return initAdjusted(true);
        if(type == type_t::Calculated) return initCalculated(false);
        if(type == type_t::CalculatedFixed) return initCalculated(false);

        Q_ASSERT_X(0, "WindowType::init", "unknown type");
        return true;
    }
    bool initCalculated(bool isFixed) const noexcept {
        m_widget->setGeometry(m_options.rect());
        setFixed(isFixed);
        return true;
    }
    bool initMinimum(bool isFixed) const noexcept {
        return initSizeable(m_widget->minimumSizeHint(), isFixed);
    }
    bool initMaximum(bool isFixed) const noexcept {
        return initSizeable(m_widget->maximumSize(), isFixed);
    }
    bool initAdjusted(bool isFixed) const noexcept {
        return initSizeable(m_widget->sizeHint(), isFixed);
    }
    void updateSize() const noexcept {
        auto const& rect = QApplication::desktop()->screenGeometry();
        if(m_widget->height() > rect.height()) init();
    }
    bool read(QVariant const& var) const {
        m_widget->restoreGeometry(var.toByteArray());
        updateSize();
        return true;
    }

protected:
    virtual bool doRead(QVariant const& var) const Q_DECL_OVERRIDE {
        Q_ASSERT_X(m_widget, "WindowGeometry::doRead", "table view is empty");
        if(!m_widget) return false;

        return var.isValid() ? read(var) : init();
    }
    virtual QVariant doWrite() const Q_DECL_OVERRIDE {
        Q_ASSERT_X(m_widget, "WindowGeometry::doWrite", "table view is empty");
        if(!m_widget) return QVariant();

        return m_widget->saveGeometry();
    }

public:
    explicit WidgetGeometry(widget_t* widget, WidgetGeometryOptions const& options)
        : m_widget(widget)
        , m_options(options){
    }
    widget_t* window() const noexcept {
        return m_widget;
    }
    void setWindow(widget_t* window) noexcept{
        m_widget = window;
    }
};
}} // end namespace ps2::default_settings
