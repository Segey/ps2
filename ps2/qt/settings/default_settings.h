/**
 * \file      ps2/ps2/qt/settings/default_settings.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 23(th), 2016, 16:11 MSK
 * \updated   March (the) 23(th), 2016, 16:11 MSK
 * \TODO
**/
#pragma once
#include <QVector>
#include <QVariant>
#include <QSettings>
#include <initializer_list>
#include "table/default_table_settings.h"
#include "default_settings_item.h"
#include "default_settings_variant.h"

/** \namespace ps2::default_settings */
namespace ps2 {

class DefaultSettings {
public:
    using class_name = DefaultSettings;
    using item_t     = default_settings::Item;
    using variant_t  = default_settings::Variant;
    using items_t    = QVector<variant_t>;

private:
    items_t   m_items;
    QSettings m_settings;

public:
    explicit DefaultSettings() Q_DECL_EQ_DEFAULT;
    DefaultSettings(DefaultSettings const& rhs)
        : m_items(rhs.m_items) {
    }
    explicit DefaultSettings(std::initializer_list<variant_t> items)
        : m_items(items) {
    }
    ~DefaultSettings() Q_DECL_EQ_DEFAULT;
    DefaultSettings& operator=(DefaultSettings const& rhs) {
        if(this != &rhs)
            m_items = rhs.m_items;
        return *this;
    }
    void addVariant(variant_t const& item) noexcept {
        m_items.push_back(item);
    }
    void setItems(items_t&& items) noexcept {
        m_items = qMove(items);
    }
    void setItems(items_t const& items) noexcept {
        m_items = items;
    }
    bool read(QString const& group) noexcept {
        int result{0};

        m_settings.beginGroup(group);
        for(auto& item: m_items)
            result += item.read(m_settings.value(item.name()));
        m_settings.endGroup();

        return result == m_items.size();
    }
    bool write(QString const& group) noexcept {
        int result{0};

        m_settings.beginGroup(group);
        for(auto& item: m_items) {
            auto const& var = item.write();
            if(var.isValid()) m_settings.setValue(item.name(), var);
            result += var.isValid();
        }
        m_settings.endGroup();

        return result == m_items.size();
    }
};

} // end namespace ps2
