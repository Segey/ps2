/**
 * \file      ps2/ps2/qt/settings/default_settings_item.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 23(th), 2016, 16:21 MSK
 * \updated   March (the) 23(th), 2016, 16:21 MSK
 * \TODO
**/
#pragma once
#include <QVariant>

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class Item {
public:
    using class_name = Item;

protected:
    virtual bool doRead(QVariant const& variant) const = 0;
    virtual QVariant doWrite() const = 0;

public:
    Item() Q_DECL_EQ_DEFAULT;
    virtual ~Item() Q_DECL_EQ_DEFAULT;
    bool read(QVariant const& variant) const noexcept {
        return doRead(variant);
    }
    QVariant write() const noexcept {
        return doWrite();
    }
};

}} // end namespace ps2::default_settings
