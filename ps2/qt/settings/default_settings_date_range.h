/**
 * \file      ps2/ps2/qt/settings/default_settings_date_range.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 26(th), 2016, 13:43 MSK
 * \updated   March (the) 29(th), 2016, 01:02 MSK
 * \TODO
**/
#pragma once
#include <QVariant>
#include "default_settings_item.h"
#include <ps2/qt/objects/date_range.h>

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class DateRange final: public Item {
public:
    using class_name = DateRange;
    using inherited  = Item;
    using range_t    = ps2::DateRange;

private:
    range_t* m_range = Q_NULLPTR;
    range_t  m_default;

protected:
    virtual bool doRead(QVariant const& var) const Q_DECL_OVERRIDE Q_DECL_FINAL {
        if(!var.isValid()) {
            *m_range = m_default;
            return true;
        }
        QByteArray arr = var.toByteArray();
        m_range->fromByteArray(arr);
        return true;
    }
    virtual QVariant doWrite() const Q_DECL_OVERRIDE Q_DECL_FINAL {
        return m_range->toVariant();
    }

public:
    explicit DateRange(range_t* range, range_t const& def) noexcept
        : m_range(range)
        , m_default(def) {
    }
};

}} // end namespace ps2::default_settings
