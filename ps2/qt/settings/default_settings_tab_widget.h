/**
 * \file      ps2/ps2/qt/settings/default_settings_tab_widget.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   Апрель (the) 04(th), 2016, 21:33 MSK
 * \updated   Апрель (the) 04(th), 2016, 21:33 MSK
 * \TODO
**/
#pragma once
#include <QTabBar>
#include <QTabWidget>
#include "default_settings_item.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class TabWidget: public Item {
public:
    using class_name = TabWidget;
    using inherited  = Item;

private:
    QTabWidget* m_widget = Q_NULLPTR;
    const quint8 m_magic = 29;

private:
    int index(QString const& text) const {
        for(auto i = 0; i != m_widget->count(); ++i)
            if(m_widget->tabText(i) == text) return i;
        return -1;
    }
    void updateTabs(QDataStream& in) const {
        QString text;
        for(auto i = 0; i != m_widget->count(); ++i) {
            in >> text;
            const auto from = index(text);
            if(from != -1)
                m_widget->tabBar()->moveTab(from, i);
        }
    }

protected:
    virtual bool doRead(QVariant const& var) const Q_DECL_OVERRIDE {
        if(!var.isValid())
            return true;

        quint8 qt, magic, count, current;
        auto buff = var.toByteArray();
        QDataStream in(&buff, QIODevice::ReadOnly);
        in >> qt >> magic >> count >> current;
        if(qt != QDataStream::Qt_5_5 || magic != m_magic || current > m_widget->count())
            return false;
        if(count < 2 || m_widget->count() != count)
            return true;

        updateTabs(in);
        m_widget->setCurrentIndex(current);
        return true;
    }
    virtual QVariant doWrite() const Q_DECL_OVERRIDE Q_DECL_FINAL {
        if(m_widget->count() < 2) return QVariant();

        QByteArray buff;
        QDataStream out(&buff, QIODevice::WriteOnly);
        out << quint8(QDataStream::Qt_5_5) << m_magic
            << quint8(m_widget->count())
            << quint8(m_widget->currentIndex());

        for(auto i = 0; i != m_widget->count(); ++i)
            out << m_widget->tabText(i);

        return buff;
    }

public:
    explicit TabWidget(QTabWidget* widget)
        : m_widget(widget) {
    }
};
}} // end namespace ps2::default_settings
