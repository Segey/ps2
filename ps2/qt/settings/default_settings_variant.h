/**
 * \file      ps2/ps2/qt/settings/default_settings_variant.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 23(th), 2016, 16:21 MSK
 * \updated   March (the) 23(th), 2016, 16:21 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <QWindow>
#include <QString>
#include <QSettings>
#include <QTableView>
#include <QSharedPointer>
#include "default_settings_item.h"
#include "table/default_table_settings.h"
#include "widget/default_settings_widget.h"
#include "window/default_settings_window_state.h"
#include "default_settings_tab_widget.h"
#include "default_settings_date_range.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class Variant {
public:
    using class_name    = Variant;
    using item_t        = Item;
    using shared_item_t = QSharedPointer<Item>;

private:
    QString       m_name;
    shared_item_t m_item;

public:
    Variant() Q_DECL_EQ_DEFAULT;
    ~Variant() Q_DECL_EQ_DEFAULT;
    Variant(QString const& name, item_t* item) noexcept
        : m_name(name)
        , m_item(item) {
    }
    Variant(QString const& name, QTableView* table, TableType type = TableType::ColsContextAndLastFull)
        : class_name(name, new Table(table, type)) {
    }
    Variant(QWidget* widget, WidgetGeometryOptions const& options) noexcept
        : class_name(QStringLiteral("geometry")
          , new WidgetGeometry(widget, options)) {
    }
    Variant(QString const& name, ps2::DateRange* range, ps2::DateRange const def = {})
        : class_name(name, new DateRange(range, def)) {
    }
    Variant(QString const& name, QTabWidget* widget) noexcept
        : class_name(name, new TabWidget(widget)) {
    }
    QString const& name() const noexcept {
        return m_name;
    }
    bool read(QVariant const& variant) const {
        return m_item->read(variant);
    }
    QVariant write() const {
        return m_item->write();
    }
    static Variant state(QMainWindow* window) {
        return class_name(QStringLiteral("state")
                   , new WindowState(window));
    }
};

}} // end namespace ps2::default_settings
