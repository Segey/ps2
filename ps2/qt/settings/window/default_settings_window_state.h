/**
 * \file      ps2/ps2/qt/settings/window/default_settings_window_state.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 26(th), 2016, 13:43 MSK
 * \updated   March (the) 26(th), 2016, 13:43 MSK
 * \TODO
**/
#pragma once
#include <QRect>
#include <QDialog>
#include <QWindow>
#include <QMainWindow>
#include "../default_settings_item.h"

/** \namespace ps2::default_settings */
namespace ps2 {
namespace default_settings {

class WindowState: public Item {
public:
    using class_name = WindowState;
    using inherited  = Item;

private:
    QMainWindow* m_window = Q_NULLPTR;

    bool read(QVariant const& var) const noexcept {
        m_window->restoreState(var.toByteArray());
        return true;
    }

protected:
    virtual bool doRead(QVariant const& var) const Q_DECL_OVERRIDE Q_DECL_FINAL {
        Q_ASSERT_X(m_window, "WindowState::doRead", "table view is empty");
        if(!m_window) return false;
        if(!var.isValid()) return true;
        return read(var);
    }
    virtual QVariant doWrite() const Q_DECL_OVERRIDE Q_DECL_FINAL {
        Q_ASSERT_X(m_window, "WindowState::doWrite", "table view is empty");
        if(!m_window) return QVariant();

        return m_window->saveState();
    }

public:
    explicit WindowState(QMainWindow* window) noexcept
        : m_window(window) {
    }
    QMainWindow const* window() const noexcept {
        return m_window;
    }
    void setWindow(QMainWindow* window) noexcept {
        m_window = window;
    }
};
}} // end namespace ps2::default_settings
