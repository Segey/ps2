/**
 * \file      perfect/main/test/shim_test.h
 * \brief     The Shim_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   November (the) 12(th), 2016, 01:04 MSK
 * \updated   November (the) 12(th), 2016, 01:04 MSK
 * \TODO
**/
#pragma once
#include <string>
#include <ps2/qt/shim.h>

class ShimTest final {
public:
    using class_name = ShimTest;

private:
    static void StringToString() noexcept {
        auto&& _1 = ps2::shim::to_str("cool"s);
        assertEquals(_1, "cool"s);

        _1 = ps2::shim::to_str(_1);
        assertEquals(_1, "cool"s);
    }
    static void QStringToString() noexcept {
        auto&& _1 = ps2::shim::to_str(QStringLiteral("cool"));
        assertEquals(_1, "cool"s);
    }

public:
    void operator()() noexcept {
        StringToString();
        QStringToString();
    }
};
