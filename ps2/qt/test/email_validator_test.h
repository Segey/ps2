/**
 * \file      ps2/ps2/qt/test/email_validator_test.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April (the) 14(th), 2016, 00:22 MSK
 * \updated   April (the) 14(th), 2016, 00:22 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include <ps2/qt/validators/email_validator.h>

class EmailValidatorTest {
public:
    using class_name = EmailValidatorTest;
    using email_t    = ps2::EmailValidator;

private:
    static void EmptyEmailTest() noexcept {
        assertFalse(email_t::validate(QStringLiteral("")));
    }
    static void ValidEmailTest2() noexcept {
        assertTrue(email_t::validate(QStringLiteral("dix75@mail.com.ru")));
    }
    static void ValidEmailTest() noexcept {
        assertTrue(email_t::validate(QStringLiteral("dix75@mail.ru")));
    }
    static void InvalidEmailTest() noexcept {
        assertFalse(email_t::validate(QStringLiteral("dix75")));
    }
    static void InvalidEmailTest2() noexcept {
        assertFalse(email_t::validate(QStringLiteral("dix75@ddd@.ru")));
    }
    static void InvalidEmailTest3() noexcept {
        assertFalse(email_t::validate(QStringLiteral("dix75@mail.ru.")));
    }

public:
    void operator()() noexcept {
        EmptyEmailTest();
        ValidEmailTest();
        ValidEmailTest2();
        InvalidEmailTest();
        InvalidEmailTest2();
        InvalidEmailTest3();
    }
};

