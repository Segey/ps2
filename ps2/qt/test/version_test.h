/**
 * \file      ps2/ps2/qt/test/version_test.h
 * \brief     Version test
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 22(th), 2015, 19:11 MSK
 * \updated   May (the) 22(th), 2015, 19:11 MSK
 * \TODO
**/
#pragma once
#include <ps2/qt/version/version.h>

class VersionTest {
public:
    typedef VersionTest        class_name;
    typedef ps2::Version       tested;
    typedef ps2::VersionStatus status_t;

private:
    static void InvalidVersionAfterDefaultCreation() {
        tested ver;

        assertFalse(ver.isValid());
    }
    static void NullVersionAfterDefaultCreation() {
        tested ver;

        assertFalse(ver.isNull());
    }
    static void ValidVersionAfterFillUpCreation() {
        tested ver(5u,10u, status_t::Final, 44);

        assertTrue(ver.isValid());
    }
    static void ToString() {
        tested ver(5u,10u, status_t::Final, 44);

        assertEquals(tested::toString(ver), QStringLiteral("5.10.3.44"));
    }
    static void NotEqual() {
        tested ver1(5u,10u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Final, 4);

        assertNotEquals(ver1,  ver2);
    }
    static void Equal() {
        tested ver1(5u,10u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Final, 44);

        assertEquals( ver1, ver2);
    }
    static void LessMajor() {
        tested ver1(7u,10u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Final, 44);

        assertTrue( ver1 > ver2);
    }
    static void LessMinor() {
        tested ver1(5u,12u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Final, 44);

        assertTrue( ver1 > ver2);
    }
    static void LessStatus() {
        tested ver1(5u,10u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Beta, 44);

        assertTrue( ver1 > ver2);
    }
    static void LessStatusDetails() {
        tested ver1(5u,10u, status_t::Beta, 44);
        tested ver2(5u,10u, status_t::Beta, 22);

        assertTrue( ver1 > ver2);
    }
    static void MoreMinor() {
        tested ver1(5u,11u, status_t::Final, 44);
        tested ver2(5u,15u, status_t::Final, 44);

        assertTrue( ver1 < ver2);
    }
    static void FromStringOnlyMajor() {
        tested ver(55u, 0u, status_t::Final);

        assertEquals(ver, tested::fromString(QStringLiteral(" 55 ")));
    }
    static void ValidVersionFromStringOnlyMajor() {
        tested ver(55u, 0u, status_t::Final);

        assertTrue(ver.isValid());
    }
    static void InvlaidTypleAfterSendEmptyString() {
        tested ver(0u, 0u, status_t::Final);

        assertEquals(ver, tested::fromString(QStringLiteral("ttt")));
    }
    static void ValidMajor() {
        tested ver(0u, 0u, status_t::Final);

        assertEquals(ver, tested::fromString(QStringLiteral("ttt")));
    }
    static void ZeroMajorAfterSendFirstPoint() {
        tested ver(0u, 50u, status_t::Final);

        assertEquals(ver, tested::fromString(QStringLiteral(" .50")));
    }
    static void ValidMinor() {
        tested ver(50u, 33u, status_t::Final);

        assertEquals(ver, tested::fromString(QStringLiteral("50.33")));
    }
    static void InValidStatus() {
        tested ver(50u, 33u, status_t::Invalid);

        assertEquals(ver, tested::fromString(QStringLiteral("50.33.44")));
    }
    static void ValidStatus() {
        tested ver(50u, 33u, status_t::Beta);

        assertEquals(ver, tested::fromString(QStringLiteral("50.33.01")));
    }
    static void ValidDetails() {
        tested ver(50u, 33u, status_t::Beta, 5532u);

        assertEquals(ver, tested::fromString(QStringLiteral("50.33.01.5532")));
    }
    static void ValidFromStringFirstValue() {
        tested ver(1,0,status_t::Final,0);

        assertEquals(ver, tested::fromString(QStringLiteral("1")));
    }
    static void ValidFromStringSecondValue() {
        tested ver(12,4,status_t::Final,0);

        assertEquals(ver, tested::fromString(QStringLiteral("12.4")));
    }
    static void ValidFromStringThirdValue() {
        tested ver(25,1,status_t::Release,0);

        assertEquals(ver, tested::fromString(QStringLiteral("25.1.2")));
    }
    static void ValidFinalVersion() {
        tested ver(33,5,status_t::Final,0);

        assertEquals(ver, tested::Final(33,5));
    }
    static void MoreAndEqualMinor() {
        tested ver1(5u,11u, status_t::Final, 44u);
        tested ver2(5u,15u, status_t::Final, 44u);
        tested ver3(5u,15u, status_t::Final, 44u);

        assertTrue( ver1 <= ver2);
        assertTrue( ver2 <= ver3);
    }
    static void LessAndEqualMinor() {
        tested ver1(5u,12u, status_t::Final, 44);
        tested ver2(5u,10u, status_t::Final, 44);
        tested ver3(5u,10u, status_t::Final, 44);

        assertTrue( ver1 >= ver2);
        assertTrue( ver2 >= ver3);
    }
public:
    void operator()() {
        InvalidVersionAfterDefaultCreation();
        ValidVersionAfterFillUpCreation();
        ToString();
        NotEqual();
        Equal();
        LessMajor();
        LessMinor();
        LessStatus();
        LessStatusDetails();
        MoreMinor();
        FromStringOnlyMajor();
        ValidVersionFromStringOnlyMajor();
        InvlaidTypleAfterSendEmptyString();
        ValidMajor();
        ZeroMajorAfterSendFirstPoint();
        ValidMinor();
        InValidStatus();
        ValidStatus();
        ValidDetails();
        ValidFromStringFirstValue();
        ValidFromStringSecondValue();
        ValidFromStringThirdValue();
        ValidFinalVersion();
        MoreAndEqualMinor();
        LessAndEqualMinor();
    }
};
