/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/test/locale.h 
 * \brief     The Locale class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   July      (the) 20(th), 2017, 13:09 MSK
 * \updated   July      (the) 20(th), 2017, 13:09 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <ps2/qt/locale.h>

/** \namespace ps2 */
namespace ps2 {

class LocaleTest final {
public:
    using class_name = LocaleTest;

private:
    template<class T>
    static inline bool execute(QString const& name, T& set
                               , QChar(QLocale::* p)() const) noexcept {
        auto const size = set.size();
        for(auto i = 0; i != 360; ++i) {
            QLocale loc(static_cast<QLocale::Language>(i));
            set.insert((loc.*p)());

            if(set.size() > size) {
                assertErrorMessage(QStringLiteral("The %1 don't containt the '%2' char")
                              .arg(name, (loc.*p)()));
                return false;
            }
        }
        return true;
    }
    template<class T, class U>
    static inline bool executeLang(T const& set, U const& vec
                               , QChar(QLocale::* p)() const) noexcept {

        for(auto i: vec) {
            QLocale loc(i);
            if(set.find((loc.*p)()) == set.end()) {
                assertErrorMessage(QStringLiteral("Cannot find %1 char").
                                   arg((loc.*p)()));
                return false;
            }
        }
        return true;
    }

private:
    static inline void Groups() noexcept {
        auto groups = ps2::locale::groupSeparators();
        assertTrue(execute(QStringLiteral("groupSeparators"), groups
                               , &QLocale::groupSeparator));

        assertEquals(groups.size(), 6);
    }
    static inline void GroupsLanguage() noexcept {
        auto groups = ps2::locale::groupSeparators();
        auto langs  = ps2::locale::groupSeparatorLanguages();
        assertEquals(groups.size(), (langs.size() + 1));
        assertTrue(executeLang(groups, langs, &QLocale::groupSeparator));
    }
    static inline void Points() noexcept {
        auto set = ps2::locale::decimalPoints();
        assertTrue(execute(QStringLiteral("decimalPoints"), set
                               , &QLocale::decimalPoint));

        assertEquals(set.size(), 3);
    }
    static inline void PointsLanguage() noexcept {
        auto groups = ps2::locale::decimalPoints();
        auto langs  = ps2::locale::decimalPointLanguages();
        assertEquals(groups.size(), langs.size());
        assertTrue(executeLang(groups, langs, &QLocale::decimalPoint));
    }
    static inline void Percents() noexcept {
        auto set = ps2::locale::percents();
        assertTrue(execute(QStringLiteral("percents"), set
                               , &QLocale::percent));

        assertEquals(set.size(), 2);
    }
    static inline void PercentLanguage() noexcept {
        auto groups = ps2::locale::percents();
        auto langs  = ps2::locale::percentLanguages();
        assertEquals(groups.size(), langs.size());
        assertTrue(executeLang(groups, langs, &QLocale::percent));
    }

public:
    void operator()() noexcept {
        Groups();
        GroupsLanguage();
        Points();
        PointsLanguage();
        Percents();
        PercentLanguage();
    }
};

} // end namespace ps2
