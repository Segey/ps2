/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/test/from_aqstr.h
 * \brief     The From_oqstr class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   July      (the) 19(th), 2017, 18:30 MSK
 * \updated   July      (the) 19(th), 2017, 18:30 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/algorithm.h>

/** \namespace ps2 */
namespace ps2 {

class FromAqstr final {
public:
    using class_name = FromAqstr;

private:
    static void InvalidInt() noexcept {
        int val;
        assertFalse(ps2::from_aqstr(QStringLiteral("d100"), val));
        assertFalse(ps2::from_aqstr(QStringLiteral("100d"), val));
        assertFalse(ps2::from_aqstr(QStringLiteral("100 456.00"), val));
        assertFalse(ps2::from_aqstr(QStringLiteral("100,456 00"), val));
    }
    static void TestInt() noexcept {
        int val;
        assertTrue(ps2::from_aqstr(QStringLiteral("100"), val));
        assertEquals(val, 100);

        assertTrue(ps2::from_aqstr(QStringLiteral("100 000"), val));
        assertEquals(val, 100000);

        assertTrue(ps2::from_aqstr(QStringLiteral("100.000"), val));
        assertEquals(val, 100000);

        assertTrue(ps2::from_aqstr(QStringLiteral("100,000"), val));
        assertEquals(val, 100000);
    }
    static void TestDouble() noexcept {
        double val;
        assertTrue(ps2::from_aqstr(QStringLiteral("100"), val));
        assertDoubleEquals(val, 100);

        assertTrue(ps2::from_aqstr(QStringLiteral("550 000,0"), val));
        assertDoubleEquals(val, 550000);

        assertTrue(ps2::from_aqstr(QStringLiteral("100.000"), val));
        assertDoubleEquals(val, 100);

        assertTrue(ps2::from_aqstr(QStringLiteral("100,000"), val));
        assertDoubleEquals(val, 100.000);
    }

public:
    void operator()() noexcept {
        InvalidInt();
        TestInt();
        TestDouble();
    }
};

} // end namespace ps2
