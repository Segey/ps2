/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/test/db/dump/faked.h 
 * \brief     The SqliteFaked class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 20(th), 2017, 00:32 MSK
 * \updated   September (the) 20(th), 2017, 00:32 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class SqliteFaked {
public:
    using class_name = SqliteFaked;
     
private: 
    QString m_file;

public:
    void init() noexcept {
        m_file = FileName::temp();
        auto db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE")
                            , QStringLiteral("BuildSqliteBackupTest"));
        db.setDatabaseName(m_file);
        assertTrue(db.open());

        auto create = db.exec(QStringLiteral("CREATE TABLE test1(id integer primary key autoincrement, name text);"));
        if(create.lastError().isValid())
            assertFalse(create.lastError().isValid());

        create = db.exec(QStringLiteral("CREATE TABLE test2(id integer primary key autoincrement, name text);"));
        if(create.lastError().isValid())
            assertFalse(create.lastError().isValid());

        QSqlQuery q_insert(db);
        q_insert.prepare(QStringLiteral("INSERT INTO test1(id, name) VALUES (:id, :name)"));
        q_insert.bindValue(QStringLiteral(":id"), QVariant::fromValue(1));
        q_insert.bindValue(QStringLiteral(":name"), QStringLiteral("Volker"));
        assertTrue(q_insert.exec());

        q_insert.bindValue(QStringLiteral(":id"), QVariant::fromValue(2));
        q_insert.bindValue(QStringLiteral(":name"), QStringLiteral("Root"));
        assertTrue(q_insert.exec());

        QSqlQuery q_insert1(db);
        q_insert1.prepare(QStringLiteral("INSERT INTO test2(id, name) VALUES (:id, :name)"));
        q_insert1.bindValue(QStringLiteral(":id"), QVariant::fromValue(1));
        q_insert1.bindValue(QStringLiteral(":name"), QStringLiteral("Volker"));
        assertTrue(q_insert1.exec());

        create = db.exec(QStringLiteral("CREATE UNIQUE INDEX IDX_Test1_UniqueIndex ON Test2(id);"));
        if(create.lastError().isValid())
            assertFalse(create.lastError().isValid());

        create = db.exec(QStringLiteral("CREATE INDEX IDX_Test2_Index ON Test1(id,name);"));
        if(create.lastError().isValid())
            assertFalse(create.lastError().isValid());

        db.close();
        db = QSqlDatabase::database(QStringLiteral("disconnect"));
        QSqlDatabase::removeDatabase(QStringLiteral("BuildSqliteBackupTest"));
    }
    QString const& file() const {
        return m_file;
    }
};

}} // end namespace ps2::qt
