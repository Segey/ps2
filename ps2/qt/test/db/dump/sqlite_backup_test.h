/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/test/db/dump/sqlite_backup_test.h 
 * \brief     The Sqlite_backup_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 13(th), 2017, 00:17 MSK
 * \updated   September (the) 13(th), 2017, 00:17 MSK
 * \TODO      
**/
#pragma once
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <ps2/qt/objects/file_name.h>
#include <ps2/qt/db/dump/sqlite_backup.h>
#include <ps2/qt/db/sqlite_db.h>
#include <ps2/qt/algorithm.h>
#include "faked.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class BuildSqliteBackupTest final: SqliteFaked {
public:
    using class_name  = BuildSqliteBackupTest;
    using inherited   = SqliteFaked;

private:
    void Simple() const noexcept {
        auto _1 = ps2::db::SqliteBackup(inherited::file()).all().toCsv();
        assertEquals(_1.size(), 55);
        assertEquals(ps2::to_qstr(_1).size(), 55);
    }
    void Except() const noexcept {
        auto _1 = ps2::db::SqliteBackup(inherited::file()).all()
                .except("test1").toCsv();
        assertEquals(_1.size(), 24);
        assertEquals(ps2::to_sqstr(_1).size(), 24);
        assertEquals(_1.at(2), char(0x73));
        assertEquals(_1.at(14), char(0x31));
    }
    void Complex() const noexcept {
        auto _1 = ps2::db::SqliteBackup(inherited::file()).tables("test1").toCsv();
        assertEquals(_1.size(), 31);
        assertEquals(ps2::to_sqstr(_1).size(), 31);
        assertEquals(_1.at(4), char(0x31));
        assertEquals(_1.at(17), char(0x6f));
        assertEquals(_1.at(30), char(0x0a));
    }
    void ToCsvFile() const noexcept {
        auto const& f = FileName::temp();
        ps2::db::SqliteBackup(inherited::file()).all().toCsvFile(f);
        assertEquals(QFileInfo(f.name()).size(), qint64(55));
    }
    void ToSql() const noexcept {
        auto _1 = ps2::db::SqliteBackup(inherited::file()).all().toSql();
        assertEquals(_1.size(), 538);
        assertEquals(ps2::to_qstr(_1).size(), 538);
    }
    void ToSqlFile() const noexcept {
        auto const& f = FileName::temp();
        ps2::db::SqliteBackup(inherited::file()).all().toSqlFile(f);
        assertEquals(QFileInfo(f.name()).size(), qint64(538));
    }
    void ToBinSql() const noexcept {
        auto _1 = ps2::db::SqliteBackup(inherited::file()).all().toBinSql();
        assertEquals(_1.size(), 1113);
        assertEquals(ps2::to_qstr(_1).size(), 1113);
    }
    void ToBinSqlFile() const noexcept {
        auto const& f = FileName::temp();
        ps2::db::SqliteBackup(inherited::file()).all().toBinSqlFile(f);
        assertEquals(QFileInfo(f.name()).size(), qint64(1113));
    }

public:
    void operator()() noexcept {
        inherited::init();

        Simple();
        Except();
        Complex();
        ToCsvFile();
        ToSql();
        ToSqlFile();
        ToBinSql();
        ToBinSqlFile();
    }
};

}} // end namespace ps2::qt
