/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/test/db/dump/main.h 
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.1
 * \created   September (the) 13(th), 2017, 00:15 MSK
 * \updated   September (the) 17(th), 2017, 23:14 MSK
 * \TODO      
**/
#pragma once
#include "sqlite_backup_test.h"
#include "sqlite_restore_test.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class MainDbTestClasses final {
public:
    using class_name = MainDbTestClasses;

public:
    void operator()() noexcept {
        TEST(BuildSqliteBackupTest);
        TEST(BuildSqliteRestoreTest);
    }
};

}} // end namespace ps2::qt
