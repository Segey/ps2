/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/test/db/dump/sqlite_restore_test.h 
 * \brief     The Sqlite_restore_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   September (the) 17(th), 2017, 23:14 MSK
 * \updated   September (the) 17(th), 2017, 23:14 MSK
 * \TODO      
**/
#pragma once
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <ps2/qt/objects/file_name.h>
#include <ps2/qt/db/dump/sqlite_backup.h>
#include <ps2/qt/db/dump/sqlite_restore.h>
#include <ps2/qt/db/sqlite_db.h>
#include <ps2/qt/algorithm.h>
#include "faked.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class BuildSqliteRestoreTest final: SqliteFaked {
public:
    using class_name = BuildSqliteRestoreTest;
    using inherited   = SqliteFaked;

private:
    QString binSqlFile() const noexcept {
        auto const& f = FileName::temp();
        ps2::db::SqliteBackup(inherited::file()).all().toBinSqlFile(f);
        return f.name();
    }
    QString sqlFile() const noexcept {
        auto const& f = FileName::temp();
        ps2::db::SqliteBackup(inherited::file()).all().toSqlFile(f);
        return f.name();
    }

private:
    void BinSqlFile() const noexcept {
        auto const& out = FileName::temp();
        auto const _1 = ps2::db::SqliteRestore(binSqlFile(), ps2::db::BackupRestoreType::BinSql)
                .toFile(out.name());
        assertTrue(_1);
    }
    void SqlFile() const noexcept {
        auto const& out = FileName::temp();
        auto const _1 = ps2::db::SqliteRestore(sqlFile(), ps2::db::BackupRestoreType::Sql)
                .toFile(out.name());
        assertTrue(_1);
    }
    void BinSql() const noexcept {
        auto const& out = FileName::temp();
        auto const& data = ps2::db::SqliteBackup(inherited::file()).all().toBinSql();
        auto const _1 = ps2::db::SqliteRestore(data, ps2::db::BackupRestoreType::BinSql)
                .toFile(out.name());
        assertTrue(_1);
    }
    void Sql() const noexcept {
        auto const& out = FileName::temp();
        auto const& data = ps2::db::SqliteBackup(inherited::file()).all().toSql();
        auto const _1 = ps2::db::SqliteRestore(data, ps2::db::BackupRestoreType::Sql)
                .toFile(out.name());
        assertTrue(_1);
    }

public:
    void operator()() noexcept {
        inherited::init();

        BinSqlFile();
        SqlFile();
        BinSql();
        Sql();
    }
};

}} // end namespace ps2::qt
