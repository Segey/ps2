/**
 * \file      ps2/ps2/qt/test/literal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 15:06 MSK
 * \updated   January (the) 19(th), 2017, 15:06 MSK
 * \TODO      
**/
#pragma once
#include "qstring_test.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {
    
class LiteralsMainTestClasses {
public:
    using class_name = LiteralsMainTestClasses;

public:
    void operator()() noexcept {
        TEST(QStringLiteralTest);
    }
};

}} // end namespace ps2::qt
