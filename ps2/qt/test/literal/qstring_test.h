/**
 * \file      ps2/ps2/qt/test/literal/qstring_test.h
 * \brief     The Udl_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   October (the) 31(th), 2016, 12:42 MSK
 * \updated   January (the) 19(th), 2017, 15:07 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include <ps2/qt/literal/literals.h>

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class QStringLiteralTest {
public:
    using class_name = QStringLiteralTest;

private:
    static void Utf8Test() noexcept {
        assertEquals("привет"_utf8, QString::fromUtf8("привет"));
    }
    static void Latin1Test() noexcept {
        assertEquals("cool"_latin1, QString::fromLatin1("cool"));
    }

public:
    void operator()() noexcept {
        Utf8Test();
        Latin1Test();
    }
};

}} // end namespace ps2::qt
