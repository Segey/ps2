/**
 * \file      ps2/ps2/qt/test/objects/link_test.h
 * \brief     The Link_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 17(th), 2017, 13:53 MSK
 * \updated   January (the) 17(th), 2017, 13:53 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/objects/link.h>

/** \namespace ps2 */
namespace ps2 {
    
class LinkTest final {
public:
    using class_name = LinkTest;

private:
    static void Simple() noexcept {
        Link link;
        assertTrue(link.url().isEmpty());
        assertTrue(link.text().isEmpty());
        assertEquals(link.toHtml(), QStringLiteral("<a href=\"\"></a>"));
    }
    static void Complex() noexcept {
        Link link(QUrl(QStringLiteral("http://irondoom.ru"))
                  , QStringLiteral("It's me"));
        assertFalse(link.url().isEmpty());
        assertEquals(link.text(), QStringLiteral("It's me"));
        assertEquals(link.toHtml(), QStringLiteral("<a href=\"http://irondoom.ru\">It's me</a>"));
    }
    static void Swap() noexcept {
        Link _1(QUrl(QStringLiteral("en")), QStringLiteral("_1"));
        Link _2(QUrl(QStringLiteral("ru")), QStringLiteral("_2"));

        swap(_1, _2);
        assertEquals(_1, Link(QUrl(QStringLiteral("ru")), QStringLiteral("_2")));
        assertEquals(_2, Link(QUrl(QStringLiteral("en")), QStringLiteral("_1")));
    }
    static void EmptyParseLink() noexcept {
        auto _2 = Link::fromString(QString{});
        assertTrue(_2.isEmpty());
    }
    static void InvalidParseLink() noexcept {
        auto&& _2 = Link::fromString(QStringLiteral("<a></a>"));
        assertTrue(_2.isEmpty());
    }
    static void ValidParseLink() noexcept {
        Link _1(QUrl(QStringLiteral("ya.ru")), QStringLiteral("site"));
        auto&& _2 = Link::fromString(QStringLiteral("<a href=\"ya.ru\">site</a>"));
        assertEquals(_2, _1);
    }

public:
    void operator()() noexcept {
        Simple();
        Complex();
        Swap();
        EmptyParseLink();
        InvalidParseLink();
        ValidParseLink();
    }
};

} // end namespace ps2
