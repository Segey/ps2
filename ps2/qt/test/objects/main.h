/**
 * \file      ps2/ps2/qt/test/objects/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January   (the) 17(th), 2017, 13:53 MSK
 * \updated   March     (the) 26(th), 2017, 00:04 MSK
 * \TODO      
**/
#pragma once
#include "link_test.h"
#include "numeric/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainObjectsTest final {
public:
    using class_name = MainObjectsTest;

public:
    void operator()() noexcept {
        TEST(LinkTest);
        TEST_CLASSES(MainNumericTest);
    }
};

} // end namespace ps2
