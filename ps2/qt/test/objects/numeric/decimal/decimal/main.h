/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 23:29 MSK
 * \updated   April     (the) 05(th), 2017, 23:29 MSK
 * \TODO      
**/
#pragma once
#include "decimal_english_test.h"
#include "decimal_german_test.h"
#include "decimal_russian_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainBaseDecimalTest final {
public:
    using class_name = MainBaseDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::DecimalRussianTest);
        TEST(qt::DecimalEnglishTest);
        TEST(qt::DecimalGermanTest);
    }
};

} // end namespace ps2
