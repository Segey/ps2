/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/decimal/decimal_russian_test.h
 * \brief     The Convert_algo_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 24(th), 2017, 01:16 MSK
 * \updated   March     (the) 24(th), 2017, 01:16 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/objects/numeric/decimal.h>
#include "lib/locale.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class DecimalRussianTest final: private test::Locale {
public:
    using class_name = DecimalRussianTest;
    using inherited  = test::Locale;

private:
    static void Invalid() noexcept {
        auto const& _1 = ps2::Decimal<int, 3>::fromString(QStringLiteral("100.456"));
        assertFalse(_1.isValid());
    }
    static void Simple() noexcept {
        auto const& _1 = ps2::Decimal<int, 3>::fromString(QStringLiteral("100,456"));
        assertEquals(_1, (ps2::Decimal<int, 3>(100, 456)));
    }
    static void Simple2() noexcept {
        auto const& _1 = ps2::Decimal<int, 4>::fromString(QStringLiteral("00,01"));
        assertEquals(_1, (ps2::Decimal<int, 4>(0, 100)));
    }
    static void WholePart() noexcept {
        auto const& _1 = ps2::Decimal<int, 3>::fromString(QStringLiteral("100 456"));
        assertEquals(_1, (ps2::Decimal<int, 3>(100456,0)));
    }
    static void OnlyFraction() noexcept {
        auto const& _1 = ps2::Decimal<int, 5>::fromString(QStringLiteral(",450"));
        assertEquals(_1, (ps2::Decimal<int, 5>(0,45000)));
    }
    static void OnlyFraction2() noexcept {
        auto const& _1 = ps2::Decimal<int, 5>::fromString(QStringLiteral(",0"));
        assertEquals(_1, (ps2::Decimal<int, 5>(0,0)));
    }
    static void OnlyFraction3() noexcept {
        auto const& _1 = ps2::Decimal<int, 5>::fromString(QStringLiteral(","));
        assertFalse(_1.isValid());
    }
    static void Complex() noexcept {
        auto const& _1 = ps2::Decimal<int, 1>::fromString(QStringLiteral("352220,1"));
        assertEquals(_1, (ps2::Decimal<int, 1>(352220, 1)));
    }
    static void ComplexWithSpaces() noexcept {
        auto const& _1 = ps2::Decimal<int, 2>::fromString(QStringLiteral("1 352 220,01"));
        assertEquals(_1, (ps2::Decimal<int, 2>(1'352'220, 1)));
    }
    static void ToDecimalSimple() noexcept {
        ps2::Decimal<int, 3> _1(10,2);
        assertEquals(_1.toString(), QStringLiteral("10,002"));
    }
    static void ToDecimalComplex() noexcept {
        ps2::Decimal<int, 4> _1(100000,20);
        assertEquals(inherited::updateNBSpaceToSpace(_1.toString())
                     , QStringLiteral("100 000,0020"));
    }
    static void ToDoubleSimple() noexcept {
        ps2::Decimal<int, 3> _1(10,2);
        assertDoubleEquals(_1.toDouble(), 10.002);
    }
    static void ToDoubleDecimal() noexcept {
        ps2::Decimal<int, 4> _1(100000,20);
        assertDoubleEquals(_1.toDouble(), 100'000.0020);
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        Invalid();
        Simple();
        Simple2();
        WholePart();
        OnlyFraction();
        OnlyFraction2();
        OnlyFraction3();
        Complex();
        ComplexWithSpaces();
        ToDecimalSimple();
        ToDecimalComplex();
        ToDoubleSimple();
        ToDoubleDecimal();

        AfterTest();
    }
};

}} // end namespace ps2::qt
