/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/miyd_decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May       (the) 03(th), 2017, 17:18 MSK
 * \updated   May       (the) 03(th), 2017, 17:18 MSK
 * \TODO      
**/
#pragma once
#include "miyd_decimal_russian_test.h"
#include "miyd_decimal_english_test.h"
#include "miyd_decimal_germany_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainMiYdDecimalTest final {
public:
    using class_name = MainMiYdDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::MiYdDecimalRussianTest);
        TEST(qt::MiYdDecimalEnglishTest);
        TEST(qt::MiYdDecimalGermanyTest);
    }
};

} // end namespace ps2
