/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/miyd_decimal/miyd_decimal_russian_test.h
 * \brief     The Miyd_decimal_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May       (the) 03(th), 2017, 17:17 MSK
 * \updated   May       (the) 03(th), 2017, 17:17 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/objects/numeric/imperial_decimals.h>
#include "lib/locale.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class MiYdDecimalRussianTest final: private test::Locale {
public:
    using class_name = MiYdDecimalRussianTest;
    using inherited  = test::Locale;
    using tested     = ps2::MiYdDecimal;

private:
    static void Swap() noexcept {
        auto _1 = ps2::MiYdDecimal::fromString(QStringLiteral("10,0006"));
        auto _2 = ps2::MiYdDecimal::fromString(QStringLiteral("5,0004"));
        swap(_1, _2);
        assertEquals(_1, (ps2::MiYdDecimal(5, 4)));
        assertEquals(_2, (ps2::MiYdDecimal(10, 6)));
    }
    static void Invalid() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral("100.4562"));
        assertFalse(_1.isValid());
    }
    static void Simple() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral("100,06"));
        assertEquals(_1, (ps2::MiYdDecimal(100, 600)));
        auto const& _2 = ps2::MiYdDecimal::fromString(QStringLiteral("00,001"));
        assertEquals(_2, (ps2::MiYdDecimal(0,10)));
    }
    static void WholePart() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral("100 457"));
        assertEquals(_1, (ps2::MiYdDecimal(100457,0)));
    }
    static void InvalidOnlyFraction() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral(",1978"));
        assertFalse(_1.isValid());
    }
    static void OnlyFraction() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral(",15"));
        assertEquals(_1, (ps2::MiYdDecimal(0,1500)));
    }
    static void OnlyFraction2() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral(",0"));
        assertEquals(_1, (ps2::MiYdDecimal(0,0)));
    }
    static void OnlyFraction3() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral(","));
        assertFalse(_1.isValid());
    }
    static void InvalidComplex() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral("352220,00001"));
        assertFalse(_1.isValid());
    }
    static void ComplexWithSpaces() noexcept {
        auto const& _1 = ps2::MiYdDecimal::fromString(QStringLiteral("1 352 220,0001"));
        assertEquals(_1, (ps2::MiYdDecimal(1352220, 1)));
    }
    static void ToDecimalSimple() noexcept {
        ps2::MiYdDecimal _1(10,2);
        assertEquals(_1.toString(), QStringLiteral("10,0002"));

        ps2::MiYdDecimal _2(55,1005);
        assertEquals(_2.toString(), QStringLiteral("55,1005"));
    }
    static void ToDecimalComplex() noexcept {
        ps2::MiYdDecimal _1(100000,15);
        assertEquals(inherited::updateNBSpaceToSpace(_1.toString())
                     , QStringLiteral("100 000,0015"));
    }
    static void ToDoubleSimple() noexcept {
        tested _1(10,2);
        assertDoubleEquals(_1.toDouble(), 10.02);
    }
    static void ToDoubleDecimal() noexcept {
        tested _1(100000,15);
        assertDoubleEquals(_1.toDouble(), 100'000.15);
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        Swap();
        Invalid();
        Simple();
        WholePart();
        InvalidOnlyFraction();
        OnlyFraction();
        OnlyFraction2();
        OnlyFraction3();
        InvalidComplex();
        ComplexWithSpaces();
        ToDecimalSimple();
        ToDecimalComplex();
        ToDoubleSimple();
        ToDoubleDecimal();

        AfterTest();
    }
};

}} // end namespace ps2::qt
