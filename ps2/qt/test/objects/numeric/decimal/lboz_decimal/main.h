/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/lboz_decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 23:37 MSK
 * \updated   April     (the) 05(th), 2017, 23:37 MSK
 * \TODO      
**/
#pragma once
#include "lboz_decimal_russian_test.h"
#include "lboz_decimal_english_test.h"
#include "lboz_decimal_germany_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainLbOzDecimalTest final {
public:
    using class_name = MainLbOzDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::LbOzDecimalRussianTest);
        TEST(qt::LbOzDecimalEnglishTest);
        TEST(qt::LbOzDecimalGermanyTest);
    }
};

} // end namespace ps2
