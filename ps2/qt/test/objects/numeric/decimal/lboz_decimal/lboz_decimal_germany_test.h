/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/lboz_decimal/lboz_decimal_germany_test.h
 * \brief     The Lboz_decimal_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 12:56 MSK
 * \updated   March     (the) 30(th), 2017, 12:56 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/objects/numeric/imperial_decimals.h>
#include "lib/locale.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class LbOzDecimalGermanyTest final: private test::Locale {
public:
    using class_name = LbOzDecimalGermanyTest;
    using inherited  = test::Locale;
    using tested     = ps2::LbOzDecimal;

private:
    static void Swap() noexcept {
        auto _1 = ps2::LbOzDecimal::fromString(QStringLiteral("10,0"));
        auto _2 = ps2::LbOzDecimal::fromString(QStringLiteral("5,4"));
        swap(_1, _2);
        assertEquals(_1, (ps2::LbOzDecimal(5, 4)));
        assertEquals(_2, (ps2::LbOzDecimal(10, 0)));
    }
    static void Invalid() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("100 456"));
        assertFalse(_1.isValid());
    }
    static void Simple() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("100,6"));
        assertEquals(_1, (ps2::LbOzDecimal(100, 6)));
    }
    static void Simple2() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("00,01"));
        assertEquals(_1, (ps2::LbOzDecimal(0,1)));
    }
    static void WholePart() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("100.456"));
        assertEquals(_1, (ps2::LbOzDecimal(100456,0)));
    }
    static void InvalidOnlyFraction() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral(",16"));
        assertFalse(_1.isValid());
    }
    static void OnlyFraction() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral(",15"));
        assertEquals(_1, (ps2::LbOzDecimal(0,15)));
    }
    static void OnlyFraction2() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral(",0"));
        assertEquals(_1, (ps2::LbOzDecimal(0,0)));
    }
    static void OnlyFraction3() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral(","));
        assertFalse(_1.isValid());
    }
    static void InvalidComplex() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("352220,001"));
        assertFalse(_1.isValid());
    }
    static void ComplexWithSpaces() noexcept {
        auto const& _1 = ps2::LbOzDecimal::fromString(QStringLiteral("1.352.220,01"));
        assertEquals(_1, (ps2::LbOzDecimal(1352220, 1)));
    }
    static void ToDecimalSimple() noexcept {
        ps2::LbOzDecimal _1(10,2);
        assertEquals(_1.toString(), QStringLiteral("10,02"));
    }
    static void ToDecimalComplex() noexcept {
        ps2::LbOzDecimal _1(100000,15);
        assertEquals(_1.toString(), QStringLiteral("100.000,15"));
    }
    static void ToDoubleSimple() noexcept {
        tested _1(10,2);
        assertDoubleEquals(_1.toDouble(), 10.02);
    }
    static void ToDoubleDecimal() noexcept {
        tested _1(100000,15);
        assertDoubleEquals(_1.toDouble(), 100'000.15);
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        Swap();
        Invalid();
        Simple();
        Simple2();
        WholePart();
        InvalidOnlyFraction();
        OnlyFraction();
        OnlyFraction2();
        OnlyFraction3();
        InvalidComplex();
        ComplexWithSpaces();
        ToDecimalSimple();
        ToDecimalComplex();
        ToDoubleSimple();
        ToDoubleDecimal();

        AfterTest();
    }
};

}} // end namespace ps2::qt
