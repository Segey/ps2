/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/kggr_decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 23:33 MSK
 * \updated   April     (the) 05(th), 2017, 23:33 MSK
 * \TODO      
**/
#pragma once
#include "kggr_decimal_russian_test.h"
#include "kggr_decimal_english_test.h"
#include "kggr_decimal_germany_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainKgGrDecimalTest final {
public:
    using class_name = MainKgGrDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::KgGrDecimalRussianTest);
        TEST(qt::KgGrDecimalEnglishTest);
        TEST(qt::KgGrDecimalGermanyTest);
    }
};

} // end namespace ps2
