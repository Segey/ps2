/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.5
 * \created   March     (the) 30(th), 2017, 12:49 MSK
 * \updated   April     (the) 06(th), 2017, 00:12 MSK
 * \TODO      
**/
#pragma once
#include "decimal/main.h"
#include "kggr_decimal/main.h"
#include "lboz_decimal/main.h"
#include "miyd_decimal/main.h"
#include "mcm_decimal/main.h"
#include "cm_decimal/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainDecimalTest final {
public:
    using class_name = MainDecimalTest;

public:
    void operator()() noexcept {
        TEST_CLASSES(MainBaseDecimalTest);
        TEST_CLASSES(MainKgGrDecimalTest);
        TEST_CLASSES(MainLbOzDecimalTest);
        TEST_CLASSES(MainMiYdDecimalTest);
        TEST_CLASSES(MainCmDecimalTest);
        TEST_CLASSES(MainMCmDecimalTest);
    }
};

} // end namespace ps2
