/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/cm_decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 22:49 MSK
 * \updated   April     (the) 24(th), 2017, 22:49 MSK
 * \TODO      
**/
#pragma once
#include "cm_decimal_russian_test.h"
#include "cm_decimal_english_test.h"
#include "cm_decimal_germany_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainCmDecimalTest final {
public:
    using class_name = MainCmDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::CmDecimalRussianTest);
        TEST(qt::CmDecimalEnglishTest);
        TEST(qt::CmDecimalGermanyTest);
    }
};

} // end namespace ps2
