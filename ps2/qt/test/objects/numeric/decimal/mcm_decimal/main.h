/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/mcm_decimal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 23:33 MSK
 * \updated   April     (the) 05(th), 2017, 23:33 MSK
 * \TODO      
**/
#pragma once
#include "mcm_decimal_russian_test.h"
#include "mcm_decimal_english_test.h"
#include "mcm_decimal_germany_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainMCmDecimalTest final {
public:
    using class_name = MainMCmDecimalTest;

public:
    void operator()() noexcept {
        TEST(qt::MCmDecimalRussianTest);
        TEST(qt::MCmDecimalEnglishTest);
        TEST(qt::MCmDecimalGermanyTest);
    }
};

} // end namespace ps2
