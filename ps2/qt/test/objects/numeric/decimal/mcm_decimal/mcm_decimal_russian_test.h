/**
 * \file      ps2/ps2/qt/test/objects/numeric/decimal/mcm_decimal/mcm_decimal_russian_test.h
 * \brief     The Lboz_decimal_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 12:56 MSK
 * \updated   March     (the) 30(th), 2017, 12:56 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/objects/numeric/metric_decimals.h>
#include "lib/locale.h"

/** \namespace ps2::qt */
namespace ps2 {
namespace qt {

class MCmDecimalRussianTest final: private test::Locale {
public:
    using class_name = MCmDecimalRussianTest;
    using inherited  = test::Locale;
    using tested     = ps2::MCmDecimal;

private:
    static void Swap() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("10,6"), _1));

        tested _2;
        assertTrue(ps2::from_qstr(QStringLiteral("5,4"), _2));

        swap(_1, _2);
        assertEquals(_1, (tested(5, 40)));
        assertEquals(_2, (tested(10, 60)));
    }
    static void Invalid() noexcept {
        tested _1;
        assertFalse(ps2::from_qstr(QStringLiteral("100.45"), _1));
    }
    static void Simple() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("100,6"), _1));
        assertEquals(_1, (tested(100, 60)));
    }
    static void Simple2() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("00,01"), _1));
        assertEquals(_1, (tested(0,01)));
    }
    static void WholePart() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("100 456"), _1));
        assertEquals(_1, (tested(100456,0)));
    }
    static void InvalidOnlyFraction() noexcept {
        tested _1;
        assertFalse(ps2::from_qstr(QStringLiteral(",1001"), _1));
    }
    static void OnlyFraction() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("0,99"), _1));
        assertEquals(_1, (tested(0,99)));
    }
    static void OnlyFraction2() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("0,0"), _1));
        assertEquals(_1, (tested(0,0)));
    }
    static void OnlyFraction3() noexcept {
        tested _1;
        assertFalse(ps2::from_qstr(QStringLiteral(","), _1));
    }
    static void InvalidComplex() noexcept {
        tested _1;
        assertFalse(ps2::from_qstr(QStringLiteral("352220,0001"), _1));
    }
    static void ComplexWithSpaces() noexcept {
        tested _1;
        assertTrue(ps2::from_qstr(QStringLiteral("1 352 220,1"), _1));
        assertEquals(_1, (tested(1352220, 10)));
    }
    static void ToDecimalSimple() noexcept {
        tested _1(10,2);
        assertEquals(ps2::to_qstr(_1), QStringLiteral("10,02"));
    }
    static void ToDecimalComplex() noexcept {
        tested _1(100000,05);
        assertEquals(inherited::updateNBSpaceToSpace(ps2::to_qstr(_1))
                     , QStringLiteral("100 000,05"));
    }
    static void ToDoubleSimple() noexcept {
        tested _1(10,2);
        assertDoubleEquals(static_cast<double>(_1), 10.02);
    }
    static void ToDoubleDecimal() noexcept {
        tested _1(100000,15);
        assertDoubleEquals(static_cast<double>(_1), 100000.15);
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        Swap();
        Invalid();
        Simple();
        Simple2();
        WholePart();
        InvalidOnlyFraction();
        OnlyFraction();
        OnlyFraction2();
        OnlyFraction3();
        InvalidComplex();
        ComplexWithSpaces();
        ToDecimalSimple();
        ToDecimalComplex();
        ToDoubleSimple();
        ToDoubleDecimal();

        AfterTest();
    }
};

}} // end namesace ps2::qt
