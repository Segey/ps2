/**
 * \file      ps2/ps2/qt/test/objects/numeric/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 26(th), 2017, 00:00 MSK
 * \updated   March     (the) 26(th), 2017, 00:00 MSK
 * \TODO      
**/
#pragma once
#include "decimal/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainNumericTest final {
public:
    using class_name = MainNumericTest;

public:
    void operator()() noexcept {
        TEST_CLASSES(MainDecimalTest);
    }
};

} // end namespace ps2
