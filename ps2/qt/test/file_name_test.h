/**
 * \file      ps2/ps2/qt/test/file_name_test.h
 * \brief     File test
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   May (the) 06(th), 2015, 15:13 MSK
 * \updated   May (the) 06(th), 2015, 15:13 MSK
 * \TODO
**/
#pragma once
#include <QDebug>
#include <ps2/qt/objects/file_name.h>

class FileNameTest {
public:
    using class_name = FileNameTest;
    using tested     = ps2::FileName;

private:
    static void emptyFileNameAfterCreation() noexcept {
        tested file;
        assertTrue(file.name().isEmpty());
    }
    static void noZeroTempFileNameAfterTempCreation() noexcept {
        auto const& file = tested::temp();

        assertFalse( file.name().isEmpty());
    }
    static void correctTempFileNameAfterTempCreation() noexcept {
        auto const& file = tested::temp();

#ifndef Q_OS_LINUX
        assertFalse( file.name().contains(QChar::fromLatin1('/')));
#else
        assertTrue( file.name().contains(QChar::fromLatin1('/')));
#endif
    }
    static void passedNameAfterTempCreation() noexcept {
        auto const& file = tested::temp(QStringLiteral("cool.db"));
        assertTrue( file.name().endsWith(QStringLiteral("cool.db")));
    }

public:
    void operator()() noexcept {
        emptyFileNameAfterCreation();
        noZeroTempFileNameAfterTempCreation();
        correctTempFileNameAfterTempCreation();
        passedNameAfterTempCreation();
    }
};
