/**
 * \file      ps2/ps2/qt/test/date_range_test.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February (the) 29(th), 2016, 22:41 MSK
 * \updated   February (the) 29(th), 2016, 22:41 MSK
 * \TODO
**/
#pragma once
#include <QDate>
#include <ps2/qt/objects/date_range.h>

class DateRangeTest {
public:
    using class_name = DateRangeTest;
    using tested     =  ps2::DateRange;

private:
    static void TodayDateRange() noexcept {
        auto const& range = tested::today();
        assertEquals(range.start(), QDate::currentDate());
        assertEquals(range.end(), QDate::currentDate());
    }
    static void TodayDateRangeWithAnotherDate() noexcept {
        auto const& day = QDate(2012, 12, 1);
        auto const& range = tested::today(day);
        assertEquals(range.start(), day);
        assertEquals(range.end(), day);
    }
    static void YesterdayDateRange() noexcept {
        auto const& range = tested::yesterday();
        assertEquals(range.start(), QDate::currentDate().addDays(-1));
        assertEquals(range.end(), QDate::currentDate());
    }
    static void YesterdayDateRangeWithAnotherDate() noexcept {
        auto const& day = QDate(2012, 12, 1);
        auto const& range = tested::yesterday(day);
        assertEquals(range.start(), QDate(2012, 11, 30));
        assertEquals(range.end(), day);
    }
    static void CurrentWeek1() noexcept {
        auto const& day = QDate(2012, 12, 1);
        auto const& range = tested::currentWeek(day);

        assertEquals(range.start(), QDate(2012, 11, 26));
        assertEquals(range.end(), day);
    }
    static void CurrentWeek2() noexcept {
        auto&& day = QDate(2016, 2, 1);
        auto const& range = tested::currentWeek(day);

        assertEquals(range.start(), QDate(2016, 2, 1));
        assertEquals(range.end(), day);
    }
    static void CurrentWeek3() noexcept {
        auto&& day = QDate(2015, 10, 1);
        auto const& range = tested::currentWeek(day);

        assertEquals(range.start(), QDate(2015, 9, 28));
        assertEquals(range.end(), day);
    }
    static void CurrentMonth1() noexcept {
        auto&& day = QDate(2012, 12, 1);
        auto const& range = tested::currentMonth(day);

        assertEquals(range.start(), QDate(2012, 12, 1));
        assertEquals(range.end(), day);
    }
    static void CurrentMonth2() noexcept {
        auto const& day = QDate(2016, 2, 13);
        auto const& range = tested::currentMonth(day);

        assertEquals(range.start(), QDate(2016, 2, 1));
        assertEquals(range.end(), day);
    }
    static void CurrentMonth3() noexcept {
        auto const& day = QDate(2015, 10, 30);
        auto const& range = tested::currentMonth(day);

        assertEquals(range.start(), QDate(2015, 10, 1));
        assertEquals(range.end(), day);
    }
    static void PriorWeek1() noexcept {
        auto const& day = QDate(2012, 12, 1);
        auto const& range = tested::priorWeek(day);

        assertEquals(range.start(), QDate(2012, 11, 19));
        assertEquals(range.end(), day);
    }
    static void PriorWeek2() noexcept {
        auto const& day = QDate(2016, 1, 5);
        auto const& range = tested::priorWeek(day);

        assertEquals(range.start(), QDate(2015, 12, 28));
        assertEquals(range.end(), day);
    }
    static void PriorWeek3() noexcept {
        auto const& day = QDate(2015, 10, 1);
        auto const& range = tested::priorWeek(day);

        assertEquals(range.start(), QDate(2015, 9, 21));
        assertEquals(range.end(), day);
    }
    static void PriorMonth1() noexcept {
        auto const& day = QDate(2012, 12, 1);
        auto const& range = tested::priorMonth(day);

        assertEquals(range.start(), QDate(2012, 11, 1));
        assertEquals(range.end(), day);
    }
    static void PriorMonth2() noexcept {
        auto const& day = QDate(2016, 2, 13);
        auto const& range = tested::priorMonth(day);

        assertEquals(range.start(), QDate(2016, 1, 1));
        assertEquals(range.end(), day);
    }
    static void PriorMonth3() noexcept {
        auto const& day = QDate(2016, 1, 30);
        auto const& range = tested::priorMonth(day);

        assertEquals(range.start(), QDate(2015, 12, 1));
        assertEquals(range.end(), day);
    }

public:
    void operator()() noexcept {
        TodayDateRange();
        TodayDateRangeWithAnotherDate();
        YesterdayDateRange();
        YesterdayDateRangeWithAnotherDate();
        CurrentWeek1();
        CurrentWeek2();
        CurrentWeek3();
        CurrentMonth1();
        CurrentMonth2();
        CurrentMonth3();
        PriorWeek1();
        PriorWeek2();
        PriorWeek3();
        PriorMonth1();
        PriorMonth2();
        PriorMonth3();
    }
};

