/**
 * \file      ps2/ps2/qt/test/string_test.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 03(th), 2016, 17:13 MSK
 * \updated   March (the) 03(th), 2016, 17:13 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/convert/string.h>

class QStringTest {
public:
    using class_name = QStringTest;

private:
    static void ToPlainText() {
        auto const& s = QStringLiteral("<i>Test:</i><img src=\"blah.png\" /><br> A test case");

        assertEquals( QStringLiteral("Test: A test case"), ps2::toPlainText(s));
    }
    static void ToPlainText2() {
        auto const& s = QStringLiteral("<body><i>Test:</i><img src=\"blah.png\" /><br> A test case</body>");

        assertEquals( QStringLiteral("Test: A test case"), ps2::toPlainText(s));
    }
    static void extractHtmlBody() {
        auto const& s = QStringLiteral("<body font=\"ddd\"><i>Test:</i><img src=\"blah.png\" /><br> A test case</body>");
        assertEquals( QStringLiteral("<body><i>Test:</i><img src=\"blah.png\" /><br> A test case</body>"), ps2::html::extractBody(s));
    }
    static void extractHtmlBody2() {
        auto const& s = QStringLiteral("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Courier'; font-size:10pt; color:#aa00aa;\">libpng warning: iCCP:</span></p></body></html>");

        assertEquals( QStringLiteral("<body>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Courier'; font-size:10pt; color:#aa00aa;\">libpng warning: iCCP:</span></p></body>"), ps2::html::extractBody(s));
    }
    static void NotEmptyStringIfBodyAlmostEmpty() {
        auto s = QStringLiteral("<body>\n<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body>");
        s = ps2::toPlainText(s);

        assertEquals(s, QStringLiteral("\n"));
    }
    static void EmptyStringIfBodyAlmostEmpty() {
        auto s = QStringLiteral("<body>\n<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body>");
        s = ps2::html::extractNoEmptyBody(s);

        assertTrue(s.isEmpty());
    }
    static void NotEmptyStringIfBodyContaintsAnInteger() {
        auto const& str = QStringLiteral("<body>\n<p style=\"-qt-paragraph-type:empty;\"><br /></p>9</body>");
        assertEquals(str, ps2::html::extractNoEmptyBody(str));
    }
    static void NotEmptyStringIfBodyContaintsAChar() {
        auto const& str = QStringLiteral("<body>ы\n<p style=\"-qt-paragraph-type:empty;\"><br /></p></body>");
        assertEquals(str, ps2::html::extractNoEmptyBody(str));
    }
    static void EmptyStringIfBodyContaintsSpaces() {
        auto const& str = QStringLiteral("<body>    \n<p style=\"-qt-paragraph-type:empty;\"><br /></p>    </body>");
        assertEquals(ps2::html::extractNoEmptyBody(str), QString());
    }
    static void TruncateIfStringLengthMore() {
        assertEquals(QStringLiteral("cool..."), ps2::truncate(QStringLiteral("cool-bool"), 7));
    }
    static void TruncateIfStringLengthLess() {
        assertEquals(QStringLiteral("cool-bool"), ps2::truncate(QStringLiteral("cool-bool"), 10));
    }
    static void TruncateIfStringLengthEnought() {
        assertEquals(QStringLiteral("cool-..."), ps2::truncate(QStringLiteral("cool-bool"), 8));
    }


public:
    void operator()() {
        ToPlainText();
        ToPlainText2();
        extractHtmlBody();
        extractHtmlBody2();
        NotEmptyStringIfBodyAlmostEmpty();
        EmptyStringIfBodyAlmostEmpty();
        NotEmptyStringIfBodyContaintsAnInteger();
        NotEmptyStringIfBodyContaintsAChar();
        EmptyStringIfBodyContaintsSpaces();
        TruncateIfStringLengthMore();
        TruncateIfStringLengthLess();
        TruncateIfStringLengthEnought();
    }
};

