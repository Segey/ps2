/**
 * \file      ps2/ps2/qt/test/main.h
 * \brief     MainQt test class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.11
 * \created   May       (the) 07(th), 2015, 15:44 MSK
 * \updated   September (the) 13(th), 2017, 00:14 MSK
 * \TODO
**/
#pragma once
#include <ps2/thetest/qt/helpers.h>
#include "file_name_test.h"
#include "version_test.h"
#include "date_range_test.h"
#include "string_test.h"
#include "email_validator_test.h"
#include "array/main.h"
#include "objects/main.h"
#include "widgets/main.h"
#include "../libs/test/main.h"
#include "literal/main.h"
#include "locale_test.h"
#include "from_aqstr.h"
#include "db/dump/main.h"
#include "shim_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainQtTest final {
public:
    using class_name = MainQtTest;

public:
    void operator()() noexcept {
        TEST(LocaleTest);
        TEST(FromAqstr);
        TEST(FileNameTest);
        TEST(VersionTest);
        TEST(DateRangeTest);
        TEST(ShimTest);
        TEST(QStringTest);
        TEST(EmailValidatorTest);

        TEST_CLASSES(MainWidgetsTest);
        TEST_CLASSES(MainObjectsTest);
        TEST_CLASSES(MainArraysTest);
        TEST_CLASSES(MainLibsTest);
        TEST_CLASSES(ps2::qt::LiteralsMainTestClasses);
        TEST_CLASSES(ps2::qt::MainDbTestClasses);
    }
};

} // end namespace ps2
