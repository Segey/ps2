/**
 * \file      ps2/ps2/qt/test/array/field_test.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 28(th), 2016, 11:58 MSK
 * \updated   January (the) 28(th), 2016, 11:58 MSK
 * \TODO
**/
#pragma once
#include <list>
#include <ps2/qt/array/field.h>

class FieldTest {
public:
    using class_name = FieldTest;
    using tested     = ps2::Field<int>;
    using status_t   = tested::status_t;

private:
    static const status_t invalid = status_t::Invalid;
    static const status_t pure    = status_t::Pure;
    static const status_t added   = status_t::Added;
    static const status_t edited  = status_t::Edited;
    static const status_t removed = status_t::Removed;

    static tested testWalkBy() noexcept {
        tested arr;
        arr.init({
              {added, 3}
            , {edited, 4}
            , {pure, 5}
            , {added, 6}
        });

        arr.remove(0u);
        arr.remove(1u);
        arr.addPure(7);
        return arr;
    }

private:
    static void Init() noexcept {
        tested arr;

        assertEquals(arr.size(), 0);
        assertTrue(arr.isEmpty());
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 0);
    }
    static void Swap() noexcept {
        tested _1;
        _1.addPure(33);

        tested _2 = testWalkBy();
        swap(_1, _2);

        assertEquals(_1.size(), 4);
        assertEquals(_1.available_size(), 3);
        assertEquals(_1.unavailable_size(), 1);

        assertEquals(_2.size(), 1);
        assertEquals(_2.available_size(), 1);
        assertEquals(_2.unavailable_size(), 0);
    }
    static void Add() noexcept {
        tested arr;
        arr.add(1);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 1);
        assertEquals(arr.unavailable_size(), 0);
        assertEquals(arr[0].status, status_t::Added);
        assertEquals(arr[0].value, 1);
    }
    static void AddEdit() noexcept {
        tested arr;
        arr.add(5);
        arr.edit(0, 1);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 1);
        assertEquals(arr.unavailable_size(), 0);
        assertEquals(arr[0].status, status_t::Added);
        assertEquals(arr[0].value, 1);
    }
    static void AddRemove() noexcept {
        tested arr;
        arr.add(1);
        arr.remove(0);

        assertEquals(arr.size(), 0);
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 0);
    }
    static void Pure() noexcept {
        tested arr;
        arr.addPure(55);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 1);
        assertEquals(arr.unavailable_size(), 0);
        assertEquals(arr[0].status, status_t::Pure);
        assertEquals(arr[0].value, 55);
    }
    static void PureEdit() noexcept {
        tested arr;
        arr.addPure(55);
        arr.edit(0,103);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 1);
        assertEquals(arr.unavailable_size(), 0);
        assertEquals(arr[0].status, status_t::Edited);
        assertEquals(arr[0].value, 103);
    }
    static void PureRemove() noexcept {
        tested arr;
        arr.addPure(55);
        arr.remove(0);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 1);
        assertEquals(arr[0].status, status_t::Removed);
        assertEquals(arr[0].value, 55);
    }
    static void PureRemoveEdit() noexcept {
        tested arr;
        arr.addPure(3);
        arr.remove(0);
        arr.edit(0u, 52);

        assertEquals(arr.size(), 1);
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 1);
        assertEquals(arr[0].status, status_t::Removed);
        assertEquals(arr[0].value, 3);
    }
    static void PureEditRemove() noexcept {
        tested arr;
        arr.addPure(3);
        arr.edit(0,555);
        arr.remove(0);

        assertEquals(arr.size(), 1);
        assertEquals(arr[0].status, status_t::Removed);
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 1);
    }
    static void WalkByAvailAbleItems() noexcept {

        auto arr = testWalkBy();
        assertEquals(int(arr.makeAvailableIterator().size()), 3);
        assertEquals(int(arr.makeUnavailableIterator().size()), 1);
        assertEquals(arr.size(), 4);
        assertEquals(arr.available_size(), 3);
        assertEquals(arr.unavailable_size(), 1);
    }
    static void RemoveAll() noexcept {
        auto arr = testWalkBy();
        arr.removeAll();

        assertEquals(arr.size(), 3);
        assertEquals(arr.available_size(), 0);
        assertEquals(arr.unavailable_size(), 3);
        assertEquals(int(arr.makeAvailableIterator().size()), 0);
        assertEquals(int(arr.makeUnavailableIterator().size()), 3);
    }
    static void RemoveItemsFrom1Count2() noexcept {
        auto arr = testWalkBy();
        arr.remove(1,2);

        assertEquals(arr.size(), 3);
        assertEquals(arr.available_size(), 1);
        assertEquals(arr.unavailable_size(), 2);
        assertEquals(int(arr.makeAvailableIterator().size()), 1);
        assertEquals(int(arr.makeUnavailableIterator().size()), 2);
    }
    static void ToVector() noexcept {
        auto arr = testWalkBy();
        auto _1  = toVector(arr);

        assertEquals(_1.size(), 4);
    }

public:
    void operator()() noexcept {
        Init();
        Swap();
        Add();
        AddEdit();
        AddRemove();
        Pure();
        PureEdit();
        PureRemove();
        PureRemoveEdit();
        PureEditRemove();
        WalkByAvailAbleItems();
        RemoveAll();
        RemoveItemsFrom1Count2();
        ToVector();
    }
};
