/**
 * \file      ps2/ps2/qt/test/array/category_array_test.h
 * \brief     The Caption_array_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 03(th), 2016, 15:22 MSK
 * \updated   October (the) 03(th), 2016, 15:22 MSK
 * \TODO
**/
#pragma once
#include <ps2/qt/array/category.h>

class CategoryArrayTest {
public:
    using class_name = CategoryArrayTest;
    using tested     = ps2::CategoryArray<int>;
    using item_t     = tested::item_t;

private:
    static inline decltype(auto) toAdd() noexcept {
        return item_t{false, 1};
    }
    static inline decltype(auto) toEdit(int x) noexcept {
        return item_t{true, x};
    }

private:
    static inline tested first() noexcept {
        tested arr;
        arr.addItem({true, 41});
        arr.addItem({true, 22});
        arr.addItem({false, 23});
        return arr;
    }
    static inline tested second() noexcept {
        tested arr;
        arr.setCategory({true, 44});
        arr.addItem({true, 1});
        arr.addItem({false, 2});
        arr.addItem({true, 3});
        arr.addItem({false, 4});
        arr.addItem(5);
        return arr;
    }

private:
    static void Empty() noexcept {
        tested arr;

        assertEquals(arr.size(), 0);
        assertTrue(arr.empty());
    }
    static void SetCaption() noexcept {
        tested arr;
        arr.setCategory(toAdd());
        assertTrue(arr.empty());
        assertEquals(arr.category(), toAdd());

        arr.setCategory(toEdit(55));
        assertEquals(arr.category(), toEdit(55));
    }
    static void Add() noexcept {
        tested arr;

        arr.addItem(toAdd());
        assertEquals(arr.size(), 1);
        assertFalse(arr.empty());

        arr.addItem(toEdit(555));
        assertEquals(arr.size(), 2);
        assertEquals(arr[1], toEdit(555));
    }
    static void AddItems() noexcept {
        tested arr;
        assertTrue(arr.empty());

        arr.addItems({toAdd(), toEdit(22)});
        assertEquals(arr.size(), 2);
        assertEquals(arr[0], toAdd());
        assertEquals(arr[1], toEdit(22));
    }
    static void Edit() noexcept {
        tested arr;
        arr.addItem(toAdd());

        arr.editItem(0, toEdit(33));
        assertEquals(arr[0], toEdit(33));
    }
    static void Toggle() noexcept {
        tested arr;
        arr.addItem(toAdd());
        assertEquals(arr[0], toAdd());

        arr.toggle(0, 55);
        assertTrue(arr[0].status);
        assertEquals(arr[0].value, 55);

        arr.toggle(0, 33);
        assertFalse(arr[0].status);
        assertEquals(arr[0].value, 33);

        arr.toggle(0, 12);
        assertTrue(arr[0].status);
        assertEquals(arr[0].value, 12);
    }
    static void SwitchOnOff() noexcept {
        tested arr;
        arr.addItem(toAdd());
        assertEquals(arr[0], toAdd());

        arr.switchOff(0, 55);
        assertFalse(arr[0].status);
        assertEquals(arr[0].value, 55);

        arr.switchOn(0, 23);
        assertTrue(arr[0].status);
        assertEquals(arr[0].value, 23);
    }
    static void Remove() noexcept {
        tested arr;
        arr.addItem(toAdd());
        assertEquals(arr[0], toAdd());

        arr.remove(0);
        assertTrue(arr.empty());
    }
    static void RemoveSome() noexcept {
        auto&& arr = second();
        assertEquals(arr.size(), 5);

        arr.remove(1,3);
        assertEquals(arr.size(), 2);
        assertEquals(arr[0].value, 1);
        assertEquals(arr[1].value, 5);
    }
    static void RemoveAll() noexcept {
        auto&& arr = second();
        assertEquals(arr.size(), 5);

        arr.clear();
        assertTrue(arr.empty());
    }
    static void Swap() noexcept {
        auto&& _1 = first();
        auto&& _2 = second();
        assertEquals(_1.size(), 3);
        assertEquals(_2.size(), 5);

        qSwap(_1, _2);
        assertEquals(_1, second());
        assertEquals(_2, first());
    }

public:
    void operator()() noexcept {
        Empty();
        SetCaption();
        AddItems();
        Add();
        Edit();
        Toggle();
        SwitchOnOff();
        Remove();
        RemoveSome();
        RemoveAll();
        Swap();
    }
};
