/**
 * \file      ps2/ps2/qt/test/array/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 25(th), 2017, 14:26 MSK
 * \updated   January (the) 25(th), 2017, 14:26 MSK
 * \TODO      
**/
#pragma once
#include "field_test.h"
#include "ptr_field_test.h"
#include "toggle_array_test.h"
#include "category_array_test.h"

class MainArraysTest {
public:
    using class_name = MainArraysTest;

public:
    void operator()() noexcept {
        TEST(FieldTest);
        TEST(ps2::PtrFieldTest);
        TEST(ToggleArrayTest);
        TEST(CategoryArrayTest);
    }
};

