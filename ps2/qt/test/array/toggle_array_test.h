/**
 * \file      ps2/ps2/qt/test/array/toggle_array_test.h
 * \brief     The Toggle_array_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 30(th), 2016, 16:33 MSK
 * \updated   September (the) 30(th), 2016, 16:33 MSK
 * \TODO
**/
#pragma once
#include <ps2/qt/array/toggle.h>

class ToggleArrayTest {
public:
    using class_name = ToggleArrayTest;
    using tested     = ps2::ToggleArray<int>;

private:
    static tested first() noexcept {
        tested arr;
        arr.add(true, 41);
        arr.add(22);
        arr.add(true, 23);
        return arr;
    }
    static tested second() noexcept {
        tested arr;
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(true, 5);
        return arr;
    }

private:
    static void Empty() noexcept {
        tested arr;

        assertEquals(arr.size(), 0);
        assertTrue(arr.empty());
    }
    static void Add() noexcept {
        tested arr;

        arr.add(true, 1);
        assertEquals(arr.size(), 1);
        assertFalse(arr.empty());

        arr.add(false, 101);
        assertEquals(arr.size(), 2);

        arr.add(5);
        assertEquals(arr.size(), 3);
        assertFalse(arr[2].status);
        assertEquals(arr[2].value, 5);
    }
    static void Edit() noexcept {
        tested arr;
        arr.add(66);
        assertFalse(arr[0].status);
        assertEquals(arr[0].value, 66);

        arr.edit(0, true, 33);
        assertTrue(arr[0].status);
        assertEquals(arr[0].value, 33);

        arr.edit(0, false, 44);
        assertFalse(arr[0].status);
        assertEquals(arr[0].value, 44);

        arr.edit(0, {true, 55});
        assertTrue(arr[0].status);
        assertEquals(arr[0].value, 55);
    }
    static void Toggle() noexcept {
        tested arr;
        arr.add(66);
        assertFalse(arr[0].status);

        arr.toggle(0);
        assertTrue(arr[0].status);

        arr.toggle(0);
        assertFalse(arr[0].status);

        arr.toggle(0);
        assertTrue(arr[0].status);
    }
    static void Remove() noexcept {
        tested arr;
        arr.add(1);

        assertFalse(arr[0].status);
        assertEquals(arr[0].value, 1);

        arr.remove(0);
        assertTrue(arr.empty());
    }
    static void RemoveSome() noexcept {
        auto&& arr = second();
        assertEquals(arr.size(), 5);

        arr.remove(1,3);
        assertEquals(arr.size(), 2);
        assertEquals(arr[0].value, 1);
        assertEquals(arr[1].value, 5);
    }
    static void RemoveAll() noexcept {
        auto&& arr = second();
        assertEquals(arr.size(), 5);

        arr.clear();
        assertTrue(arr.empty());
    }
    static void Swap() noexcept {
        auto&& _1 = first();
        auto&& _2 = second();
        assertEquals(_1.size(), 3);
        assertEquals(_2.size(), 5);

        qSwap(_1, _2);
        assertEquals(_1, second());
        assertEquals(_2, first());
    }

public:
    void operator()() noexcept {
        Empty();
        Add();
        Edit();
        Toggle();
        Remove();
        RemoveSome();
        RemoveAll();
        Swap();
    }
};
