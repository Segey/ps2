/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/kggr_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 28(th), 2017, 11:59 MSK
 * \updated   March     (the) 28(th), 2017, 11:59 MSK
 * \TODO      
**/
#pragma once
#include "kggr_double_validator_english_test.h"
#include "kggr_double_validator_russian_test.h"
#include "kggr_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainKgGrDoubleValidatorTest final {
public:
    using class_name = MainKgGrDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(KgGrDoubleValidatorRussianTest);
        TEST(KgGrDoubleValidatorEnglishTest);
        TEST(KgGrDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
