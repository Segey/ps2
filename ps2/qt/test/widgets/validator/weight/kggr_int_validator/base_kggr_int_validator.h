/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/kggr_int_validator/base_kggr_int_validator.h
 * \brief     The Base_kggr_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 02:07 MSK
 * \updated   April     (the) 02(th), 2017, 02:07 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/weight/kggr_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseKgGrIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseKgGrIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = KgGrIntValidator;

protected:
    static inline auto Grs(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::grs());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
