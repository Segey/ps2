/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/kggr_int_validator/kggr_int_validator_germany_test.h
 * \brief     The Lboz_int_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 01(th), 2017, 23:55 MSK
 * \updated   April     (the) 01(th), 2017, 23:55 MSK
 * \TODO      
**/
#pragma once
#include "base_kggr_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class KgGrIntValidatorGermanyTest final: private BaseKgGrIntValidatorTest {
public:
    using class_name = KgGrIntValidatorGermanyTest;
    using inherited  = BaseKgGrIntValidatorTest;

private:
    static void InvalidGrsHigher() noexcept {
        assertEquals(Grs(QStringLiteral("1.000")), QValidator::Invalid);
    }
    static void InvalidGrsLower() noexcept {
        assertEquals(Grs(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidGrsHigher() noexcept {
        assertEquals(Grs(QStringLiteral("999")), QValidator::Acceptable);
    }
    static void ValidGrsLower() noexcept {
        assertEquals(Grs(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidKgs() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::kgs());
        auto str = QStringLiteral("1.500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        InvalidGrsHigher();
        InvalidGrsLower();
        ValidGrsHigher();
        ValidGrsLower();
        ValidKgs();

        AfterTest();
    }
};

} // end namespace ps2
