/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlb_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 28(th), 2017, 11:59 MSK
 * \updated   March     (the) 28(th), 2017, 11:59 MSK
 * \TODO      
**/
#pragma once
#include "stlb_double_validator_russian_test.h"
#include "stlb_double_validator_english_test.h"
#include "stlb_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainStLbDoubleValidatorTest final {
public:
    using class_name = MainStLbDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(StLbDoubleValidatorRussianTest);
        TEST(StLbDoubleValidatorEnglishTest);
        TEST(StLbDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
