/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/tkggr_int_validator/tkggr_int_validator_russian_test.h
 * \brief     The Tkggr_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 01:23 MSK
 * \updated   April     (the) 02(th), 2017, 01:23 MSK
 * \TODO      
**/
#pragma once
#include "base_tkggr_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class TKgGrIntValidatorRussianTest final: private BaseTKgGrIntValidatorTest {
public:
    using class_name = TKgGrIntValidatorRussianTest;
    using inherited  = BaseTKgGrIntValidatorTest;

private:
    static void InvalidGrHigher() noexcept {
        assertEquals(Grs(QStringLiteral("1 000")), QValidator::Invalid);
    }
    static void InvalidGrLower() noexcept {
        assertEquals(Grs(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidGrHigher() noexcept {
        assertEquals(Grs(QStringLiteral("999")), QValidator::Acceptable);
    }
    static void ValidGrLower() noexcept {
        assertEquals(Grs(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void InvalidKgHigher() noexcept {
        assertEquals(Kgs(QStringLiteral("1 000")), QValidator::Invalid);
    }
    static void InvalidKgLower() noexcept {
        assertEquals(Kgs(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidKgHigher() noexcept {
        assertEquals(Kgs(QStringLiteral("999")), QValidator::Acceptable);
    }
    static void ValidKgLower() noexcept {
        assertEquals(Kgs(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidTones() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::tones());
        auto str = QStringLiteral("1 500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        InvalidGrHigher();
        InvalidGrLower();
        ValidGrHigher();
        ValidGrLower();
        InvalidKgHigher();
        InvalidKgLower();
        ValidKgHigher();
        ValidKgLower();
        ValidTones();

        AfterTest();
    }
};

} // end namespace ps2
