/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/tkggr_int_validator/base_tkggr_int_validator.h
 * \brief     The Base_tkggr_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 01:46 MSK
 * \updated   April     (the) 02(th), 2017, 01:46 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/weight/tkggr_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseTKgGrIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseTKgGrIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = TKgGrIntValidator;

protected:
    template<class F>
    static inline auto items(F fun, QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(fun(Q_NULLPTR));
        return t->validate(str, pos);
    }
    static inline auto Kgs(QString&& str) noexcept {
        return items(tested::kgs, qMove(str));
    }
    static inline auto Grs(QString&& str) noexcept {
        return items(tested::grs, qMove(str));
    }
};

} // end namespace ps2
