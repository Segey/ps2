/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlb_int_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 02:26 MSK
 * \updated   April     (the) 02(th), 2017, 02:26 MSK
 * \TODO      
**/
#pragma once
#include "stlb_int_validator_english_test.h"
#include "stlb_int_validator_russian_test.h"
#include "stlb_int_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainStLbIntValidatorTest final {
public:
    using class_name = MainStLbIntValidatorTest;

public:
    void operator()() noexcept {
        TEST(StLbIntValidatorRussianTest);
        TEST(StLbIntValidatorEnglishTest);
        TEST(StLbIntValidatorGermanyTest);
    }
};

} // end namespace ps2
