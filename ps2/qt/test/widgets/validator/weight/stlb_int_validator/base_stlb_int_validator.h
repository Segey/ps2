/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlb_int_validator/base_stlb_int_validator.h
 * \brief     The Base_stlb_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 02:07 MSK
 * \updated   April     (the) 02(th), 2017, 02:07 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/weight/stlb_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseStLbIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseStLbIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = StLbIntValidator;

protected:
    static inline auto Pounds(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::pounds());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
