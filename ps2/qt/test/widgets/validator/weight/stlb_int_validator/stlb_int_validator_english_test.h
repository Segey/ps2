/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlb_int_validator/stlb_int_validator_english_test.h
 * \brief     The Lboz_int_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 01(th), 2017, 23:55 MSK
 * \updated   April     (the) 01(th), 2017, 23:55 MSK
 * \TODO      
**/
#pragma once
#include "base_stlb_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class StLbIntValidatorEnglishTest final: private BaseStLbIntValidatorTest {
public:
    using class_name = StLbIntValidatorEnglishTest;
    using inherited  = BaseStLbIntValidatorTest;

private:
    static void InvalidPoundsHigher() noexcept {
        assertEquals(Pounds(QStringLiteral("14")), QValidator::Invalid);
    }
    static void InvalidPoundsLower() noexcept {
        assertEquals(Pounds(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidPoundsHigher() noexcept {
        assertEquals(Pounds(QStringLiteral("13")), QValidator::Acceptable);
    }
    static void ValidPoundsLower() noexcept {
        assertEquals(Pounds(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidStones() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::stones());
        auto str = QStringLiteral("1,500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        InvalidPoundsHigher();
        InvalidPoundsLower();
        ValidPoundsHigher();
        ValidPoundsLower();
        ValidStones();

        AfterTest();
    }
};

} // end namespace ps2
