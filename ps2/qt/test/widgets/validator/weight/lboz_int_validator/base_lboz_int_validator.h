/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/lboz_int_validator/base_lboz_int_validator.h
 * \brief     The Lboz_int_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 01(th), 2017, 23:55 MSK
 * \updated   April     (the) 01(th), 2017, 23:55 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/weight/lboz_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseLbOzIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseLbOzIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = LbOzIntValidator;

protected:
    static inline auto Ounces(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::ounces());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
