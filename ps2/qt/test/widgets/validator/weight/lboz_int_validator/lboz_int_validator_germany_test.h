/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/lboz_int_validator/lboz_int_validator_germany_test.h
 * \brief     The Lboz_int_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 01(th), 2017, 23:55 MSK
 * \updated   April     (the) 01(th), 2017, 23:55 MSK
 * \TODO      
**/
#pragma once
#include "base_lboz_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class LbOzIntValidatorGermanyTest final: private BaseLbOzIntValidatorTest {
public:
    using class_name = LbOzIntValidatorGermanyTest;
    using inherited  = BaseLbOzIntValidatorTest;

private:
    static void InvalidOuncesHigher() noexcept {
        assertEquals(Ounces(QStringLiteral("16")), QValidator::Invalid);
    }
    static void InvalidOuncesLower() noexcept {
        assertEquals(Ounces(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidOuncesHigher() noexcept {
        assertEquals(Ounces(QStringLiteral("15")), QValidator::Acceptable);
    }
    static void ValidOuncesLower() noexcept {
        assertEquals(Ounces(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidPounds() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::pounds());
        auto str = QStringLiteral("1.500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        InvalidOuncesHigher();
        InvalidOuncesLower();
        ValidOuncesHigher();
        ValidOuncesLower();
        ValidPounds();

        AfterTest();
    }
};

} // end namespace ps2
