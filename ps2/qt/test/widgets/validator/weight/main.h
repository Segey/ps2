/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 11:15 MSK
 * \updated   April     (the) 05(th), 2017, 11:15 MSK
 * \TODO      
**/
#pragma once
#include "kggr_double_validator/main.h"
#include "lboz_double_validator/main.h"
#include "stlb_double_validator/main.h"
#include "kggr_int_validator/main.h"
#include "tkggr_int_validator/main.h"
#include "lboz_int_validator/main.h"
#include "stlb_int_validator/main.h"
#include "stlboz_int_validator/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainWeightValidatorTest final {
public:
    using class_name = MainWeightValidatorTest;

public:
    void operator()() noexcept {
        TEST_CLASSES(MainKgGrDoubleValidatorTest);
        TEST_CLASSES(MainLbOzDoubleValidatorTest);
        TEST_CLASSES(MainStLbDoubleValidatorTest);
        TEST_CLASSES(MainKgGrIntValidatorTest);
        TEST_CLASSES(MainTKgGrIntValidatorTest);
        TEST_CLASSES(MainLbOzIntValidatorTest);
        TEST_CLASSES(MainStLbIntValidatorTest);
        TEST_CLASSES(MainStLbOzIntValidatorTest);
    }
};

} // end namespace ps2
