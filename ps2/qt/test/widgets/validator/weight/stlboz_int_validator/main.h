/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlboz_int_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 22:33 MSK
 * \updated   March     (the) 30(th), 2017, 22:33 MSK
 * \TODO      
**/
#pragma once
#include "stlboz_int_validator_english_test.h"
#include "stlboz_int_validator_russian_test.h"
#include "stlboz_int_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainStLbOzIntValidatorTest final {
public:
    using class_name = MainStLbOzIntValidatorTest;

public:
    void operator()() noexcept {
        TEST(StLbOuncesIntValidatorEnglishTest);
        TEST(StLbOuncesIntValidatorRussianTest);
        TEST(StLbOuncesIntValidatorGermanyTest);
    }
};

} // end namespace ps2
