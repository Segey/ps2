/**
 * \file      ps2/ps2/qt/test/widgets/validator/weight/stlboz_int_validator/stlboz_int_validator_germany_test.h
 * \brief     The Tkggr_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 01:23 MSK
 * \updated   April     (the) 02(th), 2017, 01:23 MSK
 * \TODO      
**/
#pragma once
#include "base_stlboz_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class StLbOuncesIntValidatorGermanyTest final: private BaseStLbOuncesIntValidatorTest {
public:
    using class_name = StLbOuncesIntValidatorGermanyTest;
    using inherited  = BaseStLbOuncesIntValidatorTest;

private:
    static void InvalidOuncesHigher() noexcept {
        assertEquals(Ounces(QStringLiteral("16")), QValidator::Invalid);
    }
    static void InvalidOuncesLower() noexcept {
        assertEquals(Ounces(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidOuncesHigher() noexcept {
        assertEquals(Ounces(QStringLiteral("15")), QValidator::Acceptable);
    }
    static void ValidOuncesLower() noexcept {
        assertEquals(Ounces(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void InvalidPoundsHigher() noexcept {
        assertEquals(Pounds(QStringLiteral("14")), QValidator::Invalid);
    }
    static void InvalidPoundsLower() noexcept {
        assertEquals(Pounds(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidPoundsHigher() noexcept {
        assertEquals(Pounds(QStringLiteral("13")), QValidator::Acceptable);
    }
    static void ValidPoundsLower() noexcept {
        assertEquals(Pounds(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidTones() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::tones());
        auto str = QStringLiteral("1.500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        InvalidOuncesHigher();
        InvalidOuncesLower();
        ValidOuncesHigher();
        ValidOuncesLower();
        InvalidPoundsHigher();
        InvalidPoundsLower();
        ValidPoundsHigher();
        ValidPoundsLower();
        ValidTones();

        AfterTest();
    }
};

} // end namespace ps2
