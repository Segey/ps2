/**
 * \file      ps2/ps2/qt/test/widgets/validator/base_double_validator_helper.h
 * \brief     The Base_double_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 11:46 MSK
 * \updated   April     (the) 06(th), 2017, 11:46 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/decimal_double_validators.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

template<class T>
class BaseDoubleValidatorHelper: public test::Locale {
public:
    using class_name = BaseDoubleValidatorHelper;
    using inherited  = test::Locale;
    using tested     = T;

protected:
    static inline auto Validate(QString str) noexcept {
        int pos;
        tested t;
        return t.validate(str, pos);
    }
    template<class U>
    static inline auto Validate(QString str, U&& bottom, U top) noexcept {
        int pos;
        tested t(bottom, top);
        return t.validate(str, pos);
    }
};

} // end namespace ps2
