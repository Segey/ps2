/**
 * \file      ps2/ps2/qt/test/widgets/validator/double_validator/base_double_validator_test.h
 * \brief     The Base_int_validator_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 13:22 MSK
 * \updated   April     (the) 05(th), 2017, 13:22 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/cpp/typedefs.h>
#include <ps2/qt/objects/numeric/decimal.h>
#include <ps2/qt/widgets/validator/double_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseDoubleValidatorTest: protected test::Locale {
public:
    using class_name = BaseDoubleValidatorTest;
    using inherited  = test::Locale;
    using decimal1   = ps2::Decimal<int,1>;
    using decimal3   = ps2::Decimal<int,3>;
    using tested1    = DoubleValidator<decimal1>;
    using tested3    = DoubleValidator<decimal3>;

protected:
    static inline decltype(auto) Double1(QString const& s
           , double bottom = DBL_MIN , double top = DBL_MAX) noexcept {
        int pos;
        auto str = s;
        return tested1(bottom, top).validate(str, pos);
    }
    static inline decltype(auto) Double3(QString const& s
             , double bottom = DBL_MIN, double top = DBL_MAX) noexcept {
        int pos;
        auto str = s;
        return tested3(bottom, top).validate(str, pos);
    }
};

} // end namespace ps2
