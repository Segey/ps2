/**
 * \file      ps2/ps2/qt/test/widgets/validator/double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 13:21 MSK
 * \updated   April     (the) 05(th), 2017, 13:21 MSK
 * \TODO      
**/
#pragma once
#include "double_validator_english_test.h"
#include "double_validator_russian_test.h"
#include "double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainDoubleValidatorTest final {
public:
    using class_name = MainDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(DoubleValidatorRussianTest);
        TEST(DoubleValidatorEnglishTest);
        TEST(DoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
