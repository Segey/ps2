/**
 * \file      ps2/ps2/qt/test/widgets/validator/double_validator/double_validator_english_test.h
 * \brief     The Int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 13:21 MSK
 * \updated   April     (the) 05(th), 2017, 13:21 MSK
 * \TODO      
**/
#pragma once
#include "base_double_validator_test.h"

/** \namespace ps2 */
namespace ps2 {

class DoubleValidatorEnglishTest final: private BaseDoubleValidatorTest {
public:
    using class_name = DoubleValidatorEnglishTest;
    using inherited  = BaseDoubleValidatorTest;

private:
    static void InvalidValidate1() noexcept {
        assertEquals(Double1(QStringLiteral("55 678")), QValidator::Invalid);
    }
    static void InvalidValidate3() noexcept {
        assertEquals(Double3(QStringLiteral("55 678")), QValidator::Invalid);
    }
    static void InvalidPrecision1() noexcept {
        assertEquals(Double1(QStringLiteral("55678.33")), QValidator::Invalid);
    }
    static void InvalidPrecision3() noexcept {
        assertEquals(Double3(QStringLiteral("55.2345")), QValidator::Invalid);
    }
    static void ValidValidate1() noexcept {
        assertEquals(Double1(QStringLiteral("410,555")), QValidator::Acceptable);
    }
    static void ValidValidate3() noexcept {
        assertEquals(Double3(QStringLiteral("410,555")), QValidator::Acceptable);
    }
    static void ValidBorder1() noexcept {
        assertEquals(Double1(QStringLiteral("22"), 1, 23), QValidator::Acceptable);
    }
    static void ValidBorder3() noexcept {
        assertEquals(Double3(QStringLiteral("22"), 1, 23), QValidator::Acceptable);
    }
    static void ValidPrecision1() noexcept {
        assertEquals(Double1(QStringLiteral("15.2")), QValidator::Acceptable);
    }
    static void ValidPrecision3() noexcept {
        assertEquals(Double3(QStringLiteral("15.234")), QValidator::Acceptable);
    }

    static void Invalid1() noexcept {
        auto _1 = tested1::current(QStringLiteral("1 00"));
        assertFalse(_1.isValid());
    }
    static void Invalid3() noexcept {
        auto _1 = tested3::current(QStringLiteral("1 00"));
        assertFalse(_1.isValid());
    }
    static void Simple1() noexcept {
        auto _1 = tested1::current(QStringLiteral("100"));
        assertEquals(_1, decimal1(100,0));
    }
    static void Simple3() noexcept {
        auto _1 = tested3::current(QStringLiteral("100"));
        assertEquals(_1, decimal3(100,0));
    }
    static void Zero1() noexcept {
        auto _1 = tested1::current(QString::Null());
        assertEquals(_1, decimal1(0,0));
    }
    static void Zero3() noexcept {
        auto _1 = tested3::current(QString::Null());
        assertEquals(_1, decimal3(0,0));
    }
    static void Complex1() noexcept {
        auto _1 = tested1::current(QStringLiteral("135"));
        assertEquals(_1, decimal1(135,0));
    }
    static void Complex3() noexcept {
        auto _1 = tested3::current(QStringLiteral("135"));
        assertEquals(_1, decimal3(135,0));
    }
    static void ToDecimalSimple1() noexcept {
        assertEquals(tested1::createString(10,5)
                           , QStringLiteral("10.5"));
    }
    static void ToDecimalSimple3() noexcept {
        assertEquals(tested3::createString(10,5)
                           , QStringLiteral("10.005"));
    }
    static void ToDecimalComplex1() noexcept {
        assertEquals(tested1::createString(100,0)
                           , QStringLiteral("100.0"));
    }
    static void ToDecimalComplex3() noexcept {
        assertEquals(tested3::createString(100, 23)
                           , QStringLiteral("100.023"));
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        InvalidValidate1();
        InvalidValidate3();
        InvalidPrecision1();
        InvalidPrecision3();

        ValidValidate1();
        ValidValidate3();
        ValidBorder1();
        ValidBorder3();
        ValidPrecision1();
        ValidPrecision3();

        Invalid1();
        Invalid3();
        Simple1();
        Simple3();

        Zero1();
        Zero3();
        Complex1();
        Complex3();
        ToDecimalSimple1();
        ToDecimalSimple3();
        ToDecimalComplex1();
        ToDecimalComplex3();

        AfterTest();
    }
};

} // end namespace ps2
