/**
 * \file      ps2/ps2/qt/test/widgets/validator/int_validator/int_validator_germany_test.h
 * \brief     The _int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 23:10 MSK
 * \updated   March     (the) 30(th), 2017, 23:10 MSK
 * \TODO      
**/
#pragma once
#include "base_int_validator_test.h"

/** \namespace ps2 */
namespace ps2 {

class IntValidatorGermanyTest final: private BaseIntValidatorTest {
public:
    using class_name = IntValidatorGermanyTest;
    using inherited  = BaseIntValidatorTest;

private:
    static void InvalidValidate() noexcept {
        assertEquals(Int(QStringLiteral("55 678")), QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Int(QStringLiteral("410")), QValidator::Acceptable);
    }
    static void Valid() noexcept {
        assertEquals(Int(QStringLiteral("1.999.999")), QValidator::Acceptable);
    }
    static void ValidZero() noexcept {
        assertEquals(Int(QStringLiteral("0")), QValidator::Acceptable);
    }

    static void Invalid() noexcept {
        auto _1 = tested::current(QStringLiteral("1 00"));
        assertEquals(_1, INT_MAX);
    }
    static void Simple() noexcept {
        auto _1 = tested::current(QStringLiteral("100"));
        assertEquals(_1, 100);
    }
    static void Zero() noexcept {
        auto _1 = tested::current(QString::Null());
        assertEquals(_1, 0);
    }
    static void Complex() noexcept {
        auto _1 = tested::current(QStringLiteral("135"));
        assertEquals(_1, 135);
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested::createString(10), QStringLiteral("10"));
    }
    static void ToDecimalComplex() noexcept {
        assertEquals(tested::createString(100) , QStringLiteral("100"));
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        InvalidValidate();
        ValidValidate();
        Valid();
        ValidZero();

        Invalid();
        Simple();
        Zero();
        Complex();
        ToDecimalSimple();
        ToDecimalComplex();

        AfterTest();
    }
};

} // end namespace ps2
