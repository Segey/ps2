/**
 * \file      ps2/ps2/qt/test/widgets/validator/int_validator/base_int_validator_test.h
 * \brief     The _int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 30(th), 2017, 23:10 MSK
 * \updated   March     (the) 30(th), 2017, 23:10 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = IntValidator;

protected:
    static inline decltype(auto) Int(QString const& s) noexcept {
        int pos;
        auto str = s;
        return tested().validate(str, pos);
    }
};

} // end namespace ps2
