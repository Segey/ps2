/**
 * \file      ps2/ps2/qt/test/widgets/validator/int_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 30(th), 2017, 23:08 MSK
 * \updated   March     (the) 30(th), 2017, 23:08 MSK
 * \TODO      
**/
#pragma once
#include "int_validator_english_test.h"
#include "int_validator_russian_test.h"
#include "int_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainIntValidatorTest final {
public:
    using class_name = MainIntValidatorTest;

public:
    void operator()() noexcept {
        TEST(IntValidatorRussianTest);
        TEST(IntValidatorEnglishTest);
        TEST(IntValidatorGermanyTest);
    }
};

} // end namespace ps2
