/**
 * \file      ps2/ps2/qt/test/widgets/validator/decimal_validator/decimal_validator_english_test.h
 * \brief     The Decimal_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 26(th), 2017, 22:56 MSK
 * \updated   April     (the) 26(th), 2017, 22:56 MSK
 * \TODO      
**/
#pragma once
#include <ps2/cpp/decimal.h>
#include "base_decimal_validator_test.h"

/** \namespace ps2 */
namespace ps2 {

class DecimalValidatorEnglishTest final: private BaseDecimalValidatorTest {
public:
    using class_name = DecimalValidatorEnglishTest;
    using inherited  = BaseDecimalValidatorTest;

private:
    static void EmptyIntermediate() noexcept {
        assertEquals(Decimal(QStringLiteral("")), QValidator::Intermediate);
    }
    static void InvalidPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("55.6789")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55678.3113")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55.2345")) , QValidator::Invalid);
    }
    static void ValidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("6"), 22_kg , 61_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("12"), 0_kg, 123_kg)
                                             , QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("12"), 25_kg, 12105_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("6"), 53_kg, 121_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("1"), 30_kg, 900_kg)
                                             , QValidator::Intermediate);
    }
    static void InvalidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("2"), 3_kg, 12_kg)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("138"), 353_gr, 3759_gr)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("66"), 3_kg, 12_kg)
                                             , QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Decimal(QStringLiteral("0.5")), QValidator::Acceptable);
    }
    static void ValidFullPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("15.2")), QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("5.234")), QValidator::Acceptable);
    }
    static void InvalidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("3.0"), 3100_gr, 5500_gr)
                                         , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("135.5"), 13000_gr, 135500_gr)
                                         , QValidator::Invalid);
    }
    static void ValidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("0.01"), 50_gr, 500_gr)
                     , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("3.5"), 35_kg, 335001_gr)
                     , QValidator::Intermediate);
    }
    static void InvalidCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("1 00"));
        assertFalse(ps2::is_valid(_1));

        auto _2 = tested(10_kg, 30_kg).current(QStringLiteral("30 000"));
        assertFalse(ps2::is_valid(_2));
    }
    static void SimpleCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("100"));
        assertEquals(_1, decimal_t(100.0));
    }
    static void ZeroCurrent() noexcept {
        auto _1 = tested().current(QString::Null());
        assertEquals(_1, decimal_t(0));
    }
    static void ComplexCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("135.5"));
        assertEquals(_1, decimal_t(135,500));

        auto _2 = tested().current(QStringLiteral("0.235"));
        assertEquals(_2, decimal_t(0,235));
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested().createString(0,501), QStringLiteral("0.501"));
        assertEquals(tested().createString(11,3), QStringLiteral("11.003"));
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        EmptyIntermediate();
        InvalidPrecision();
        ValidValidate();

        ValidMiddle();
        InvalidMiddle();

        ValidFullPrecision();
        InvalidMiddlePrecision();
        ValidMiddlePrecision();

        InvalidCurrent();
        SimpleCurrent();
        ZeroCurrent();
        ComplexCurrent();

        ToDecimalSimple();

        AfterTest();
    }
};

} // end namespace ps2
