/**
 * \file      ps2/ps2/qt/test/widgets/validator/decimal_validator/base_decimal_validator_test.h
 * \brief     The Base_decimal_validator_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 23:35 MSK
 * \updated   April     (the) 24(th), 2017, 23:35 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/cpp/typedefs.h>
#include <ps2/qt/objects/numeric/decimal.h>
#include <ps2/qt/widgets/validator/decimal_double_validator.h>
#include <main/literals.h>
#include <main/user/metric_weight.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseDecimalValidatorTest: protected test::Locale {
public:
    using class_name = BaseDecimalValidatorTest;
    using inherited  = test::Locale;
    using decimal_t  = ps2::decimal<long long int,3>;
    using tested     = DecimalDoubleValidator<MetricWeight
                            , decimal_t, MetricWeight::fromKgGr>;

protected:
    static inline decltype(auto) Decimal(QString const& s) noexcept {
        int pos;
        auto str = s;
        return tested().validate(str, pos);
    }
    static inline decltype(auto) Decimal(QString const& s
           , Weight bottom, Weight top) noexcept {

        int pos;
        auto str = s;
        return tested(bottom, top).validate(str, pos);
    }
};

} // end namespace ps2
