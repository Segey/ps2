/**
 * \file      ps2/ps2/qt/test/widgets/validator/decimal_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 23:34 MSK
 * \updated   April     (the) 24(th), 2017, 23:34 MSK
 * \TODO      
**/
#pragma once
#include "decimal_validator_english_test.h"
#include "decimal_validator_russian_test.h"
#include "decimal_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainDecimalValidatorTest final {
public:
    using class_name = MainDecimalValidatorTest;

public:
    void operator()() noexcept {
        TEST(DecimalValidatorRussianTest);
        TEST(DecimalValidatorEnglishTest);
        TEST(DecimalValidatorGermanyTest);
    }
};

} // end namespace ps2
