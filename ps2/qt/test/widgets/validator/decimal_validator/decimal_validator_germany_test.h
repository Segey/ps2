/**
 * \file      ps2/ps2/qt/test/widgets/validator/decimal_validator/decimal_validator_germany_test.h
 * \brief     The Decimal_validator_germany_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 27(th), 2017, 01:03 MSK
 * \updated   April     (the) 27(th), 2017, 01:03 MSK
 * \TODO      
**/
#pragma once
#include "base_decimal_validator_test.h"

/** \namespace ps2 */
namespace ps2 {

class DecimalValidatorGermanyTest final: private BaseDecimalValidatorTest {
public:
    using class_name = DecimalValidatorGermanyTest;
    using inherited  = BaseDecimalValidatorTest;

private:
    static void EmptyIntermediate() noexcept {
        assertEquals(Decimal(QStringLiteral("")), QValidator::Intermediate);
    }
    static void InvalidPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("55,6789")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55678,3113")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55,2345")) , QValidator::Invalid);
    }
    static void ValidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("1"), 5_kg , 11_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("66"), 0_kg, 77_kg)
                                             , QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("9"), 40_kg, 400_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("6"), 53_kg, 99_kg)
                                             , QValidator::Intermediate);
    }
    static void InvalidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("9"), 22_kg, 89_kg)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("22"), 353_gr, 999_gr)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("660"), 0_kg, 100_kg)
                                             , QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Decimal(QStringLiteral("0,5")), QValidator::Acceptable);
    }
    static void ValidFullPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("15,2")), QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("5,234")), QValidator::Acceptable);
    }
    static void InvalidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("3.1"), 3_gr, 500_gr)
                                         , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("88.5"), 130000_gr, 135500_gr)
                                         , QValidator::Invalid);
    }
    static void ValidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("0,1"), 99_gr, 101_gr)
                     , QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("0,056"), 550_gr, 10001_gr)
                     , QValidator::Intermediate);
    }
    static void InvalidCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("1 00"));
        assertFalse(ps2::is_valid(_1));

        auto _2 = tested(10_kg, 30_kg).current(QStringLiteral("300 000"));
        assertFalse(ps2::is_valid(_2));
    }
    static void SimpleCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("100"));
        assertEquals(_1, decimal_t(100.0));
    }
    static void ZeroCurrent() noexcept {
        auto _1 = tested().current(QString::Null());
        assertEquals(_1, decimal_t(0));
    }
    static void ComplexCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("11,05"));
        assertEquals(_1, decimal_t(11,50));

        auto _2 = tested().current(QStringLiteral("160,23"));
        assertEquals(_2, decimal_t(160,230));
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested().createString(0,501), QStringLiteral("0,501"));
        assertEquals(tested().createString(11,3), QStringLiteral("11,003"));
    }

public:
    void operator()() noexcept {
        BeforeGermanyTest();

        EmptyIntermediate();
        InvalidPrecision();
        ValidValidate();

        ValidMiddle();
        InvalidMiddle();

        ValidFullPrecision();
        InvalidMiddlePrecision();
        ValidMiddlePrecision();

        InvalidCurrent();
        SimpleCurrent();
        ZeroCurrent();
        ComplexCurrent();

        ToDecimalSimple();

        AfterTest();
    }
};

} // end namespace ps2
