/**
 * \file      ps2/ps2/qt/test/widgets/validator/decimal_validator/decimal_validator_russian_test.h
 * \brief     The Decimal_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 23:34 MSK
 * \updated   April     (the) 24(th), 2017, 23:34 MSK
 * \TODO      
**/
#pragma once
#include "base_decimal_validator_test.h"

/** \namespace ps2 */
namespace ps2 {

class DecimalValidatorRussianTest final: private BaseDecimalValidatorTest {
public:
    using class_name = DecimalValidatorRussianTest;
    using inherited  = BaseDecimalValidatorTest;

private:
    static void EmptyIntermediate() noexcept {
        assertEquals(Decimal(QStringLiteral("")), QValidator::Intermediate);
    }
    static void InvalidPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("55,6789")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55678,3113")), QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("55,2345")) , QValidator::Invalid);
    }
    static void ValidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("6"), 53_kg , 121_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("12"), 253_kg, 1210_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("12"), 25_kg, 12105_kg)
                                             , QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("38"), 353_kg, 3759_kg)
                                             , QValidator::Intermediate);
    }
    static void InvalidMiddle() noexcept {
        assertEquals(Decimal(QStringLiteral("2"), 353_kg, 1210_kg)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("38"), 353_kg, 375_kg)
                                             , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("666"), 53_kg, 121_kg)
                                             , QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Decimal(QStringLiteral("410 555"))
                     , QValidator::Acceptable);
    }
    static void ValidFullPrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("15,2")), QValidator::Acceptable);
        assertEquals(Decimal(QStringLiteral("15,234")), QValidator::Acceptable);
    }
    static void InvalidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("3,0"), 3100_gr, 5500_gr)
                                         , QValidator::Invalid);
        assertEquals(Decimal(QStringLiteral("35,5"), 3000_gr, 35500_gr)
                                         , QValidator::Invalid);
    }
    static void ValidMiddlePrecision() noexcept {
        assertEquals(Decimal(QStringLiteral("3,1"), 3100_gr, 5500_gr), QValidator::Intermediate);
        assertEquals(Decimal(QStringLiteral("33,5"), 33_kg, 33501_gr)
                     , QValidator::Acceptable);
    }
    static void InvalidCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("1.00"));
        assertFalse(ps2::is_valid(_1));

        auto _2 = tested(10_kg, 30_kg).current(QStringLiteral("30,00"));
        assertFalse(ps2::is_valid(_2));
    }
    static void SimpleCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("100"));
        assertEquals(_1, decimal_t(100,0));
    }
    static void ZeroCurrent() noexcept {
        auto _1 = tested().current(QString::Null());
        assertEquals(_1, decimal_t(0));
    }
    static void ComplexCurrent() noexcept {
        auto _1 = tested().current(QStringLiteral("135,5"));
        assertEquals(_1, decimal_t(135,500));

        auto _2 = tested().current(QStringLiteral("0,235"));
        assertEquals(_2, decimal_t(0,235));
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested().createString(10,5), QStringLiteral("10,005"));
        assertEquals(tested().createString(11,355), QStringLiteral("11,355"));
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        EmptyIntermediate();
        InvalidPrecision();
        ValidValidate();

        ValidMiddle();
        InvalidMiddle();

        ValidFullPrecision();
        InvalidMiddlePrecision();
        ValidMiddlePrecision();

        InvalidCurrent();
        SimpleCurrent();
        ZeroCurrent();
        ComplexCurrent();

        ToDecimalSimple();

        AfterTest();
    }
};

} // end namespace ps2
