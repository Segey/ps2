/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/ftin_int_validator/ftin_int_validator_english_test.h
 * \brief     The Ftin_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 11:06 MSK
 * \updated   April     (the) 05(th), 2017, 11:06 MSK
 * \TODO      
**/
#pragma once
#include "base_ftin_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class FtInIntValidatorEnglishTest final: private BaseFtInIntValidatorTest {
public:
    using class_name = FtInIntValidatorEnglishTest;
    using inherited  = BaseFtInIntValidatorTest;

private:
    static void InvalidInchesHigher() noexcept {
        assertEquals(Inches(QStringLiteral("12")), QValidator::Invalid);
    }
    static void InvalidInchesLower() noexcept {
        assertEquals(Inches(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidInchesHigher() noexcept {
        assertEquals(Inches(QStringLiteral("11")), QValidator::Acceptable);
    }
    static void ValidInchesLower() noexcept {
        assertEquals(Inches(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidStones() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::feet());
        auto str = QStringLiteral("1,500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        InvalidInchesHigher();
        InvalidInchesLower();
        ValidInchesHigher();
        ValidInchesLower();
        ValidStones();

        AfterTest();
    }
};

} // end namespace ps2
