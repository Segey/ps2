/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/ftin_int_validator/base_ftin_int_validator.h
 * \brief     The Base_ftin_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 11:06 MSK
 * \updated   April     (the) 05(th), 2017, 11:06 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/length/ftin_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseFtInIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseFtInIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = FtInIntValidator;

protected:
    static inline auto Inches(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::inches());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
