/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/mcm_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 01:28 MSK
 * \updated   April     (the) 06(th), 2017, 01:28 MSK
 * \TODO      
**/
#pragma once
#include <main/literals.h>
#include "mcm_double_validator_english_test.h"
#include "mcm_double_validator_russian_test.h"
#include "mcm_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainMCmDoubleValidatorTest final {
public:
    using class_name = MainMCmDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(MCmDoubleValidatorRussianTest);
        TEST(MCmDoubleValidatorEnglishTest);
        TEST(MCmDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
