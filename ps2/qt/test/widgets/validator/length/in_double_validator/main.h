/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/in_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 28(th), 2017, 14:38 MSK
 * \updated   April     (the) 28(th), 2017, 14:38 MSK
 * \TODO      
**/
#pragma once
#include "in_double_validator_russian_test.h"
#include "in_double_validator_english_test.h"
#include "in_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainInDoubleValidatorTest final {
public:
    using class_name = MainInDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(InDoubleValidatorRussianTest);
        TEST(InDoubleValidatorEnglishTest);
        TEST(InDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
