/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/in_int_validator/base_insub_int_validator.h
 * \brief     The Base_in_sub_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 28(th), 2017, 17:36 MSK
 * \updated   April     (the) 28(th), 2017, 17:36 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/length/insub_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseInSubIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseInSubIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = InSubIntValidator;

protected:
    static inline auto SubInches(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::subInches());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
