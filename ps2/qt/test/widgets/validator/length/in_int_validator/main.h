/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/in_int_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 28(th), 2017, 17:35 MSK
 * \updated   April     (the) 28(th), 2017, 17:35 MSK
 * \TODO      
**/
#pragma once
#include "insub_int_validator_english_test.h"
#include "insub_int_validator_russian_test.h"
#include "insub_int_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainInSubIntValidatorTest final {
public:
    using class_name = MainInSubIntValidatorTest;

public:
    void operator()() noexcept {
        TEST(InSubIntValidatorRussianTest);
        TEST(InSubIntValidatorEnglishTest);
        TEST(InSubIntValidatorGermanyTest);
    }
};

} // end namespace ps2
