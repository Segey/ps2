/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/in_int_validator/insub_int_validator_english_test.h
 * \brief     The In_int_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 28(th), 2017, 17:36 MSK
 * \updated   April     (the) 28(th), 2017, 17:36 MSK
 * \TODO      
**/
#pragma once
#include "base_insub_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class InSubIntValidatorEnglishTest final: private BaseInSubIntValidatorTest {
public:
    using class_name = InSubIntValidatorEnglishTest;
    using inherited  = BaseInSubIntValidatorTest;

private:
    static void InvalidSubInchesHigher() noexcept {
        assertEquals(SubInches(QStringLiteral("100")), QValidator::Invalid);
    }
    static void InvalidSubInchesLower() noexcept {
        assertEquals(SubInches(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidSubInchesHigher() noexcept {
        assertEquals(SubInches(QStringLiteral("99")), QValidator::Acceptable);
    }
    static void ValidSubInchesLower() noexcept {
        assertEquals(SubInches(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidInches() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::inches());
        auto str = QStringLiteral("21,500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        InvalidSubInchesHigher();
        InvalidSubInchesLower();
        ValidSubInchesHigher();
        ValidSubInchesLower();
        ValidInches();

        AfterTest();
    }
};

} // end namespace ps2
