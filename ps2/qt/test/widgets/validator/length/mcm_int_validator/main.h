/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/mcm_int_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 12:18 MSK
 * \updated   April     (the) 05(th), 2017, 12:18 MSK
 * \TODO      
**/
#pragma once
#include "mcm_int_validator_russian_test.h"
#include "mcm_int_validator_english_test.h"
#include "mcm_int_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainMCmIntValidatorTest final {
public:
    using class_name = MainMCmIntValidatorTest;

public:
    void operator()() noexcept {
        TEST(MCmIntValidatorRussianTest);
        TEST(MCmIntValidatorEnglishTest);
        TEST(MCmIntValidatorGermanyTest);
    }
};

} // end namespace ps2
