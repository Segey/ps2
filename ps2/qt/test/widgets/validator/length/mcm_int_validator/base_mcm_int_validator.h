/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/mcm_int_validator/base_mcm_int_validator.h
 * \brief     The Base_mcm_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 12:19 MSK
 * \updated   April     (the) 05(th), 2017, 12:19 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/length/mcm_int_validator.h>
#include "lib/locale.h"

/** \namespace ps2 */
namespace ps2 {

class BaseMCmIntValidatorTest: protected test::Locale {
public:
    using class_name = BaseMCmIntValidatorTest;
    using inherited  = test::Locale;
    using tested     = MCmIntValidator;

protected:
    static inline auto Cms(QString&& str) noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::cms());
        return t->validate(str, pos);
    }
};

} // end namespace ps2
