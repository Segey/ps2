/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/mcm_int_validator/mcm_int_validator_russian_test.h
 * \brief     The Mcm_int_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 12:18 MSK
 * \updated   April     (the) 05(th), 2017, 12:18 MSK
 * \TODO      
**/
#pragma once
#include "base_mcm_int_validator.h"

/** \namespace ps2 */
namespace ps2 {

class MCmIntValidatorRussianTest final: private BaseMCmIntValidatorTest {
public:
    using class_name = MCmIntValidatorRussianTest;
    using inherited  = BaseMCmIntValidatorTest;

private:
    static void InvalidCmsHigher() noexcept {
        assertEquals(Cms(QStringLiteral("100")), QValidator::Invalid);
    }
    static void InvalidCmsLower() noexcept {
        assertEquals(Cms(QStringLiteral("-1")), QValidator::Invalid);
    }
    static void ValidCmsHigher() noexcept {
        assertEquals(Cms(QStringLiteral("99")), QValidator::Acceptable);
    }
    static void ValidCmsLower() noexcept {
        assertEquals(Cms(QStringLiteral("0")), QValidator::Acceptable);
    }
    static void ValidPounds() noexcept {
        int pos;
        auto t = std::unique_ptr<tested>(tested::meters());
        auto str = QStringLiteral("1 500");
        assertEquals(t->validate(str, pos), QValidator::Acceptable);
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        InvalidCmsHigher();
        InvalidCmsLower();
        ValidCmsHigher();
        ValidCmsLower();
        ValidPounds();

        AfterTest();
    }
};

} // end namespace ps2
