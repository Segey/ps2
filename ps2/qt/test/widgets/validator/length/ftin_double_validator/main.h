/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/ftin_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 13:09 MSK
 * \updated   April     (the) 06(th), 2017, 13:09 MSK
 * \TODO      
**/
#pragma once
#include "ftin_double_validator_russian_test.h"
#include "ftin_double_validator_english_test.h"
#include "ftin_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainFtInDoubleValidatorTest final {
public:
    using class_name = MainFtInDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(FtInDoubleValidatorRussianTest);
        TEST(FtInDoubleValidatorEnglishTest);
        TEST(FtInDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
