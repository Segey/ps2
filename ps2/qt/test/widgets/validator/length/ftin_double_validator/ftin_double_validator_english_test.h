/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/ftin_double_validator/ftin_double_validator_english_test.h
 * \brief     The Ftin_double_validator_english_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 13:09 MSK
 * \updated   April     (the) 06(th), 2017, 13:09 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/decimal_double_validators.h>
#include "../..//base_double_validator_helper.h"

/** \namespace ps2 */
namespace ps2 {

class FtInDoubleValidatorEnglishTest final: private BaseDoubleValidatorHelper<FtInDoubleValidator> {
public:
    using class_name = FtInDoubleValidatorEnglishTest;
    using tested     = FtInDoubleValidator;
    using decimal_t  = FtInDecimal;
    using inherited  = BaseDoubleValidatorHelper<tested>;

private:
    static void InvalidValidate() noexcept {
        assertEquals(Validate(QStringLiteral("55 7")), QValidator::Invalid);
    }
    static void InvalidPrecisionValidate() noexcept {
        assertEquals(Validate(QStringLiteral("55.67")), QValidator::Invalid);
    }
    static void InvalidBorderValidate() noexcept {
        assertEquals(Validate(QStringLiteral("25.8"), 10_ft, 20_ft)
                                             , QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Validate(QStringLiteral("4,510.7")), QValidator::Acceptable);
    }
    static void ValidBorderValidate() noexcept {
        assertEquals(Validate(QStringLiteral("15.11"), 10_ft, 20_ft)
                                             , QValidator::Acceptable);
    }

    static void Invalid() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100,4"));
        assertFalse(_1.isValid());
    }
    static void Simple() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100.01"));
        assertEquals(_1, decimal_t(100, 1));
    }
    static void Simple2() noexcept {
        auto const& _1 = tested().current(QStringLiteral("00.1"));
        assertEquals(_1, decimal_t(0, 10));
    }
    static void OnlyIntegral() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100,456"));
        assertEquals(_1, decimal_t(100456,0));
    }
    static void OnlyFraction() noexcept {
        auto const& _1 = tested().current(QStringLiteral(".2"));
        assertEquals(_1, decimal_t(0, 2));
    }
    static void OnlyFraction2() noexcept {
        auto const& _1 = tested().current(QStringLiteral(".02"));
        assertEquals(_1, decimal_t(0, 2));
    }
    static void Zero() noexcept {
        auto const& _1 = tested().current(QString::Null());
        assertEquals(_1, decimal_t::zero());
    }
    static void Complex() noexcept {
        auto const& _1 = tested().current(QStringLiteral("352220.01"));
        assertEquals(_1, decimal_t(352220, 1));
    }
    static void ComplexWithSpaces() noexcept {
        auto const& _1 = tested().current(QStringLiteral("1,352,220.10"));
        assertEquals(_1, decimal_t(1352220, 10));
    }
    static void InvalidBorder() noexcept {
        auto const& _1 = tested(10_ft, 20_ft)
                            .current(QStringLiteral("220.01"));
        assertFalse(_1.isValid());
    }
    static void ValidBorder() noexcept {
        auto const& _1 = tested(10_ft, 20_ft)
                            .current(QStringLiteral("15.01"));
        assertEquals(_1, decimal_t(15, 1));
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested::createString(10,1), QStringLiteral("10.01"));
    }
    static void ToDecimalComplex() noexcept {
        assertEquals(inherited::updateNBSpaceToSpace(tested::createString(100'000, 10))
                     , QStringLiteral("100,000.10"));
    }

public:
    void operator()() noexcept {
        BeforeEnglishTest();

        InvalidValidate();
        InvalidPrecisionValidate();
        InvalidBorderValidate();
        ValidValidate();
        ValidBorderValidate();

        Invalid();
        Simple();
        Simple2();
        OnlyFraction();
        OnlyFraction2();
        OnlyIntegral();
        Zero();
        Complex();
        ComplexWithSpaces();
        InvalidBorder();
        ValidBorder();
        ToDecimalSimple();
        ToDecimalComplex();

        AfterTest();
    }
};

} // end namespace ps2
