/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/cm_double_validator/cm_double_validator_russian_test.h
 * \brief     The Cm_double_validator_russian_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 19:05 MSK
 * \updated   April     (the) 24(th), 2017, 19:05 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/qt/widgets/validator/decimal_double_validators.h>
#include "../..//base_double_validator_helper.h"

/** \namespace ps2 */
namespace ps2 {

class CmDoubleValidatorRussianTest final
        : private BaseDoubleValidatorHelper<CmMmDoubleValidator> {
public:
    using class_name = CmDoubleValidatorRussianTest;
    using tested     = CmMmDoubleValidator;
    using decimal_t  = CmMmDecimal;
    using inherited  = BaseDoubleValidatorHelper<tested>;

private:
    static void InvalidValidate() noexcept {
        assertEquals(Validate(QStringLiteral("55.6")), QValidator::Invalid);
    }
    static void InvalidPrecisionValidate() noexcept {
        assertEquals(Validate(QStringLiteral("55,10")), QValidator::Invalid);
    }
    static void InvalidBorderValidate() noexcept {
        assertEquals(Validate(QStringLiteral("25,8"), 10_cm, 20_cm)
                            , QValidator::Invalid);
    }
    static void ValidValidate() noexcept {
        assertEquals(Validate(QStringLiteral("4 510,9")), QValidator::Acceptable);
    }
    static void ValidBorderValidate() noexcept {
        assertEquals(Validate(QStringLiteral("15,8"), 10_cm, 20_cm)
                     , QValidator::Acceptable);
    }

    static void Invalid() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100.5"));
        assertFalse(ps2::is_valid(_1));
    }
    static void Simple() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100,9"));
        assertEquals(_1, decimal_t(100, 9));
    }
    static void Simple2() noexcept {
        auto const& _1 = tested().current(QStringLiteral("00,1"));
        assertEquals(_1, decimal_t(0, 1));
    }
    static void OnlyIntegral() noexcept {
        auto const& _1 = tested().current(QStringLiteral("100 456"));
        assertEquals(_1, decimal_t(100456,0));
    }
    static void OnlyFraction() noexcept {
        auto const& _1 = tested().current(QStringLiteral("0,2"));
        assertEquals(_1, decimal_t(0, 2));
    }
    static void OnlyFraction2() noexcept {
        auto const& _1 = tested().current(QStringLiteral("0,2"));
        assertEquals(_1, decimal_t(0, 2));
    }
    static void Zero() noexcept {
        auto const& _1 = tested().current(QString::Null());
        assertEquals(_1, decimal_t(0));
    }
    static void Complex() noexcept {
        auto const& _1 = tested().current(QStringLiteral("352220,1"));
        assertEquals(_1, decimal_t(352220, 1));
    }
    static void ComplexWithSpaces() noexcept {
        auto const& _1 = tested().current(QStringLiteral("1 352 220,1"));
        assertEquals(_1, decimal_t(1352220, 1));
    }
    static void InvalidBorder() noexcept {
        auto const& _1 = tested(10_cm, 20_cm).current(QStringLiteral("220,1"));
        assertFalse(ps2::is_valid(_1));
    }
    static void ValidBorder() noexcept {
        auto const& _1 = tested(10_cm, 20_cm).current(QStringLiteral("15,1"));
        assertEquals(_1, decimal_t(15, 1));
    }
    static void ToDecimalSimple() noexcept {
        assertEquals(tested::createString(10,2), QStringLiteral("10,2"));
    }
    static void ToDecimalComplex() noexcept {
        assertEquals(inherited::updateNBSpaceToSpace(tested::createString(100'000, 2))
                     , QStringLiteral("100 000,2"));
    }

public:
    void operator()() noexcept {
        BeforeRussianTest();

        InvalidValidate();
        InvalidPrecisionValidate();
        InvalidBorderValidate();
        ValidValidate();
        ValidBorderValidate();

        Invalid();
        Simple();
        Simple2();
        OnlyFraction();
        OnlyFraction2();
        OnlyIntegral();
        Zero();
        Complex();
        ComplexWithSpaces();
        InvalidBorder();
        ValidBorder();
        ToDecimalSimple();
        ToDecimalComplex();

        AfterTest();
    }
};

} // end namespace ps2
