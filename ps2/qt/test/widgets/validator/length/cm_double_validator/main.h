/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/cm_double_validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 24(th), 2017, 19:05 MSK
 * \updated   April     (the) 24(th), 2017, 19:05 MSK
 * \TODO      
**/
#pragma once
#include "cm_double_validator_english_test.h"
#include "cm_double_validator_russian_test.h"
#include "cm_double_validator_germany_test.h"

/** \namespace ps2 */
namespace ps2 {

class MainCmDoubleValidatorTest final {
public:
    using class_name = MainCmDoubleValidatorTest;

public:
    void operator()() noexcept {
        TEST(CmDoubleValidatorRussianTest);
        TEST(CmDoubleValidatorEnglishTest);
        TEST(CmDoubleValidatorGermanyTest);
    }
};

} // end namespace ps2
