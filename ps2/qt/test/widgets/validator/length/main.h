/**
 * \file      ps2/ps2/qt/test/widgets/validator/length/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.5
 * \created   April     (the) 05(th), 2017, 10:55 MSK
 * \updated   April     (the) 28(th), 2017, 14:26 MSK
 * \TODO      
**/
#pragma once
#include "mcm_double_validator/main.h"
#include "mcm_int_validator/main.h"
#include "cm_double_validator/main.h"
#include "ftin_int_validator/main.h"
#include "ftin_double_validator/main.h"
#include "in_double_validator/main.h"
#include "in_int_validator/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainLengthValidatorTest final {
public:
    using class_name = MainLengthValidatorTest;

public:
    void operator()() noexcept {
        TEST_CLASSES(MainMCmIntValidatorTest);
        TEST_CLASSES(MainCmDoubleValidatorTest);
        TEST_CLASSES(MainMCmDoubleValidatorTest);
        TEST_CLASSES(MainFtInDoubleValidatorTest);
        TEST_CLASSES(MainFtInIntValidatorTest);
        TEST_CLASSES(MainInDoubleValidatorTest);
        TEST_CLASSES(MainInSubIntValidatorTest);
    }
};

} // end namespace ps2
