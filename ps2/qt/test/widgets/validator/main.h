/**
 * \file      ps2/ps2/qt/test/widgets/validator/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.14
 * \created   March     (the) 27(th), 2017, 23:25 MSK
 * \updated   March     (the) 27(th), 2017, 23:25 MSK
 * \TODO      
**/
#pragma once
#include "int_validator/main.h"
#include "double_validator/main.h"
#include "decimal_validator/main.h"
#include "weight/main.h"
#include "length/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainValidatorTest final {
public:
    using class_name = MainValidatorTest;

public:
    void operator()() noexcept {
        TEST_CLASSES(MainIntValidatorTest);
        TEST_CLASSES(MainDoubleValidatorTest);
        TEST_CLASSES(MainDecimalValidatorTest);

        TEST_CLASSES(MainWeightValidatorTest);
        TEST_CLASSES(MainLengthValidatorTest);
    }
};

} // end namespace ps2
