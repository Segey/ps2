/**
 * \file      ps2/ps2/qt/test/widgets/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   January   (the) 17(th), 2017, 13:39 MSK
 * \updated   March     (the) 27(th), 2017, 23:24 MSK
 * \TODO      
**/
#pragma once
#include "link_widget_test.h"
#include "color_lcdnumber.h"
#include "validator/main.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainWidgetsTest final {
public:
    using class_name = MainWidgetsTest;

public:
    void operator()() noexcept {
        TEST(ps2::LinkWidgetTest);
        TEST(ps2::ColorLcdNumberTest);

        TEST_CLASSES(MainValidatorTest);
    }
};

} // end namespace ps2
