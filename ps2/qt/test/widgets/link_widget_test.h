/**
 * \file      ps2/ps2/qt/test/widgets/link_widget_test.h
 * \brief     The Link_widget_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 17(th), 2017, 13:43 MSK
 * \updated   January (the) 17(th), 2017, 13:43 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/link_widget.h>

/** \namespace ps2 */
namespace ps2 {
    
class LinkWidgetTest final {
public:
    using class_name = LinkWidgetTest;

private:
    static void Simple() {
        LinkWidget link;
        assertTrue(link.url().isEmpty());
        assertTrue(link.text().isEmpty());
    }
    static void Complex() {
        LinkWidget link(QUrl(QStringLiteral("http://irondoom.ru"))
                  , QStringLiteral("It's me"));
        assertEquals(link.url(), QUrl(QStringLiteral("http://irondoom.ru")));
        assertEquals(link.text(), QStringLiteral("It's me"));
    }

public:
    void operator()() noexcept {
        Simple();
        Complex();
    }
};

} // end namespace ps2
