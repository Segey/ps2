/**
 * \file      ps2/ps2/qt/test/widgets/link_widget_test.h
 * \brief     The Link_widget_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 17(th), 2017, 13:43 MSK
 * \updated   January (the) 17(th), 2017, 13:43 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/lcd/color_lcdnumber.h>

/** \namespace ps2 */
namespace ps2 {
    
class ColorLcdNumberTest final {
public:
    using class_name = ColorLcdNumberTest;

private:
    static void Zero() noexcept {
        ColorLcdNumber lcd(Q_NULLPTR);

        lcd.addKey(Qt::Key_Clear);
        assertEquals(lcd.strValue(), "0"_utf8);

        lcd.setValue("0"_utf8);
        lcd.addKey(Qt::Key_Clear);
        assertEquals(lcd.strValue(), "0"_utf8);

        lcd.setValue("55"_utf8);
        lcd.addKey(Qt::Key_Clear);
        assertEquals(lcd.strValue(), "0"_utf8);

        lcd.setValue("55.22"_utf8);
        lcd.addKey(Qt::Key_Clear);
        assertEquals(lcd.strValue(), "0"_utf8);
    }
    static void Digits() noexcept {
        ColorLcdNumber lcd(Q_NULLPTR);

        lcd.addKey(Qt::Key_1);
        assertEquals(lcd.strValue(), "1"_utf8);

        lcd.setValue("0"_utf8);
        lcd.addKey(Qt::Key_5);
        assertEquals(lcd.strValue(), "5"_utf8);

        lcd.setValue("20"_utf8);
        lcd.addKey(Qt::Key_9);
        assertEquals(lcd.strValue(), "209"_utf8);
    }
    static void DoubleDigits() noexcept {
        ColorLcdNumber lcd(Q_NULLPTR);

        lcd.addKey(Qt::Key_1);
        assertEquals(lcd.strValue(), "1"_utf8);

        lcd.setValue("0.55"_utf8);
        lcd.addKey(Qt::Key_7);
        assertEquals(lcd.strValue(), "0.557"_utf8);

        lcd.setValue("0.0255"_utf8);
        lcd.addKey(Qt::Key_2);
        assertEquals(lcd.strValue(), "0.02552"_utf8);
    }
    static void Comma() noexcept {
        ColorLcdNumber lcd(Q_NULLPTR);

        lcd.addKey(Qt::Key_Comma);
        assertEquals(lcd.strValue(), "0."_utf8);

        lcd.setValue("0.55"_utf8);
        lcd.addKey(Qt::Key_Comma);
        assertEquals(lcd.strValue(), "0.55"_utf8);

        lcd.setValue("25.5"_utf8);
        lcd.addKey(Qt::Key_Comma);
        assertEquals(lcd.strValue(), "25.5"_utf8);
    }
    static void BackSpace() noexcept {
        ColorLcdNumber lcd(Q_NULLPTR);

        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "0"_utf8);

        lcd.setValue("5"_utf8);
        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "0"_utf8);

        lcd.setValue("0.55"_utf8);
        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "0.5"_utf8);

        lcd.setValue("25"_utf8);
        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "2"_utf8);

        lcd.setValue("25."_utf8);
        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "25"_utf8);

        lcd.setValue("25.5"_utf8);
        lcd.addKey(Qt::Key_Backspace);
        assertEquals(lcd.strValue(), "25."_utf8);
    }

public:
    void operator()() noexcept {
        Zero();
        Digits();
        DoubleDigits();
        Comma();
        BackSpace();
    }
};

} // end namespace ps2
