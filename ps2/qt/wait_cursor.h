/**
 * \file      ps2/ps2/qt/wait_cursor.h
 * \brief     The class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   August (the) 03(th), 2015, 13:30 MSK
 * \updated   August (the) 03(th), 2015, 13:30 MSK
 * \TODO      
**/
#pragma once
#include <QApplication>

/** \namespace ps2 */
namespace ps2 {

class WaitCursor {
public:
    typedef WaitCursor class_name;

private: 

public:
    WaitCursor(Qt::CursorShape cursor = Qt::WaitCursor)  {
        QApplication::setOverrideCursor(cursor);
    }
    ~WaitCursor(){
        QApplication::restoreOverrideCursor();
    }
};

} // end namespace ps2
