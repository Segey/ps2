/**
 * \file      ps2/ps2/qt/macros.h
 * \brief     The All macros for library
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.01
 * \date      Created on 19 July 2012 y., 00:21
 * \TODO
**/
#pragma once
#include <QApplication>
#include <QMessageBox>

#define OVERRIDE_CURSOR(_a) {\
    QApplication::setOverrideCursor(Qt::WaitCursor);\
    { _a; }\
    QApplication::restoreOverrideCursor();\
}

#define ALERT(_a)  {\
    QMessageBox msgBox;\
    msgBox.setText(QString("%1").arg(_a));\
    msgBox.exec();\
}

#define GOLDEN_RULE 1.61803398874989L
