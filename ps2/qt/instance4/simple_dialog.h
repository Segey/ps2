/**
 * \file      ps2/ps2/qt/instance4/simple_dialog.h
 * \brief     The Dialog_instance class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 06(th), 2016, 12:19 MSK
 * \updated   December (the) 06(th), 2016, 12:19 MSK
 * \TODO      
**/
#pragma once
#include "func_traits.h"

/** \namespace ps2::instance4  */
namespace ps2 {
namespace instance4 {
/**
 * <pre>
 *       template<class T>
 *       class Instance {
 *       public:
 *           using class_name = Instance<T>;
 *
 *       public:
 *           T* m_parent = nullptr;
 *
 *       public:
 *           void doInstanceForm() noexcept {}
 *           void doInstanceWidgets() noexcept {}
 *           void doInstanceLayouts() noexcept {}
 *           void doSetBuddy() noexcept {}
 *           void doUpdateShortKeys() noexcept {}
 *           void doTranslate() noexcept {}
 *       };
 * </pre>
 * \code
 *    ps2::instance4::SimpleDialog<Instance>(this)();
 * \endcode
**/
template<template<class> class U>
struct SimpleDialog {
    template<class P>
    SimpleDialog(P* const parent) noexcept {
        U<P> instance;
        instance.m_parent = parent;
        doInstanceForm(&instance);
        doInstanceWidgets(&instance);
        doInstanceLayouts(&instance);
        doSetBuddy(&instance);
        doUpdateShortKeys(&instance);
        doTranslate(&instance);
    }
    inline void operator()() noexcept {}
};

}} // end namespace p2::instance4

