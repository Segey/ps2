/**
 * \file      ps2/ps2/qt/instance4/simple_ui_dialog.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 04(th), 2015, 02:50 MSK
 * \updated   October (the) 04(th), 2015, 02:50 MSK
 * \TODO      
**/
#pragma once
#include "func_traits.h"

/** \namespace ps2  */
namespace ps2 {
namespace instance4 {
/**
 * <pre>
 *       template<class T, class U>
 *       class Instance {
 *       public:
 *           using class_name = Instance<T,U>;
 *
 *       public:
 *           T* m_parent = nullptr;
 *           U* ui       = nullptr;
 *
 *       public:
 *           void doInstanceForm() noexcept {}
 *           void doInstanceWidgets() noexcept {}
 *           void doInstanceLayouts() noexcept {}
 *           void doSetBuddy() noexcept {}
 *           void doUpdateShortKeys() noexcept {}
 *           void doTranslate() noexcept {}
 *       };
 * </pre>
 * \code
 *    ps2::SimpleUiDialog<Instance>(this, ui)();
 * \endcode
**/
template<template<class, class> class X>
class SimpleUiDialog {
public:
    template<class P, class U>
    SimpleUiDialog(P* const parent, U* ui) {
        X<P,U> instance;
        instance.m_parent = parent;
        instance.ui = ui;
        doInstanceForm(&instance);
        doInstanceWidgets(&instance);
        doInstanceLayouts(&instance);
        doSetBuddy(&instance);
        doUpdateShortKeys(&instance);
        doTranslate(&instance);
    }
    inline void operator()() noexcept {}
};

}} // end namespace p2::instance4

