/**
 * \file      ps2/ps2/qt/instance4/panel.h
 * \brief     The Ui_simple_panels class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 07(th), 2017, 00:46 MSK
 * \updated   February  (the) 07(th), 2017, 00:46 MSK
 * \TODO      
**/
#pragma once
#include <memory>
#include <QWidget>
#include <QDockWidget>
#include "func_traits.h"

/** \namespace ps2::instance4  */
namespace ps2 {
namespace instance4 {
/**
 * <pre>
 *       #include <ps2/qt/instance4/panel.h>
 *
 *       class Instance: public ps2::instance4::Panel {
 *       public:
 *           using class_name = Instance;
 *
 *       public:
 *           void doInstanceForm() noexcept {}
 *           void doInstanceWidgets() noexcept {}
 *           void doInstanceLayouts() noexcept {}
 *           void doTranslate() noexcept {}
 *       };
 * </pre>
 * \code
 *    ps2::SimplePanel<Instance>(this)();
 * \endcode
**/
template<class P>
class PanelT {
public:
    using class_name = PanelT<P>;

protected:
    P* parent = nullptr;

protected:
    virtual void doInstanceForm()    {}
    virtual void doInstanceWidgets() {}
    virtual void doInstanceLayouts() {}
    virtual void doTranslate()       {}

protected:
    void setObjectName(QString const& name) noexcept {
        if (parent->objectName().isEmpty())
            parent->setObjectName(name);
    }
    void setObjectName(QString&& name) noexcept {
        if (parent->objectName().isEmpty())
            parent->setObjectName(std::move(name));
    }

public:
    virtual ~PanelT() = default;
    virtual void instance(P* const parent) noexcept {
        this->parent = parent;
        doInstanceForm();
        doInstanceWidgets();
        doInstanceLayouts();
        doTranslate();
    }
};

using Dock  = PanelT<QDockWidget>;
using Panel = PanelT<QWidget>;

}} // end namespace p2::instance4

