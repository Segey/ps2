/**
 * \file      ps2/ps2/qt/instance4/func_traits.h
 * \brief     The Func_traits class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 06(th), 2016, 14:47 MSK
 * \updated   December (the) 06(th), 2016, 14:47 MSK
 * \TODO      
**/
#pragma once
#include <type_traits>

#define PS2_TRAITS_FUNC(func)                    \
template<class U, typename = typename std::enable_if< std::is_member_function_pointer<decltype(&U::func )>::value, U>::type > \
static inline void func (U* instance) noexcept { \
    instance->func();                            \
}                                                \
template<class U>                                \
static inline void func (U&&) noexcept {}        \


PS2_TRAITS_FUNC(doInstanceForm)
PS2_TRAITS_FUNC(doInstanceWidgets)
PS2_TRAITS_FUNC(doInstanceLayouts)
PS2_TRAITS_FUNC(doSetBuddy)
PS2_TRAITS_FUNC(doUpdateShortKeys)
PS2_TRAITS_FUNC(doTranslate)
PS2_TRAITS_FUNC(doConnect)
