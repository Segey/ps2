/**
 * \file      ps2/ps2/qt/instance4/simple_panels.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 15(th), 2016, 12:30 MSK
 * \updated   January (the) 15(th), 2016, 12:30 MSK
 * \TODO      
**/
#pragma once
#include <memory>
#include "func_traits.h"

/** \namespace ps2::instance4  */
namespace ps2 {
namespace instance4 {
/**
 * <pre>
 *       #include <ps2/qt/instance/panels_instance.h>
 *       template<class T>
 *       class Instance {
 *       public:
 *           typedef Instance<T> class_name;
 *
 *       public:
 *           T* m_parent = nullptr;
 *
 *       public:
 *           void doInstanceForm() noexcept {}
 *           void doInstanceWidgets() noexcept {}
 *           void doInstanceLayouts() noexcept {}
 *           void doSetBuddy() noexcept {}
 *           void doUpdateShortKeys() noexcept {}
 *           void doTranslate() noexcept {}
 *       };
 * </pre>
 * \code
 *    ps2::SimplePanel<Instance>(this)();
 * \endcode
**/
template<template<class> class U>
struct SimplePanel {
    template<class P>
    SimplePanel(P* const parent) noexcept {
        U<P> instance;
        instance.m_parent = parent;
        doInstanceForm(&instance);
        doInstanceWidgets(&instance);
        doInstanceLayouts(&instance);
        doSetBuddy(&instance);
        doUpdateShortKeys(&instance);
        doTranslate(&instance);
    }
    void operator()() noexcept {}
};

}} // end namespace p2::instance4

