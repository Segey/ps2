/**
 * \file      ps2/ps2/qt/instance4/dialog.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January   (the) 25(th), 2016, 12:16 MSK
 * \updated   August    (the) 05(th), 2019, 15:07 MSK
 * \TODO
**/
#pragma once
#include <memory>
#include <QString>
#include <QPixmap>
#include <QDialogButtonBox>

/** \namespace ps2::instance4  */
namespace ps2 {
namespace instance4 {
/**
 * <pre>
 *      #include <ps2/qt/instance4/dialog_instance.h>
 *      template <class T>
 *      class Instance: public ps2::instance4::Dialog {
 *      public:
 *           using parent     = T;
 *           using class_name = Instance<T>;
 *           using inherited  = ps2::instance4::Dialog<T>;
 *
 *       private:
 *           virtual void doInstanceForm() override final {}
 *           virtual void doInstanceWidgets() override final {}
 *           virtual void doInstanceLayouts() override final {}
 *           virtual void doTranslate() override final {}
 *           virtual void doSetBuddy() override final {}
 *           virtual void doConnect() override final {}
 *     };
 * </pre>
 * \code
 *    ps2::instance4::Dialog<Instance>(this, QStringLiteral("Cool"))();
 * \endcode
**/
template<class T>
class Dialog {
public:
    using class_name = Dialog<T>;
    using button_t  = QDialogButtonBox::StandardButton;

protected:
    T* parent = nullptr;
    QString m_title;

protected:
    virtual void doInstanceForm()    {}
    virtual void doInstanceWidgets() {}
    virtual void doInstanceLayouts() {}
    virtual void doTranslate()       {}
    virtual void doSetBuddy()        {}
    virtual void doConnect()         {}

protected:
    void instance() noexcept {
        doInstanceForm();
        doInstanceWidgets();
        doInstanceLayouts();
        doTranslate();
        doSetBuddy();
        doConnect();
    }

protected:
    void setTitle(QString const& title) noexcept {
       m_title = title;
    }
    QString const& title() const noexcept {
       return m_title;
    }
    void instanceForm(QString const& name) noexcept {
        if (parent->objectName().isEmpty())
            parent->setObjectName(name);
        parent->setWindowModality(Qt::WindowModal);
        parent->setSizeGripEnabled(true);
        parent->setModal(true);
    }
    void setOkButtonEnabled(QDialogButtonBox* buts, bool is) noexcept {
        if(auto but = buts->button(QDialogButtonBox::Ok))
            but->setEnabled(is);
    }

public:
    virtual ~Dialog() = default;
    virtual void instance(T* const p, QString const& title) noexcept {
        this->parent = p;
        this->m_title = title;
        p->setWindowTitle(title);
        instance();
    }
};

}} // end namespace p2::instance4

