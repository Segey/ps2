/**
 * \file      ps2/ps2/qt/instance4/ui_dialog.h
 * \brief     The UiDialog class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 24(th), 2016, 02:32 MSK
 * \updated   October (the) 24(th), 2016, 02:32 MSK
 * \TODO
**/
#pragma once
#include <memory>
#include <QString>
#include <QPixmap>
#include <QDialogButtonBox>
#include "dialog.h"

/** \namespace ps2::instance4  */
namespace ps2 {
namespace instance4 {
/**
 * \last projects/Perfect/ExercisesGuide/EgExerciseDialogs/EgExerciseDialog/dialog_instance.h
 * <pre>
 *      #include <ps2/qt/instance4/dialog.h>
 *
 *      template <class UI, class T>
 *      class DialogInstance: public ps2::instance4::UiDialog<UI, T> {
 *      public:
 *          using class_name = DialogInstance<UI, T>;
 *          using inherited  = ps2::instance4::UiDialog<UI, T>;
 *
 *      private:
 *          virtual void doInstanceForm() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL { }
 *          virtual void doInstanceWidgets() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL { }
 *          virtual void doInstanceLayouts() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL {}
 *          virtual void doTranslate() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL{}
 *          virtual void doSetBuddy() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL{}
 *          virtual void doConnect() noexcept Q_DECL_OVERRIDE Q_DECL_FINAL{}
 *
 *      public:
 *      };
 * </pre>
 * \code
 *    ps2::instance4::UiDialog<Instance>(this, QStringLiteral("Cool"))();
 * \endcode
**/
template<class UI, class T>
class UiDialog: public Dialog<T> {
public:
    using class_name = UiDialog<UI, T>;
    using inherited  = Dialog<T>;
    using button_t   = QDialogButtonBox::StandardButton;

protected:
    std::unique_ptr<UI> ui = std::make_unique<UI>();

public:
    virtual ~UiDialog() = default;
    /**
     *  \code
     *     (*ui)->m_buttons;
     *  \endcode
    **/
    UI* operator->() {
        return ui.get();
    }
    UI const* operator->() const {
        return ui.get();
    }
    void instance(T* const p, QString const& title) noexcept override {
        this->parent = p;
        this->m_title = title;
        this->parent->setWindowTitle(title);
        this->ui->setupUi(p);
        inherited::instance();
    }
};

}} // end namespace ps2::instance4

