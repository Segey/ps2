#include <string>
#include <system_error>
#include <QDebug>
#include <QCoreApplication>
#include <ps2/thetest/test.h>
#include <ps2/thetest/qt/helpers.h>
#include "expression/main.h"

extern bool g_log_maybe_exception = false;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TEST_START()
        TEST_SCLASSES(ps2::MainExpressionTest);
	TEST_END()

    return EXIT_SUCCESS;
}
