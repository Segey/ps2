/**
 * \file      ps2/ps2/qt/libs/runtests/expression/expr_test.h 
 * \brief     The Simple_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 27(th), 2017, 13:48 MSK
 * \updated   November  (the) 03(th), 2017, 13:51 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/libs/expressions/expression/expression.h>

/** \namespace ps2::expr */
namespace ps2::expr {

class Test final {
public:
    using class_name = Test;

private:
    static void TestEmptyTemplate() {
        assertException(eval(""), std::logic_error);
        assertException(eval(" "), std::logic_error);
        assertException(eval("\t"), std::logic_error);
        assertException(eval("\t\t "), std::logic_error);
    }
    static void TestErrors() {
        assertException(eval("("), std::logic_error);
        assertException(eval("(5))"), std::logic_error);
        assertException(eval("((5)"), std::logic_error);
        assertException(eval("5+5-"), std::logic_error);
    }
    static void TestSimpleDigit() {
        assertEquals(int(eval("5")), 5);
        assertEquals(int(eval("56")), 56);
        assertDoubleEquals(eval("-516.57"
                         , QLocale(QLocale::English)), -516.57);
        assertDoubleEquals(eval("+1516.57"
                         , QLocale(QLocale::English)), 1516.57);
    }
    static void TestSimpleOperation() {
        assertEquals(int(eval("5 -62 ")), -57);
        assertEquals(int(eval("5+6\t+ 4")), 15);
        assertEquals(int(eval("1*6")), 6);
        assertEquals(int(eval(" 0 * 6")), 0);
        assertEquals(int(eval(" 4 / 2")), 2);
    }
    static void TestDivideByZero() {
        assertException(eval("5/0"), std::logic_error);
    }
    static void TestDoubleOperation() {
        assertEquals(int(eval("5+6+4")), 15);
        assertEquals(int(eval("5 -62 + 66")), 9);
        assertEquals(int(eval("1*6*3")), 18);
        assertEquals(int(eval(" 4 / 2 /2")), 1);
    }
    static void TestDifferentPriorityOperation() {
        assertEquals(int(eval("5+6*4")), 29);
        assertEquals(int(eval("5-6*6")), -31);
        assertEquals(int(eval("5*6 + 6")), 36);
        assertEquals(int(eval("12/2 - 4")), 2);
    }
    static void TestComplexOperation() {
        assertEquals(int(eval("5+6*4+12*5")), 89);
        assertEquals(int(eval("5*0+4 - 12-5")), -13);
    }
    static void TestLeftBracketError() {
        assertException(eval("(0"), std::logic_error);
    }
    static void TestSimpleBrackets() {
        assertEquals(int(eval("(5)")), 5);
        assertEquals(int(eval("(5+1)")), 6);
        assertEquals(int(eval("(5*1)")), 5);
        assertEquals(int(eval("(5/2)")), 2);
        assertEquals(int(eval("12-(5-9)*2")), 20);
    }
    static void TestDoubleBrackets() {
        assertEquals(int(eval("(5)+(5)")), 10);
        assertEquals(int(eval("(5+1)+(4+4)")), 14);
        assertEquals(int(eval("(5*1)*5")), 25);
        assertEquals(int(eval("(5/2)+2*(4-1)")), 8);
    }
    static void TestComplex() {
        assertEquals(int(eval("12-(5-9)*2+(5-0)+66")), 91);
        assertEquals(int(eval("12-(5-9)*2+ 3*(5-100)/66 - 44")), -28);
        assertEquals(int(eval("(88-32*4 + 9)")), -31);
        assertEquals(int(eval("(88-32*4 + 9) -152 *3*(44+9)")), -24199);
        assertEquals(int(eval("2*(66)/52  + 88 - 3*(100/2-8*7.2)"
                         , QLocale(QLocale::English))), 113);
    }
    static void TestUnaryComplex() {
        assertEquals(int(eval("12-(-5-9)*2+(5-0)++66")), 111);
        assertEquals(int(eval("-12-(+5-9)*-2+ 6*(+5--100)/+66 - +44")), -54);
        assertEquals(int(eval("-(88-32*4 + 9)")), 31);
        assertEquals(int(eval("(+88-32*+4 + -9) -152 *+3*(44+9)")), -24217);
        assertEquals(int(eval("2*((66))/-(-52)  + 88 - +3*(+100/2/4-8*7.2)"
                         , QLocale(QLocale::English))), 225);
    }
    static void TestQString() {
        assertEquals(int(eval(QStringLiteral("((44+66))-77"))), 33);
        assertEquals(int(eval(QStringLiteral("55-0+0--8"))), 63);
        assertEquals(int(eval(QStringLiteral("33.688*5-8*4/1 --(55)")
                         , QLocale(QLocale::English))), 191);
        assertEquals(int(eval(QStringLiteral("43,12 - 8,55 * 2,55")
                         , QLocale(QLocale::Russian))), 21);
        assertException(eval(QStringLiteral("43.12 - 8.55")
                         , QLocale(QLocale::Russian)), std::logic_error);
        assertException(eval(QStringLiteral("43,12 - 8.55")
                         , QLocale(QLocale::English)), std::logic_error);
    }
    static void TestWString() {
        assertEquals(int(eval(L"7 -0 *4-7")), 0);
    }

public:
    void operator()() noexcept {
        try{
            TestEmptyTemplate();
            TestErrors();
            TestSimpleDigit();
            TestSimpleOperation();
            TestDoubleOperation();
            TestDivideByZero();
            TestDifferentPriorityOperation();
            TestComplexOperation();
            TestLeftBracketError();
            TestSimpleBrackets();
            TestDoubleBrackets();
            TestComplex();
            TestUnaryComplex();
            TestQString();
            TestWString();
        } catch(std::logic_error const& e) {
            assertErrorMessage(e.what());
        }
    }
};
} // end namespace ps2::expr
