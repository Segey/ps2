/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/runtests/expression/main.h 
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.2.0
 * \created   October   (the) 27(th), 2017, 13:40 MSK
 * \updated   November  (the) 03(th), 2017, 13:49 MSK
 * \TODO      
**/
#pragma once
#include "expr_test.h"
#include "varexpr_test.h"

/** \namespace ps2 */
namespace ps2 {
    
class MainExpressionTest final {
public:
    using class_name = MainExpressionTest;

public:
    void operator()() noexcept {
        TEST(expr::Test);
        TEST(vexpr::Test);
    }
};

} // end namespace ps2
