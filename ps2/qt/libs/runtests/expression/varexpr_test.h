/**
 * \file      D:/Projects/perfect/projects/perfect/Libs/test/varexpr_test.h 
 * \brief     The Varexpr_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 02(th), 2017, 15:35 MSK
 * \updated   November  (the) 02(th), 2017, 15:35 MSK
 * \TODO      
**/
#pragma once
#include <ps2/thetest/qt/locale.h>
#include <ps2/qt/libs/expressions/varexpression/expression.h>

/** \namespace ps2::vexpr */
namespace ps2::vexpr {

class Test final {
public:
    using class_name = Test;

private:
    static void TestEmptyTemplate() {
        assertException(eval(""), std::logic_error);
        assertException(eval(" "), std::logic_error);
        assertException(eval("\t"), std::logic_error);
        assertException(eval("\t\t "), std::logic_error);
    }
    static void TestErrors() {
        assertException(eval("("), std::logic_error);
        assertException(eval("(5))"), std::logic_error);
        assertException(eval("((5)"), std::logic_error);
        assertException(eval("5+5-"), std::logic_error);
    }
    static void TestSimpleDigit() {
        assertEquals(int(eval("5")), 5);
        assertEquals(int(eval("56")), 56);
        assertDoubleEquals(eval("-516.57", thetest::Locale::en()), -516.57);
        assertDoubleEquals(eval("+1516.57", thetest::Locale::en()), 1516.57);
    }
    static void TestSimpleOperation() {
        assertEquals(int(eval("5 -62 ")), -57);
        assertEquals(int(eval("5+6\t+ 4")), 15);
        assertEquals(int(eval("1*6")), 6);
        assertEquals(int(eval(" 0 * 6")), 0);
        assertEquals(int(eval(" 4 / 2")), 2);
    }
    static void TestDivideByZero() {
        assertException(eval("5/0"), std::logic_error);
    }
    static void TestDoubleOperation() {
        assertEquals(int(eval("5+6+4")), 15);
        assertEquals(int(eval("5 -62 + 66")), 9);
        assertEquals(int(eval("1*6*3")), 18);
        assertEquals(int(eval(" 4 / 2 /2")), 1);
    }
    static void TestDifferentPriorityOperation() {
        assertEquals(int(eval("5+6*4")), 29);
        assertEquals(int(eval("5-6*6")), -31);
        assertEquals(int(eval("5*6 + 6")), 36);
        assertEquals(int(eval("12/2 - 4")), 2);
    }
    static void TestComplexOperation() {
        assertEquals(int(eval("5+6*4+12*5")), 89);
        assertEquals(int(eval("5*0+4 - 12-5")), -13);
    }
    static void TestLeftBracketError() {
        assertException(eval("(0"), std::logic_error);
    }
    static void TestSimpleBrackets() {
        assertEquals(int(eval("(5)")), 5);
        assertEquals(int(eval("(5+1)")), 6);
        assertEquals(int(eval("(5*1)")), 5);
        assertEquals(int(eval("(5/2)")), 2);
        assertEquals(int(eval("12-(5-9)*2")), 20);
    }
    static void TestDoubleBrackets() {
        assertEquals(int(eval("(5)+(5)")), 10);
        assertEquals(int(eval("(5+1)+(4+4)")), 14);
        assertEquals(int(eval("(5*1)*5")), 25);
        assertEquals(int(eval("(5/2)+2*(4-1)")), 8);
    }
    static void TestComplex() {
        assertEquals(int(eval("12-(5-9)*2+(5-0)+66")), 91);
        assertEquals(int(eval("12-(5-9)*2+ 3*(5-100)/66 - 44")), -28);
        assertEquals(int(eval("(88-32*4 + 9)")), -31);
        assertEquals(int(eval("(88-32*4 + 9) -152 *3*(44+9)")), -24199);
        assertEquals(int(eval("2*(66)/52  + 88 - 3*(100/2-8*7.2)"
                         , QLocale(QLocale::English))), 113);
    }
    static void TestUnaryComplex() {
        assertEquals(int(eval("12-(-5-9)*2+(5-0)++66")), 111);
        assertEquals(int(eval("-12-(+5-9)*-2+ 6*(+5--100)/+66 - +44")), -54);
        assertEquals(int(eval("-(88-32*4 + 9)")), 31);
        assertEquals(int(eval("(+88-32*+4 + -9) -152 *+3*(44+9)")), -24217);
        assertEquals(int(eval("2*((66))/-(-52)  + 88 - +3*(+100/2/4-8*7.2)"
                         , thetest::Locale::en())), 225);
    }
    static void TestQString() {
        assertEquals(int(eval(QStringLiteral("((44+66))-77"))), 33);
        assertEquals(int(eval(QStringLiteral("55-0+0--8"))), 63);
        assertEquals(int(eval(QStringLiteral("33.688*5-8*4/1 --(55)")
                         , thetest::Locale::en())), 191);
        assertEquals(int(eval(QStringLiteral("43,12 - 8,55 * 2,55")
                         , QLocale(QLocale::Russian))), 21);
        assertException(eval(QStringLiteral("43.12 - 8.55")
                         , QLocale(QLocale::Russian)), std::logic_error);
        assertException(eval(QStringLiteral("43,12 - 8.55")
                         , QLocale(QLocale::English)), std::logic_error);
    }
    static void TestWString() {
        assertEquals(int(eval(L"7 -0 *4-7")), 0);
    }
    static void TestVariable() {
        assertException(eval(QStringLiteral("4-5xy5")
                         , QLocale(QLocale::English)), std::logic_error);
        assertEquals(int(eval("4-5x", {{"x", 2.}})), -6);
        assertEquals(int(eval("4-x", {{"x", 2.}})), 2);
        assertEquals(int(eval("4+(x)", {{"x", 3.}})), 7);
        assertEquals(int(eval("4x+(x)", {{"x", 1.5}})), 7);
        assertEquals(int(eval("4xx+((x))-30", {{"x", 3}})), 9);
        assertEquals(int(eval("4xxx+((x))-30", {{"x", 3}})), 81);
        assertEquals(int(eval("4xxxxx+((x))-130", {{"x", 3}})), 845);
        assertEquals(int(eval("4xx*(x)*xx+((x))-130", {{"x", 3}})), 845);
        assertEquals(int(eval("4xxx(x)x+((x))-130", {{"x", 3}})), 845);
    }
    static void TestDoubleVariables() {
        assertEquals(int(eval("4y-5x + 6y", {{"x", 2.}, {"y", 6}})), 50);
        assertEquals(int(eval("10 - yx -6", {{"x", 2.}, {"y", 3}})), -2);
        assertEquals(int(eval("4xy-y+yx -6", {{"x", 2.}, {"y", 3}})), 21);
        assertException(eval("4xy-y+yx -6", {{"y", 3}}
                     , QLocale(QLocale::English)), std::logic_error);
        assertEquals(int(eval("-4xyx-xy+xyx -6", {{"xy", 2.}, {"x", 3}})), -26);
        assertEquals(int(eval("-4(xy)x-xy+(xy)(x) -6", {{"xy", 2.}, {"x", 3}})), -26);
        assertException(eval("-4(xy)x-xy+(xy)(x) -6 + 8yx", {{"xy", 2.}, {"x", 3}}
                     , QLocale(QLocale::English)), std::logic_error);
    }
    static void TestComplexVariable() {
        assertEquals(int(eval("x-22xx", {{"xx", 2.}, {"x", 3}})), -41);
        assertEquals(int(eval("x-22xx++88xxx", {{"xx", 2.}, {"x", 3}})), 487);
        assertEquals(int(eval("x-22xx++88xxx+(xx)(x)", {{"xx", 2.}, {"x", 3}})), 493);
        assertEquals(int(eval("-77(x)xxxx", {{"xx", 2.}, {"x", 3}})), -4158);
        assertEquals(int(eval("x-22xx++88xxx+(xx)(x) -77(x)xxxx"
                              , {{"xx", 2.}, {"x", 3}})), -3665);
    }
    static void TestDifference() {
        assertEquals(int(eval(QStringLiteral("22x-5x")
                      , {{QStringLiteral("x"), 2.}, })), 34);
        assertEquals(int(eval(L"22x-5x", {{L"x", 2.}})), 34);
    }

public:
    void operator()() noexcept {
        try{
            TestEmptyTemplate();
            TestErrors();
            TestSimpleDigit();
            TestSimpleOperation();
            TestDoubleOperation();
            TestDivideByZero();
            TestDifferentPriorityOperation();
            TestComplexOperation();
            TestLeftBracketError();
            TestSimpleBrackets();
            TestDoubleBrackets();
            TestComplex();
            TestUnaryComplex();
            TestQString();
            TestWString();
            TestVariable();
            TestDoubleVariables();
            TestComplexVariable();
            TestDifference();
        } catch(std::logic_error const& e) {
            assertErrorMessage(e.what());
        }
    }
};
} // end namespace ps2::vexpr
