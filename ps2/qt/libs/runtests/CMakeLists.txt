project(RunTests)
set(EXECUTABLE_NAME RunTests)
include(../ps2.cmake)

message("-- " ${EXECUTABLE_NAME} " app")

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_PATH})

include_directories(${PS2_PATH})

# Find the QtWidgets library
find_package(Qt5Core REQUIRED)

set(HEADER_FILES
    expression/main.h
    expression/expr_test.h
    expression/varexpr_test.h
)
set(SOURCE_FILES
    main.cpp
)
set(LIBS
    ps2expr
    ps2vexpr
)
source_group("Header Files" FILES ${HEADER_FILES})
source_group("Source Files" FILES ${SOURCE_FILES})

add_executable(${EXECUTABLE_NAME} ${GUI_TYPE}
    ${HEADER_FILES}
    ${SOURCE_FILES}
    ${MOC_FILES}
)
target_link_libraries(${EXECUTABLE_NAME}
    Qt5::Core
    ${LIBS}
)
