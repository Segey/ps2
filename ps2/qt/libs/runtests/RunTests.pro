#-------------------------------------------------
#
# Project created by QtCreator 2017-10-26T19:25:25
#
#-------------------------------------------------
TEMPLATE            = app
QT                 += core
QT                 -= gui
TARGET              = ps2Test
PRECOMPILED_HEADER  = ps2_run_tests_pch.h

include(../ps2.pri)

MY_LIBS = -lps2expr \
          -lps2vexpr

LIBS += -L$$DESTDIR $$MY_LIBS

INCLUDEPATH += \
    $$PS2_PATH

HEADERS += \
    expression/main.h \
    expression/expr_test.h \
    expression/varexpr_test.h

SOURCES += \
    main.cpp
