/**
 * \file      perfect/Tools/LibRunner/lib_runner_pch.h
 * \brief     The Lib_runner_pch class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 01(th), 2017, 14:57 MSK
 * \updated   March     (the) 01(th), 2017, 14:57 MSK
 * \TODO      
**/
#include <QDebug>
