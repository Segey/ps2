#-------------------------------------------------
#
# Project created 2016-02-20T01:40:27
#
#-------------------------------------------------
QMAKE_CXXFLAGS += -DQT_NO_CAST_FROM_ASCII      \
                  -DQT_NO_CAST_TO_ASCII        \
                  -DQT_NO_CAST_FROM_BYTEARRAY  \
                  -DQT_NO_URL_CAST_FROM_STRING \
                  -DQT_USE_QSTRINGBUILDER      \
                  -DD_SCL_SECURE_NO_WARNINGS

PS2_PATH          = $$PWD/../../../../ps2
OUTPUT_PATH       = $$PWD/../../../bin/qmake

CONFIG             += c++1z console precompile_header
DEFINES            += QT \
                      QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS     += -Wall
INCLUDEPATH        += .

CONFIG(debug, debug|release) {
    DEFINES += _DEBUG
    CONFIG  += console
    BUILD_TYPE = debug
} else {
    BUILD_TYPE = release
    CONFIG    += build_translations  # Updates ts files
}

unix{
    OS_TYPE = unix
    macx{
        OS_TYPE = macx
    }
    linux{
        !contains(QT_ARCH, x86_64){
            message("Compiling for 32bit system")
            OS_TYPE = linux32
        }else{
            message("Compiling for 64bit system")
            OS_TYPE = linux64
        }
    }
}
win32 {
    OS_PATH = win32
    QMAKE_CXXFLAGS += -wd4096 \
                      -wd4146 \
                      -wd4365 \
                      -wd4371 \
                      -wd4571 \
                      -wd4599 \
                      -wd4619 \
                      -wd4625 \
                      -wd4626 \
                      -wd4628 \
                      -wd4774 \
                      -wd4820 \
                      -wd4868 \
                      -wd4946 \
                      -wd4996 \
                      -wd5026 \
                      -wd5027

    QMAKE_CXXFLAGS += -std:c++17
}

DESTDIR   = $${OUTPUT_PATH}/$${BUILD_TYPE}
