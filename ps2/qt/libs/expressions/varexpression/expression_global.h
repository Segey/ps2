/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/expression_global.h 
 * \brief     The Expression_global class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:23 MSK
 * \updated   November  (the) 03(th), 2017, 15:23 MSK
 * \TODO      
**/
#pragma once

#if defined(PS2_VEXPR_LIBRARY)
    #define PS2_VEXPR_SHARED_EXPORT Q_DECL_EXPORT
#else
    #define PS2_VEXPR_SHARED_EXPORT Q_DECL_IMPORT
#endif
