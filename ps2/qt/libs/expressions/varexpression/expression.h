/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/expression.h 
 * \brief     The Expression class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:23 MSK
 * \updated   November  (the) 03(th), 2017, 15:23 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QString>
#include "expression_global.h"
#include <ps2/cpp/algorithm/functional.h>

/** \namespace ps2::vexpr */
namespace ps2::vexpr {

double PS2_VEXPR_SHARED_EXPORT eval(const char*
                                      , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(const char*
      , std::map<std::string, double, ps2::StrGreater<std::string>> const& , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(std::string const&
                                       , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(std::string const&
      , std::map<std::string, double, ps2::StrGreater<std::string>> const& , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(std::wstring const&
                                       , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(std::wstring const&
      , std::map<std::wstring, double, ps2::StrGreater<std::wstring>> const& , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(QString const&
                                       , QLocale const& = {});
double PS2_VEXPR_SHARED_EXPORT eval(QString const&
      , std::map<QString, double, ps2::StrGreater<QString>> const& , QLocale const& = {});

} // end namespace ps2::vexpr
