/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/token_type.h 
 * \brief     The Token_type class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:23 MSK
 * \updated   November  (the) 03(th), 2017, 15:23 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QString>
#include <ps2/qt/out.h>

enum class TokenType : quint8 {
     Invalid         = 0
    , Plus           = 1
    , Minus          = 2
    , Multiplication = 3
    , Division       = 4
    , LBracket       = 5
    , RBracket       = 6
    , Number         = 7
    , Variable       = 8
};

inline QDebug operator<<(QDebug dbg, TokenType type) {
    return dbg.nospace() << ps2::to_utype(type);
}
inline std::ostream& operator<<(std::ostream& out, TokenType type) {
    return ps2::cout(out, type);
}

Q_DECLARE_METATYPE(TokenType)

