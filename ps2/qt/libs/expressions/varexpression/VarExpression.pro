#-------------------------------------------------
#
# Project created by QtCreator 2017-10-02T15:14:21
#
#-------------------------------------------------
TEMPLATE            = lib
QT                 += core 
TARGET              = ps2vexpr
DEFINES            += PS2_VEXPR_LIBRARY
PRECOMPILED_HEADER  = varexpr_pch.h

include(../../ps2.pri)

INCLUDEPATH += \
    $$PS2_PATH

HEADERS += \
    app.h \
    expression.h \
    expression_global.h \
    token_type.h \
    action/action.h \
    action/variable_action.h \
    action/variable_algo.h

SOURCES += \
    expression.cpp
