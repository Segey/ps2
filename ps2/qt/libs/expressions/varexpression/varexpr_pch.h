/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/varexpr_pch.h 
 * \brief     The Varexpr_pch class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:23 MSK
 * \updated   November  (the) 03(th), 2017, 15:23 MSK
 * \TODO      
**/
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <stdexcept>
#include <QStack>
#include <QDebug>
#include <QString>
#include <QLocale>
#include <QStringRef>
