/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/action/action.h 
 * \brief     The Action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:25 MSK
 * \updated   November  (the) 03(th), 2017, 15:25 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/error.h>
#include <ps2/qt/libs/expressions/lib/action_algo.h>
#include <ps2/qt/libs/expressions/lib/number_algo.h>
#include "../app.h"
#include "../token_type.h"
#include "variable_algo.h"
#include "variable_action.h"

/** \namespace ps2::vexpr */
namespace ps2::vexpr {

class Action {
public:
    using class_name = Action;
    using type_t     = OperationType;

private:
    bool isOp(type_t type) noexcept {
       if(App::ops().empty())
           return false;
       return doPriority(App::ops().top()) <= doPriority(type);
    }
    type_t opActions() {
        auto const type = App::extractOperation();
        while(isOp(type) && App::numsSize() > 1)
            expr::act_algo::calc(App::nums(), App::ops());
        return type;
    }

protected:
    virtual void doOpLeftBracket() = 0;
    virtual void doOpRightBracket() = 0;
    virtual int doPriority(type_t) noexcept = 0;

public:
    virtual ~Action() = default;
    void operator()() {
        while(App::hasNext()) {
            auto const& ch = App::current();
            switch (ch.toLatin1()) {
                case ' ':
                case '\t':
                    App::nextPos();
                    break;
                case '+':
                case '-':
                    if(App::isUnary())
                        App::updateUnary(ch);
                    else
                        App::ops().push(opActions());
                    App::setPrevOp(TokenType::Minus);
                    break;
                case '*':
                case '/':
                    App::ops().push(opActions());
                    App::setPrevOp(TokenType::Multiplication);
                    break;
                case '(':
                    App::ops().push(type_t::LBracket);
                    App::nextPos();
                    doOpLeftBracket();
                    break;
                case ')':
                    App::setPrevOp(TokenType::RBracket);
                    doOpRightBracket();
                    App::nextPos();
                    return;
                default:
                    if(expr::num_algo::isNumber<App>(ch)) {
                         App::nums().push(expr::num_algo::number<App>());
                         App::setPrevOp(TokenType::Number);
                    }
                    else if(var_algo::isVariable(ch)) {
                        VariableAction act;
                        act(var_algo::variable());
                        App::setPrevOp(TokenType::Variable);
                    }
                    else
                        THROW_LOGIC_ERROR("Unsupported char", ch);
            }
        }
    }
    void evaluate() {
        expr::act_algo::evaluate(App::nums(), App::ops());
    }
};

int Action::doPriority(type_t type) noexcept {
    return type == type_t::Minus
        || type == type_t::Plus ? 2 : 1;
}

} // end namespace ps2::vexpr
