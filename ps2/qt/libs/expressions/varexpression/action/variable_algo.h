/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/action/variable_algo.h 
 * \brief     The Variable_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:27 MSK
 * \updated   November  (the) 03(th), 2017, 15:27 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QString>
#include <QStringRef>
#include "app.h"

/** \namespace ps2::vexpr::var_algo */
namespace ps2::vexpr::var_algo {

/** \namespace empty */
namespace {
    static inline bool isNextVariable(QChar const& ch) {
        return ch.isLetterOrNumber();
    }
} // end namespace empty

static inline bool isVariable(QChar const& ch) noexcept {
    return ch.isLetter();
}
static inline QStringRef createValue(int beg, int end) noexcept {
    return QStringRef(&App::data(), beg, end - beg);
}
static auto variable() {
    auto _1 = App::pos();
    while(isNextVariable(App::current()) && App::hasNext())
        App::nextPos();
    return qMakePair(_1, App::pos());
}

} // end namespace ps2::vexpr::var_algo
