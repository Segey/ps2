/**
 * \file      D:/Projects/perfect/projects/perfect/Libs/VarExpression/action/variable_action.h 
 * \brief     The Variable_action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 02(th), 2017, 18:42 MSK
 * \updated   November  (the) 02(th), 2017, 18:42 MSK
 * \TODO      
**/
#pragma once
#include <QStringRef>
#include <ps2/qt/libs/expressions/lib/operation_type.h>
#include "../app.h"

/** \namespace ps2::vexpr */
namespace ps2::vexpr {
    
class VariableAction final {
public:
    using class_name = VariableAction;

private:
    void updateUnary(double value) const noexcept {
        App::nums().push(value);

        if(App::prevOp() == TokenType::Number
            || App::prevOp() == TokenType::Variable
            || App::prevOp() == TokenType::LBracket
            || App::prevOp() == TokenType::RBracket)
             App::ops().push(OperationType::Multiplication);
    }
    bool parse(int beg, int end) const {
        if(beg >= end)
            return false;

        auto val = var_algo::createValue(beg, end);
        for(auto const& it: App::vars()) {
            if(val == QString::fromStdString(it.first)) {
                 updateUnary(it.second);
                 App::setPrevOp(TokenType::Variable);
                 return true;
            }
        }
        if(!parse(beg, --end))
            return false;

        beg = end;
        return parse(beg, ++end);
    }

public:
    void operator()(QPair<int, int> const& p) {
        if(App::vars().empty())
            THROW_LOGIC_ERROR("No one variable is presented!");
        if(!parse(p.first, p.second))
            THROW_LOGIC_ERROR("Cannot find a variable");

    }
};

} // end namespace ps2::vexpr
