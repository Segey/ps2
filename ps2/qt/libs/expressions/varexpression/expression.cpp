#include <ps2/qt/libs/expressions/lib/parser.h>
#include <ps2/qt/libs/expressions/lib/main_action.h>
#include <ps2/qt/libs/expressions/lib/folding_action.h>
#include "app.h"
#include "expression.h"
#include "action/action.h"

/** \namespace ps2::vexpr */
namespace ps2::vexpr {

static auto fromWStr(std::map<std::wstring, double, ps2::StrGreater<std::wstring>> const& map) noexcept {
    std::map<std::string, double, ps2::StrGreater<std::string>> result;
    for(auto const& it: map) {
        std::string s(std::begin(it.first), std::end(it.first));
        result.insert<std::pair<std::string, double>>(std::make_pair(s, it.second));
    }
    return result;
}
static auto fromQStr(std::map<QString, double, ps2::StrGreater<QString>> const& map) noexcept {
    std::map<std::string, double, ps2::StrGreater<std::string>> result;
    for(auto const& it: map)
        result.insert<std::pair<std::string, double>>(std::make_pair(it.first.toStdString(), it.second));
    return result;
}
double eval() {
    return expr::Parser<App, expr::MainAction<App, Action>>()();
}

double eval(char const* s, QLocale const& loc) {
    return eval(s, {{}}, loc);
}
double eval(char const* s, std::map<std::string, double, ps2::StrGreater<std::string>> const& vars, QLocale const& loc) {
    if(strlen(s) < 1)
        THROW_LOGIC_ERROR("Empty string");

    App::init(loc, QString::fromLatin1(s), vars);
    return eval();
}
double eval(std::string const& s, QLocale const& loc) {
    return eval(s, {{}}, loc);
}
double eval(std::string const& s, std::map<std::string, double, ps2::StrGreater<std::string>> const& vars, QLocale const& loc) {
    if(s.empty())
        THROW_LOGIC_ERROR("Empty string");

    App::init(loc, QString::fromStdString(s), vars);
    return eval();
}
double eval(std::wstring const& s, QLocale const& loc) {
    return eval(s, {{}}, loc);
}
double eval(std::wstring const& s, std::map<std::wstring, double, ps2::StrGreater<std::wstring>> const& vars, QLocale const& loc) {
    if(s.empty())
        THROW_LOGIC_ERROR("Empty string");

    App::init(loc, QString::fromStdWString(s), fromWStr(vars));
    return eval();
}
double eval(QString const& s, QLocale const& loc) {
    return eval(s, {{}}, loc);
}
double eval(QString const& s, std::map<QString, double, ps2::StrGreater<QString>> const& vars, QLocale const& loc) {
    if(s.isEmpty())
        THROW_LOGIC_ERROR("Empty string");

    App::init(loc, s, fromQStr(vars));
    return eval();
}

} // end namespace ps2::vexrp
