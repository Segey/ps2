/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/app.h 
 * \brief     The App class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:24 MSK
 * \updated   November  (the) 03(th), 2017, 15:24 MSK
 * \TODO      
**/
#pragma once
#include <map>
#include <QStack>
#include <ps2/cpp/algorithm/functional.h>
#include <ps2/qt/libs/expressions/lib/operation_type.h>
#include "token_type.h"

/** \namespace ps2::vexpr */
namespace ps2::vexpr {

struct App final {
public:
    using class_name = App;
    using type_t     = OperationType;
    using nums_t     = QStack<double>;
    using ops_t      = QStack<type_t>;
    using vars_t     = std::map<std::string, double, ps2::StrGreater<std::string>>;
    using token_t    = TokenType;

public: 
    static QLocale m_loc;
    static QString m_data;
    static nums_t m_nums;
    static ops_t m_ops;
    static int m_pos;
    static vars_t m_vars;
    static token_t m_prev;

public:
    static void init(QLocale const& loc, QString const& data,
                     vars_t const& vars) noexcept {
        m_loc = loc;
        m_data = data;
        m_nums.clear();
        m_ops.clear();
        m_pos  = 0;
        m_vars = vars;
        m_prev = token_t::Invalid;
    }
    static QLocale const& loc() noexcept {
        return m_loc;
    }
    static QString& data() noexcept {
        return m_data;
    }
    static decltype(m_data[m_pos]) current() noexcept {
        return m_data[m_pos];
    }
    static type_t extractOperation() {
        OperationType type;
        ps2::from_qstr(m_data[m_pos++], type);
        return type;
    }
    static int& pos() noexcept {
        return m_pos;
    }
    static void nextPos() noexcept {
        ++m_pos;
    }
    static nums_t& nums() noexcept {
        return m_nums;
    }
    static decltype(m_nums.size()) numsSize() noexcept {
        return m_nums.size();
    }
    static ops_t& ops() noexcept {
        return m_ops;
    }
    static decltype(m_ops.size()) opsSize() noexcept {
        return m_ops.size();
    }
    static bool hasNext() noexcept {
        return m_pos < m_data.size();
    }
    static vars_t& vars() noexcept {
        return m_vars;
    }
    static token_t prevOp() noexcept {
        return m_prev;
    }
    static void setPrevOp(TokenType type) noexcept {
        m_prev = type;
    }
    static bool isUnary() noexcept {
        return m_prev != token_t::LBracket
            && m_prev != token_t::RBracket
            && m_prev != token_t::Variable
            && m_prev != token_t::Number;
    }
    static void updateUnary(QChar const& ch) noexcept {
        m_ops.push(type_t::Multiplication);
        m_nums.push(ch.toLatin1() == '-' ? -1 : 1);
        ++m_pos;
    }
};

QLocale App::m_loc;
QString App::m_data;
int App::m_pos = 0;
App::nums_t App::m_nums;
App::ops_t App::m_ops;
App::vars_t App::m_vars;
App::token_t App::m_prev;

} // end namespace ps2::vexpr
