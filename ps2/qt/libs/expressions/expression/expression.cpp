#include <ps2/qt/libs/expressions/lib/parser.h>
#include <ps2/qt/libs/expressions/lib/main_action.h>
#include <ps2/qt/libs/expressions/lib/folding_action.h>
#include "app.h"
#include "action.h"
#include "expression.h"

/** \namespace ps2::expr */
namespace ps2::expr {

double eval(std::string const& s, QLocale const& loc) {
    return eval(QString::fromStdString(s), loc);
}
double eval(char const* s, QLocale const& loc) {
    return eval(QString::fromLatin1(s), loc);
}
double eval(std::wstring const& s, QLocale const& loc) {
    return eval(QString::fromStdWString(s), loc);
}
double eval(QString const& s, QLocale const& loc) {
    if(s.isEmpty())
        THROW_LOGIC_ERROR("Empty string");

    App::init(loc, s);
    return Parser<App, MainAction<App, Action>>()();
}

} // end namespace ps2::expr
