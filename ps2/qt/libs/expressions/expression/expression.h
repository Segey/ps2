/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/expression/expression.h 
 * \brief     The Expression class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 26(th), 2017, 18:38 MSK
 * \updated   October   (the) 26(th), 2017, 18:38 MSK
 * \note      Algorithm Бауэра и Замельзона
 * \TODO
**/
#pragma once
#include <QLocale>
#include <QString>
#include "expression_global.h"

/** \namespace ps2::expr */
namespace ps2::expr {

double PS2_EXPR_SHARED_EXPORT eval(char const*
                                         , QLocale const& = {});
double PS2_EXPR_SHARED_EXPORT eval(std::string const&
                                         , QLocale const& = {});
double PS2_EXPR_SHARED_EXPORT eval(std::wstring const&
                                         , QLocale const& = {});
double PS2_EXPR_SHARED_EXPORT eval(QString const&
                                         , QLocale const& = {});

} // end namespace ps2::expr
