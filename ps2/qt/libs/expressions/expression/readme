============================================================================
FORMAT - HELP

; Created:  26th October  2017
; Updated:  02th November 2017

============================================================================

Simple math expresssion

## **Supported features** ##
 * Expresssions: + - * /
 * Unlimited parentheses
 * Skips ' ' and '\t' characters
 * Errors classes

## **Supported strings** ##
 * std::string
 * std::wstring
 * QString

## ** Functions ** ##
 * double eval(std::string const& s, QLocale const& loc = {}); 
 * double eval(std::wstring const& s, QLocale const& loc = {});
 * double eval(QString const& s, QLocale const& loc = {});

### **Error supports** ###
 * std::logic_error

```
#c++
#include <ps2/qt/libs/expressions/expression/expression.h>
try {
    ps2::expr::eval("(5))");
}
catch(std::logic_error const& e) {
}
```

### **Error string supports** ###
 * Empty string
 * Cannot find any number - Expression doesn't contain any number
 * More than one number has found - Expresssion contain more that 1 number 
 * Found unused operations - Expresssion contain unused operations
 * Unknown type of OperationType 
 * Invalid OperationType {number}
 * Unsupported char - {char} - Found unsupported char
 * Divide by zero 
 * Invalid operation - {type}
 * Found ')' character - '(' character doesn't exist
 * Invalid double 

```
#c++
    auto const res = ps2::expr::eval("2*((66))/-(-52)  + 88 - +3*(+100/2/4-8*7.2)");
    auto const res = ps2::expr::eval(L"2*((66))/-(-52)  + 88 - +3*(+100/2/4-8*7.2)");
    auto const res = ps2::expr::eval(QStringLiteral("33.688*5-8*4/1 --(55)"), QLocale(QLocale::English));
```

```=============================== End of file ================================
