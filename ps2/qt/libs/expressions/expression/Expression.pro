#-------------------------------------------------
#
# Project created by QtCreator 2017-10-26T18:13:27
#
#-------------------------------------------------
TEMPLATE            = lib
QT                 += core
TARGET              = ps2expr
DEFINES            += PS2_EXPR_LIBRARY
PRECOMPILED_HEADER  = expression_pch.h

include(../../ps2.pri)

INCLUDEPATH += \
    $$PS2_PATH

HEADERS += \
    expression.h \
    expression_global.h \
    action.h \
    app.h

SOURCES += \
    expression.cpp
