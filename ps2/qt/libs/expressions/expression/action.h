/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/libs/expression/action/action.h 
 * \brief     The Action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 31(th), 2017, 00:30 MSK
 * \updated   October   (the) 31(th), 2017, 00:30 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/error.h>
#include <ps2/qt/libs/expressions/lib/action_algo.h>
#include <ps2/qt/libs/expressions/lib/number_algo.h>
#include "app.h"

/** \namespace ps2::expr */
namespace ps2::expr {

class Action {
public:
    using class_name = Action;
    using type_t     = OperationType;

private:
    bool isOp(type_t type) noexcept {
       if(App::ops().empty())
           return false;
       return doPriority(App::ops().top()) <= doPriority(type);
    }
    type_t opActions() {
        auto const type = App::extractOperation();
        while(isOp(type) && App::numsSize() > 1)
            act_algo::calc(App::nums(), App::ops());
        return type;
    }

protected:
    virtual void doOpLeftBracket() = 0;
    virtual void doOpRightBracket() = 0;
    virtual int doPriority(type_t) noexcept = 0;

public:
    virtual ~Action() = default;
    void operator()() {
        auto unaryOp = true;
        while(App::hasNext()) {
            auto const& ch = App::current();
            switch (ch.toLatin1()) {
                case ' ':
                case '\t':
                    App::nextPos();
                    break;
                case '+':
                case '-':
                    if(unaryOp)
                        App::updateUnary(ch);
                    else
                        App::ops().push(opActions());
                    unaryOp = true;
                    break;
                case '*':
                case '/':
                    App::ops().push(opActions());
                    unaryOp = true;
                    break;
                case '(':
                    App::ops().push(type_t::LBracket);
                    App::nextPos();
                    doOpLeftBracket();
                    unaryOp = false;
                    break;
                case ')':
                    unaryOp = false;
                    doOpRightBracket();
                    App::nextPos();
                    return;
                default:
                    if(num_algo::isNumber<App>(ch)) {
                         unaryOp = false;
                         App::nums().push(num_algo::number<App>());
                    }
                    else
                        THROW_LOGIC_ERROR("Unsupported char", ch);
            }
        }
    }
    void evaluate() {
        act_algo::evaluate(App::nums(), App::ops());
    }
};

int Action::doPriority(type_t type) noexcept {
    return type == type_t::Minus
        || type == type_t::Plus ? 2 : 1;
}

} // end namespace ps2::expr
