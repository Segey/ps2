/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/expression/dll.h 
 * \brief     The Dll class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 01(th), 2017, 18:44 MSK
 * \updated   November  (the) 01(th), 2017, 18:44 MSK
 * \TODO      
**/
#pragma once
#include <QStack>
#include <ps2/qt/libs/expressions/lib/operation_type.h>

/** \namespace ps2::expr */
namespace ps2::expr {

struct App final {
public:
    using class_name = App;
    using type_t     = OperationType;
    using nums_t     = QStack<double>;
    using ops_t      = QStack<type_t>;
     
public: 
    static QLocale m_loc;
    static QString m_data;
    static nums_t m_nums;
    static ops_t m_ops;
    static int m_pos;

public:
    static void init(QLocale const& loc, QString const& data) noexcept {
        m_loc = loc;
        m_data = data;
        m_nums.clear();
        m_ops.clear();
        m_pos  = 0;
    }
    static QLocale const& loc() noexcept {
        return m_loc;
    }
    static QString& data() noexcept {
        return m_data;
    }
    static decltype(m_data[m_pos]) current() noexcept {
        return m_data[m_pos];
    }
    static type_t extractOperation() {
        OperationType type;
        ps2::from_qstr(m_data[m_pos++], type);
        return type;
    }
    static int& pos() noexcept {
        return m_pos;
    }
    static void nextPos() noexcept {
        ++m_pos;
    }
    static nums_t& nums() noexcept {
        return m_nums;
    }
    static decltype(m_nums.size()) numsSize() noexcept {
        return m_nums.size();
    }
    static ops_t& ops() noexcept {
        return m_ops;
    }
    static decltype(m_ops.size()) opsSize() noexcept {
        return m_ops.size();
    }
    static bool hasNext() noexcept {
        return m_pos < m_data.size();
    }
    static void updateUnary(QChar const& ch) noexcept {
        m_ops.push(type_t::Multiplication);
        m_nums.push(ch.toLatin1() == '-' ? -1 : 1);
        ++m_pos;
    }
};

QLocale App::m_loc;
QString App::m_data;
int App::m_pos = 0;
App::nums_t App::m_nums;
App::ops_t App::m_ops;

} // end namespace ps2::expr
