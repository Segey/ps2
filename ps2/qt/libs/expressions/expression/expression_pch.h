/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/expression/expression_pch.h 
 * \brief     The Expression_pch class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 26(th), 2017, 18:46 MSK
 * \updated   October   (the) 26(th), 2017, 18:46 MSK
 * \TODO      
**/
#include <memory>
#include <string>
#include <stdexcept>
#include <QStack>
#include <QDebug>
#include <QString>
#include <QLocale>
#include <QStringRef>
