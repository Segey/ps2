/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/expression/expression_global.h 
 * \brief     The Expression_global class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 26(th), 2017, 18:40 MSK
 * \updated   October   (the) 26(th), 2017, 18:40 MSK
 * \TODO      
**/
#pragma once

#if defined(PS2_EXPR_LIBRARY)
    #define PS2_EXPR_SHARED_EXPORT Q_DECL_EXPORT
#else
    #define PS2_EXPR_SHARED_EXPORT Q_DECL_IMPORT
#endif
