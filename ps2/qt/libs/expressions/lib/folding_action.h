/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/action/folding_action.h 
 * \brief     The Folding_action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:26 MSK
 * \updated   November  (the) 03(th), 2017, 15:26 MSK
 * \TODO      
**/
#pragma once
#include "action_algo.h"
#include "operation_type.h"
#include "action_algo.h"
#include "operation_type.h"
#include "folding_action.h"
#include "main_action.h"

/** \namespace ps2::vexpr */
namespace ps2::expr {
    
template<class App, class T>
class FoldingAction final: public T {
public:
    using class_name = FoldingAction<App, T>;
    using inherited  = T;
    using type_t     = OperationType;

protected:
    virtual void doOpLeftBracket() override final {
        class_name act;
        act();
    }
    virtual void doOpRightBracket() override final {
        while(App::nums().size() >= 2
              && App::ops().top() != type_t::LBracket)
            expr::act_algo::calc(App::nums(), App::ops());
        App::ops().pop();
    }
   virtual int doPriority(type_t type) noexcept override final {
        if(type == type_t::LBracket)
            return 4;
        return inherited::doPriority(type);
    }

public:
    explicit FoldingAction() = default;
};

} // end namespace ps2::expr
