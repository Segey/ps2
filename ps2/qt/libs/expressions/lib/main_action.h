/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/action/main_action.h 
 * \brief     The Main_action class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.1
 * \created   November  (the) 03(th), 2017, 15:26 MSK
 * \updated   November  (the) 04(th), 2017, 01:35 MSK
 * \TODO      
**/
#pragma once

/** \namespace ps2::expr */
namespace ps2::expr {
    
template<class App, class T>
class MainAction final: public T {
public:
    using class_name = MainAction<App, T>;
    using inherited  = T;
    using type_t     = typename T::type_t;

protected:
    virtual void doOpLeftBracket() override final {
        FoldingAction<App, T> act;
        act();
    }
    virtual void doOpRightBracket() override final {
        THROW_LOGIC_ERROR("Found ')' character");
    }
    virtual int doPriority(type_t type) noexcept override final {
        return inherited::doPriority(type);
    }

public:
    explicit MainAction() = default;
};

} // end namespace ps2::vexpr
