/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/action/action_algo.h 
 * \brief     The Action_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:25 MSK
 * \updated   November  (the) 03(th), 2017, 15:25 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/error.h>
#include "operation_type.h"

/** \namespace ps2::vexpr::act_algo */
namespace ps2::expr::act_algo {

/** \namespace empty */
namespace {
    template<class N, class O>
    static double exec(N& nums, O& ops) {
        auto _2 = nums.pop();
        auto _1 = nums.pop();
        auto type = ops.pop();

        if(type == OperationType::Plus)
            return _1 + _2;
        if(type == OperationType::Minus)
            return _1 - _2;
        if(type == OperationType::Multiplication)
            return _1 * _2;
        if(type == OperationType::Division) {
            if(_2 == 0)
               THROW_LOGIC_ERROR("Divide by zero");
            return _1 / _2;
        }

        THROW_LOGIC_ERROR("invalid operation - ", ps2::to_tr(type));
        return .0;
    }
} // end namespace empty

template<class N, class O>
static void calc(N& nums, O& ops) {
    nums.push(exec(nums, ops));
}

template<class N, class O>
static inline void evaluate(N& nums, O& ops) {
    while(nums.size() > 1 && !ops.empty())
       calc(nums, ops);
}

} // end namespace ps2::vexpr::act_algo
