/**
 * \file      projects/ps2/ps2/qt/libs/expression/action/number_algo.h 
 * \brief     The Number_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 01(th), 2017, 17:19 MSK
 * \updated   November  (the) 03(th), 2017, 18:12 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QString>
#include <QStringRef>
#include <ps2/qt/error.h>

/** \namespace ps2::expr::num_algo */
namespace ps2::expr::num_algo {

/** \namespace empty */
namespace {
    template<class App>
    static inline double createNumber(QStringRef const& val) {
        bool ok = false;
        auto const res = App::loc().toDouble(val, &ok);
        if(!ok)
            THROW_LOGIC_ERROR("Invalid double ", val.toString());
        return res;
    }
} // end namespace empty

template<class App>
static inline bool isNumber(QChar const& ch) noexcept {
    return ch.isDigit()
        || ch == App::loc().decimalPoint()
        || ch == App::loc().groupSeparator();
}

template<class App>
static inline double number() {
    auto _1 = App::pos();
    while(isNumber<App>(App::current()) && App::hasNext())
        App::nextPos();
    return createNumber<App>(QStringRef(&App::data(), _1, App::pos() - _1));
}

} // end num_algo::namespace ps2::num_algo
