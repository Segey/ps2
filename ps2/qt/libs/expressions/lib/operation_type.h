/**
 * \file      C:/projects/perfect/projects/ps2/ps2/qt/libs/expression/item/operation_type.h 
 * \brief     The Operation_type class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 29(th), 2017, 00:39 MSK
 * \updated   October   (the) 29(th), 2017, 00:39 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>
#include <QString>
#include <ps2/qt/out.h>
#include <ps2/qt/error.h>

/** begin namespace ps2 */
namespace ps2 {
enum class OperationType : quint8 {
     Invalid         = 0
    , Plus           = 1
    , Minus          = 2
    , Multiplication = 3
    , Division       = 4
    , LBracket       = 5
    , RBracket       = 6
    , LCurBracket    = 7
    , RCurBracket    = 8
    , Percent        = 9
    , Colon          = 10
};

static inline QLatin1Char to_tr(OperationType type) {
    if (type == OperationType::Plus)           return QLatin1Char('+');
    if (type == OperationType::Minus)          return QLatin1Char('-');
    if (type == OperationType::Multiplication) return QLatin1Char('*');
    if (type == OperationType::Division)       return QLatin1Char('/');
    if (type == OperationType::LBracket)       return QLatin1Char('(');
    if (type == OperationType::RBracket)       return QLatin1Char(')');
    if (type == OperationType::LCurBracket)    return QLatin1Char('{');
    if (type == OperationType::RCurBracket)    return QLatin1Char('}');
    if (type == OperationType::Percent)        return QLatin1Char('%');
    if (type == OperationType::Colon)          return QLatin1Char(':');

    THROW_LOGIC_ERROR("Unknown type of OperationType");
    return QLatin1Char(' ');
}
static inline QLatin1Char to_qstr(OperationType type) {
    return to_tr(type);
}
static inline QLatin1Char to_sqtr(OperationType type) {
    return to_qstr(type);
}
static inline bool from_qstr(QChar const& ch, OperationType& val
                                  , QLocale const& = {}) {
    if (ch == QLatin1Char('*')) val = OperationType::Multiplication;
    else if (ch == QLatin1Char('/')) val = OperationType::Division;
    else if (ch == QLatin1Char('+')) val = OperationType::Plus;
    else if (ch == QLatin1Char('-')) val = OperationType::Minus;
    else if (ch == QLatin1Char('(')) val = OperationType::LBracket;
    else if (ch == QLatin1Char(')')) val = OperationType::RBracket;
    else if (ch == QLatin1Char('{')) val = OperationType::LCurBracket;
    else if (ch == QLatin1Char('}')) val = OperationType::RCurBracket;
    else if (ch == QLatin1Char('%')) val = OperationType::Percent;
    else if (ch == QLatin1Char(':')) val = OperationType::Colon;
    else
        THROW_LOGIC_ERROR("Invalid OperationType ", ch);
    return true;
}
inline void from_value(quint8 id, OperationType& val) {
    if (id == 1) val = OperationType::Plus;
    else if (id == 2) val = OperationType::Minus;
    else if (id == 3) val = OperationType::Multiplication;
    else if (id == 4) val = OperationType::Division;
    else if (id == 5) val = OperationType::LBracket;
    else if (id == 6) val = OperationType::RBracket;
    else if (id == 7) val = OperationType::LCurBracket;
    else if (id == 8) val = OperationType::RCurBracket;
    else if (id == 9) val = OperationType::Percent;
    else if (id == 10) val = OperationType::Colon;
    else
        THROW_LOGIC_ERROR("Invalid OperationType ", id);
}
} // end namespace ps2

inline QDebug operator<<(QDebug dbg, ps2::OperationType type) {
    return dbg.nospace() << ps2::to_qstr(type);
}
inline std::ostream& operator<<(std::ostream& out, ps2::OperationType type) {
    return ps2::cout(out, type);
}

Q_DECLARE_METATYPE(ps2::OperationType)

