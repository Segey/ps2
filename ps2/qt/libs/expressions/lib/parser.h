/**
 * \file      D:/Projects/perfect/projects/ps2/ps2/qt/libs/varexpression/parser.h 
 * \brief     The Parser class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   November  (the) 03(th), 2017, 15:23 MSK
 * \updated   November  (the) 04(th), 2017, 01:45 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/error.h>

/** \namespace ps2::vexpr */
namespace ps2::expr {

template<class App, class MainAction>
class Parser final {
public:
    using class_name = Parser<App, MainAction>;

private:
    double extractValue() {
        if(App::numsSize() == 1 && App::ops().empty())
            return App::nums().top();

        if(App::nums().empty())
            THROW_LOGIC_ERROR("Cannot find any number");

        if(App::nums().size() > 1)
            THROW_LOGIC_ERROR("More than one number has found");

        THROW_LOGIC_ERROR("Found unused operations");
        return .0;
    }

public:
    double operator()() {
        MainAction act;
        act();
        act.evaluate();
        return extractValue();
    }
};

} // end namespace ps2::expr
