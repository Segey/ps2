/**
 * \file      ps2/ps2/qt/libs/test/qstring_format_test.h
 * \brief     The Qstring_format_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 20(th), 2016, 00:58 MSK
 * \updated   December (the) 20(th), 2016, 00:58 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/libs/format/format.h>

class QStringFormatTest {
public:
    using class_name = QStringFormatTest;

private:
    static void Empty() noexcept {
        auto&& result = fmt::print(QString{});

        assertTrue(result.isEmpty());
    }
    static void Simple() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool its"));

        assertFalse(result.isEmpty());
        assertEquals(result, QStringLiteral("Cool its"));
    }
    static void SimpleBrackets() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool its {}"));

        assertFalse(result.isEmpty());
        assertEquals(result, QStringLiteral("Cool its {}"));
    }
    static void SimpleFirst() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool its {}"), 1);
        assertEquals(result, QStringLiteral("Cool its 1"));
    }
    static void SimpleSecond() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool {} its {}"), 1
                                   , QStringLiteral("hello"));
        assertEquals(result, QStringLiteral("Cool 1 its hello"));
    }
    static void Invalid() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool {} its {"), 1
                                   ,  QStringLiteral("hello"));
        assertTrue(result.isEmpty());
    }
    static void InvalidSecond() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool {{} its {}"), 1
                                       , QStringLiteral("hello"));
        assertEquals(result, QStringLiteral("Cool {} its 1"));
    }
    static void InvalidThird() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool {{} its {}{}{}"), 1
                                           , QStringLiteral("hello"));
        assertTrue(result.isEmpty());
    }
    static void Comment() noexcept {
        auto&& result = fmt::print(QStringLiteral("Cool {{{} its {}"), 2
                                               , QStringLiteral("hello"));
        assertEquals(result, QStringLiteral("Cool {2 its hello"));
    }

public:
    void operator()() noexcept {
        Empty();
        Simple();
        SimpleBrackets();
        SimpleFirst();
        SimpleSecond();
        Invalid();
        InvalidSecond();
        InvalidThird();
        Comment();
    }
};

