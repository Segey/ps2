/**
 * \file      ps2/ps2/qt/libs/test/string_format_test.h
 * \brief     The String_format_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 20(th), 2016, 00:58 MSK
 * \updated   December (the) 20(th), 2016, 00:58 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/libs/format/format.h>

class StringFormatTest {
public:
    using class_name = StringFormatTest;

private:
    static void Empty() noexcept {
        auto&& result = fmt::print(""s);

        assertTrue(result.empty());
    }
    static void Simple() noexcept {
        auto&& result = fmt::print("Cool its"s);

        assertFalse(result.empty());
        assertEquals(result, "Cool its"s);
    }
    static void SimpleBrackets() noexcept {
        auto&& result = fmt::print("Cool its {}"s);

        assertFalse(result.empty());
        assertEquals(result, "Cool its {}"s);
    }
    static void SimpleFirst() noexcept {
        auto&& result = fmt::print("Cool its {}"s, 1);
        assertEquals(result, "Cool its 1"s);
    }
    static void SimpleSecond() noexcept {
        auto&& result = fmt::print("Cool {} its {}"s, 1, "hello");
        assertEquals(result, "Cool 1 its hello"s);
    }
    static void Invalid() noexcept {
        auto&& result = fmt::print("Cool {} its {"s, 1, "hello");
        assertTrue(result.empty());
    }
    static void InvalidSecond() noexcept {
        auto&& result = fmt::print("Cool {{} its {}"s, 1, "hello");
        assertEquals(result, "Cool {} its 1"s);
    }
    static void InvalidThird() noexcept {
        auto&& result = fmt::print("Cool {{} its {}{}{}"s, 1, "hello");
        assertTrue(result.empty());
    }
    static void Comment() noexcept {
        auto&& result = fmt::print("Cool {{{} its {}"s, 1, "hello");
        assertEquals(result, "Cool {1 its hello"s);
    }

public:
    void operator()() noexcept {
        Empty();
        Simple();
        SimpleBrackets();
        SimpleFirst();
        SimpleSecond();
        Invalid();
        InvalidSecond();
        InvalidThird();
        Comment();
    }
};

