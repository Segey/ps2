/**
 * \file      ps2/ps2/qt/libs/test/mustash_test.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   September (the) 24(th), 2015, 13:31 MSK
 * \updated   December  (the) 19(th), 2016, 01:13 MSK
 * \TODO      
**/
#pragma once
#include <QJsonArray>
#include <QJsonObject>
#include <ps2/qt/libs/mustash/mustash.h>

class MustashTest {
public:
    using class_name = MustashTest;

private:
    static void TestEmptyTemplate() {
        auto&& mold = QStringLiteral("<td>Cool</td>");

        assertEquals(mu::render(mold), mold);
    }
    static void TestEmptyTemplateWithUnaryBracket() {
        auto&& mold   = QStringLiteral("<td>{name}</td>");
        auto&& result = QStringLiteral("<td>{name}</td>");

        QJsonObject&& object{{QStringLiteral("name"), QStringLiteral("Сергей")}};

        assertEquals(mu::render(mold, object), result);
    }
    static void TestVariables() {
        auto&& mold   = QStringLiteral("{{name}}-{{age}}::{{company}}");
        auto&& result = QStringLiteral("Сергей-::<b>topcon</b>");

        QJsonObject&& object{
              {QStringLiteral("name"), QStringLiteral("Сергей")}
            , {QStringLiteral("company"), QStringLiteral("<b>topcon</b>")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestFirstValueinText() {
        auto&& mold   = QStringLiteral("{{name}}</td>");
        auto&& result = QStringLiteral("AlSo</td>");

        QJsonObject&& object{{QStringLiteral("name"), QStringLiteral("AlSo")}};

        assertEquals(mu::render(mold, object), result);
    }
    static void TestLastValueinText() {
        auto&& mold   = QStringLiteral("{{name}}</td>{{name}}");
        auto&& result = QStringLiteral("AlSo</td>AlSo");

        QJsonObject&& object{{QStringLiteral("name"), QStringLiteral("AlSo")}};

        assertEquals(mu::render(mold, object), result);
    }
    static void TestSimpleTemplate() {
        auto&& mold   = QStringLiteral("<td>{{name}}</td>");
        auto&& result = QStringLiteral("<td>cool</td>");

        QJsonObject&& object{{QStringLiteral("name"), QStringLiteral("cool")}};

        assertEquals(mu::render(mold, object), result);
    }
    static void TestDoubleNameTemplate() {
        auto&& mold   = QStringLiteral("<td>{{name}}</td><b>{{url}}</b><p>{{name}}</p>");
        auto&& result = QStringLiteral("<td>coca</td><b>lost</b><p>coca</p>");

        QJsonObject&& object{
            {QStringLiteral("name"), QStringLiteral("coca")}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestComments() {
        auto&& mold   = QStringLiteral("<td>{{!name}}</td>");
        auto&& result = QStringLiteral("<td></td>");

         QJsonObject&& object{
            {QStringLiteral("name"), QStringLiteral("coca")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestEmptyArray() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{.}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul></ul>");

        QJsonObject&& object{
            {QStringLiteral("food"), QJsonArray()}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestMissedArray() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{.}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul></ul>");

        QJsonObject&& object{
            {QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestOneValueArray() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{.}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca</li></ul>");

        QJsonArray&& food{{QStringLiteral("coca")}};

        QJsonObject&& object {
            {QStringLiteral("food"), food}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestArray() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{.}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca</li><li>brut</li><li>free your mind</li></ul>");

        QJsonArray&& food {
              {QStringLiteral("coca")}
            , {QStringLiteral("brut")}
            , {QStringLiteral("free your mind")}
        };

        QJsonObject&& object{
            {QStringLiteral("food"), food}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestSimpleFor() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca</li><li>brut</li><li>free your mind</li></ul>");

        QJsonArray&& food {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("coca")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("brut")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("free your mind")}}}
        };

        QJsonObject&& object{
            {QStringLiteral("food"), food}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestEmbeddedName() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}-{{fox}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca-green</li><li>brut-green</li><li>free your mind-green</li></ul>");

        QJsonArray&& food {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("coca")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("brut")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("free your mind")}}}
        };
        QJsonObject&& object{
            {QStringLiteral("food"), food}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
            ,{QStringLiteral("fox"), QStringLiteral("green")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestFakedEmbeddedName() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}-{{fox}}::{{xxx}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca-green::</li><li>brut-green::</li><li>free your mind-green::</li></ul>");

        QJsonArray&& food {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("coca")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("brut")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("free your mind")}}}
        };

        QJsonObject&& object{
            {QStringLiteral("food"), food}
            ,{QStringLiteral("url"), QStringLiteral("lost")}
            ,{QStringLiteral("fox"), QStringLiteral("green")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestTwoSections() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}-{{fox}}</li>{{/food}}</ul>{{#food}}<a>{{name}}</a>{{/food}}");
        auto&& result = QStringLiteral("<ul><li>coca-green</li><li>brut-green</li></ul><a>coca</a><a>brut</a>");

        QJsonArray&& food {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("coca")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("brut")}}}
        };
        QJsonObject&& object{
            {QStringLiteral("food"), food}
            ,{QStringLiteral("fox"), QStringLiteral("green")}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestComplexEmbeddedSections() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}{{#fox}}<a>{{name}}</a>{{/fox}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca<a>xxx</a><a>fox</a></li><li>brut<a>xxx</a><a>fox</a></li></ul>");

        QJsonArray fox {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("xxx")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("fox")}}}
        };
        QJsonObject&& _1{
            {QStringLiteral("name"), QStringLiteral("coca")}
            , {QStringLiteral("fox"), fox}
         };

        QJsonObject _2{
            {QStringLiteral("name"), QStringLiteral("brut")}
            , {QStringLiteral("fox"), fox}
         };

        QJsonArray food;
        food << _1;
        food << _2;

        QJsonObject&& object{
            {QStringLiteral("food"), food}
        };
        assertEquals(mu::render(mold, object), result);
    }
    static void TestSuperComplexEmbeddedSections() {
        auto&& mold   = QStringLiteral("<ul>{{#food}}<li>{{name}}{{#fox}}<a>{{name}}</a>{{/fox}}</li>{{/food}}</ul>");
        auto&& result = QStringLiteral("<ul><li>coca<a>xxx</a><a>fox</a></li><li>brut<a>super</a><a>me</a><a>cool</a></li></ul>");

        QJsonArray fox {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("xxx")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("fox")}}}
        };
        QJsonArray fox2 {
              {QJsonObject{{QStringLiteral("name"), QStringLiteral("super")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("me")}}}
            , {QJsonObject{{QStringLiteral("name"), QStringLiteral("cool")}}}
        };
        QJsonObject&& _1{
            {QStringLiteral("name"), QStringLiteral("coca")}
            , {QStringLiteral("fox"), fox}
         };

        QJsonObject _2{
            {QStringLiteral("name"), QStringLiteral("brut")}
            , {QStringLiteral("fox"), fox2}
         };

        QJsonArray food;
        food << _1;
        food << _2;

        QJsonObject&& object{
            {QStringLiteral("food"), food}
        };
        assertEquals(mu::render(mold, object), result);
    }

public:
    void operator()() noexcept {
        TestEmptyTemplate();
        TestComments();
        TestVariables();
        TestFirstValueinText();
        TestLastValueinText();
        TestEmptyTemplateWithUnaryBracket();
        TestSimpleTemplate();
        TestDoubleNameTemplate();
        TestEmptyArray();
        TestMissedArray();
        TestOneValueArray();
        TestArray();
        TestSimpleFor();
        TestEmbeddedName();
        TestFakedEmbeddedName();
        TestTwoSections();
        TestComplexEmbeddedSections();
        TestSuperComplexEmbeddedSections();
    }
};
