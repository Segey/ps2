/**
 * \file      ps2/ps2/qt/libs/test/main.h
 * \brief     MainQt test class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.7
 * \created   May  (the) 07(th), 2015, 15:44 MSK
 * \updated   July (the) 14(th), 2016, 01:04 MSK
 * \TODO
**/
#pragma once
#include <ps2/thetest/qt/helpers.h>
#include "mustash_test.h"
#include "string_format_test.h"
#include "qstring_format_test.h"

class MainLibsTest {
public:
    using class_name = MainLibsTest;

public:
    void operator()() noexcept {
        TEST(MustashTest);
        TEST(StringFormatTest);
        TEST(QStringFormatTest);
    }
};

