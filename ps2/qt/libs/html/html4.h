/**
 * \file      ps2/ps2/qt/libs/html/html4.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 18(th), 2015, 15:35 MSK
 * \updated   September (the) 18(th), 2015, 15:35 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QStringList>
#include "html4/mate.h"
#include "html4/converter.h"
#include "html4/items/img.h"
#include "html4/items/a.h"
#include "html4/items/b.h"
#include "html4/items/p.h"
#include "html4/items/h.h"
#include "html4/head.h"
#include "html4/body.h"
#include "html4/html.h"

