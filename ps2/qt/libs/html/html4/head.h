/**
 * \file      ps2/ps2/qt/libs/html/html4/head.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   September (the) 22(th), 2015, 12:17 MSK
 * \updated   September (the) 22(th), 2015, 12:17 MSK
 * \TODO      
**/
#pragma once
#include <QFile>
#include <QString>
#include <QStringList>
#include "mate.h"

/** \namespace html4 */
namespace html4 {
    
class Head {
public:
    typedef Head                        class_name;
    typedef QStringList                 styles_t;
    typedef QStringList::value_type     style_t;
    typedef style_t                     value_type;
    typedef QStringList::iterator       iterator;
    typedef QStringList::const_iterator const_iterator;

private: 
    QString  m_title;
    QString  m_file;
    styles_t m_styles;

    static QString styles(QString const& text) noexcept {
        return QStringLiteral("<style type=\"text/css\" media=\"screen\">%1</style>").arg(text);
    }
    QString creatTitle() const noexcept {
        return createTagIfExists(QStringLiteral("title"), m_title);
    } 
    QString loadStyles() const noexcept {
        QFile file(m_file);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) return QString();
        return styles(QString::fromLatin1(file.readAll()));
    }
    QString creatStyles() const noexcept {
        return styles(m_styles.join(QString()));
    }

public:
    explicit Head(QString const& title = QString()) noexcept
        : m_title(title), m_file(QString()){
    }
    explicit Head(QString const& title, QString const& file) noexcept
        : m_title(title), m_file(file){
    }
    QString const& title() const noexcept {
        return m_title;
    }
    void setTitle(QString const& title) noexcept {
        m_title = title;
    }
    void setTitle(QString&& title) noexcept {
        m_title = qMove(title);
    }
    styles_t const& styles() const noexcept {
        return m_styles;
    }
    void setStyles(styles_t const& styles) noexcept {
        m_styles = styles;
    }
    void addStyle(style_t const& item) noexcept {
        m_styles.push_back(item);
    }
    void addStyle(style_t&& item) noexcept {
        m_styles.push_back(std::move(item));
    }
    style_t const& style(int index) const noexcept {
        return m_styles[index];
    }
    style_t& style(int index) noexcept {
        return m_styles[index];
    }
    iterator stylesBegin() noexcept {
        return m_styles.begin();
    }
    iterator stylesEnd() noexcept {
        return m_styles.end();
    }
    const_iterator stylesBegin() const noexcept {
        return m_styles.begin();
    }
    const_iterator stylesEnd() const noexcept {
        return m_styles.end();
    }
    bool isStylesEmpty() const noexcept {
        return m_styles.empty();
    }
    size_t stylesSize() const noexcept {
        return m_styles.size();
    } 
    QString operator()() const noexcept {
        QString result = creatTitle();
        if(!m_file.isEmpty()) result += loadStyles();
        else result += creatStyles();
        return QStringLiteral("<head><meta charset=\"utf-8\">%1</head>").arg(result);
    }
};

} // end namespace html4


