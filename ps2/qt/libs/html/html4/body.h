/**
 * \file      ps2/ps2/qt/libs/html/html4/body.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 13:41 MSK
 * \updated   September (the) 22(th), 2015, 13:41 MSK
 * \TODO      
**/
#pragma once
#include "mate.h"
#include "items/item.h"
#include "items/a.h"
#include "items/h.h"
#include "items/p.h"
#include "items/ul.h"

/** \namespace html4 */
namespace html4 {
    
class Body {
public:
    typedef Body                    class_name;
    typedef QSharedPointer<Item>    value_type;
    typedef QList<value_type>       items_t;
    typedef items_t::size_type      size_t;
    typedef items_t::iterator       iterator;
    typedef items_t::const_iterator const_iterator;

private: 
    items_t m_items;

    QString createItems() const {
        QString result;
        for (auto item: m_items)
            result += (*item)();
        return result;
    }

public:
    explicit Body() Q_DECL_NOTHROW  {
    }
    virtual ~Body() Q_DECL_NOTHROW {
    }
    void addItem(Item* item) {
        m_items.push_back(value_type(item));
    }
    value_type operator[](size_t index) const {
        return m_items[index];
    }
    value_type operator[](size_t index) {
        return m_items[index];
    }
    iterator begin() {
        return m_items.begin();
    }
    iterator end() {
        return m_items.end();
    }
    const_iterator begin() const {
        return m_items.begin();
    }
    const_iterator end() const {
        return m_items.end();
    }
    bool empty() const {
        return m_items.empty();
    }
    size_t size() const {
        return m_items.size();
    } 
    QString operator()() const {
        return createTag(QStringLiteral("body"), createItems());
    }
};

} // end namespace html4

