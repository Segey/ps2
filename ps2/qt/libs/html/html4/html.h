/**
 * \file      ps2/ps2/qt/libs/html/html4/html.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 13:37 MSK
 * \updated   September (the) 22(th), 2015, 13:37 MSK
 * \TODO      
**/
#pragma once
#include "head.h"
#include "body.h"

/** \namespace html4 */
namespace html4 {
    
class Html {
public:
    typedef Html    class_name;
    typedef Item    item_t;
    typedef QString style_t;

private: 
    Head m_head;
    Body m_body;

public:
    explicit Html(Head const& head = Head(), Body const& body = Body())
        : m_head(head), m_body(body){
    }
    Head const& head() const {
        return m_head;
    }
    void setHead(Head const& head) {
        m_head = head;
    }
    void setHead(Head&& head) {
        m_head = std::move(head);
    }
    Body const& body() const {
        return m_body;
    }
    void setBody(Body const& body) {
        m_body = body;
    }
    void setBody(Body&& body) {
        m_body = std::move(body);
    }
    QString operator()() const {
        QString result = m_head(); 
        result += m_body(); 
        return QStringLiteral("<!DOCTYPE html><html>%1</html>").arg(result);
    }
    void addBodyItem(item_t* item) {
        m_body.addItem(item);
    }
    void addHeadStyle(style_t const& style) {
        m_head.addStyle(style);
    }
};

} // end namespace html4

