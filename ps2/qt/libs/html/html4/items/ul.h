/**
 * \file      ps2/ps2/qt/libs/html/html4/items/ul.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 16:05 MSK
 * \updated   September (the) 22(th), 2015, 16:05 MSK
 * \TODO      
**/
#pragma once
#include <QStringList>
#include "item.h"

/** \namespace html4 */
namespace html4 {
    
class Ul : public Item { 
public:
    typedef Ul   class_name;
    typedef Item inherited;

private: 
    QStringList m_list;

protected: 
    virtual QString doExec() const Q_DECL_OVERRIDE {
        if(m_list.isEmpty()) return QString();

        QString result;
        for(auto&& item: m_list)
            result += html4::createTag(QStringLiteral("li"), item);
        return html4::createTag(QStringLiteral("ul"), result);
    }

public:
    /**
     * \code
     *      html.addBodyItem(new Ul({{"cool"}}));
     * \endcode
    **/
    explicit Ul(QStringList const& list) 
        : m_list(list){
    }
    QStringList const& list() const {
        return m_list;
    }
    void setText(QStringList const& list) {
        m_list = list;
    }
};

} // end namespace html4

