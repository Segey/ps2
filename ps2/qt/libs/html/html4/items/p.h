/**
 * \file      ps2/ps2/qt/libs/html/html4/items/p.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 15:45 MSK
 * \updated   September (the) 22(th), 2015, 15:45 MSK
 * \TODO      
**/
#pragma once
#include "item.h"

/** \namespace html4 */
namespace html4 {
    
class P : public Item { 
public:
    typedef P class_name;
    typedef Item inherited;

private: 
    QString m_text;

protected: 
    virtual QString doExec() const Q_DECL_OVERRIDE {
        return createTag(QStringLiteral("p"), m_text);
    }

public:
    /**
     * \code
     *      html.addBodyItem(new P("cool"));
     * \endcode
    **/
    explicit P(QString const& text) 
        : m_text(text){
    }
    QString const& text() const {
        return m_text;
    }
    void setText(QString const& text) {
        m_text = text;
    }
};

} // end namespace html4

