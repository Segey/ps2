/**
 * \file      ps2/ps2/qt/libs/html/html4/items/h.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 13:56 MSK
 * \updated   September (the) 22(th), 2015, 13:56 MSK
 * \TODO      
**/
#pragma once
#include "item.h"

/** \namespace empty */
namespace {

template<uint N>
class H : public html4::Item {
public:
    typedef H<N>        class_name;
    typedef html4::Item inherited;

private: 
    QString m_text;

protected: 
    virtual QString doExec() const Q_DECL_OVERRIDE {
        return html4::createTag(QStringLiteral("h%1").arg(N), m_text);
    }

public:
    /**
     * \code
     *      html.addBodyItem(new H1("cool"));
     * \endcode
    **/
    explicit H(QString const& text) 
        : m_text(text){
    }
    QString const& text() const {
        return m_text;
    }
    void setText(QString const& text) {
        m_text = text;
    }
};

} // end empty namespace

/** \namespace html4 */
namespace html4 {
   
typedef H<1> H1;
typedef H<2> H2;
typedef H<3> H3;
typedef H<4> H4;
typedef H<5> H5;
typedef H<6> H6;

} // end namespace html4

