/**
 * \file      ps2/ps2/qt/libs/html/html4/items/img.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 23(th), 2015, 14:24 MSK
 * \updated   September (the) 23(th), 2015, 14:24 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QLocale>
#include "item.h"

/** \namespace html4 */
namespace html4 {
#define ATTR(attr, value) html4::createAttr(QStringLiteral(attr), value);
    
enum class ImgAlign : std::uint_fast8_t {
    Invalid  = 0u
    , Bottom = 1u
    , Left   = 2u
    , Middle = 4u
    , Right  = 8u
    , Top    = 16u
};

inline QString toStr(ImgAlign align) {
    switch (align) {
        default:
        case ImgAlign::Invalid: return QString();
        case ImgAlign::Bottom:  return QStringLiteral("bottom");
        case ImgAlign::Left:    return QStringLiteral("left");
        case ImgAlign::Middle:  return QStringLiteral("middle");
        case ImgAlign::Right:   return QStringLiteral("right");
        case ImgAlign::Top:     return QStringLiteral("top");
    }
    return QString();
}
template<>
inline ImgAlign fromStr<ImgAlign>(QString const& str) {
    if(str == QStringLiteral("bottom")) return ImgAlign::Bottom;
    if(str == QStringLiteral("left"))   return ImgAlign::Left;
    if(str == QStringLiteral("middle")) return ImgAlign::Middle;
    if(str == QStringLiteral("right"))  return ImgAlign::Right;
    if(str == QStringLiteral("top"))    return ImgAlign::Top;
    return ImgAlign::Invalid;
}

class Img: public Item {
public:
    typedef Img      class_name;
    typedef Item     inherited;
    typedef ImgAlign align_t;

private: 
    align_t m_align = align_t::Invalid;
    QString m_src;
    QString m_alt;
    uint    m_border = 0;
    QString m_width;
    QString m_height;
    uint    m_hspace = 0;
    uint    m_vspace = 0;

protected: 
    virtual QString doExec() const Q_DECL_OVERRIDE {
        QStringList result;
        if(m_align != align_t::Invalid) result << ATTR("align", toStr(m_align));
        if(m_border != 0)       result << ATTR("border", QLocale().toString(m_border));
        if(m_hspace != 0)       result << ATTR("hspace", QLocale().toString(m_hspace));
        if(m_vspace != 0)       result << ATTR("vspace", QLocale().toString(m_vspace));
        if(!m_height.isEmpty()) result << ATTR("height", m_height);
        if(!m_width.isEmpty())  result << ATTR("width", m_width);
        if(!m_alt.isEmpty())    result << ATTR("alt", m_alt);
        if(!m_src.isEmpty())    result << ATTR("scr", m_src);

        return QStringLiteral("<img %1>").arg(result.join(QStringLiteral(" ")));
    }
public:
    explicit Img(QString const& src, QString const& alt) 
        : m_src(src), m_alt(alt) {
    }
    explicit Img(QString const& src, QString const& alt, QString const& width, QString const& height) 
        : m_src(src), m_alt(alt), m_width(width), m_height(height) {
    }
    align_t const& align() const {
        return m_align;
    }
    void setAlign(align_t const& align) {
        m_align = align;
    }
    QString const& alt() const {
        return m_alt;
    }
    void setAlt(QString const& alt) {
        m_alt = alt;
    }
    uint border() const {
        return m_border;
    }
    void setBorder(uint border) {
        m_border = border;
    }
    QString const& height() const {
        return m_height;
    }
    void setHeight(QString const& height) {
        m_height = height;
    }
    uint hspace() const {
        return m_hspace;
    }
    void setHspace(uint hspace) {
        m_hspace = hspace;
    }
    QString const& src() const {
        return m_src;
    }
    void setSrc(QString const& src) {
        m_src = src;
    }
    uint vspace() const {
        return m_vspace;
    }
    void setVspace(uint vspace) {
        m_vspace = vspace;
    }
    QString const& width() const {
        return m_width;
    }
    void setWidth(QString const& width) {
        m_width = width;
    }
};
    
} // end namespace 
