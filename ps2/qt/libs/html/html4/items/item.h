/**
 * \file      ps2/ps2/qt/libs/html/html4/items/item.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 13:52 MSK
 * \updated   September (the) 22(th), 2015, 13:52 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include "../mate.h"

/** \namespace html4 */
namespace html4 {

class Item {
public:
    using class_name = Item;

private:
    QString m_id;
    QString m_style;

protected:
    virtual QString doExec() const = 0;

public:
    explicit Item() noexcept Q_DECL_EQ_DEFAULT;
    explicit Item(QString const& id, QString const& style)
        : m_id(id), m_style(style){
    }
    explicit Item(QString&& id, QString&& style)
        : m_id(qMove(id)), m_style(qMove(style)){
    }
    virtual ~Item() Q_DECL_EQ_DEFAULT;
    QString const& id() const {
        return m_id;
    }
    void setId(QString const& id) {
        m_id = id;
    }
    void setId(QString&& id) {
        m_id = qMove(id);
    }
    QString const& style() const {
        return m_style;
    }
    void setStyle(QString const& style) {
        m_style = style;
    }
    void setStyle(QString&& style) {
        m_style = qMove(style);
    }
    QString operator()() const {
        return doExec();
    }
    QString exec() const {
        return doExec();
    }
};
} // end namespace html4
