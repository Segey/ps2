/**
 * \file      ps2/ps2/qt/libs/html/html4/items/a.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 13:56 MSK
 * \updated   September (the) 22(th), 2015, 13:56 MSK
 * \TODO
**/
#pragma once
#include "item.h"

/** \namespace html4 */
namespace html4 {

class A : public Item {
public:
    using class_name = A;
    using inherited  = Item;

private:
    QString m_link;
    QString m_text;

protected:
    virtual QString doExec() const noexcept Q_DECL_OVERRIDE {
        return QStringLiteral("<a href=\"%1\">%2</a>")
                .arg(m_link).arg(m_text);
    }

public:
    /**
     * \code
     *      html.addBodyItem(new A("ya.ru", "cool"));
     * \endcode
    **/
    explicit A(QString const& link, QString const& text) noexcept
        : m_link(link)
        , m_text(text){
    }
    explicit A(QString&& link, QString&& text) noexcept
        : m_link(qMove(link)), m_text(qMove(text)){
    }
    ~A() Q_DECL_EQ_DEFAULT;
    QString const& link() const noexcept {
        return m_link;
    }
    void setLink(QString const& link) noexcept {
        m_link = link;
    }
    void setLink(QString&& link) noexcept {
        m_link = qMove(link);
    }
    QString const& text() const noexcept {
        return m_text;
    }
    void setText(QString const& text) noexcept {
        m_text = text;
    }
    void setText(QString&& text) noexcept {
        m_text = qMove(text);
    }
};
} // end namespace html4
