/**
 * \file      ps2/ps2/qt/libs/html/html4/items/b.h
 * \brief     The class represents the B html tag
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 16(th), 2016, 01:15 MSK
 * \updated   May (the) 16(th), 2016, 01:15 MSK
 * \TODO
**/
#pragma once
#include "item.h"

/** \namespace html4 */
namespace html4 {

class B : public Item {
public:
    using class_name = B;
    using inherited  = Item;

private:
    QString m_text;

protected:
    virtual QString doExec() const noexcept Q_DECL_OVERRIDE {
        return html4::createTag(QStringLiteral("b"), m_text);
    }

public:
    /**
     * \code
     *      html.addBodyItem(new B("ya.ru", "cool"));
     * \endcode
    **/
    explicit B(QString const& text) noexcept
        : m_text(text) {
    }
    explicit B(QString&& text) noexcept
        :  m_text(qMove(text)){
    }
    ~B() Q_DECL_EQ_DEFAULT;
    QString const& text() const noexcept {
        return m_text;
    }
    void setText(QString const& text) noexcept {
        m_text = text;
    }
    void setText(QString&& text) noexcept {
        m_text = qMove(text);
    }
};
} // end namespace html4
