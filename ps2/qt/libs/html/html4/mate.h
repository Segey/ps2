/**
 * \file      ps2/ps2/qt/libs/html/html4/mate.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 22(th), 2015, 12:38 MSK
 * \updated   September (the) 22(th), 2015, 12:38 MSK
 * \TODO
**/
#pragma once
#include <QString>

/** \namespace html4 */
namespace html4 {

template<class T>
T fromStr(QString const& str);

inline QString createTag(QString const& tag, QString const& text) noexcept {
    return QStringLiteral("<%1>%2</%1>").arg(tag).arg(text);
}
inline QString createTag(QString&& tag, QString&& text) noexcept {
    return QStringLiteral("<%1>%2</%1>").arg(qMove(tag)).arg(qMove(text));
}
inline QString createTagIfExists(QString const& tag, QString const& text) noexcept {
    return text.isEmpty() ? QString() : createTag(tag, text);
}
inline QString createTagIfExists(QString&& tag, QString&& text) noexcept {
    return text.isEmpty() ? QString() : createTag(qMove(tag), qMove(text));
}
/**
 * \code
 *      return html4::createAttr(QStringLiteral("align"), str);
 * \endcode
**/
inline QString createAttr(QString const& attr, QString const& text) noexcept {
    return QStringLiteral("%1=\"%2\"").arg(attr).arg(text);
}
inline QString createAttrIfExists(QString const& attr, QString const& text) noexcept {
    return text.isEmpty() ? QString() : createAttr(attr, text);
}
} // end namespace html4
