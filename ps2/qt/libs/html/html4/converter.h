/**
 * \file      ps2/ps2/qt/libs/html/html4/converter.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 23(th), 2015, 13:25 MSK
 * \updated   September (the) 23(th), 2015, 13:25 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace html4 */
namespace html4 {

inline QString replaceEndLine(QString const& str, QChar ch = QChar::fromLatin1('p')) {
    auto&& list = str.split(QString::fromLatin1("\n"));

    QString result;
    for(auto&& item: list) {
        if(item.isEmpty()) continue;
        result += QStringLiteral("<%1>%2</%1>").arg(ch).arg(item);
    }
    return result;
}
    
} // end namespace html4
