#-------------------------------------------------
#
# Project created 2016-02-20T01:40:27
#
#-------------------------------------------------
cmake_minimum_required(VERSION 3.9 FATAL_ERROR)

if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    set(CMAKE_PREFIX_PATH "c:\\Qt\\Qt5.9.2\\5.9.2\\msvc2017_64")
    set(GUI_TYPE WIN32)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(QT_PATH "/home/dix/Qt/5.9/gcc_64")
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Apple")
    set(GUI_TYPE MACOSX_BUNDLE)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")

set(PS2_PATH ${CMAKE_CURRENT_LIST_DIR}/../../../../ps2)
set(OUTPUT_PATH ${CMAKE_CURRENT_LIST_DIR}/../../../bin/cmake)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DQT_NO_CAST_FROM_ASCII -DQT_NO_CAST_TO_ASCII -DQT_NO_CAST_FROM_BYTEARRAY -DQT_NO_URL_CAST_FROM_STRING -DQT_USE_QSTRINGBUILDER -D_SCL_SECURE_NO_WARNINGS")

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set_property(GLOBAL PROPERTY AUTOGEN_TARGETS_FOLDER AutoMoc)

add_definitions(-DQT)

# Add the following compiling options to all projects:
if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj /MP /EHc /std:c++latest")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -ftemplate-depth-1000 -std=c++1z")
endif()


set(RESOURCES
    ${RESOURCES_PATH}/images/default.qrc
    ${RESOURCES_PATH}/images/ico.qrc
    ${RESOURCES_PATH}/images/introduction.qrc
    ${RESOURCES_PATH}/images/panel.qrc
    ${RESOURCES_PATH}/images/png8.qrc
    ${RESOURCES_PATH}/images/png12.qrc
    ${RESOURCES_PATH}/images/png16.qrc
    ${RESOURCES_PATH}/images/png32.qrc
    ${RESOURCES_PATH}/images/png64.qrc
    ${RESOURCES_PATH}/images/png48.qrc
    ${RESOURCES_PATH}/lists/lists.qrc
    ${RESOURCES_PATH}/data/data.qrc
    ${RESOURCES_PATH}/reports/reports.qrc
    ${RESOURCES_PATH}/mustash/mustash.qrc
    ${RESOURCES_PATH}/css/css.qrc
    ${RESOURCES_PATH}/fonts/fonts.qrc
    ${RESOURCES_PATH}/qml/qml.qrc
)
set(TranslationFiles
    ${TRANSLATIONS_PATH}/resources/langs/en.ts 
    ${TRANSLATIONS_PATH}/resources/langs/ru.ts
)
set(QmTranslationFiles
    ${TRANSLATIONS_PATH}/resources/langs/en.qm
    ${TRANSLATIONS_PATH}/resources/langs/ru.qm
)

#if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
#    set_target_properties(BSEd PROPERTIES LINK_FLAGS_DEBUG "/SUBSYSTEM:WINDOWS")
#    set_target_properties(BSEd PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:CONSOLE")
#endif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
