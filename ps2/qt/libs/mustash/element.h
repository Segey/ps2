/**
 * \file      ps2/ps2/qt/libs/mustash/element.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   October (the) 03(th), 2015, 16:19 MSK
 * \updated   October (the) 03(th), 2015, 16:19 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include "type.h"

/** \namespace mu */
namespace mu {

class Element {
public:
    typedef Element class_name;
    typedef Type    type_t;

private:
    QString m_text;

protected:
    virtual QString doRender(QJsonObject const& object) const = 0;
    virtual QString doRender(QJsonValue const& value) const = 0;
    virtual type_t doType() const = 0;

public:
    explicit Element()
        : m_text(QString()) {
    }
    explicit Element(QString const& text)
        : m_text(text) {
    }
    explicit Element(QString&& text)
        : m_text(qMove(text)) {
    }
    virtual ~Element() Q_DECL_EQ_DEFAULT;
    bool isValid() const {
        return !m_text.isEmpty();
    }
    type_t type() const {
        return doType();
    }
    QString const& text() const {
        return m_text;
    }
    void setText(QString const& text) {
        m_text = text;
    }
    QString render(QJsonObject const& object) const {
        return doRender(object);
    }
    QString render(QJsonValue const& value) const {
        return doRender(value);
    }
}; 

} // end namespace mu

