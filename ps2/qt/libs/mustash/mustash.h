/**
 * \file      ps2/ps2/qt/libs/mustash/mustash.h
 * \brief     The class provides mu functionality
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.0
 * \created   September (the) 24(th), 2015, 13:14 MSK
 * \updated   September (the) 24(th), 2015, 13:14 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>
#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include "functions/algo.h"
#include "functions/log.h"
#include "type.h"
#include "list.h"
#include "element.h"
#include "text.h"
#include "comment.h"
#include "create_element.h"
#include "section/section.h"
#include "renders/render_final.h"

namespace mu {

inline QString render(QString const& mold, QJsonObject const& value = {}) {
    if(mold.isEmpty()) return mold;

    Section section(mold);
    auto&& result = section.initRender(value);

    RenderFinal render(value);
    return render.render(result);
}

} // end namespace mu

