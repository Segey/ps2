/**
 * \file      ps2/ps2/qt/libs/mustash/renders/render.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 08(th), 2015, 21:27 MSK
 * \updated   October (the) 08(th), 2015, 21:27 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>

/** \namespace mu */
namespace mu {

class Render {
public:
    typedef Render class_name;

protected:
    QRegExp simpleRegex() const {
        QRegExp rx(QStringLiteral(R"(\{\{(.*)\}\})"));
        rx.setMinimal(true);
        return rx;
    }
    virtual QRegExp doRegex() const = 0; 
    virtual void doInit() = 0;
    virtual void doFoundText(QString const& text) = 0;
    virtual void doFoundValue(QString const& key) = 0;
    virtual QString doFinish(QString const& text) = 0;
public:
    explicit Render() {
    }
    virtual ~Render() {
    }
    QString render(QString const& mold) {
        if(mold.isEmpty()) return QString();

        auto&& rx = doRegex();
        int pos = 0, last = 0;

        doInit();
        while ((pos = rx.indexIn(mold, pos)) != -1) {
            auto&& key = rx.cap(1).trimmed();

            doFoundText(mold.mid(last, pos - last));
            doFoundValue(key);

            last = pos += rx.matchedLength();
        }
        return last == 0 ? mold
                         : doFinish(mold.mid(last));
    }
};    
} // end namespace mu
