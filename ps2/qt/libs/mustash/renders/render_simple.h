/**
 * \file      ps2/ps2/qt/libs/mustash/renders/render_simple.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 06(th), 2015, 19:08 MSK
 * \updated   October (the) 06(th), 2015, 19:08 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>
#include <QJsonArray>
#include "../list.h"
#include "../element.h"
#include "../text.h"
#include "../comment.h"
#include "../functions/algo.h"
#include "render.h"

/** \namespace mu */
namespace mu {

class RenderSimple: public Render {
public:
    typedef RenderSimple  class_name;
    typedef Render        inherited;
    typedef List<Element> elements_t;
     
private:
    elements_t m_elements;
    QJsonValue m_value;

    virtual QRegExp doRegex() const Q_DECL_OVERRIDE {
        return inherited::simpleRegex();
    }
    virtual void doInit() Q_DECL_OVERRIDE {
        m_elements.clear();
    }
    virtual QString doFinish(QString const& text) Q_DECL_OVERRIDE {
        doFoundText(text); 

        if(m_value.isObject()) m_value = m_value.toObject();
        return renderCollection(m_elements, m_value);
    }
    virtual void doFoundText(QString const& text) Q_DECL_OVERRIDE {
        m_elements.push_back(new Text(text));
    }
    virtual void doFoundValue(QString const& key) Q_DECL_OVERRIDE {
        auto&& elem = createElement(key);
        if(elem) m_elements.push_back(elem);
    }
public:
    QString operator()(QString const& mold, QJsonValue const& value) {
        m_value = value;
        return inherited::render(mold);
    }
 };

} // end namespace mu

