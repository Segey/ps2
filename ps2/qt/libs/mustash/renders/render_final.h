/**
 * \file      ps2/ps2/qt/libs/mustash/renders/render_final.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 08(th), 2015, 19:20 MSK
 * \updated   October (the) 08(th), 2015, 19:20 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonObject>
#include "../functions/log.h"

/** \namespace mu */
namespace mu {

class RenderFinal: public Render {
public:
    typedef RenderFinal   class_name;
    typedef Render        inherited;
     
private:
    QString     m_result;
    QJsonObject m_object;

protected:
    virtual QRegExp doRegex() const Q_DECL_OVERRIDE {
        return inherited::simpleRegex();
    }
    virtual void doInit() Q_DECL_OVERRIDE {
        m_result.clear();
    }
    virtual QString doFinish(QString const& text) Q_DECL_OVERRIDE {
        doFoundText(text); 
        return m_result;
    }
    virtual void doFoundText(QString const& text) Q_DECL_OVERRIDE {
        m_result += text;
    }
    virtual void doFoundValue(QString const& key) Q_DECL_OVERRIDE {
        Q_UNUSED(key)

#ifndef _TEST
        MUSTASH_LOG_INFO3("The key", key, "isn't present in the object", m_object);
#endif
    }

public:
    RenderFinal(QJsonObject const& object)
        : m_object(object){
    }
 };
} // end namespace mu

