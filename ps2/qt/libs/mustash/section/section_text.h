/**
 * \file      ps2/ps2/qt/libs/mustash/section/section_text.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 06(th), 2015, 12:21 MSK
 * \updated   October (the) 06(th), 2015, 12:21 MSK
 * \TODO      
**/
#pragma once
#include <QJsonValue>
#include "section_item.h"

/** \namespace mu */
namespace mu {
    
class SectionText : public SectionItem { 
public:
    typedef SectionText       class_name;
    typedef SectionItem       inherited;
    typedef inherited::type_t type_t;

protected:
    virtual QString doRender(QJsonValue const&) const Q_DECL_OVERRIDE {
        return inherited::mold();
    }
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Text;
    }

public:
    explicit SectionText(QString const& text) 
        : inherited(text){
    }
};
} // end namespace mu
