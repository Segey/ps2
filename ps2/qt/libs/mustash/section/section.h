/**
 * \file      ps2/ps2/qt/libs/mustash/section/section.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.0
 * \created   October (the) 03(th), 2015, 16:52 MSK
 * \updated   October (the) 06(th), 2015, 12:25 MSK
 * \TODO      
**/
#pragma once
#include <QRegExp>
#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include "../list.h"
#include "../section/section_item.h"
#include "../section/section_text.h"
#include "../section/section.h"
#include "../renders/render_simple.h"

/** \namespace mu */
namespace mu {
   
class Section: public SectionItem {
public:
    typedef Section           class_name;
    typedef SectionItem       inherited;
    typedef List<SectionItem> sections_t;

private:
    QString m_key;

    static QRegExp regex() {
        QRegExp rx(QStringLiteral(R"(\{\{#([^}]+)\}\}(.+)\{\{/\1\}\})"));
        rx.setMinimal(true);
        return rx;
    }
    QJsonArray getArray(QJsonValue const& value) const {
        if(!value.isObject())
            return MUSTASH_LOG_RINFO2("The Value", value, "isn't an Object!!!", QJsonArray());

        auto&& object = value.toObject();
        if(object.isEmpty())
            return MUSTASH_LOG_RINFO2("The Object", value, "is empty!!!", QJsonArray());

        auto&& val = object[m_key];
        if(val.isUndefined() || !val.isArray()) {
#ifndef _TEST
            return MUSTASH_LOG_RINFO3("The key", m_key, "isn't present in", object, QJsonArray());
#endif
        }
        return val.toArray();

    }
    static QString renderComplex(QString const& mold, QJsonValue const& value) {
        auto&& rx = regex();
        int last = 0, pos = 0;

        sections_t sections;
        while ((pos = rx.indexIn(mold, pos)) != -1) {
            auto&& text = rx.cap(2).trimmed();
            auto&& key = rx.cap(1).trimmed();

            sections.push_back(new SectionText(mold.mid(last, pos - last)));
            sections.push_back(new Section(text, key));
            last = pos += rx.matchedLength();
        }
        if(sections.empty()) return mold;

        sections.push_back(new SectionText(mold.mid(last)));
        return renderCollection(sections, value);
    }
public:
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Section;
    }
    QString doRender(QJsonValue const& value) const Q_DECL_OVERRIDE {
        auto&& mold = inherited::mold();
        if(mold.isEmpty() || value.isUndefined()) return mold;

        QString result;
        for(auto&& item: getArray(value)) {
            auto&& meld = renderComplex(mold, item);
            result+= RenderSimple()(meld, item);
        }
        return result;
    }
public:
    explicit Section(QString const& text, QString const& key = QString())
        : inherited(text), m_key(key) {
    }
    QString initRender(QJsonObject const& object) {
        auto&& mold = inherited::mold();
        if(mold.isEmpty() || object.isEmpty()) return mold;

        auto&& result = renderComplex(mold, object);
        return RenderSimple()(result, object);
    }
}; 

} // end namespace mu

