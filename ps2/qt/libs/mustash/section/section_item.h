/**
 * \file      ps2/ps2/qt/libs/mustash/section/section_item.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 06(th), 2015, 12:19 MSK
 * \updated   October (the) 06(th), 2015, 12:19 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonValue>
#include "../type.h"

/** \namespace mu */
namespace mu {

class SectionItem {
public:
    typedef SectionItem class_name;
    typedef Type         type_t;
     
private: 
    QString m_mold;

protected:
    virtual type_t doType() const = 0;
    virtual QString doRender(QJsonValue const& object) const = 0;

public:
    explicit SectionItem(QString const& text) 
        : m_mold(text){
    }
    virtual ~SectionItem(){
    }
    QString const& mold() const {
        return m_mold;
    }
    void setMold(QString const& mold) {
        m_mold = mold;
    }
    QString render(QJsonValue const& value) const {
        return doRender(value);
    }
    type_t type() const {
        return doType();
    }
 };

} // end namespace mu
