/**
 * \file      ps2/ps2/qt/libs/mustash/create_element.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 03(th), 2015, 17:21 MSK
 * \updated   October (the) 03(th), 2015, 17:21 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include "type.h"
#include "element.h"
#include "array.h"
#include "value.h"
#include "comment.h"

/** \namespace mu */
namespace mu {

Type fromStr(QString const& str) noexcept {
    if(str.isEmpty()) return Type::Invalid;

    auto const ch = str[0];
    if(ch == QChar::fromLatin1('!')) return Type::Comment;
    if(ch == QChar::fromLatin1('#')) return Type::Section;
    if(ch == QChar::fromLatin1('.')) return Type::Array;
    return Type::Value;
}
Element* createElement(QString key) noexcept {
    switch (fromStr(key)) {
        default:
        case Type::Invalid: return Q_NULLPTR;
        case Type::Comment: {
            auto&& str = key.right(key.size() - 1);
            return new Comment(str);
        }
        case Type::Value:
            return new Value(key);
        case Type::Array:
            return new Array(key);
    }
    return Q_NULLPTR;
}

} // end namespace mu
