/**
 * \file      ps2/ps2/qt/libs/mustash/type.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 03(th), 2015, 16:25 MSK
 * \updated   October (the) 03(th), 2015, 16:25 MSK
 * \TODO      
**/
#pragma once

/** \namespace mu */
namespace mu {

enum class Type: std::uint8_t {
    Invalid                = 0u
    , Text                 = 1u  // just a text
    , Value                = 2u  // A {{key}} tag
    , Section              = 3u  // A {{#section}}...{{/section}}} tag
    , Comment              = 4u  // A {{! comment }} tag
    , Array                = 5u  // A {{.}}
}; 

} // end namespace mu

