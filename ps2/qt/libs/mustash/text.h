/**
 * \file      ps2/ps2/qt/libs/mustash/text.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 03(th), 2015, 16:35 MSK
 * \updated   October (the) 03(th), 2015, 16:35 MSK
 * \TODO      
**/
#pragma once
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include "element.h"

/** \namespace mu */
namespace mu {

class Text : public Element {
public:
    typedef Text              class_name;
    typedef Element           inherited;
    typedef inherited::type_t type_t;

protected:
    virtual QString doRender(QJsonObject const&) const Q_DECL_OVERRIDE {
        return inherited::text();
    }
    virtual QString doRender(QJsonValue const&) const Q_DECL_OVERRIDE {
        return inherited::text();
    }
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Text;
    }
public:
    explicit Text(QString const& text)
        : inherited(text) {
    }
};
    
} // end namespace mu

