/**
 * \file      ps2/ps2/qt/libs/mustash/array.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 06(th), 2015, 18:43 MSK
 * \updated   October (the) 06(th), 2015, 18:43 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include "element.h"

/** \namespace mu */
namespace mu {

class Array : public Element {
public:
    typedef Array             class_name;
    typedef Element           inherited;
    typedef inherited::type_t type_t;

protected:
    virtual QString doRender(QJsonObject const& object) const Q_DECL_OVERRIDE {
        return object[inherited::text()].toString();
    }
    virtual QString doRender(QJsonValue const& value) const Q_DECL_OVERRIDE {
        if(!value.isUndefined() && value.isString()) return value.toString();

        MUSTASH_LOG_INFO2("Invalid value", value, "in an array");
        return QString{};
    }
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Array;
    }

public:
    explicit Array(QString const& text) noexcept
        : inherited(text) {
    }
};    

} // end namespace mu

