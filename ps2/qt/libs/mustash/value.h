/**
 * \file      ps2/ps2/qt/libs/mustash/value.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 03(th), 2015, 18:28 MSK
 * \updated   October (the) 03(th), 2015, 18:28 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include "element.h"

/** \namespace mu */
namespace mu {

class Value : public Element {
public:
    typedef Value             class_name;
    typedef Element           inherited;
    typedef inherited::type_t type_t;

private:
    QString reback() const {
        return QStringLiteral("{{%1}}").arg(inherited::text());
    }

protected:
    virtual QString doRender(QJsonObject const& object) const Q_DECL_OVERRIDE {
        auto&& value = object[inherited::text()];
        return value.isUndefined() ? reback()
                                   : value.toString();
    }
    virtual QString doRender(QJsonValue const& value) const Q_DECL_OVERRIDE {
        return doRender(value.toObject());
    }
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Value;
    }

public:
    explicit Value(QString const& text) noexcept
        : inherited(text) {
    }
    explicit Value(QString&& text) noexcept
        : inherited(qMove(text)) {
    }
};

} // end namespace mu

