/**
 * \file      ps2/ps2/qt/libs/mustash/comment.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   October (the) 03(th), 2015, 16:32 MSK
 * \updated   October (the) 03(th), 2015, 16:32 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QJsonObject>
#include <QJsonValue>
#include "element.h"

/** \namespace mu */
namespace mu {

class Comment : public Element {
public:
    typedef Comment           class_name;
    typedef Element           inherited;
    typedef inherited::type_t type_t;

protected:
    virtual QString doRender(QJsonObject const&) const Q_DECL_OVERRIDE {
        return QString();
    }
    virtual QString doRender(QJsonValue const&) const Q_DECL_OVERRIDE {
        return QString();
    }
    virtual type_t doType() const Q_DECL_OVERRIDE {
        return type_t::Comment;
    }

public:
    explicit Comment(QString const& text)
        : inherited(text) {
    }
}; 

} // end namespace mu

