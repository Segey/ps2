/**
 * \file      ps2/ps2/qt/libs/mustash/functions/algo.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 08(th), 2015, 18:04 MSK
 * \updated   October (the) 08(th), 2015, 18:04 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace mu */
namespace mu {

template<typename T, typename U>
static QString renderCollection(T const& collection, U const& value) {
    QString result;

    for (auto&& item : collection)
        result += item->render(value);
    
    return result;
} 

} // end namespace mu
