/**
 * \file      ps2/ps2/qt/libs/mustash/functions/escapted_html.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 07(th), 2015, 13:10 MSK
 * \updated   October (the) 07(th), 2015, 13:10 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace mu */
namespace mu {

inline QString escapeHtml(const QString& input) {
    QString escaped(input);
    for (int i=0; i < escaped.count();) {
        const char* replacement = 0;
        ushort ch = escaped.at(i).unicode();
        if (ch == '&') {
            replacement = "&amp;";
        } else if (ch == '<') {
            replacement = "&lt;";
        } else if (ch == '>') {
            replacement = "&gt;";
        } else if (ch == '"') {
            replacement = "&quot;";
        }
        if (replacement) {
            escaped.replace(i, 1, QLatin1String(replacement));
            i += strlen(replacement);
        } else {
            ++i;
        }
    }
    return escaped;
} 
} // end namespace mu
