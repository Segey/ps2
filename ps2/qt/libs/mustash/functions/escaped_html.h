/**
 * \file      ps2/ps2/qt/libs/mustash/functions/escaped_html.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 07(th), 2015, 13:11 MSK
 * \updated   October (the) 07(th), 2015, 13:11 MSK
 * \TODO      
**/
#pragma once
#include <QString>

/** \namespace mu */
namespace mu {
inline QString unescapeHtml(QString const& escaped) {
    QString unescaped(escaped);
    unescaped.replace(QLatin1String("&lt;"), QLatin1String("<"));
    unescaped.replace(QLatin1String("&gt;"), QLatin1String(">"));
    unescaped.replace(QLatin1String("&amp;"), QLatin1String("&"));
    unescaped.replace(QLatin1String("&quot;"), QLatin1String("\""));
    return unescaped;
} 
} // end namespace mu
