/**
 * \file      ps2/ps2/qt/libs/mustash/functions/log.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 08(th), 2015, 18:25 MSK
 * \updated   October (the) 08(th), 2015, 18:25 MSK
 * \TODO      
**/
#pragma once
#include <QDebug>

#ifdef _DEBUG

#define EMBEDDED_MUSTASH_LOG(msg) qDebug() << "INFO:" << __FILE__ << ":" << __LINE__ << "|" << msg
#define MUSTASH_LOG(msg) {\
    EMBEDDED_MUSTASH_LOG(msg);\
}
#define MUSTASH_LOG_INFO(msg) {\
    EMBEDDED_MUSTASH_LOG(msg);\
}
#define MUSTASH_LOG_INFO2(msg, _1, _2) {\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2; \
}
#define MUSTASH_LOG_INFO3(msg, _1, _2, _3) {\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2 << _3; \
}
#define MUSTASH_LOG_RINFO(msg,result) ([&]{\
    EMBEDDED_MUSTASH_LOG(msg);\
    return result;\
})();
#define MUSTASH_LOG_RINFO2(msg, _1, _2, result) ([&]{\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2;\
    return result;\
})();
#define MUSTASH_LOG_RINFO3(msg, _1, _2, _3, result) ([&]{\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2 << _3;\
    return result;\
})();
#define MUSTASH_LOG_RINFO4(msg, _1, _2, _3, _4, result) ([&]{\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2 << _3 << _4;\
    return result;\
})();
#define MUSTASH_LOG_RINFO5(msg, _1, _2, _3, _4, _5, result) ([&]{\
    EMBEDDED_MUSTASH_LOG(msg) << _1 << _2 << _3 << _4 << _5;\
    return result;\
})();
#else
#define EMBEDDED_MUSTASH_LOG(msg)
#define MUSTASH_LOG(msg)
#define MUSTASH_LOG_INFO(msg)
#define MUSTASH_LOG_INFO2(msg, _1, _2)
#define MUSTASH_LOG_INFO3(msg, _1, _2, _3)
#define MUSTASH_LOG_RINFO(msg,result) ([&]{\
    return result;\
})();
#define MUSTASH_LOG_RINFO2(msg, _1, _2, result) ([&]{\
    return result;\
})();
#define MUSTASH_LOG_RINFO3(msg, _1, _2, _3, result) ([&]{\
    return result;\
})();
#define MUSTASH_LOG_RINFO4(msg, _1, _2, _3, _4, result) ([&]{\
    return result;\
})();
#define MUSTASH_LOG_RINFO5(msg, _1, _2, _3, _4, _5, result) ([&]{\
    return result;\
})();
#endif
