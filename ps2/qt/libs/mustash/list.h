/**
 * \file      ps2/ps2/qt/libs/mustash/list.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 06(th), 2015, 12:45 MSK
 * \updated   October (the) 06(th), 2015, 12:45 MSK
 * \TODO      
**/
#pragma once
#include <QList>
#include <QSharedPointer>

/** \namespace mu */
namespace mu {

template<class T>
class List {
public:
    typedef List<T>                           class_name;
    typedef T                                 item_t;
    typedef QSharedPointer<item_t>            value_type;
    typedef QList<value_type>                 items_t;
    typedef typename items_t::size_type       size_t;
    typedef typename items_t::iterator        iterator;
    typedef typename items_t::const_iterator  const_iterator;

private:
    items_t m_items;

public:
    void push_back(item_t* item) {
        m_items.push_back(value_type(item));
    }
    value_type const& operator[](size_t index) const {
        return m_items[index];
    }
    value_type& operator[](size_t index) {
        return m_items[index];
    }
    iterator begin() {
        return m_items.begin();
    }
    iterator end() {
        return m_items.end();
    }
    const_iterator begin() const {
        return m_items.begin();
    }
    const_iterator end() const {
        return m_items.end();
    }
    bool empty() const {
        return m_items.empty();
    }
    size_t size() const {
        return m_items.size();
    }
    void clear() {
        m_items.clear();
    }
}; 

} // end namespace mu

