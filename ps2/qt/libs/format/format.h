/**
 * \file      ps2/ps2/qt/libs/format/format.h
 * \brief     The Mustash class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 19(th), 2016, 16:25 MSK
 * \updated   December (the) 19(th), 2016, 16:25 MSK
 * \TODO      
**/
#pragma once
#include <string>
#include "print_str.h"
#include "print_qstr.h"

namespace fmt {

template<class... Args>
inline std::string print(std::string const& s, Args&&... args) noexcept {
    if(sizeof...(args) < 1) return s;

    string::Format format(s);
    return format(std::forward<Args>(args)...);
}
template<class... Args>
inline std::string print(std::string&& s, Args&&... args) noexcept {
    if(sizeof...(args) < 1) return s;

    string::Format format(std::move(s));
    return format(std::forward<Args>(args)...);
}
template<class... Args>
inline QString print(QString const& s, Args&&... args) noexcept {
    if(sizeof...(args) < 1) return s;

    qstring::Format format(s);
    return format(std::forward<Args>(args)...);
}
template<class... Args>
inline QString print(QString&& s, Args&&... args) noexcept {
    if(sizeof...(args) < 1) return s;

    qstring::Format format(std::move(s));
    return format(std::forward<Args>(args)...);
}

} // end namespace fmt 
