/**
 * \file      ps2/ps2/qt/libs/format/print_str.h
 * \brief     The Print_str class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 20(th), 2016, 00:13 MSK
 * \updated   December (the) 20(th), 2016, 00:13 MSK
 * \TODO      
**/
#pragma once
#include <string>
#include <sstream>
#include <vector>

/** \namespace fmt::string */
namespace fmt {
namespace string {

class Format {
public:
    using class_name = Format;
     
private: 
    std::string              m_format;
    std::vector<std::string> m_vec;

private: 
    template<class T>
    inline std::string to_string(T&& value) noexcept {
        std::stringstream out;
        out << std::forward<T>(value);
        return out.str();
    }
    inline void expand() noexcept { }
    template<class T, class... Args>
    inline void expand(T&& head, Args&&... tail) noexcept {
        m_vec.push_back(to_string(std::forward<T>(head)));
        expand(std::forward<Args>(tail)...);
    }

public:
    explicit Format(std::string const& str)  noexcept
        : m_format(str) {
    }
    explicit Format(std::string&& str)  noexcept
        : m_format(std::move(str)) {
    }
    template<class... Args>
    inline std::string operator()(Args&&... args) noexcept {
        expand(std::forward<Args>(args)...);
         //(m_vec.push_back(args), ...);

        std::stringstream out;
        decltype(m_format.size()) i{};
        decltype(m_vec.size()) cx{};

        while(i != m_format.size()) {
            if(m_format[i] == '{') {
                if(++i > m_format.size()
                  || (cx >= m_vec.size())) return std::string{};
                if(m_format[i] == '{') {
                    out << '{';
                    ++i;
                    continue;
                }
                if(m_format[i++] !='}')
                    return std::string{};
                out << m_vec[cx++];
                continue;
            }
            out << m_format[i];
            ++i;
        }
        return out.str();
    }
};

}} // end namespace fmt::string
