/**
 * \file      ps2/ps2/qt/libs/format/print_qstr.h
 * \brief     The Print_qstr class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   December (the) 20(th), 2016, 00:38 MSK
 * \updated   December (the) 20(th), 2016, 00:38 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <QVector>
#include <QVariant>

/** \namespace fmt::qstring */
namespace fmt {
namespace qstring {

class Format {
public:
    using class_name = Format;
     
private: 
    QString           m_format;
    QVector<QVariant> m_vec;

private: 
    inline void expand() noexcept { }
    template<class T, class... Args>
    inline void expand(T&& head, Args&&... tail) noexcept {
        m_vec.push_back(QVariant::fromValue(std::forward<T>(head)));
        expand(std::forward<Args>(tail)...);
    }

public:
    explicit Format(QString const& str)  noexcept
        : m_format(str) {
    }
    explicit Format(QString&& str)  noexcept
        : m_format(qMove(str)) {
    }
    template<class... Args>
    inline QString operator()(Args&&... args) noexcept {
        expand(std::forward<Args>(args)...);

        QString out;
        decltype(m_format.size()) i{};
        decltype(m_vec.size()) cx{};

        while(i != m_format.size()) {
            if(m_format[i] == QChar::fromLatin1('{')) {
                if(++i > m_format.size()
                  || (cx >= m_vec.size())) return QString{};
                if(m_format[i] == QChar::fromLatin1('{')) {
                    out.append(QChar::fromLatin1('{'));
                    ++i;
                    continue;
                }
                if(m_format[i++] != QChar::fromLatin1('}'))
                    return QString{};
                out.append(m_vec[cx++].toString());
                continue;
            }
            out.append(m_format[i]);
            ++i;
        }
        return out;
    }
};

}} // end namespace fmt::qstring
