/**
 * \file      ps2/ps2/qt/widgets/combobox_algo.h
 * \brief     The Combobox_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 25(th), 2017, 23:57 MSK
 * \updated   February  (the) 25(th), 2017, 23:57 MSK
 * \TODO      
**/
#pragma once
#include <type_traits>
#include <QComboBox>
#include <ps2/qt/convert/enum.h>
#include <ps2/qt/convert/string.h>

/** \namespace ps2::cb */
namespace ps2::cb {

/**
 * \code
    #include <ps2/qt/widgets/combobox_algo.h>
    auto const is = ps2::cb::canSetCurrent(m_switcher.ui()->m_type, type);
 * \endcode
**/
template<class T, typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
static inline bool canSetCurrent(QComboBox* box, T type) noexcept {
    return box->findData(ps2::to_utype(type)) != -1;
}
template<class T, typename std::enable_if<!std::is_enum<T>::value, int>::type = 0 >
static inline bool canSetCurrent(QComboBox* box, T val) noexcept {
    return box->findData(QVariant::fromValue<T>(val)) != -1;
}

/**
 * \code
    ps2::cb::setCurrent(m_switcher.ui()->m_type, type);
 * \endcode
**/
template<class T>
static inline bool setCurrent(QComboBox* const box, T val) noexcept {
    int index = -1;
    if constexpr(std::is_enum_v<T>)
        index = box->findData(ps2::to_utype(val));
    else
        index = box->findData(QVariant::fromValue<T>(val));
    if(index == -1) return false;
    box->setCurrentIndex(index);
    return true;
}

/**
 * \code
    item.setType(ps2::cb::current<item_t::type_t>((*ui)->m_type));
    auto const type = ps2::cb::current<MuscleType>((*ui)->m_muscle);
 * \endcode
**/
template<class T>
static inline T current(QComboBox const* const box) noexcept {
    return box->currentData().value<T>();
}
template<class T>
static inline void fill(QComboBox* box, T type) noexcept {
    if constexpr(std::is_enum_v<T>)
//        if constexpr(!std::is_void_v<decltype(ps2::to_tr(T{}), void())>)
            box->addItem(ps2::to_tr(type), ps2::to_utype(type));
    Q_UNUSED(type);
    Q_UNUSED(box);
}

/**
 * \code
 *      ps2::cb::fill(m_reps, 0, 51);
 * \endcode
**/
template<class T>
static inline void fill(QComboBox* cb, T start, T end, T step = 1) noexcept {
    if(start > end) return;
    for(;start != end; start += step)
        cb->addItem(ps2::to_qstr(start), QVariant::fromValue<T>(start));
}
static inline void fill(QComboBox* cb, int start, int end, int step = 1) noexcept {
    if(start > end) return;
    for(;start != end; start += step)
        cb->addItem(ps2::to_qstr(start), start);
}
/**
 * \code
 *      ps2::cb::fill(m_reps, 0., 51., .5, 1);
 * \endcode
**/
static inline void fill(QComboBox* cb, double start, double end, double step = 1., int precision = 3) noexcept {
    if(start > end) return;
    for(;start < end; start += step)
        cb->addItem(ps2::to_qstr(start, QLocale(), precision), start);
}   

} // end namespace ps2::cb
