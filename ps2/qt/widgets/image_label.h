/**
 * \file      ps2/ps2/qt/widgets/image_label.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.0
 * \created   February (the) 24(th), 2016, 23:30 MSK
 * \updated   February (the) 24(th), 2016, 23:30 MSK
 * \TODO      
**/
#pragma once
#include <QObject>
#include <QPixmap>
#include <QString>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *      auto label = new ps2::ImageLabel(img12::info(), parent->title(), parent);
 * \endcode
**/
class ImageLabel: public QLabel {
public:
    typedef ImageLabel class_name;
    typedef QLabel     inherited;
     
private: 
    QString  m_image;
    QString  m_text;

    void instance() {
        inherited::setText(QStringLiteral("<img src='%1' /> %2").arg(m_image).arg(m_text));
    }
public:
    explicit ImageLabel(QString const& image, QString const& text, QWidget* parent = Q_NULLPTR)
        : inherited(parent)
        , m_image(image)
        , m_text(text) {
        instance();
    }
    QString const& imageFile() const {
        return m_image;
    }
    void setImageFile(QString const& image) {
        m_image = image;
        instance();
    }
    QString const& text() const {
        return m_text;
    }
    void setText(QString const& text) {
        m_text = text;
        instance();
    }
};
    
} // end namespace ps2
