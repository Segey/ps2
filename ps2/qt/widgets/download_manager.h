/**
 * \file      ps2/ps2/qt/widgets/download_manager.h
 * \brief     DownloadManager the main class for download all data
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   June (the) 17(th), 2015, 19:28 MSK
 * \updated   June (the) 17(th), 2015, 19:28 MSK
 * \note      Must be included in a project
 * \TODO
**/
#pragma once
#include <QUrl>
#include <QObject>
#include <QString>
#include <QtNetwork>

namespace ps2 {

/**
 * \code
 *      ps2::DownloadManager m_manager;
 *      m_manager.download();
 *      connect(&m_manager, SIGNAL(downloadComplete(QNetworkReply*)),
 *               SLOT(downloadComplete(QNetworkReply*)));
 *      connect(&m_manager, &ps2::DownloadManager::downloadFailed,[]() {
 *          exit();
 *      });
 * \endcode
**/
class DownloadManager final: public QObject {
    Q_OBJECT

public:
    using class_name = DownloadManager;
    using inherited  = QObject;

private:
    QNetworkAccessManager m_manager;

signals:
    void downloadComplete(QNetworkReply* reply);
    void downloadFailed(QString const& error);

public:
    DownloadManager() {
        connect(&m_manager, &QNetworkAccessManager::finished,[this](QNetworkReply* reply) {
            if(reply->error()) emit downloadFailed(reply->errorString());
            else emit downloadComplete(reply);
        });
    }
    void download(QUrl const& url) {
        m_manager.get(QNetworkRequest(url));
    }
};

} // end namespace ps2
