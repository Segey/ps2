/**
 * \file      ps2/ps2/qt/widgets/layout_algo.h
 * \brief     The Layout_algo class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   February  (the) 25(th), 2017, 16:56 MSK
 * \updated   February  (the) 25(th), 2017, 16:56 MSK
 * \TODO      
**/
#pragma once
#include <QWidget>
#include <QGridLayout>
#include <QLayoutItem>

/** \namespace ps2::layout */
namespace ps2 {
namespace layout {

static inline void add(QGridLayout* lay, QWidget* item, int r, int c) noexcept {
    lay->addWidget(item, r, c);
    item->setVisible(true);
}
static inline void add(QGridLayout* lay, QLayoutItem* item, int r, int c = 0) noexcept {
    lay->addItem(item, r, c);
}
static inline void remove(QLayout* lay, QLayoutItem* item) noexcept {
    lay->removeItem(item);
}
static inline void remove(QLayout* lay, QWidget* item) noexcept {
    lay->removeWidget(item);
    item->setVisible(false);
}

}} // end namespace ps2::layout
