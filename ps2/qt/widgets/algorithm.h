/**
 * \file      ps2/ps2/qt/widgets/algorithm.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   August (the) 25(th), 2015, 15:31 MSK
 * \updated   August (the) 25(th), 2015, 15:31 MSK
 * \TODO      
**/
#pragma once
#include <QRect>
#include <QPoint>
#include <QString>
#include <QApplication>
#include <QDesktopWidget>
#include <ps2/qt/convert/string.h>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *     m_window->setWindowTitle(ps2::widgetTitle(title, false));
 * \endcode
**/
static inline QString widgetTitle(QString const& text, bool is) noexcept {
    auto&& value = rstrip(text, QChar::fromLatin1('*'));
    return is ? QStringLiteral("%1*").arg(value) : value;
}
/**
 * \code
 *     m_window->setWindowTitle(ps2::windowTitle(title));
 * \endcode
**/
inline QString windowTitle(QString const& text) noexcept {
    return QStringLiteral("%1 [*]").arg(text);
}
/**
 * \code
 *     ps2::windowTitle(this, is);
 * \endcode
**/
template<class T>
inline void windowTitle(T* widget, bool is) {
    auto const& text = widgetTitle(widget->windowTitle(), is);
    widget->setWindowTitle(text);
}
/**
 * \code
 *     ps2::widgetTitle(this, true);
 * \endcode
**/
template<class T>
inline void widgetTitle(T* widget, bool is) {
    widget->setTitle(widgetTitle(widget->title(), is));
}
/**
 * \code
 *     ps2::removeChildren<QObject*>(this);
 * \endcode
**/
template<class T>
inline void removeChildren(QObject* parent) {
    while ( auto w = parent->findChild<T>() )
        delete w;
}
/**
 * \code
 *     ps2::removeChildren(this);
 * \endcode
**/
inline void removeChildren(QObject* parent) {
    while ( auto w = parent->findChild<QObject*>() )
        delete w;
}

} // end namespace ps2
