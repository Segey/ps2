/**
 * \file      ps2/ps2/qt/widgets/validator/decimal_double_validators.h
 * \brief     The Double_validators class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   April     (the) 05(th), 2017, 23:00 MSK
 * \updated   April     (the) 06(th), 2017, 01:03 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/objects/numeric/metric_decimals.h>
#include <ps2/qt/objects/numeric/imperial_decimals.h>
#include <main/user/metric_weight.h>
#include <main/user/imperial_weight.h>
#include <main/user/metric_length.h>
#include <main/user/imperial_length.h>
#include "decimal_double_validator.h"

/** \namespace ps2 */
namespace ps2 {

using KgGrDoubleValidator = DecimalDoubleValidator<MetricWeight
                                , KgGrDecimal, MetricWeight::fromKgGr>;
using MCmDoubleValidator  = DecimalDoubleValidator<MetricLength
                                , MCmDecimal, MetricLength::fromMCm>;
using CmMmDoubleValidator = DecimalDoubleValidator<MetricLength
                                , CmMmDecimal, MetricLength::fromCmMm>;

using StLbDoubleValidator = DecimalDoubleValidator<ImperialWeight
                                , StLbDecimal, ImperialWeight::fromStLb>;
using LbOzDoubleValidator = DecimalDoubleValidator<ImperialWeight
                                , LbOzDecimal, ImperialWeight::fromLbOz>;
using FtInDoubleValidator = DecimalDoubleValidator<ImperialLength
                                , FtInDecimal, ImperialLength::fromFtIn>;
using InDoubleValidator   = DecimalDoubleValidator<ImperialLength
                                , InSubDecimal, ImperialLength::fromInSub>;

} // end ps2 namespace
