/**
 * \file      ps2/ps2/qt/widgets/validator/length/mcm_int_validator.h
 * \brief     The Mcm_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 12:36 MSK
 * \updated   April     (the) 05(th), 2017, 12:36 MSK
 * \TODO      
**/
#pragma once
#include <ps2/cpp/convertor/length.h>
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createMCmalidator() noexcept {
        auto validator = new ps2::MCmIntValidator::pounds(m_kg_field);
        m_mcm_field->setValidator(validator);
    }
 * \endcode
**/
class MCmIntValidator final: public IntValidator {
public:
    using class_name = MCmIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* meters(int bottom, int top, QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{bottom, top, parent};
    }
    static inline class_name* meters(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* cms(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, LengthConvertor::m_cm - 1, parent};
    }
    static inline int currentMeters(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
    static inline int currentCms(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
};

} // end ps2 namespace
