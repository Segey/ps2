/**
 * \file      ps2/ps2/qt/widgets/validator/double_validator.h
 * \brief     The Double_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 05(th), 2017, 13:05 MSK
 * \updated   April     (the) 05(th), 2017, 13:05 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QDoubleValidator>
#include "ps2/cpp/typedefs.h"
#include "ps2/qt/algorithm.h"

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createValidator() noexcept {
        auto validator = new ps2::DoubleValidator<ps2::KgGrDecimal, 3>(m_field);
        m_field->setValidator(validator);
    }
 * \endcode
**/
template<class T>
class DoubleValidator final: public QDoubleValidator {
public:
    using class_name = DoubleValidator;
    using inherited  = QDoubleValidator;
    using decimal_t  = T;

public:
    explicit DoubleValidator(QObject* parent = Q_NULLPTR) noexcept
        : DoubleValidator(DBL_MIN, DBL_MAX, parent) {
    }
    explicit DoubleValidator(double bottom, double top, QObject* parent = Q_NULLPTR) noexcept
        : inherited(bottom,top,decimal_t::precision(), parent) {
        inherited::setNotation(QDoubleValidator::StandardNotation);
    }
    static inline double currentDouble(QString const& str
                                        , QLocale const& loc = {}) noexcept {
        double res;
        ps2::from_qstr(str, res, loc);
        return res;
    }
    static inline decimal_t current(QString const& str, QLocale const& loc = {}) noexcept {
        if(str.isEmpty()) return decimal_t::zero();
        return decimal_t::fromString(str, loc);
    }
    static inline QString createString(int fraction, int integral
                                , QLocale const& loc = {}) noexcept {
        return decimal_t(fraction, integral).toString(loc);
    }
};

} // end ps2 namespace
