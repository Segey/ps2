/**
 * \file      ps2/ps2/qt/widgets/validator/weight/kggr_int_validator.h
 * \brief     The Kggr_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 27(th), 2017, 23:18 MSK
 * \updated   March     (the) 27(th), 2017, 23:18 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createGrValidator() noexcept {
        auto validator = new ps2::KgGrIntValidator::grs(m_gr_field);
        m_gr_field->setValidator(validator);
    }
 * \endcode
**/
class KgGrIntValidator final: public IntValidator {
public:
    using class_name = KgGrIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* kgs(int bottom, int top, QObject* parent = nullptr) noexcept {
        return new class_name{bottom, top, parent};
    }
    static inline class_name* kgs(QObject* parent = nullptr) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* grs(QObject* parent = nullptr) noexcept {
        return new class_name{0, 999, parent};
    }
    static inline int currentKgs(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
    static inline int currentGrs(QString const& str, QLocale const& loc = {}) noexcept {
		return currentKgs(str, loc);
    }
};

} // end ps2 namespace
