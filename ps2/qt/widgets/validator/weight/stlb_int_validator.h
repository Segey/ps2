/**
 * \file      ps2/ps2/qt/widgets/validator/weight/stlb_int_validator.h
 * \brief     The Stlb_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 02:20 MSK
 * \updated   April     (the) 02(th), 2017, 02:20 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createKgValidator() noexcept {
        auto validator = new ps2::StLbIntValidator::pounds(m_kg_field);
        m_kg_field->setValidator(validator);
    }
 * \endcode
**/
class StLbIntValidator final: public IntValidator {
public:
    using class_name = StLbIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* stones(int bottom, int top, QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{bottom, top, parent};
    }
    static inline class_name* stones(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* pounds(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, 13, parent};
    }
    static inline int currentStones(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
    static inline int currentPoundes(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
};

} // end ps2 namespace
