/**
 * \file      ps2/ps2/qt/widgets/validator/weight/stlboz_int_validator.h
 * \brief     The Stlboz_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 02:51 MSK
 * \updated   April     (the) 02(th), 2017, 02:51 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createTonesValidator() noexcept {
        auto validator = new ps2::KgGrIntValidator::tones(m_kg_field);
        m_kg_field->setValidator(validator);
    }
 * \endcode
**/
class StLbOzIntValidator final: public IntValidator {
public:
    using class_name = StLbOzIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* tones(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* pounds(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name(0, 13, parent);
    }
    static inline class_name* ounces(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, 15, parent};
    }
};

} // end ps2 namespace
