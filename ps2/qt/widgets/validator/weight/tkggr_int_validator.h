/**
 * \file      ps2/ps2/qt/widgets/validator/weight/tkggr_int_validator.h
 * \brief     The Tkggr_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 02(th), 2017, 01:01 MSK
 * \updated   April     (the) 02(th), 2017, 01:01 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createKgValidator() noexcept {
        auto validator = new ps2::KgGrIntValidator::kgs(m_kg_field);
        m_kg_field->setValidator(validator);
    }
 * \endcode
**/
class TKgGrIntValidator final: public IntValidator {
public:
    using class_name = TKgGrIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* tones(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* kgs(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, 999, parent};
    }
    static inline class_name* grs(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, 999, parent};
    }
};

} // end ps2 namespace
