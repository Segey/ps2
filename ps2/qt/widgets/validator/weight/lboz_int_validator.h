/**
 * \file      ps2/ps2/qt/widgets/validator/weight/lboz_int_validator.h
 * \brief     The Lb_int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   March     (the) 31(th), 2017, 18:13 MSK
 * \updated   March     (the) 31(th), 2017, 18:13 MSK
 * \TODO      
**/
#pragma once
#include <ps2/qt/widgets/validator/int_validator.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createPoundsValidator() noexcept {
        auto validator = new ps2::LbOzIntValidator::pounds(m_kg_field);
        m_kg_field->setValidator(validator);
    }
 * \endcode
**/
class LbOzIntValidator final: public IntValidator {
public:
    using class_name = LbOzIntValidator;
    using inherited  = IntValidator;

private:
    using IntValidator::IntValidator;

public:
    static inline class_name* pounds(int bottom, int top, QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{bottom, top, parent};
    }
    static inline class_name* pounds(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{INT_MIN, INT_MAX - 1, parent};
    }
    static inline class_name* ounces(QObject* parent = Q_NULLPTR) noexcept {
        return new class_name{0, 15, parent};
    }
    static inline int currentPounds(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
    static inline int currentOunces(QString const& str, QLocale const& loc = {}) noexcept {
        return inherited::current(str, loc);
    }
};

} // end ps2 namespace
