/**
 * \file      ps2/ps2/qt/widgets/validator/int_validator.h
 * \brief     The Int_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 01(th), 2017, 02:22 MSK
 * \updated   April     (the) 01(th), 2017, 02:22 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QIntValidator>
#include <ps2/qt/convert/integer.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createLbValidator() noexcept {
        auto validator = new ps2::IntValidator(m_kg_field);
        m_kg_field->setValidator(validator);
    }
 * \endcode
**/
class IntValidator: public QIntValidator {
public:
    using class_name = IntValidator;
    using inherited  = QIntValidator;

public:
    explicit IntValidator(QObject* parent = nullptr) noexcept
        : IntValidator(INT_MIN, INT_MAX, parent) {
    }
    explicit IntValidator(int bottom, int top, QObject* parent = nullptr) noexcept
        : inherited(bottom, top, parent) {
    }
    IntValidator(class_name const& rhs) noexcept
        : inherited(rhs.bottom(), rhs.top(), rhs.parent()) {
    }
    static inline int current(QString const& str, QLocale const& loc = {}) noexcept {
        if(str.isEmpty()) return 0;
        return toInt(str, INT_MAX, loc);
    }
    static inline QString createString(int val, QLocale const& loc = {}) noexcept {
        return loc.toString(val);
    }
};

} // end ps2 namespace
