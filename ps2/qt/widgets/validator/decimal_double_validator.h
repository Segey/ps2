/**
 * \file      ps2/ps2/qt/widgets/validator/decimal_double_validator.h
 * \brief     The Decimal_validator class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 21(th), 2017, 18:03 MSK
 * \updated   April     (the) 21(th), 2017, 18:03 MSK
 * \TODO      
**/
#pragma once
#include <QLocale>
#include <QValidator>
#include <ps2/cpp/integer.h>
#include <ps2/cpp/typedefs.h>
#include <ps2/cpp/decimal.h>

/** \namespace ps2 */
namespace ps2 {
/**
 * \code
    void createValidator() noexcept {
        auto validator = new ps2::DecimalDoubleValidator<ps2::KgGrDecimal, 3>(m_field);
        m_field->setValidator(validator);
    }
 * \endcode
**/
template<class T, class D, T(*F)(D const&)>
class DecimalDoubleValidator: public QValidator {
public:
    using class_name = DecimalDoubleValidator<T,D,F>;
    using inherited  = QValidator;
    using user_t     = T;
    using decimal_t  = D;

private:
    user_t m_bottom{INT_MIN};
    user_t m_top{INT_MAX};

private:
    template<class Y>
    static inline decltype(auto) cuttedValue(Y val, Y count
                                         , bool is = false) noexcept {
        return ps2::cutTail(val, ps2::digitCount(val) - count
                            - (is ? 1 : 0));
    }

    template<class Y>
    QValidator::State rangeCompare(Y bottom, Y top, Y val) const noexcept {
        auto const is   = ps2::digitCount(top) > ps2::digitCount(bottom);
        auto const c    = ps2::digitCount(val);
        auto const b    = cuttedValue(bottom, c);
        auto const t    = cuttedValue(top, c, is);

        if(val < b) val *= 10;
        return b <= val && val <= t ? QValidator::Intermediate
                                    : QValidator::Invalid;
    }
    bool isFullRange() const noexcept {
        return (m_bottom.toInt() == INT_MIN
                || m_top.toInt() == INT_MAX);
    }

public:
    DecimalDoubleValidator(QObject* parent = Q_NULLPTR) noexcept
        : inherited(parent) {
    }
    DecimalDoubleValidator(T const& bottom, T const& top, QObject* parent = Q_NULLPTR) noexcept
        : inherited(parent)
        ,  m_bottom(bottom)
        , m_top(top) {
    }
    DecimalDoubleValidator(T&& bottom, T&& top, QObject* parent = Q_NULLPTR) noexcept
        : inherited(parent)
        ,  m_bottom(qMove(bottom))
        , m_top(qMove(top)) {
    }
    static inline double currentDouble(QString const& str, QLocale const& loc = {}) noexcept {
        double res = 0;
        ps2::from_qstr(str, res, loc);
        return res;
    }
    decimal_t current(QString const& str, QLocale const& loc = {}) const noexcept {
        if(str.isEmpty())
            return decimal_t(0);

        int pos;
        auto input = str;
        decimal_t result = ps2::null<decimal_t>();
        if(validate(input, pos) == QValidator::Acceptable)
            ps2::from_qstr(str, result, loc);
        return result;
    }
    static inline QString createString(int fraction, int integral
                                , QLocale const& loc = {}) noexcept {
        return to_qstr(decimal_t(fraction, integral), loc);
    }
    virtual QValidator::State validate(QString& input, int&) const override final {
        if(input.isEmpty()) return QValidator::Intermediate;

        auto const inx = input.indexOf(inherited::locale().decimalPoint());
        if(inx == input.size() - 1 && inx > 0) return QValidator::Intermediate;

        decimal_t val;
        if(!from_qstr(input, val, inherited::locale()))
            return QValidator::Invalid;

        auto const& value = F(val);

        if(isFullRange()) return QValidator::Acceptable;
        if(value >= m_top) return QValidator::Invalid;
        if(value > m_bottom) return QValidator::Acceptable;
        return rangeCompare(m_bottom.toInt(), m_top.toInt(), value.toInt());
    }
};

} // end ps2 namespace
