/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/qt/widgets/clicked_lcdnumber.h 
 * \brief     The Clicked_lcdnumber class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   May       (the) 24(th), 2017, 18:03 MSK
 * \updated   May       (the) 24(th), 2017, 18:03 MSK
 * \TODO      
**/
#pragma once
#include <QMouseEvent>
#include <QLCDNumber>

/** \namespace ps2 */
namespace ps2 {

class ClickedLcdnumber final: public QLCDNumber {
    Q_OBJECT
public:
    using class_name = ClickedLcdnumber;
    using inherited  = QLCDNumber;
     
private: 
    virtual void mousePressEvent(QMouseEvent* e) Q_DECL_OVERRIDE Q_DECL_FINAL {
        if(e->button() == Qt::RightButton) return;
        emit clicked();
        inherited::mousePressEvent(e);
    }

public:
    using inherited::inherited;

signals:
    void clicked() const;

};

} // end namespace ps2
