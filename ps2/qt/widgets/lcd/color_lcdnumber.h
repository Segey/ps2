/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/qt/widgets/lcd/color_lcdnumber.h 
 * \brief     The Color_lcdnumber class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   May       (the) 24(th), 2017, 18:44 MSK
 * \updated   May       (the) 24(th), 2017, 18:44 MSK
 * \TODO      
**/
#pragma once
#include <QMouseEvent>
#include <QLCDNumber>

/** \namespace ps2 */
namespace ps2 {

class ColorLcdNumber final: public QLCDNumber {
    Q_OBJECT
public:
    using class_name = ColorLcdNumber;
    using inherited  = QLCDNumber;
     
private: 
    QColor m_active   = Qt::black;
    QColor m_inactive = Qt::gray;
    QString m_value   = QStringLiteral("0");

private:
    virtual void mousePressEvent(QMouseEvent* e) Q_DECL_OVERRIDE Q_DECL_FINAL {
        if(e->button() == Qt::RightButton) return;
        emit clicked();
        setActive(true);
        inherited::mousePressEvent(e);
    }

public:
    using inherited::inherited;
    explicit ColorLcdNumber(QColor const& active, QColor const& inactive, QWidget* parent = Q_NULLPTR) noexcept
        : inherited(parent)
        , m_active(active)
        , m_inactive(inactive) {
    }
    bool isActive() const noexcept {
        return inherited::palette().color(QPalette::WindowText)
                                    == m_active;
    }
    void setActive(bool is = true) noexcept {
        auto p = inherited::palette();
        p.setColor(QPalette::WindowText, is ? m_active : m_inactive );
        inherited::setPalette(p);
    }
    void setInActive(bool is = true) noexcept {
        setActive(!is);
    }
    void setToggleColors(QColor const& active,  QColor const& inactive) noexcept {
        m_active = active;    
        m_inactive = inactive;    
    }
    QColor const& active() const noexcept {
        return m_active;
    }
    void setActiveColor(QColor const& active) noexcept {
        m_active = active;
    }
    void setValue(QString const& val) noexcept {
        m_value = val;
        inherited::display(val);
    }
    QString const& strValue() const noexcept {
        return m_value;
    }
    QColor const& inactive() const noexcept {
        return m_inactive;
    }
    void setInactiveColor(QColor const& inactive) noexcept {
        m_inactive = inactive;
    }
    void addKey(Qt::Key key) noexcept {
        auto const zero = QStringLiteral("0");
        auto const point = QStringLiteral(".");
        if(key == Qt::Key_Clear) {
            return (void)setValue(zero);
        }
        if(Qt::Key_Backspace == key) {
            if(strValue().length() < 2)
                setValue(zero);
            else
                setValue(strValue().mid(0, strValue().length() - 1));
            return;
        }
        else if(Qt::Key_Comma == key) {
            if(strValue().contains(point))
                return;
            if(strValue().isEmpty())
                setValue(zero);
            return setValue(strValue() + point);
        }

        auto const val = key - Qt::Key_0;
        if(strValue() == zero)
            setValue(QString::number(val));
        else
            setValue(strValue() + QString::number(val));
    }

signals:
    void clicked();

};

} // end namespace ps2
