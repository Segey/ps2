/**
 * \file      ps2/ps2/qt/widgets/dock.h
* \brief     A Dock widget
* \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
* \version   v.2.2
* \created   February (the) 25(th), 2012, 01:12 MSK
* \updated   July     (the) 01(th), 2015, 20:57 MSK
* \TODO
**/
#pragma once
#include <QDockWidget>

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *  ps2::Dock* dock = new ps2::Dock<QCalendarWidget>(m_parent);
 *      (*dock)->setGridVisible(true);
 *      m_parent->addDockWidget(Qt::LeftDockWidgetArea, dock);
 * \endcode
**/
template<class T>
class Dock final: public QDockWidget {
public:
    using inherited  = QDockWidget;
    using value_t    = T;
    using class_name = Dock<value_t>;
    using dock_type  = QDockWidget;

private:
    QWidget* m_parent = Q_NULLPTR;
    value_t* m_widget = Q_NULLPTR;

public:
    template<class... Args>
    explicit Dock(QWidget* parent, Args&&... args) noexcept
        : inherited(parent)
        , m_parent(parent)
        , m_widget(new value_t(std::forward<Args>(args)..., m_parent)) {
        inherited::setWidget(m_widget);
    }
    value_t* widget() const noexcept {
        return m_widget; 
    }
    value_t const* operator->() const noexcept {
        return m_widget; 
    }
    value_t* operator->() noexcept {
        return m_widget; 
    }
    QWidget* parent() const noexcept {
        return m_parent;
    }
};

/**
 * \code
 *      m_names = ps2::makeDock<QTreeView>(this
                  , Qt::LeftDockWidgetArea, QStringLiteral("Cool"));

        inherited::m_parent->m_weight = ps2::makeDock<the::CbWeightWidget>(
            inherited::m_window , Qt::LeftDockWidgetArea, QStringLiteral("DockWeight")
            ,  edition::widget::weight::CbEdition {
                  { WeightType::KgGrDouble, {40_kg, 151_kg, 500_gr}}
                , { WeightType::LbOzDouble, {88_lb, 331_lb, 4_oz }}
            });
 * \endcode
 */
template<class R, class U, class... Args>
static inline Dock<R>* makeDock(U* parent, Qt::DockWidgetArea area
                    , QString const& name, Args&&... args) noexcept {
    auto result = new Dock<R>(parent, std::forward<Args>(args)...);
    parent->addDockWidget(area, result);
    result->setObjectName(name);
    return result;
}

} // end namespace ps2

