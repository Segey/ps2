/**
 * \file      ps2/ps2/qt/widgets/check_header_view.h
* \brief      The Check Header View
* \author     S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
* \version    v.1.00
* \date       Created on 20 August 2012 y., 13:15
* \TODO
*/
#pragma once
#include <QHeaderView>
#include <QList>
#include <QMap>

class QAbstractItemModel;

/** namespace go */
namespace go {
class CheckHeaderView: public QHeaderView {
Q_OBJECT
public:
    typedef QHeaderView         inherited;
    typedef CheckHeaderView     class_name;
    typedef QList<int>          indexes_type;

private:
    indexes_type                checkable_indexes_;
    QMap<int, Qt::CheckState>   check_states_;
    mutable QMap<int, QRect>    section_rects_;

public:
    /**
     * \brief Ctor
     * \param {in: Qt::Orientation} orientation 
     * \param {in: QWidget* } parent 
     * \code
     *      auto header = new go::CheckHeaderView(Qt::Horizontal, tableView);
     *      tableView->setHorizontalHeader(header);
     *      header->addCheckable(1); 
     * \endcode
     */
    explicit CheckHeaderView(Qt::Orientation orientation, QWidget* parent = NULL);
    virtual ~CheckHeaderView();

    indexes_type checkableIndexes() const {
        return checkable_indexes_;
    }
    bool addCheckable(int index) {
        if (NULL != model() && false == checkable_indexes_.contains(index)) {
            checkable_indexes_.append(index);
            check_states_.insert(index, Qt::Unchecked);
            updateCheckbox(index);
            return true;
        }
        return false;
    }
    void removeCheckable(int index) {
        checkable_indexes_.removeOne(index);
        check_states_.remove(index);
    }
    void setModel(QAbstractItemModel* model) {
        section_rects_.clear();
        QHeaderView::setModel(model);

        foreach(int logicalIndex, checkable_indexes_) {
            updateCheckbox(logicalIndex);
        }
    }

protected slots:
    virtual void dataChanged(const QModelIndex& topLeft, const QModelIndex & bottomRight);

protected:
    void mousePressEvent(QMouseEvent* event);
    void paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const;
    bool blockDataChanged;
    void updateCheckbox(int logicalIndex) {
        bool allchecked = true;
        bool allunchecked = true;

        if (orientation() == Qt::Horizontal)
        {
            for (int i = 0; i < model()->rowCount(rootIndex()); ++i)
            {
                if (model()->index(i, logicalIndex, rootIndex()).data(Qt::CheckStateRole).toInt()) { allunchecked = false; }
                else { allchecked = false; }
            }
        } else if (orientation() == Qt::Vertical)
        {
            for (int i = 0; i < model()->columnCount(rootIndex()); ++i) {
                if (model()->index(logicalIndex, i, rootIndex()).data(Qt::CheckStateRole).toInt()) {
                    allunchecked = false;
                }
                else allchecked = false;
            }
        }
        if (allunchecked) check_states_.insert(logicalIndex, Qt::Unchecked);
        else if (allchecked) check_states_.insert(logicalIndex, Qt::Checked);
        else check_states_.insert(logicalIndex, Qt::PartiallyChecked);
    }
    void updateModel(int logicalIndex) {
        Qt::CheckState checked = check_states_.value(logicalIndex);
        if (orientation() == Qt::Horizontal) {
            for (int i = 0; i < model()->rowCount(rootIndex()); ++i) {
                model()->setData(model()->index(i, logicalIndex, rootIndex()), checked, Qt::CheckStateRole);
            }
        } else if (orientation() == Qt::Vertical) {
            for (int i = 0; i < model()->columnCount(rootIndex()); ++i) {
                model()->setData(model()->index(logicalIndex, i, rootIndex()), checked, Qt::CheckStateRole);
            }
        }
    }
    QRect checkRect(const QStyleOptionHeader& option, const QRect& bounding) const;
    bool rowIntersectsSelection(int row) const {
        return (selectionModel() ? selectionModel()->rowIntersectsSelection(row, rootIndex()) : false);
    }
    inline bool columnIntersectsSelection(int column) const {
        return (selectionModel() ? selectionModel()->columnIntersectsSelection(column, rootIndex()) : false);
    }
    inline bool sectionIntersectsSelection(int logical) const {
        return (orientation() == Qt::Horizontal ? columnIntersectsSelection(logical) : rowIntersectsSelection(logical));
    }
    inline bool isRowSelected(int row) const {
        return (selectionModel() ? selectionModel()->isRowSelected(row, rootIndex()) : false);
    }
    inline bool isColumnSelected(int column) const {
        return (selectionModel() ? selectionModel()->isColumnSelected(column, rootIndex()) : false);
    }
};
} // end namespace go
