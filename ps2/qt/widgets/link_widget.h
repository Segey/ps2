/**
 * \file      ps2/ps2/qt/widgets/link_widget.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.0
 * \created   August  (the) 29(th), 2015, 15:18 MSK
 * \updated   January (the) 17(th), 2017, 13:30 MSK
 * \TODO      
**/
#pragma once
#include <QUrl>
#include <QString>
#include <QLabel>
#include <ps2/qt/objects/link.h>

/** \namespace ps2 */
namespace ps2 {
    
class LinkWidget : public Link, public QLabel {
public:
    using class_name = LinkWidget;
    using inherited  = QLabel;

private: 
    void update() noexcept {
        QLabel::setText(Link::toHtml());
    }

public:
    explicit LinkWidget(QUrl const& url = QUrl(), QString const& text = QString()) 
        : Link(url, text) {
        QLabel::setOpenExternalLinks(true);
        update();
    }
    explicit LinkWidget(QUrl&& url, QString&& text)
        : Link(qMove(url), qMove(text)) {
        QLabel::setOpenExternalLinks(true);
        update();
    }
    using Link::url;
    void setUrl(QUrl const& url) noexcept {
        Link::setUrl(url);
        update();
    }
    void setUrl(QUrl&& url) noexcept {
        Link::setUrl(qMove(url));
        update();
    }
    void setUrl(QString const& url) noexcept {
        Link::setUrl(url);
        update();
    }
    using Link::text;
    void setText(QString const& text) noexcept {
        Link::setText(text);
        update();
    }
    void setText(QString&& text) noexcept {
        Link::setText(qMove(text));
        update();
    }
};

} // end namespace ps2

