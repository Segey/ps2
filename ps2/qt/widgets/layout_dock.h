/**
 * \file      ps2/ps2/qt/widgets/layout_dock.h
 * \brief     The Layout_dock class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 14(th), 2017, 01:16 MSK
 * \updated   March     (the) 14(th), 2017, 01:16 MSK
 * \TODO      
**/
#pragma once
#include <QDockWidget>

/** namespace ps2 */
namespace ps2 {
/**
 * \code
 *  ps2::LayoutDock* dock = new ps2::LayoutDock<QCalendarWidget>(m_parent);
 *      (*dock)->setGridVisible(true);
 *      m_parent->addDockWidget(Qt::LeftDockWidgetArea, dock);
 * \endcode
**/
class LayoutDock: public QDockWidget {
public:
    using inherited  = QDockWidget;
    using class_name = LayoutDock;
    using dock_type  = QDockWidget;

private:
    QWidget* m_parent = Q_NULLPTR;
    QWidget* m_widget = Q_NULLPTR;
    QLayout* m_layout = Q_NULLPTR;

public:
    explicit LayoutDock(QWidget* parent) noexcept
        : inherited(parent)
        , m_widget(new QWidget(m_parent)) {
        inherited::setWidget(m_widget);
    }
    explicit LayoutDock(QWidget* parent, QLayout* layout) noexcept
        : inherited(parent)
        , m_parent(parent)
        , m_widget(new QWidget(m_parent)) {
        inherited::setWidget(m_widget);
        init(layout);
    }
    void init(QLayout* layout) noexcept {
        m_layout = layout;
        m_widget->setLayout(layout);
    }
    QWidget* widget() const noexcept {
        return m_widget; 
    }
    QLayout* operator->() const noexcept {
        return m_layout;
    }
    QLayout* operator->() noexcept {
        return m_layout;
    }
    QWidget* parent() const noexcept {
        return m_parent;
    }
    QLayout* layout() const noexcept {
        return m_layout;
    }
};

/**
 * \code
 *      m_names = ps2::makeDock<QTreeView>(this, Qt::LeftDockWidgetArea, QStringLiteral("Cool"));
 * \endcode
 */
//template<class T>
//static inline LayoutDock* makeLayoutDock(T* parent, Qt::DockWidgetArea area
//                                , QString const& name) noexcept {
//    auto result = new LayoutDock<T>(parent);
//    parent->addDockWidget(area, result);
//    result->setObjectName(name);
//    return result;
//}

} // end namespace ps2

