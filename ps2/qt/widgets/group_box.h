/**
 * \file      ps2/ps2/qt/widgets/group_box.h
*   \brief		The GroupBox
*   \author		S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
*   \version	v.1.00
*   \date		Created on 25 Febuary 2012 y., 01:12
*   \TODO
*/
#pragma once

/** namespace go */
namespace go {

template<class T>
class GroupBox : public QGroupBox {
public:
    typedef QGroupBox                       inherited;
    typedef T           		    value_type;
    typedef GroupBox<value_type>            class_name;
    value_type*                             pointer;

private:
    value_type* widget_;

public:
    /**
     * \brief Ctor
     * \param {in: QWidget*} parent - The parent of GroupBox
     * \code
     *	    go::GroupBox* group = new go::GroupBox<QCalendarWidget>(parent_);
     *	    (*group)->setGridVisible(true);
     * \endcode
     */
    explicit GroupBox(QWidget* parent)
        : inherited(parent)
        , widget_(new value_type) {

        QHBoxLayout* lay = new QHBoxLayout(this);
        lay->addWidget(widget_);
    }
    value_type* widget() const {
        return widget_; 
    }
    value_type* operator->() const {
        return widget_; 
    }
    value_type* operator->() {
        return widget_; 
    }
};

} // end namespace go

