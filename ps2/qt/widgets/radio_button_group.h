/**
 * \file      ps2/ps2/qt/widgets/radio_button_group.h
* \brief      The RadioButtonGroup
* \author     S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
* \version    v.2.01
* \date       Created on 18 August 2012 y., 21:46
* \TODO
**/
#pragma once
/** namespace go */
namespace go {
template<int MAX>
class RadioButtonGroup : public QGroupBox {
public:
    typedef QGroupBox		   inherited;
    typedef RadioButtonGroup<MAX>  class_name;

private:
    QGridLayout*    lay_;
    QButtonGroup*   group_;
    uint            cx_;
    uint            cy_;

public:
    /**
     * \brief Ctor
     * \param {in: QString const&} title - The title of RadioButton Group 
     * \param {in: QWidget*} parent - The parent of MainWindow 
     * \code
     *	   go::RadioButtonGroup<1>* group = new go::RadioButtonGroup<1>("Cool", parent_);
     *	   parent_->addRadioButtonGroupWidget(Qt::LeftRadioButtonGroupWidgetArea, dock);
     * \endcode
     */
    explicit RadioButtonGroup(QString const& title = QString() , QWidget* parent = nullptr )
        : inherited(title, parent), lay_(new QGridLayout), group_(new QButtonGroup(parent)), cx_(0), cy_(0) {
        inherited::setLayout(lay_);
    }
    /**
     * \brief The function creates and adds QRadioButton to group 
     * \param {in: QString const& } title - The title of QRadioButton
     * \param {in: int } id - The id of QRadioButton
     */
    void addRadioButton(QString const& title, int id) { 
        QRadioButton* button = new QRadioButton(title, this);
        group_->addButton(button, id);
        if(cx_ >= MAX) {
            ++cy_; 
            cx_ = 0;
        }
        lay_->addWidget(button, cy_, cx_++);
    }
    /**
     * \code
     *      bottom_box_->group()->button(1)->setChecked(true);
     * \endcode
     */
    QButtonGroup* group() {
       return group_; 
    }
};
} // end namespace go

