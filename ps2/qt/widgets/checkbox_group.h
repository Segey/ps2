/**
 * \file      ps2/ps2/qt/widgets/checkbox_group.h
* \brief      The CheckBoxGroup
* \author     S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
* \version    v.1.00
* \date       Created on 22 September 2012 y., 23:59
* \TODO
*/
#pragma once

/** namespace go */
namespace go {
template<int MAX>
class CheckBoxGroup : public QGroupBox {
public:
    typedef QGroupBox           inherited;
    typedef CheckBoxGroup<MAX>  class_name;

private:
    QGridLayout*                lay_;
    QButtonGroup*               group_;
    uint                        cx_;
    uint                        cy_;

public:
    /**
     * \brief Ctor
     * \param {in: QString const&} title - The title of CheckBox Group 
     * \param {in: QWidget*} parent      - The parent of MainWindow 
     * \code
     *     go::CheckBoxGroup<1>* group = new go::CheckBoxGroup<1>("Cool", parent_);
	 *	   parent_->addCheckBoxGroupWidget(Qt::LeftCheckBoxGroupWidgetArea, dock); 
     * \endcode
     */
    explicit CheckBoxGroup(QString const& title = QString() , QWidget* parent = nullptr ) : inherited(title, parent), lay_(new QGridLayout), group_(new QButtonGroup(parent)), cx_(0), cy_(0) {
        group_->setExclusive(false);
        inherited::setLayout(lay_);
    }
    /**
     * \brief The function creates and adds QCheckBox to group 
     * \param {in: QString const& } title - The title of QCheckBox
     * \param {in: int } id - The id of QCheckBox
     */
    void addCheckBox(QString const& title, int id) { 
        QCheckBox* button = new QCheckBox(title, this);
        group_->addButton(button, id);
        if(cx_ >= MAX) {
            ++cy_; 
            cx_ = 0;
        }
        lay_->addWidget(button, cy_, cx_++);
    }
    
    /**
     * \code
     *      bottom_box_->group()->button(1)->setChecked(true);
     * \endcode
     */
    QButtonGroup* group() {
       return group_; 
    }
};
} // end namespace go

