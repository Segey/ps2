/**
 * \file      ps2/ps2/qt/out.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 14(th), 2016, 15:00 MSK
 * \updated   April     (the) 05(th), 2021, 12:27 MSK
 * \TODO
**/
#pragma once
#include <memory>
#include <QDebug>
#include <QString>
#include <type_traits>
#include <iostream>
#include <ps2/qt/convert/enum.h>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
    inline std::ostream& operator<<(std::ostream& out, Info const& info) noexcept {
        return ps2::cout(out, info);
    }
 * \endcode
**/
template<class T>
static inline std::ostream& cout(std::ostream& out, T const& item) noexcept {
    QString s;
    QDebug(&s) << item;
    out << qPrintable(s);
    return out;
}
template<class T>
static inline QString cout(T const& item) noexcept {
    QString s;
    QDebug(&s) << item;
    return s;
}
static inline QString cout(QString const& item) noexcept {
    return item;
}
template<class T, typename = typename std::enable_if<std::is_enum<T>::value, T>::type>
inline QDebug operator<<(QDebug dbg, T type)  {
    return dbg.nospace()
        << "Type: { name: " << toString(type)
        << ", id: " << ps2::to_utype(type) << "}";
}
template<class T, typename = typename std::enable_if<std::is_enum<T>::value, T>::type>
inline std::ostream& operator<<(std::ostream& out, T type) noexcept {
    return ps2::cout(out, type);
}

} // end namespace ps2

template <class T>
inline QDebug operator<<(QDebug debug, std::vector<std::shared_ptr<T>> const& c) noexcept
{
    const bool oldSetting = debug.autoInsertSpaces();
    typename std::vector<std::shared_ptr<T>>::const_iterator it = c.begin(), end = c.end();
    if (it != end) {
        debug << *(*it);
        ++it;
    }
    while (it != end) {
        debug << ", " << *(*it);
        ++it;
    }
    debug << ')';
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();
}
