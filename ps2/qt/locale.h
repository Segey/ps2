/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/locale.h 
 * \brief     The Locale class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   July      (the) 20(th), 2017, 13:17 MSK
 * \updated   July      (the) 20(th), 2017, 13:17 MSK
 * \TODO      
**/
#pragma once
#include <QSet>
#include <QVector>
#include <QLocale>

/** \namespace ps2::locale */
namespace ps2::locale {

inline QSet<QChar> groupSeparators() noexcept {
    return {{
          QChar::fromLatin1('.')
        , QChar::fromLatin1(',')
        , QChar::fromLatin1(' ')
        , QChar(0x066c)
        , QChar(0x2019)
        , QChar::Nbsp
    }};
}
//!< less than groupSeparators because the char 'space'
inline QVector<QLocale::Language> groupSeparatorLanguages() noexcept {
    QVector<QLocale::Language> result;
    result.push_back(QLocale::Russian);
    result.push_back(QLocale::English);
    result.push_back(QLocale::German);
    result.push_back(QLocale::CentralKurdish);
    result.push_back(QLocale::SwissGerman);
    return result;
}
inline QSet<QChar> decimalPoints() noexcept {
    return {{
        QChar::fromLatin1('.')
      , QChar::fromLatin1(',')
      , QChar(0x066b)
    }};
}
inline QVector<QLocale::Language> decimalPointLanguages() noexcept {
    QVector<QLocale::Language> result;
    result.push_back(QLocale::Russian);
    result.push_back(QLocale::English);
    result.push_back(QLocale::Arabic);
    return result;
}
inline QSet<QChar> percents() noexcept {
    return {{
        QChar::fromLatin1('%')
      , QChar(0x066a)
    }};
}
inline QVector<QLocale::Language> percentLanguages() noexcept {
    QVector<QLocale::Language> result;
    result.push_back(QLocale::Russian);
    result.push_back(QLocale::Pashto);
    return result;
}
inline QVector<QLocale::Language> doubleLanguages() noexcept {
    QVector<QLocale::Language> result;
    result.push_back(QLocale::Russian);
    result.push_back(QLocale::English);
    result.push_back(QLocale::German);
    return result;
}

} // end namespace ps2::locale
