/**
 * \file      ps2/ps2/qt/validators/email_validator.h
 * \brief     Sex class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 06(th), 2015, 18:57 MSK
 * \updated   May (the) 06(th), 2015, 18:57 MSK
 * \TODO
**/
#pragma once
#include <QRegExp>
#include <QRegExpValidator>
#include <ps2/qt/regex/regex_match.h>
#include <ps2/qt/regex/regex_validator.h>

/** \namespace ps2 */
namespace ps2 {

class EmailValidator final: public QRegExpValidator {
public:
    using class_name = EmailValidator;
    using inherited  = QRegExpValidator;

public:
    explicit EmailValidator(QObject* parent = Q_NULLPTR) noexcept
        : inherited(regexp::validator::email(), parent) {
    }
    /**
     * \code
     *     bool is = ps2::EmailValidator::validate(QStringLiteral("dix75@mail.ru"));
     * \endcode
     **/
    static bool validate(QString const& mail) noexcept {
        return regexp::match::email(mail);
    }
};

} // end ps2 namespace
