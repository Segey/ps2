/**
 * \file      ps2/ps2/qt/printer.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 19(th), 2015, 23:32 MSK
 * \updated   September (the) 19(th), 2015, 23:32 MSK
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QString>
#include <QTextEdit>
#include <QPrinter>
#include <QPrintDialog>
#include <QApplication>
#include <QSharedPointer>
#include <QAbstractPrintDialog>
#include <QPrintPreviewDialog>

/** \namespace  */
namespace ps2  {

template <class T>
inline void print(T* widget, QString const& window_title) {
    QPrinter printer(QPrinter::HighResolution);
    QSharedPointer<QPrintDialog> dialog(new QPrintDialog(&printer));
    if(widget->textCursor().hasSelection()) dialog->addEnabledOption(QAbstractPrintDialog::PrintSelection);
    dialog->setWindowTitle(window_title);

    if(dialog->exec() == QDialog::Accepted)
        widget->print(&printer);
}

template<class T, class U>
inline void printPreview(T* widget, U* parent) {
    QPrinter printer(QPrinter::HighResolution);
    QPrintPreviewDialog preview(&printer, parent);
    QObject::connect(&preview, &QPrintPreviewDialog::paintRequested, [&](QPrinter* printer){
        widget->print(printer);
    });
    preview.exec();
}

template<class U>
inline void printPreview(QString const& text, U* parent) {
    QSharedPointer<QTextEdit> edit(new QTextEdit(parent));
    edit->document()->setPlainText(text);
    printPreview(edit.data(), parent);
}

} // end namespace
