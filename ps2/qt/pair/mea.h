/**
 * \file      ps2/ps2/qt/pair/mea.h
 * \brief     The Mea class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May       (the) 04(th), 2017, 16:27 MSK
 * \updated   May       (the) 04(th), 2017, 16:27 MSK
 * \TODO      
**/
#pragma once
#include <QTypeInfo>
#include <QtCore/qglobal.h>
#include <QTypeInfoMerger>
#include <ps2/qt/out.h>

/** \namespace ps2 */
namespace ps2 {

template<class T>
struct MeaPair final {
public:
    using class_name = MeaPair<T>;
    using value_t = T;
 
public: 
    value_t metric;
    value_t imperial;

public:
    MeaPair() = default;
    MeaPair(value_t metric, value_t imperial) noexcept
        : metric(metric)
        , imperial(imperial) {
    }
    void swap(class_name& rhs) noexcept {
        qSwap(metric, rhs.metric);
        qSwap(imperial, rhs.imperial);
    }
};

template<class T>
inline QDebug operator<<(QDebug dbg, MeaPair<T> const& pair) noexcept {
    return dbg.nospace() << "\"MeaPair\": {"
                         << "\"metric\": " << pair.metric
                         << ",\"imperial\": " << pair.imperial
                         << "}";
}
template<class T>
inline std::ostream& operator<<(std::ostream& out, MeaPair<T> const& e) noexcept {
    return ps2::cout(out, e);
}

} // end namespace ps2

template<class T>
class QTypeInfo<ps2::MeaPair<T> > : public QTypeInfoMerger<ps2::MeaPair<T>, T> {
}; // Q_DECLARE_TYPEINFO
