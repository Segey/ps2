/**
 * \file      ps2/ps2/qt/pair/bool.h
 * \brief     The Bool class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   September (the) 30(th), 2016, 14:52 MSK
 * \updated   September (the) 30(th), 2016, 14:52 MSK
 * \TODO
**/
#pragma once
#include "../out.h"
#include <QTypeInfo>
#include <QtCore/qglobal.h>
#include <QTypeInfoMerger>

/** \namespace ps2 */
namespace ps2 {

template <class T>
struct BoolPair {
    using bool_t  = bool;
    using value_t  = T;

    bool_t  status;
    value_t value;

    Q_DECL_CONSTEXPR BoolPair() Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_default_constructible<T>::value)
        : status(false)
        , value() {
    }
    Q_DECL_CONSTEXPR BoolPair(T const& t) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_copy_constructible<T>::value)
        : status(false)
        , value(t) {
    }
    Q_DECL_CONSTEXPR BoolPair(bool status, T const& t) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_copy_constructible<T>::value)
        : status(status)
        , value(t) {
    }
    template <class TT>
    Q_DECL_CONSTEXPR BoolPair(BoolPair<TT> const& p) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_constructible<TT&>::value)
        : status(p.status)
        , value(p.value) {
    }
//    template <class TT>
//    Q_DECL_RELAXED_CONSTEXPR BoolPair& operator=(BoolPair<TT> const& p) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_assignable<T, TT&>::value) {
//        status = p.status;
//        value = p.value;
//        return *this;
//    }
#ifdef Q_COMPILER_RVALUE_REFS
    Q_DECL_CONSTEXPR BoolPair(T&& t) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_constructible<T>::value)
        : status(false)
        , value(std::move(t)) {
    }
    Q_DECL_CONSTEXPR BoolPair(bool status, T&& t) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_constructible<T>::value)
        : status(status)
        , value(std::move(t)) {
    }
    template <class TT>
    Q_DECL_CONSTEXPR BoolPair(BoolPair<TT>&& p) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_constructible<TT>::value)
        : status(std::move(p.status))
        , value(std::move(p.value)) {
    }
//    template <class TT>
//    Q_DECL_RELAXED_CONSTEXPR BoolPair& operator=(BoolPair<TT>&& p) Q_DECL_NOEXCEPT_EXPR(std::is_nothrow_assignable<T, TT>::value) {
//        status = std::move(p.status);
//        value = std::move(p.value);
//        return *this;
//    }
#endif

    Q_DECL_RELAXED_CONSTEXPR void swap(BoolPair& other) Q_DECL_NOEXCEPT_EXPR(noexcept(qSwap(other.status, other.status))) {
        // use qSwap() to pick up ADL swaps automatically:
        qSwap(status, other.status);
        qSwap(value, other.value);
    }
};

template <class T>
void swap(BoolPair<T>& lhs, BoolPair<T>& rhs) Q_DECL_NOEXCEPT_EXPR(noexcept(lhs.swap(rhs))) {
    lhs.swap(rhs);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator==(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p1.status == p2.status && p1.value == p2.value)) {
    return p1.status == p2.status
        && p1.value == p2.value;
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator!=(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p1 == p2))) {
    return !(p1 == p2);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator<(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p1.status < p2.status || (!(p2.status < p1.status) && p1.value < p2.value))) {
    return p1.status < p2.status || (!(p2.status < p1.status) && p1.value < p2.value);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator>(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p2 < p1)) {
    return p2 < p1;
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator<=(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p2 < p1))) {
    return !(p2 < p1);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator>=(BoolPair<T> const& p1, BoolPair<T> const& p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p1 < p2))) {
    return !(p1 < p2);
}
//template <class T>
//Q_DECL_CONSTEXPR Q_OUTOFLINE_TEMPLATE BoolPair<T> qMakePair(bool x, T const& y) Q_DECL_NOEXCEPT_EXPR(noexcept(BoolPair<T>(x, y))) {
    //return BoolPair<T>(x, y);
//}

template<class T>
inline QDebug operator<<(QDebug dbg, BoolPair<T> const& pair) noexcept {
    return dbg.nospace() << "BoolPair: {"
                         << "status: " << pair.status
                         << " ,value: " << pair.value
                         << "}";
}
template<class T>
inline std::ostream& operator<<(std::ostream& out, BoolPair<T> const& e) noexcept {
    return ps2::cout(out, e);
}

} // end namespace ps2

template<class T>
class QTypeInfo<ps2::BoolPair<T> > : public QTypeInfoMerger<ps2::BoolPair<T>, T> {}; // Q_DECLARE_TYPEINFO
