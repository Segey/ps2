/**
 * \file      ps2/ps2/qt/pair/status.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   February (the) 02(th), 2016, 15:26 MSK
 * \updated   January  (the) 19(th), 2017, 13:36 MSK
 * \TODO
**/
#pragma once
#include <QTypeInfo>
#include <QtCore/qglobal.h>
#include <QTypeInfoMerger>
#include <ps2/qt/out.h>

/** \namespace ps2 */
namespace ps2 {

enum class FieldStatus: quint8 {
    Invalid   = 0u
    , Pure    = 1u
    , Added   = 2u
    , Edited  = 4u
    , Removed = 8u
};

template <class T>
struct StatusPair final {
public:
    using class_name = StatusPair<T>;
    using status_t = FieldStatus;
    using value_t  = T;

public:
    FieldStatus status;
    T           value;

    Q_DECL_CONSTEXPR StatusPair() Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_default_constructible<T>::value))
        : status()
        , value() {
    }
    Q_DECL_CONSTEXPR StatusPair(status_t const& t1, T const& t2) Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_copy_constructible<T>::value))
        : status(t1)
        , value(t2) {
    }
    template <class TT>
    Q_DECL_CONSTEXPR StatusPair(StatusPair<TT> const& p) Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_constructible<TT&>::value))
        : status(p.status)
        , value(p.value) {
    }
    template <class TT>
    Q_DECL_RELAXED_CONSTEXPR StatusPair &operator=(StatusPair<TT> const& p) Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_assignable<T, TT&>::value)) {
        status = p.status;
        value = p.value;
        return *this; 
    }
#ifdef Q_COMPILER_RVALUE_REFS
    template <class TT>
    Q_DECL_CONSTEXPR StatusPair(StatusPair<TT> &&p) Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_constructible<T, TT>::value))
        : status(static_cast<TT &&>(p.status))
        , value(static_cast<TT &&>(p.value)) {
    }
    template <class TT>
    Q_DECL_RELAXED_CONSTEXPR StatusPair &operator=(StatusPair<TT> &&p) Q_DECL_NOEXCEPT_EXPR((std::is_nothrow_assignable<T, TT>::value)) {
        status = std::move(p.status);
        value = std::move(p.value);
        return *this; 
    }
#endif

    Q_DECL_RELAXED_CONSTEXPR void swap(StatusPair& rhs) Q_DECL_NOEXCEPT_EXPR(noexcept(qSwap(rhs.status, rhs.status)) && noexcept(qSwap(rhs.value, rhs.value))) {
        qSwap(status, rhs.status);
        qSwap(value, rhs.value);
    }
};

template <class T>
void swap(StatusPair<T> &lhs, StatusPair<T> &rhs) Q_DECL_NOEXCEPT_EXPR(noexcept(lhs.swap(rhs))) {
    lhs.swap(rhs); 
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator==(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p1.status == p2.status && p1.value == p2.value)) {
    return p1.status == p2.status 
        && p1.value == p2.value; 
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator!=(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p1 == p2))) {
    return !(p1 == p2); 
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator<(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p1.status < p2.status || (!(p2.status < p1.status) && p1.value < p2.value))) {
    return p1.status < p2.status || (!(p2.status < p1.status) && p1.value < p2.value);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator>(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(p2 < p1)) {
    return p2 < p1;
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator<=(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p2 < p1))) {
    return !(p2 < p1);
}
template <class T>
Q_DECL_CONSTEXPR Q_INLINE_TEMPLATE bool operator>=(const StatusPair<T> &p1, const StatusPair<T> &p2) Q_DECL_NOEXCEPT_EXPR(noexcept(!(p1 < p2))) {
    return !(p1 < p2);
}
template <class T>
Q_DECL_CONSTEXPR Q_OUTOFLINE_TEMPLATE StatusPair<T> qMakeStatusPair(const T &x, const T &y) Q_DECL_NOEXCEPT_EXPR(noexcept(StatusPair<T>(x, y))) {
    return StatusPair<T>(x, y);
}

template<class T>
inline QDebug operator<<(QDebug dbg, StatusPair<T> const& pair) noexcept {
    return dbg.nospace() << "\"StatusPair\": {"
                         << "\"status\": " << pair.status
                         << ",\"value\": " << pair.value
                         << "}";
}
template<class T>
inline std::ostream& operator<<(std::ostream& out, StatusPair<T> const& e) noexcept {
    return ps2::cout(out, e);
}

} // end namespace ps2

template<class T>
class QTypeInfo<ps2::StatusPair<T> > : public QTypeInfoMerger<ps2::StatusPair<T>, T> {
}; // Q_DECLARE_TYPEINFO
