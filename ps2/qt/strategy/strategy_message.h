/**
 * \file      ps2/ps2/qt/strategy/strategy_message.h
 * \brief     the Strategy File
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.04
 * \date      Created on 01 April 2012 y., 15:29
 * \TODO		
**/
#pragma	once

#include <QMessageBox>

/** \namespace ps::strategy::message  */
namespace ps {
namespace strategy {
namespace message {

/**
 * \brief The Base strategy
 * \note  Использование данной стратегии возможно лишь, только в наследовании.
**/
struct base {
    typedef	base	class_name;

    enum message_type { none = 1, critical = 2, info = 3, warning = 4 };
    
protected:
    base() {}
    ~base() {}
};

/**
	\brief Ничего не делающая стратегия.
	\note Использование данной стратегии возможно лишь, только в случаи необходимости не выводить сообщения.
 */
struct none : public base {
    typedef none	class_name;
	/**
		\brief Статическая функция показа сообщения.
		\param message - Текст сообщения.
	*/
    static void show(QString const&, base::message_type) {} 
};

/** \brief Стратегия для вывода сообщений на консоль. */
struct console : public base {
    typedef	console	class_name;

	/**
		\brief Статическая функция показа сообщения.
		\param message - Текст сообщения.
	*/
    static void show(QString const& message, base::message_type type) {
        const QString str = type == base::critical 	? " - Critical error: " :
							type == base::info 		? " - Information error: " :
													  "";
        std::wcout << str.toStdWString() << message.toStdWString() << std::endl;
    }
};

/** \brief Стратегия для вывода сообщений в окне. */
struct window : public base {
    typedef	window	class_name;

	/**
		\brief Статическая функция показа сообщения.
		\param message - Текст сообщения.
	*/
    static void show(const QString& message, base::message_type type) {
        if (type == base::critical) QMessageBox::critical(0, program::name_full(), message ,QMessageBox::Ok);
        else if (type == base::info) QMessageBox::information(0, program::name_full(), message ,QMessageBox::Ok);
	else QMessageBox::warning(0, program::name_full(), message ,QMessageBox::Ok);
    }
};

}}} // end namespace ps::strategy::message

