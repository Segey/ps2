/**
 * \file      ps2/ps2/qt/qml_exclusive_window.h
 * \brief     Base class to work with qml for only one window
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   June (the) 22(th), 2015, 15:32 MSK
 * \updated   June (the) 22(th), 2015, 15:32 MSK
 * \TODO
**/
#pragma once
#include <QUrl>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QQuickWindow>
#include <QCoreApplication>
#include <QQmlApplicationEngine>

namespace ps2 {

/**
 * \code
 *      class Calculator : public QObject, public ps2::QmlWindow<ps2::qmlexclusive_window::None> {
 *      protected:
 *          virtual void doDisconnect(QQuickWindow* window) override { }
 *          virtual void doConnect(QQuickWindow* window) override { }
 *      }
**/
namespace qmlexclusive_window {
    class None {
    public:
        typedef None class_name;
        void operator()() {
        }
    };
    class Terminate {
    public:
        typedef Terminate class_name;
        void operator()() {
            QCoreApplication::instance()->quit();
        }
    };
}

template<typename S = qmlexclusive_window::Terminate>
class QmlExclusiveWindow {
public:
    typedef S                     terminate_s;
    typedef QmlExclusiveWindow<S> class_name;

private:
    QQuickWindow*         m_window;
    QQmlApplicationEngine m_engine;

protected:
    virtual void doDisconnect(QQuickWindow* window) = 0;
    virtual void doConnect(QQuickWindow* window) = 0;

    QQuickWindow* window() {
        return m_window;
       }

public:
    QmlExclusiveWindow(QQuickWindow* window = Q_NULLPTR)
        : m_window(window){
    }
    virtual ~QmlExclusiveWindow() Q_DECL_EQ_DEFAULT;
    bool setProperty(const char* name, QVariant const& value) {
        return m_window ? m_window->setProperty(name, value) : false;
    }
    bool setProperty(const char *name, QVariant&& value) {
        return m_window ? m_window->setProperty(name, std::move(value)) : false;
    }
    QVariant property(const char *name) const {
        return m_window ? m_window->property(name) : QVariant();
    }
    void init(QUrl const& url) {
        if (m_window != nullptr) doDisconnect(m_window);
        m_engine.load(url);
        if(m_engine.rootObjects().isEmpty()) return terminate_s()();
        m_window = qobject_cast<QQuickWindow*>(m_engine.rootObjects().at(0));
        if (!m_window) return terminate_s()();
        else doConnect(m_window);
    }
};
} // end namespace ps2
