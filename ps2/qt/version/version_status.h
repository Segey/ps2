/**
 * \file      ps2/ps2/qt/version/version_status.h
 * \brief     VersionStatus class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 22(th), 2015, 19:06 MSK
 * \updated   May (the) 22(th), 2015, 19:06 MSK
 * \TODO
**/
#pragma once

/** \namespace ps2 */
namespace ps2 {
enum class VersionStatus: std::int_fast8_t {
    Invalid   = -1
    , Alpha   = 0u
    , Beta    = 1u
    , Release = 2u
    , Final   = 3u
};
} // end namespace ps2
