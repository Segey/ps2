/**
 * \file      ps2/ps2/qt/version/version.h
 * \brief     Version class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.3
 * \created   May     (the) 22(th), 2015, 19:05 MSK
 * \updated   January (the) 18(th), 2017, 18:03 MSK
 * \TODO
**/
#pragma once
#include <QString>
#include <QDebug>
#include <QStringList>
#include <ps2/qt/out.h>
#include <ps2/qt/convert/integer.h>
#include "version_status.h"

#ifdef minor
#pragma push_macro("minor")
#undef minor
#define my_temp_macro_minor
#endif

#ifdef major
#pragma push_macro("major")
#undef major
#define my_temp_macro_major
#endif

/**
 * \code
 *     auto const& version = ps2::Version::fromString(str);
 *     if(version > program::version) ;
 * \endcode
 * \brief major.minor.status.status_details
 *    1.2.1.2 instead of 1.2-b2  (beta with some bug fixes)
 *    1.2.2.3 instead of 1.2-rc3 (release candidate)
 *    1.2.3.0 instead of 1.2-r   (commercial distribution)
 *    1.2.3.5 instead of 1.2-r5  (commercial distribution with many bug fixes)
 * - status
 *  0 - alpha (status)
 *  1 - beta (status)
 *  2 - release candidate
 *  3 - (final) release
**/
/** \namespace ps2 */
namespace ps2 {

class Version {
public:
    using class_name = Version;
    using status_t   = VersionStatus;

private:
    uint     m_major = 0u;
    uint     m_minor = 0u;
    status_t m_status = status_t::Invalid;
    uint     m_status_details = 0u;

    static inline uint toUint(QString const& val) noexcept{
        return ps2::toUint(val,0u);
    }
    static status_t toStatus(QString const& value) noexcept{
        const int val = ps2::toInt(value,-1);
        if(-1 <= val && val <= 3) return static_cast<status_t>(val);
        return status_t::Invalid;
    }

public:
    Q_DECL_CONSTEXPR Version(uint major = 0u, uint minor = 0u, status_t status = status_t::Invalid, uint status_details = 0u) noexcept
        : m_major(major)
        , m_minor(minor)
        , m_status(status)
        , m_status_details(status_details) {
    }
    Q_DECL_CONSTEXPR static inline Version Final(uint major = 0u, uint minor = 0u) noexcept {
        return Version(major, minor, status_t::Final, 0u);
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.major()             == rhs.major()
                && lhs.minor()         == rhs.minor()
                && lhs.status()        == rhs.status()
                && lhs.statusDetails() == rhs.statusDetails();
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs == rhs);
    }
    friend bool operator> (class_name const& lhs, class_name const& rhs) noexcept {
        if(lhs.major()  != rhs.major()) return lhs.major()   > rhs.major();
        if(lhs.minor()  != rhs.minor()) return lhs.minor()   > rhs.minor();
        if(lhs.status() != rhs.status()) return lhs.status() > rhs.status();
        return lhs.statusDetails() > rhs.statusDetails();
    }
    friend bool operator< (class_name const& lhs, class_name const& rhs) noexcept {
        return rhs > lhs;
    }
    friend bool operator>= (class_name const& lhs, class_name const& rhs) noexcept {
        return lhs > rhs
                || rhs == lhs;
    }
    friend bool operator<= (class_name const& lhs, class_name const& rhs) noexcept {
        return lhs < rhs
                || rhs == lhs;
    }
    Q_DECL_CONSTEXPR bool isNull() const noexcept {
        return m_major != 0u
               && m_minor != 0u
               && m_status != status_t::Invalid
               && m_status_details != 0u;
    }
    Q_DECL_CONSTEXPR bool isValid() const noexcept {
        return m_major != 0u
               && m_status != status_t::Invalid;
    }
    Q_DECL_CONSTEXPR void setMajor(uint major) noexcept{
        m_major = major;
    }
    Q_DECL_CONSTEXPR uint major() const noexcept{
        return m_major;
    }
    Q_DECL_CONSTEXPR void setMinor(uint minor) noexcept{
        m_minor = minor;
    }
    Q_DECL_CONSTEXPR uint minor() const noexcept{
        return m_minor;
    }
    Q_DECL_CONSTEXPR void setStatus(status_t status) noexcept{
        m_status = status;
    }
    Q_DECL_CONSTEXPR status_t status() const noexcept{
        return m_status;
    }
    Q_DECL_CONSTEXPR void setStatusDetails(uint status_details) noexcept{
        m_status_details = status_details;
    }
    Q_DECL_CONSTEXPR uint statusDetails() const noexcept{
        return m_status_details;
    }
    static QString toString(class_name const& cls) noexcept{
        return QStringLiteral("%1.%2.%3.%4")
                    .arg(cls.major())
                    .arg(cls.minor())
                    .arg(static_cast<uint>(cls.status()))
                    .arg(cls.statusDetails());
    }
    static Version fromString(QString const& val) noexcept{
        class_name ver (val.toUInt(), 0u, status_t::Final, 0u);

        QStringList const& list = val.split(QChar::fromLatin1('.'));
        if(list.size() > 0) ver.setMajor(toUint(list[0]));
        if(list.size() > 1) ver.setMinor(toUint(list[1]));
        if(list.size() > 2) ver.setStatus(toStatus(list[2]));
        if(list.size() > 3) ver.setStatusDetails(toUint(list[3]));
        return ver;
    }
};

inline QDebug operator<<(QDebug dbg, Version const& version)
{
    return dbg.nospace() << "\"Version\": { \"value\": \""
                  << version.major() << "."
                  << version.minor() << "."
                  << static_cast<uint>(version.status()) << "."
                  << version.statusDetails()
                  << "\"}";
}
inline std::ostream& operator<<(std::ostream& out, Version const& version) noexcept {
    return ps2::cout(out, version);
}

#ifdef my_temp_macro_minor
#pragma pop_macro("minor")
#undef my_temp_macro_minor
#endif

#ifdef my_temp_macro_major
#pragma pop_macro("major")
#undef my_temp_macro_major
#endif

} // end namespace ps2
