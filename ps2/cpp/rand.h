/**
 * \file      ps2/ps2/cpp/rand.h
 * \brief     The File is used to describe the parameters necessary for randoming data.
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \date      Created on 09 April 2014 y., 14:39
 * \TODO
 * \note      You need to create seed for the random.  That is needed only once on application startup. Usually in main function 
 *                srand (time(nullptr));
**/
#pragma once
#include <stdlig.h>
#include <time.h>

/** namespace ps */
namespace ps {

/**
 * \brief Random number between low and high
 * \code
 *    auto num = ps::randInt(3, 5);
 * \endcode
**/
static unsigned randInt(unsigned low, unsigned high) {
    return rand() % ((high + 1) - low) + low;
}
/**
 * \brief Random number between 0 and high
 * \code
 *    auto num = ps::randInt(5);
 * \endcode
**/
static unsigned randInt(unsigned high) {
    return randInt(0, high);
}
/**
 * \brief Random value ether true or false
 * \code
 *    auto val = ps::randBool();
 * \endcode
**/
static bool randBool() noexcept {
    return static_cast<bool>(randInt(0, 1));
}
/**
 * \brief Random number between low and high with an hole
 * \code
 *    auto val = ps::randIntWithHole(0,7, index++);
 * \endcode
**/
static unsigned randIntWithHole(unsigned low, unsigned high, unsigned empty) noexcept {
    auto val = randInt(low, high - 1);
    return val >= empty ? val + 1 : val;
}
/**
 * \brief Random number between low and high with a container of holes
 * \code
 *    auto val = ps::randIntWithHole(0,7, index++);
 * \endcode
**/
template<class Con>
static unsigned randIntWithHoles(unsigned low, unsigned high, Con const& empties) {
    for(int i = 0; i !=100; ++i) {
        auto val = randInt(low, high);
        auto it = std::find(empties.begin(), empties.end(), val);
        if(it == empties.end()) return val;
    }
    return low;
}

} // end namespace ps
