/**
 * \file      ps2/ps2/cpp/iterator/const_iterator.h
 * \brief     Typedefs
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   July (the) 06(th), 2016, 16:02 MSK
 * \updated   July (the) 06(th), 2016, 16:02 MSK
 * \TODO
**/
#pragma once
#include <functional>

/* \namespace ps2 */
namespace  ps2 {

template<class I>
class ConstIterator {
public:
    using class_name     = ConstIterator;
    using const_iterator = I;

private:
    const_iterator m_begin;
    const_iterator m_end;

public:
    constexpr ConstIterator(const_iterator begin, const_iterator end) noexcept
        : m_begin(begin)
        , m_end(end) {
    }
    constexpr const_iterator begin() const noexcept {
        return m_begin;
    }
    constexpr const_iterator end() const noexcept {
        return m_end;
    }
    constexpr const_iterator cbegin() const noexcept {
        return m_begin;
    }
    constexpr const_iterator cend() const noexcept {
        return m_end;
    }
    constexpr auto size() const noexcept {
        return std::distance(m_begin, m_end);
    }
    void swap(class_name& rhs) noexcept {
        std::swap(m_begin, rhs.m_begin);
        std::swap(m_end, rhs.m_end);
    }
};

template<class T>
constexpr static inline void swap(ConstIterator<T>& lhs, ConstIterator<T>& rhs) noexcept {
    lhs.swap(rhs);
}

} // namespace ps2
