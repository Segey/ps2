/**
 * \file      ps2/ps2/cpp/iterator/iterator.h
 * \brief     Typedefs
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   July (the) 06(th), 2016, 16:02 MSK
 * \updated   July (the) 06(th), 2016, 16:02 MSK
 * \TODO
**/
#pragma once

/* namespace ps2 */
namespace  ps2 {

template<typename I>
class Iterator {
public:
    using class_name = Iterator;
    using iterator   = I;

private:
    iterator m_begin;
    iterator m_end;

public:
    Iterator(iterator begin, iterator end) noexcept
        : m_begin(begin)
        , m_end(end) {
    }
    iterator begin() noexcept {
        return m_begin;
    }
    iterator end() noexcept {
        return m_end;
    }
};
} // namespace ps2
