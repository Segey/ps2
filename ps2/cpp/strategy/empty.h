/**
 * \file      ps2/ps2/cpp/strategy/empty.h
 * \brief     EmptyStrategy class supports simple behaviour
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   June (the) 17(th), 2015, 19:24 MSK
 * \updated   June (the) 17(th), 2015, 19:24 MSK
 * \TODO
**/
#pragma once
/** \namespace ps2 */
namespace ps2 {

/**
 * \code
 *      Strategy<ps2::EmptyStrategy<void>> s;
 *      s();
 * \endcode
**/
template <typename R = void>
class EmptyStrategy {
public:
    typedef R                return_t;
    typedef EmptyStrategy<R> class_name;

public:
    return_t operator()() {
        return return_t();
    }
};
typedef EmptyStrategy<void> VoidEmptyStrategy;

} // end namespace ps2
