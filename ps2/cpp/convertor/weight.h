/**
 * \file      ps2/ps2/cpp/convertor/weight.h
 * \brief     The Weight class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 21:28 MSK
 * \updated   April     (the) 06(th), 2017, 21:28 MSK
 * \TODO      
**/
#pragma once

/** \namespace ps2 */
namespace ps2 {

class WeightConvertor final {
public:
    using class_name = WeightConvertor;

public:
    constexpr static const int t_gr  = 1'000'000;
    constexpr static const int t_kg  = 1000;
    constexpr static const int kg_gr = 1000;
    constexpr static const int gr_mg = 1000;
    constexpr static const int st_oz = 224;
    constexpr static const int st_lb = 14;
    constexpr static const int lb_oz = 16;
    constexpr static const long double oz_gr = .03527396195;
    constexpr static const long double gr_oz = 28.34952313;
};

} // end namespace ps2
