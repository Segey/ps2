/**
 * \file      ps2/ps2/cpp/convertor/length.h
 * \brief     The Length class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 06(th), 2017, 18:55 MSK
 * \updated   April     (the) 06(th), 2017, 18:55 MSK
 * \TODO      
**/
#pragma once
/** \namespace ps2 */
namespace ps2 {

class LengthConvertor final {
public:
    using class_name = LengthConvertor;

public:
    constexpr static const int km_mm = 1'000'000;
    constexpr static const int km_m  = 1'000;
    constexpr static const int km_cm = 100'000;
    constexpr static const int m_mm = 1000;
    constexpr static const int m_cm = 100;
    constexpr static const int cm_mm = 10;

    constexpr static const int mi_subin = 6'336'000;
    constexpr static const int mi_yd    = 1760;
    constexpr static const int yd_subin = 3600;
    constexpr static const int yd_ft    = 3;
    constexpr static const int ft_in    = 12;
    constexpr static const int yd_in    = 36;
    constexpr static const int mi_in    = 63360;
    constexpr static const int ft_subin = 1200;
    constexpr static const int in_subin = 100;
    constexpr static const long double mm_in = 25.4;
};

} // end namespace ps2
