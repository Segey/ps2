/**
 * \file      ps2/ps2/cpp/bitwise.h
 * \brief     The class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April (the) 23(th), 2016, 15:39 MSK
 * \updated   April (the) 23(th), 2016, 15:39 MSK
 * \TODO      
**/
#pragma once

/** \namespace ps2 */
namespace ps2 {

constexpr static inline bool isPowerOfTwo(std::uint_fast32_t x) noexcept {
    while (((x & 1) == 0) && x > 1)
       x >>= 1;
     return x == 1;
}

} // end namespace ps2

