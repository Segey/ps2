/**
 * \file      ps2/ps2/cpp/string_algo.h
 * \brief     Work with double
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.4
 * \created   May (the) 17(th), 2013, 14:31 MSK
 * \updated   May (the) 12(th), 2015, 16:16 MSK
 * \TODO
**/
#ifndef StringAlgoH
#define StringAlgoH

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

/** namespace ps2 */
namespace ps2 {

//!< trim from start
template<class T>
inline std::basic_string<T> ltrim(std::basic_string<T> const& str) {
    std::basic_string<T> s = str;
    s.erase(0, str.find_first_not_of(' '));
    return s;
}

//!< trim from end
template<class T>
inline std::basic_string<T> rtrim(std::basic_string<T> const& str) {
    std::basic_string<T> s = str;
    s.erase(str.find_last_not_of(' ')+1);
    return s;
}

/**
 * \brief Trims from both ends
 * \code
 *    auto const& str = ps2::trim(std::string("Hello"));
 * \endcode
**/
template<class T>
inline std::basic_string<T> trim(std::basic_string<T> const& str) {
    return ltrim(rtrim(str));
}

/**
 * \brief Adds a char to left side of a given string with the given size
 * \code
 *    auto const& str
 *       = ps2::leftJustified(std::string("Hello"), 9, '0'); // 000Hello
 *
 * \endcode
**/
#ifdef CPPBUILDER2007
#define noexcept
#else
template<class T>
inline auto leftJustified(std::basic_string<T> const& str
        , decltype(std::declval<std::basic_string<T>>().size()) size, T ch) noexcept -> std::basic_string<T> {
    if(size <= str.size()) return str;

    auto const len = size - str.size();
    std::basic_string<T> res;
    for(decltype(size) i = 0 ; i != len; ++i)
        res += ch;

    return res + str;
}
#endif

/**
 * \brief to UpperCase
 * \code
 *    auto str = ps2::toUpper(std::string("Hello"));
 * \endcode
**/
template<class T>
inline std::basic_string<T> toUpper(std::basic_string<T> const& s) noexcept {
    using char_t = typename std::basic_string<T>::value_type ;
    std::basic_string<T> result;
    std::transform(s.begin(), s.end(), std::back_inserter(result), [=] (char_t ch) -> char_t {
            return std::use_facet< std::ctype< char_t > >( std::locale() ).toupper(ch);
    });
    return result;
}
/**
 * \brief to LowerCase
 * \code
 *    auto str = ps2::toLower(std::string("Hello"));
 * \endcode
**/
template<class T>
inline std::basic_string<T> toLower(std::basic_string<T> const& s) noexcept {
    using char_t = typename std::basic_string<T>::value_type ;
    std::basic_string<T> result;
    std::transform(s.begin(), s.end(), std::back_inserter(result), [=] (char_t ch) -> char_t {
        return std::use_facet< std::ctype< char_t > >( std::locale() ).tolower(ch);
    });
    return result;
}

} // namespace ps2
#endif
