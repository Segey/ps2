/**
 * \file      /home/dix/projects/perfect/projects/ps2/ps2/cpp/impl/decimal/decimal_impl.h 
 * \brief     The Decimal_impl class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 04(th), 2017, 12:01 MSK
 * \updated   June      (the) 04(th), 2017, 12:01 MSK
 * \TODO      
**/
#pragma once
#include <array>
#include <cstring>
#include <ps2/cpp/typedefs.h>
#include <ps2/cpp/wide_int.h>

namespace std {

// numeric limits
template <class T, size_t S>
class numeric_limits<ps2::decimal<T, S>> {
public:
    using decimal_t  = ps2::decimal<T, S>;
    using class_name = numeric_limits<decimal_t>;

public:
    static  constexpr bool is_specialized = true;
    static  constexpr bool is_signed = std::numeric_limits<T>::is_signed;
    static  constexpr bool is_integer = true;
    static  constexpr bool is_exact = true;
    static  constexpr bool has_infinity = false;
    static  constexpr bool has_quiet_NaN = false;
    static  constexpr bool has_signaling_NaN = true;
    static  constexpr std::float_denorm_style has_denorm = std::denorm_absent;
    static  constexpr bool has_denorm_loss = false;
    static  constexpr std::float_round_style round_style = std::round_toward_zero;
    static  constexpr bool is_iec559 = false;
    static  constexpr bool is_bounded = true;
    static  constexpr bool is_modulo = true;
    static  constexpr int digits = std::numeric_limits<T>::digits;
    static  constexpr int digits10 = digits * 0.30103 /*std::log10(2)*/;
    static  constexpr int max_digits10 = 0;
    static  constexpr int radix = 2;
    static  constexpr int min_exponent = 0;
    static  constexpr int min_exponent10 = 0;
    static  constexpr int max_exponent = 0;
    static  constexpr int max_exponent10 = 0;
    static  constexpr bool traps = true;
    static  constexpr bool tinyness_before = false;

    static decimal_t min() noexcept {
        return std::numeric_limits<T>::min();
    }
    static decimal_t lowest() noexcept {
        return min();
    }
    static decimal_t max() noexcept {
        return std::numeric_limits<T>::max();
    }
    constexpr static decimal_t epsilon() noexcept {
        return 0;
    }
    constexpr static decimal_t round_error() noexcept {
        return 0;
    }
    constexpr static decimal_t infinity() noexcept {
        return 0;
    }
    constexpr static decimal_t quiet_NaN() noexcept {
        return 0;
    }
    constexpr static decimal_t signaling_NaN() noexcept {
        return 0;
    }
    constexpr static decimal_t denorm_min() noexcept {
        return 0;
    }
};


} // namespace std
