/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/wide_int_impl.h 
 * \brief     The Wide_int_impl class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 01(th), 2017, 17:18 MSK
 * \updated   June      (the) 01(th), 2017, 17:18 MSK
 * \TODO      
**/
#pragma once
#include <array>
#include <cstring>
#include <ps2/cpp/typedefs.h>
#include <ps2/cpp/wide_int.h>

namespace std {

// numeric limits
template <size_t M, wide_integer_s Signed>
class numeric_limits<wide_integer<M, Signed>> {
public:
    using wide_t     = wide_integer<M, Signed>;
    using class_name = numeric_limits<wide_t>;

public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = Signed == wide_integer_s::Signed;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = true;
    static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr std::float_round_style round_style = std::round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(long) * M - (Signed == wide_integer_s::Signed ? 1 : 0);
    static constexpr int digits10 = digits * 0.30103 /*std::log10(2)*/;
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static wide_t  min() noexcept {
        if (Signed == wide_integer_s::Signed) {
            wide_integer<M, wide_integer_s::Signed> res{};
            res.m_arr[0] = std::numeric_limits<typename wide_integer<M, Signed>::signed_base_type>::min();
            return res;
        } else
            return 0;
    }
    static wide_t lowest() noexcept {
        return min();
    }
    static wide_t max() noexcept {
        wide_t res{};
        res.m_arr[0] = Signed == wide_integer_s::Signed
               ? std::numeric_limits<typename wide_integer<M, Signed>::signed_base_type>::max()
               : std::numeric_limits<typename wide_integer<M, Signed>::base_type>::max();
        for (auto i = 1; i < wide_t::_impl::arr_size; ++i)
            res.m_arr[i] = std::numeric_limits<typename wide_integer<M, Signed>::base_type>::max();

        return res;
    }
    constexpr static wide_t epsilon() noexcept {
        return 0;
    }
    constexpr static wide_t round_error() noexcept {
        return 0;
    }
    constexpr static wide_t infinity() noexcept {
        return 0;
    }
    constexpr static wide_t quiet_NaN() noexcept {
        return 0;
    }
    constexpr static wide_t signaling_NaN() noexcept {
        return 0;
    }
    constexpr static wide_t denorm_min() noexcept {
        return 0;
    }
};

/** \namespace detail */
namespace detail {
template <class T>
constexpr bool valid_specialized_numeric_limits(std::false_type /*is_array*/) {
    return std::numeric_limits<T>::is_specialized;
}
template <class T>
constexpr bool valid_specialized_numeric_limits(std::true_type /*is_array*/) {
    return false;
}
} // end namespace detail

template <typename T>
static bool ArithmeticConcept() noexcept {
    return std::detail::valid_specialized_numeric_limits<T>(std::is_array<T>());
}

namespace detail {
template <class T>
bool valid_specialized_numeric_limits_and_integral(std::false_type /*is_array*/) {
    return std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer;
}
template <class T>
bool valid_specialized_numeric_limits_and_integral(std::true_type /*is_array*/) {
    return false;
}
}

template <class T>
static bool IntegralConcept() noexcept {
    return std::detail::valid_specialized_numeric_limits_and_integral<T>(std::is_array<T>());
}

// type traits
template <size_t M, wide_integer_s Signed, size_t M2, wide_integer_s S2>
struct common_type<wide_integer<M, Signed>, wide_integer<M2, S2>> {
    using type = std::conditional_t < M == M2,
          wide_integer<M,
                       (Signed == wide_integer_s::Signed && S2 == wide_integer_s::Signed)
                           ? wide_integer_s::Signed
                           : wide_integer_s::Unsigned>,
          std::conditional_t<
              M2<M,
                            wide_integer<M, Signed>,
                            wide_integer<M2, S2>>>;
};

template <size_t M, wide_integer_s Signed, class Arithmetic>
struct common_type<wide_integer<M, Signed>, Arithmetic> {
    static_assert(ArithmeticConcept<Arithmetic>(), "");

    using type = std::conditional_t <
                 std::is_floating_point<Arithmetic>::value,
          Arithmetic,
          std::conditional_t<
              sizeof(Arithmetic) < M,
              wide_integer<M, Signed>,
              std::conditional_t<
                  M<sizeof(Arithmetic),
                               Arithmetic,
                               std::conditional_t<
                                   M == sizeof(Arithmetic) && (Signed == wide_integer_s::Signed || std::is_signed<Arithmetic>::value),
                                   Arithmetic,
                                   wide_integer<M, Signed>>>>>;
};

template <class Arithmetic, size_t M, wide_integer_s Signed>
struct common_type<Arithmetic, wide_integer<M, Signed>>
    : std::common_type<wide_integer<M, Signed>, Arithmetic> {};

template <size_t M, wide_integer_s Signed>
struct wide_integer<M, Signed>::_impl {
    // utils
    static const int arr_size = sizeof(long) * M;
    static const int base_bits = sizeof(base_type) * CHAR_BIT;

    template <size_t B, wide_integer_s S>
    static bool is_negative(const wide_integer<B, S>& n) noexcept {
        (void)n;
        return S == wide_integer_s::Signed && static_cast<signed_base_type>(n.m_arr[0]) < 0;
    }
    template <size_t B, wide_integer_s S>
    constexpr static wide_integer<B, S> make_positive(const wide_integer<B, S>& n) noexcept {
        return is_negative(n) ? operator_unary_minus(n) : n;
    }
    template <class T, class = typename std::enable_if<std::is_signed<T>::value, T>::type>
    constexpr static int64_t to_Integral(T f) noexcept {
        return static_cast<int64_t>(f);
    }
    template <class T, class = typename std::enable_if<!std::is_signed<T>::value, T>::type>
    constexpr static uint64_t to_Integral(T f) noexcept {
        return static_cast<uint64_t>(f);
    }
    template <class Integral>
    CONSTEXPR static void wide_integer_from_Integral(wide_integer<M, Signed>& self, Integral rhs) noexcept {
        int r_idx = 0;

        for (; static_cast<size_t>(r_idx) < sizeof(Integral) && r_idx < arr_size; ++r_idx) {
            base_type& curr = self.m_arr[arr_size - 1 - r_idx];
            base_type curr_rhs = (rhs >> (r_idx * CHAR_BIT)) & std::numeric_limits<base_type>::max();
            curr = curr_rhs;
        }

        for (; r_idx < arr_size; ++r_idx) {
            base_type& curr = self.m_arr[arr_size - 1 - r_idx];
            curr = rhs < 0 ? std::numeric_limits<base_type>::max() : 0;
        }
    }

    template <size_t M2, wide_integer_s S2>
    static void wide_integer_from_wide_integer(wide_integer<M, Signed>& self, const wide_integer<M2, S2>& rhs) noexcept {
        //        int M_to_copy = std::min(arr_size, rhs.arr_size);
        auto rhs_arr_size = wide_integer<M2, S2>::_impl::arr_size;
        int base_elems_to_copy = _impl::arr_size < rhs_arr_size ? _impl::arr_size
                                                                : rhs_arr_size;
        for (int i = 0; i < base_elems_to_copy; ++i) {
            self.m_arr[_impl::arr_size - 1 - i] = rhs.m_arr[rhs_arr_size - 1 - i];
        }
        for (int i = 0; i < arr_size - base_elems_to_copy; ++i) {
            self.m_arr[i] = is_negative(rhs) ? std::numeric_limits<base_type>::max() : 0;
        }
    }

    template <class T>
    using __keep_size = typename std::enable_if<sizeof(T) <= arr_size, wide_integer<M, Signed>>::type;
    template <size_t M2, wide_integer_s S2>
    using __need_increase_size = typename std::enable_if < M<M2, wide_integer<M2, Signed>>::type;

    static wide_integer<M, wide_integer_s::Unsigned> shift_left(const wide_integer<M, wide_integer_s::Unsigned>& rhs, int n) {
        if (static_cast<size_t>(n) >= base_bits * arr_size)
            return 0;
        if (n <= 0)
            return rhs;

        wide_integer<M, Signed> lhs = rhs;
        int cur_shift = n % base_bits;
        if (cur_shift) {
            lhs.m_arr[0] <<= cur_shift;
            for (int i = 1; i < arr_size; ++i) {
                lhs.m_arr[i - 1] |= lhs.m_arr[i] >> (base_bits - cur_shift);
                lhs.m_arr[i] <<= cur_shift;
            }
            n -= cur_shift;
        }
        if (n) {
            int i = 0;
            for (; i < arr_size - n / base_bits; ++i) {
                lhs.m_arr[i] = lhs.m_arr[i + n / base_bits];
            }
            for (; i < arr_size; ++i) {
                lhs.m_arr[i] = 0;
            }
        }
        return lhs;
    }

    static wide_integer<M, wide_integer_s::Signed> shift_left(const wide_integer<M, wide_integer_s::Signed>& rhs, int n) {
        // static_assert(is_negative(rhs), "shift left for negative lhsbers is underfined!");
        if (is_negative(rhs)) {
            throw std::runtime_error("shift left for negative lhsbers is underfined!");
        }
        return wide_integer<M, wide_integer_s::Signed>(shift_left(wide_integer<M, wide_integer_s::Unsigned>(rhs), n));
    }

    static wide_integer<M, wide_integer_s::Unsigned> shift_right(const wide_integer<M, wide_integer_s::Unsigned>& rhs, int n) noexcept {
        if (static_cast<size_t>(n) >= base_bits * arr_size)
            return 0;
        if (n <= 0)
            return rhs;

        wide_integer<M, Signed> lhs = rhs;
        int cur_shift = n % base_bits;
        if (cur_shift) {
            lhs.m_arr[arr_size - 1] >>= cur_shift;
            for (int i = arr_size - 2; i >= 0; --i) {
                lhs.m_arr[i + 1] |= lhs.m_arr[i] << (base_bits - cur_shift);
                lhs.m_arr[i] >>= cur_shift;
            }
            n -= cur_shift;
        }
        if (n) {
            int i = arr_size - 1;
            for (; i >= static_cast<int>(n / base_bits); --i) {
                lhs.m_arr[i] = lhs.m_arr[i - n / base_bits];
            }
            for (; i >= 0; --i) {
                lhs.m_arr[i] = 0;
            }
        }
        return lhs;
    }

    static wide_integer<M, wide_integer_s::Signed> shift_right(const wide_integer<M, wide_integer_s::Signed>& rhs, int n) noexcept {
        if (static_cast<size_t>(n) >= base_bits * arr_size)
            return 0;
        if (n <= 0)
            return rhs;

        bool is_neg = is_negative(rhs);
        if (!is_neg) {
            return shift_right(wide_integer<M, wide_integer_s::Unsigned>(rhs), n);
        }

        wide_integer<M, Signed> lhs = rhs;
        int cur_shift = n % base_bits;
        if (cur_shift) {
            lhs = shift_right(wide_integer<M, wide_integer_s::Unsigned>(lhs), cur_shift);
            lhs.m_arr[0] |= std::numeric_limits<base_type>::max() << (base_bits - cur_shift);
            n -= cur_shift;
        }
        if (n) {
            int i = arr_size - 1;
            for (; i >= static_cast<int>(n / base_bits); --i) {
                lhs.m_arr[i] = lhs.m_arr[i - n / base_bits];
            }
            for (; i >= 0; --i) {
                lhs.m_arr[i] = std::numeric_limits<base_type>::max();
            }
        }
        return lhs;
    }

    template <class T>
    static wide_integer<M, Signed> operator_plus_T(const wide_integer<M, Signed>& lhs, T rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        return rhs < 0 ? _operator_minus_T(lhs, -rhs)
                       :  _operator_plus_T(lhs, rhs);
    }

private:
    template <class T>
    static wide_integer<M, Signed> _operator_minus_T(const wide_integer<M, Signed>& lhs, T rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> res = lhs;

        bool is_underflow = false;
        int r_idx = 0;
        for (; static_cast<size_t>(r_idx) < sizeof(T) && r_idx < arr_size; ++r_idx) {
            base_type& res_i = res.m_arr[arr_size - 1 - r_idx];
            base_type curr_rhs = (rhs >> (r_idx * CHAR_BIT)) & std::numeric_limits<base_type>::max();

            if (is_underflow) {
                --res_i;
                is_underflow = res_i == std::numeric_limits<base_type>::max();
            }

            if (res_i < curr_rhs)
                is_underflow = true;
            res_i -= curr_rhs;
        }

        if (is_underflow && r_idx < arr_size) {
            --res.m_arr[arr_size - 1 - r_idx];
            for (int i = arr_size - 1 - r_idx - 1; i >= 0; --i) {
                if (res.m_arr[i + 1] == std::numeric_limits<base_type>::max()) {
                    --res.m_arr[i];
                } else
                    break;

            }
        }

        return res;
    }

    template <class T>
    static wide_integer<M, Signed> _operator_plus_T(const wide_integer<M, Signed>& lhs, T rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> res = lhs;

        bool is_overflow = false;
        int r_idx = 0;
        for (; static_cast<size_t>(r_idx) < sizeof(T) && r_idx < arr_size; ++r_idx) {
            base_type& res_i = res.m_arr[arr_size - 1 - r_idx];
            base_type curr_rhs = (rhs >> (r_idx * CHAR_BIT)) & std::numeric_limits<base_type>::max();

            if (is_overflow) {
                ++res_i;
                is_overflow = res_i == 0;
            }

            res_i += curr_rhs;
            if (res_i < curr_rhs) {
                is_overflow = true;
            }
        }

        if (is_overflow && r_idx < arr_size) {
            ++res.m_arr[arr_size - 1 - r_idx];
            for (int i = arr_size - 1 - r_idx - 1; i >= 0; --i) {
                if (res.m_arr[i + 1] == 0) {
                    ++res.m_arr[i];
                } else {
                    break;
                }
            }
        }

        return res;
    }

public:
    static wide_integer<M, Signed> operator_unary_tilda(const wide_integer<M, Signed>& lhs) noexcept {
        wide_integer<M, Signed> res{};
        for (int i = 0; i < arr_size; ++i) {
            res.m_arr[i] = ~lhs.m_arr[i];
        }
        return res;
    }

    static wide_integer<M, Signed> operator_unary_minus(const wide_integer<M, Signed>& lhs) noexcept(Signed == wide_integer_s::Unsigned) {
        return operator_plus_T(operator_unary_tilda(lhs), 1);
    }

    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_plus(const wide_integer<M, Signed>& lhs,
                                                                      const T& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> t = rhs;
        if (is_negative(t)) {
            return _operator_minus_wide_integer(lhs, operator_unary_minus(t));
        } else {
            return _operator_plus_wide_integer(lhs, t);
        }
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, Signed> operator_plus(const wide_integer<M, Signed>& lhs,
                                                                       const wide_integer<M2, S2>& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, S2>>::_impl::operator_plus(wide_integer<M2, Signed>(lhs), rhs);
    }

    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_minus(const wide_integer<M, Signed>& lhs,
                                                                       const T& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> t = rhs;
        if (is_negative(t)) {
            return _operator_plus_wide_integer(lhs, operator_unary_minus(t));
        } else {
            return _operator_minus_wide_integer(lhs, t);
        }
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, Signed> operator_minus(const wide_integer<M, Signed>& lhs,
                                                                        const wide_integer<M2, S2>& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, S2>>::_impl::operator_minus(wide_integer<M2, Signed>(lhs), rhs);
    }

private:
    static wide_integer<M, Signed> _operator_minus_wide_integer(const wide_integer<M, Signed>& lhs,
                                                                                     const wide_integer<M, Signed>& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> res = lhs;

        bool is_underflow = false;
        for (int idx = arr_size - 1; idx >= 0; --idx) {
            base_type& res_i = res.m_arr[idx];
            const base_type rhs_i = rhs.m_arr[idx];

            if (is_underflow) {
                --res_i;
                is_underflow = res_i == std::numeric_limits<base_type>::max();
            }

            if (res_i < rhs_i) {
                is_underflow = true;
            }
            res_i -= rhs_i;
        }

        return res;
    }
    static wide_integer<M, Signed> _operator_plus_wide_integer(const wide_integer<M, Signed>& lhs,
                                                                                    const wide_integer<M, Signed>& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
        wide_integer<M, Signed> res = lhs;

        bool is_overflow = false;
        for (int idx = arr_size - 1; idx >= 0; --idx) {
            base_type& res_i = res.m_arr[idx];
            const base_type rhs_i = rhs.m_arr[idx];

            if (is_overflow) {
                ++res_i;
                is_overflow = res_i == 0;
            }

            res_i += rhs_i;
            if (res_i < rhs_i) {
                is_overflow = true;
            }
        }

        return res;
    }

public:
    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed>
        operator_star(wide_integer<M, Signed> const& lhs,
                                                                      const T& rhs) {
        std::decay_t<decltype(lhs)> a = make_positive(lhs);
        decltype(a) t = make_positive(wide_integer<M, Signed>(rhs));
        decltype(a) res = 0;

        for (size_t i = 0; i < arr_size; ++i) {
            if ((t.m_arr[arr_size - 1] & 1) != 0) {
                res = operator_plus(res, (shift_left(a, int(i))));
            }

            t = shift_right(t, 1);
        }

        if (Signed == wide_integer_s::Signed &&
            is_negative(wide_integer<M, Signed>(rhs)) != is_negative(lhs)) {
            res = operator_unary_minus(res);
        }

        return res;
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, S2> operator_star(const wide_integer<M, Signed>& lhs,
                                                                        const wide_integer<M2, S2>& rhs) {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, S2>>::_impl::operator_star(wide_integer<M2, S2>(lhs), rhs);
    }

    template <typename T, class = __keep_size<T>>
    static bool operator_more(const wide_integer<M, Signed>& lhs, const T& rhs) noexcept {
        //        static_assert(Signed == std::is_signed<T>::value,
        //                      "warning: operator_more: comparison of integers of different signs");

        wide_integer<M, Signed> t = rhs;

        if (std::numeric_limits<T>::is_signed && (is_negative(lhs) != is_negative(t)))
            return is_negative(t);

        for (int i = 0; i < arr_size; ++i) {
            if (lhs.m_arr[i] != t.m_arr[i]) {
                return lhs.m_arr[i] > t.m_arr[i];
            }
        }

        return false;
    }

    template <size_t M2, class = __need_increase_size<M2, Signed>>
    static bool operator_more(const wide_integer<M, Signed>& lhs,
                                        const wide_integer<M2, Signed>& rhs) noexcept {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::_impl::operator_more(wide_integer<M2, Signed>(lhs), rhs);
    }

    template <typename T, class = __keep_size<T>>
    static bool operator_less(const wide_integer<M, Signed>& lhs,
                                        const T& rhs) noexcept {
        //        static_assert(Signed == std::is_signed<T>::value,
        //                      "warning: operator_less: comparison of integers of different signs");

        wide_integer<M, Signed> t = rhs;

        if (std::numeric_limits<T>::is_signed && (is_negative(lhs) != is_negative(t))) {
            return is_negative(lhs);
        }

        for (int i = 0; i < arr_size; ++i) {
            if (lhs.m_arr[i] != t.m_arr[i]) {
                return lhs.m_arr[i] < t.m_arr[i];
            }
        }

        return false;
    }

    template <size_t M2, class = __need_increase_size<M2, Signed>>
    static bool operator_less(const wide_integer<M, Signed>& lhs,
                                        const wide_integer<M2, Signed>& rhs) noexcept {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::_impl::operator_less(wide_integer<M2, Signed>(lhs), rhs);
    }

    template <class T, class = __keep_size<T>>
    static bool operator_eq(const wide_integer<M, Signed>& lhs, const T& rhs) noexcept {
        wide_integer<M, Signed> t = rhs;

        for (int i = 0; i < arr_size; ++i) {
            if (lhs.m_arr[i] != t.m_arr[i]) {
                return false;
            }
        }
        return true;
    }

    template <size_t M2, class = __need_increase_size<M2, Signed>>
    static bool operator_eq(const wide_integer<M, Signed>& lhs,
                                      const wide_integer<M2, Signed>& rhs) noexcept {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::_impl::operator_eq(wide_integer<M2, Signed>(lhs), rhs);
    }

    template <typename T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_pipe(const wide_integer<M, Signed>& lhs,
                                                                      const T& rhs) noexcept {
        wide_integer<M, Signed> t = rhs;
        wide_integer<M, Signed> res = lhs;

        for (int i = 0; i < arr_size; ++i) {
            res.m_arr[i] |= t.m_arr[i];
        }

        return res;
    }

    template <size_t M2, class = __need_increase_size<M2, Signed>>
    static wide_integer<M2, Signed> operator_pipe(const wide_integer<M, Signed>& lhs,
                                                                       const wide_integer<M2, Signed>& rhs) noexcept {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::_impl::operator_pipe(wide_integer<M2, Signed>(lhs), rhs);
    }

    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_amp(const wide_integer<M, Signed>& lhs,
                                                                     const T& rhs) noexcept {
        wide_integer<M, Signed> t = rhs;
        wide_integer<M, Signed> res = lhs;

        for (int i = 0; i < arr_size; ++i) {
            res.m_arr[i] &= t.m_arr[i];
        }

        return res;
    }

    template <size_t M2, class = __need_increase_size<M2, Signed>>
    static wide_integer<M2, Signed> operator_amp(const wide_integer<M, Signed>& lhs,
                                                                      const wide_integer<M2, Signed>& rhs) noexcept {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::_impl::operator_amp(wide_integer<M2, Signed>(lhs), rhs);
    }

private:
    template <class T>
    static void divide(const T& lhserator, const T& denominator, T& quotient, T& remainder) {
        bool is_zero = true;
        for (auto c : denominator.m_arr) {
            if (c != 0) {
                is_zero = false;
                break;
            }
        }

        if (is_zero)
            throw std::domain_error("divide by zero");

        T n = lhserator;
        T d = denominator;
        T x = 1;
        T answer = 0;

        while (!operator_more(d, n) && operator_eq(operator_amp(shift_right(d, base_bits * arr_size - 1), 1), 0)) {
            x = shift_left(x, 1);
            d = shift_left(d, 1);
        }

        while (!operator_eq(x, 0)) {
            if (!operator_more(d, n)) {
                n = operator_minus(n, d);
                answer = operator_pipe(answer, x);
            }

            x = shift_right(x, 1);
            d = shift_right(d, 1);
        }

        quotient = answer;
        remainder = n;
    }

public:
    template <class T, class = __keep_size<T>>
     static wide_integer<M, Signed> operator_slash(const wide_integer<M, Signed>& lhs,
                                                                       const T& rhs) {
        wide_integer<M, Signed> o = rhs;
        wide_integer<M, Signed> quotient{}, remainder{};
        divide(make_positive(lhs), make_positive(o), quotient, remainder);

        if (Signed == wide_integer_s::Signed &&
            is_negative(o) != is_negative(lhs)) {
            quotient = operator_unary_minus(quotient);
        }

        return quotient;
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, S2> operator_slash(const wide_integer<M, Signed>& lhs,
                                                                         const wide_integer<M2, S2>& rhs) {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::operator_slash(wide_integer<M2, S2>(lhs), rhs);
    }

    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_percent(const wide_integer<M, Signed>& lhs,
                                                                         const T& rhs) {
        wide_integer<M, Signed> o = rhs;
        wide_integer<M, Signed> quotient{}, remainder{};
        divide(make_positive(lhs), make_positive(o), quotient, remainder);
        if (Signed == wide_integer_s::Signed &&
            is_negative(lhs)) {
            remainder = operator_unary_minus(remainder);
        }
        return remainder;
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, S2> operator_percent(const wide_integer<M, Signed>& lhs,
                                                                           const wide_integer<M2, S2>& rhs) {
        return std::common_type_t<wide_integer<M, Signed>, wide_integer<M2, Signed>>::operator_percent(wide_integer<M2, S2>(lhs), rhs);
    }

    // ^
    template <class T, class = __keep_size<T>>
    static wide_integer<M, Signed> operator_circumflex(const wide_integer<M, Signed>& lhs,
                                                                            const T& rhs) noexcept {
        wide_integer<M, Signed> t(rhs);
        wide_integer<M, Signed> res = lhs;

        for (int i = 0; i < arr_size; ++i) {
            res.m_arr[i] ^= t.m_arr[i];
        }

        return res;
    }

    template <size_t M2, wide_integer_s S2, class = __need_increase_size<M2, S2>>
    static wide_integer<M2, S2> operator_circumflex(const wide_integer<M, Signed>& lhs,
                                                                              const wide_integer<M2, S2>& rhs) noexcept {
        return wide_integer<M2, S2>::operator_circumflex(wide_integer<M2, S2>(lhs), rhs);
    }

    static void from_string(const char* c, wide_integer<M, Signed>& res) {
        res = 0;

        bool is_neg = Signed == wide_integer_s::Signed && *c == '-';
        if (is_neg)
            ++c;

        if (*c == '0' && (*(c + 1) == 'x' || *(c + 1) == 'X')) { // hex
            ++c;
            ++c;
            while (*c) {
                if (*c >= '0' && *c <= '9') {
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - '0');
                    ++c;
                } else if (*c >= 'a' && *c <= 'f') {
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - 'a' + 10U);
                    ++c;
                } else if (*c >= 'A' && *c <= 'F') { // tolower must be used, but it is not
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - 'A' + 10U);
                    ++c;
                } else {
                    throw std::runtime_error("invalid char from");
                }
            }
        } else { // dec
            while (*c) {
                if (*c < '0' || *c > '9') {
                    throw std::runtime_error("invalid char from");
                }
                res = operator_star(res, 10U);
                res = operator_plus_T(res, *c - '0');
                ++c;
            }
        }

        if (is_neg) {
            res = operator_unary_minus(res);
        }
    }

    static void from_string(const wchar_t* c, wide_integer<M, Signed>& res) {
        res = 0;

        bool is_neg = Signed == wide_integer_s::Signed && *c == L'-';
        if (is_neg) {
            ++c;
        }

        if (*c == L'0' && (*(c + 1) == L'x' || *(c + 1) == L'X')) { // hex
            ++c;
            ++c;
            while (*c) {
                if (*c >= L'0' && *c <= L'9') {
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - L'0');
                    ++c;
                } else if (*c >= L'a' && *c <= L'f') {
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - L'a' + 10U);
                    ++c;
                } else if (*c >= L'A' && *c <= L'F') { // tolower must be used, but it is not
                    res = operator_star(res, 16U);
                    res = operator_plus_T(res, *c - L'A' + 10U);
                    ++c;
                } else {
                    throw std::runtime_error("invalid char from");
                }
            }
        } else { // dec
            while (*c) {
                if (*c < L'0' || *c > L'9') {
                    throw std::runtime_error("invalid char from");
                }
                res = operator_star(res, 10U);
                res = operator_plus_T(res, *c - L'0');
                ++c;
            }
        }

        if (is_neg) {
            res = operator_unary_minus(res);
        }
    }
};

// Members
template <size_t M, wide_integer_s Signed>
template <class T, typename std::enable_if<std::is_arithmetic<T>::value, int>::type>
constexpr wide_integer<M, Signed>::wide_integer(T rhs) noexcept
    : m_arr{} {
    _impl::wide_integer_from_Integral(*this, _impl::to_Integral(rhs));
}

template <size_t M, wide_integer_s Signed>
template <size_t M2, wide_integer_s S2>
 wide_integer<M, Signed>::wide_integer(const wide_integer<M2, S2>& rhs) noexcept
    : m_arr{} {
    _impl::wide_integer_from_wide_integer(*this, rhs);
}

template <size_t M, wide_integer_s Signed>
template <size_t M2, wide_integer_s S2>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator=(const wide_integer<M2, S2>& rhs) noexcept {
    _impl::wide_integer_from_wide_integer(*this, rhs);
    return *this;
}

template <size_t M, wide_integer_s Signed>
template <class I>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator=(I rhs) noexcept {
    _impl::wide_integer_from_Integral(*this, _impl::to_Integral(rhs));
    return *this;
}

template<size_t M, wide_integer_s S>
template<class T>
wide_integer<M, S>& wide_integer<M, S>::operator*=(const T& rhs) {
    *this = _impl::operator_star(*this, wide_integer<M, S>(rhs));
    return *this;
}
template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>  operator*(const wide_integer<M1, S1>& lhs, const wide_integer<M2, S2>& rhs) {
    return std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>::_impl::operator_star(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator* (wide_integer<M, S> lhs, T rhs) {
     lhs *= rhs;
     return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator* (T lhs, wide_integer<M, S> rhs) {
     rhs *= lhs;
     return rhs;
}

template <size_t M, wide_integer_s Signed>
template <class T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator/=(const T& rhs) {
     *this = _impl::operator_slash(*this, wide_integer<M, Signed>(rhs));
     return *this;
}
template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>> operator/ (wide_integer<M1, S1> const& lhs, wide_integer<M2, S2> const& rhs) {
    return std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>::_impl::operator_slash(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator/ (wide_integer<M, S> lhs, T rhs) {
    lhs /= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator/ (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result /= rhs;
    return result;
}

template <size_t M, wide_integer_s Signed>
template <class T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator+=(const T& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
    *this = _impl::operator_plus(*this, wide_integer<M, Signed>(rhs));
    return *this;
}
template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator+(wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs) {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_plus(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator+ (wide_integer<M, S> lhs, T rhs) {
    lhs += rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator+ (T lhs, wide_integer<M, S> rhs) {
    rhs += lhs;
    return rhs;
}


template <size_t M, wide_integer_s Signed>
template <typename T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator-=(const T& rhs) noexcept(Signed == wide_integer_s::Unsigned) {
    *this = _impl::operator_minus(*this, wide_integer<M, Signed>(rhs));
    return *this;
}

template <size_t M, wide_integer_s Signed>
template <typename T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator%=(const T& rhs) {
    *this = _impl::operator_percent(*this, wide_integer<M, Signed>(rhs));
    return *this;
}

template <size_t M, wide_integer_s Signed>
template <typename T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator&=(const T& rhs) noexcept {
    *this = _impl::operator_amp(*this, wide_integer<M, Signed>(rhs));
    return *this;
}

template <size_t M, wide_integer_s Signed>
template <typename T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator|=(const T& rhs) noexcept {
    *this = _impl::operator_pipe(*this, wide_integer<M, Signed>(rhs));
    return *this;
}

template <size_t M, wide_integer_s Signed>
template <typename T>
 wide_integer<M, Signed>& wide_integer<M, Signed>::operator^=(const T& rhs) noexcept {
    *this = _impl::operator_circumflex(*this, wide_integer<M, Signed>(rhs));
    return *this;
}

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed>& wide_integer<M, Signed>::operator<<=(int n) {
    *this = _impl::shift_left(*this, n);
    return *this;
}

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed>& wide_integer<M, Signed>::operator>>=(int n) noexcept {
    *this = _impl::shift_right(*this, n);
    return *this;
}

template <size_t M, wide_integer_s S>
wide_integer<M, S>& wide_integer<M, S>::operator++()
        noexcept(S == wide_integer_s::Unsigned) {
    return *this += 1;
}

template <size_t M, wide_integer_s S>
wide_integer<M, S> wide_integer<M, S>::operator++(int)
        noexcept(S == wide_integer_s::Unsigned) {
    class_name temp(*this);
    ++*this;
    return temp;
}

template <size_t M, wide_integer_s S>
wide_integer<M, S>& wide_integer<M, S>::operator--()
        noexcept(S == wide_integer_s::Unsigned) {
    return *this -= 1;
}

template <size_t M, wide_integer_s S>
wide_integer<M, S> wide_integer<M, S>::operator--(int)
        noexcept(S == wide_integer_s::Unsigned) {
    class_name temp(*this);
    --*this;
    return temp;
}

template <size_t M, wide_integer_s Signed>
 wide_integer<M, Signed>::operator bool() const noexcept {
    return !_impl::operator_eq(*this, 0);
}

template <size_t M, wide_integer_s Signed>
template <class T, class>
wide_integer<M, Signed>::operator T() const noexcept {
    static_assert(std::numeric_limits<T>::is_integer, "");
    T res = 0;
    for (size_t r_idx = 0; r_idx < _impl::arr_size && r_idx < sizeof(T); ++r_idx) {
        res |= (T(m_arr[_impl::arr_size - 1 - r_idx]) << (_impl::base_bits * r_idx));
    }
    return res;
}

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed>::operator long double() const noexcept {
    if (_impl::operator_eq(*this, 0))
        return 0;

    wide_integer<M, Signed> tmp = *this;
    if (_impl::is_negative(*this))
        tmp = -tmp;

    long double res = 0;
    for (size_t idx = 0; idx < _impl::arr_size; ++idx) {
        long double t = res;
        res *= std::numeric_limits<base_type>::max();
        res += t;
        res += tmp.m_arr[idx];
    }

    if (_impl::is_negative(*this))
        res = -res;

    return res;
}

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed>::operator double() const noexcept {
    return static_cast<long double>(*this);
}

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed>::operator float() const noexcept {
    return static_cast<long double>(*this);
}

// Unary operators
template <size_t M, wide_integer_s S>
wide_integer<M, S> operator~(wide_integer<M, S> const& lhs) noexcept {
    return wide_integer<M, S>::_impl::operator_unary_tilda(lhs);
}

template <size_t M, wide_integer_s S>
wide_integer<M, S> operator-(const wide_integer<M, S>& lhs) noexcept(S == wide_integer_s::Unsigned) {
    return wide_integer<M, S>::_impl::operator_unary_minus(lhs);
}

template <size_t M, wide_integer_s S>
wide_integer<M, S> operator+(wide_integer<M, S> lhs) noexcept(S == wide_integer_s::Unsigned) {
    return lhs;
}

// Binary operators

template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>  operator%(const wide_integer<M1, S1>& lhs, const wide_integer<M2, S2>& rhs) {
    return std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>::_impl::operator_percent(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator% (wide_integer<M, S> lhs, T rhs) {
    lhs %= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator% (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result %= rhs;
    return result;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator-(wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs) {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_minus(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator- (wide_integer<M, S> lhs, T rhs) {
    lhs -= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator- (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result -= rhs;
    return result;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator&(wide_integer<M, S> const& lhs, const wide_integer<M2, S2>& rhs) {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_amp(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator& (wide_integer<M, S> lhs, T rhs) {
    lhs &= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator& (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result &= rhs;
    return result;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator|(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_pipe(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator| (wide_integer<M, S> lhs, T rhs) {
    lhs |= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator| (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result |= rhs;
    return result;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator^(wide_integer<M, S> const& lhs , wide_integer<M2, S2> const& rhs) {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_circumflex(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator^ (wide_integer<M, S> lhs, T rhs) {
    lhs ^= rhs;
    return lhs;
}
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator^ (T lhs, wide_integer<M, S> const& rhs) {
    std::decay_t<decltype(rhs)> result(lhs);
    result ^= rhs;
    return result;
}

template <size_t M, wide_integer_s S>
 wide_integer<M, S> operator<<(wide_integer<M, S> const& lhs, int n) noexcept {
    return wide_integer<M, S>::_impl::shift_left(lhs, n);
}
template <size_t M, wide_integer_s S>
 wide_integer<M, S> operator>>(wide_integer<M, S> const& lhs, int n) noexcept {
    return wide_integer<M, S>::_impl::shift_right(lhs, n);
}

template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
bool operator< (wide_integer<M1, S1> const& lhs, wide_integer<M2, S2> const& rhs) noexcept {
    return std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>::_impl::operator_less(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator< (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs < std::decay_t<decltype(lhs)>(rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator< (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) < rhs;
}

template<size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator> (wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs) noexcept {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_more(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator> (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs > std::decay_t<decltype(lhs)>(rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator> (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) > rhs;
}

template<size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator<= (const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_less(lhs, rhs) ||
           std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_eq(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator<= (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs <= std::decay_t<decltype(lhs)>(rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator<= (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) <= rhs;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator>= (const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_more(lhs, rhs) ||
           std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_eq(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator>= (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs >= std::decay_t<decltype(lhs)>(rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator>= (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) >= rhs;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator==(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept {
    return std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_eq(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator== (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs == std::decay_t<decltype(lhs)>(rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator== (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) == rhs;
}

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator!=(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept {
    return !std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>::_impl::operator_eq(lhs, rhs);
}
template<size_t M, wide_integer_s S, class T>
bool operator!= (wide_integer<M, S> const& lhs, T rhs) noexcept {
    return lhs != std::decay_t<decltype(lhs)>(rhs);
 }
template<size_t M, wide_integer_s S, class T>
bool operator!= (T lhs, wide_integer<M, S> const& rhs) noexcept {
    return std::decay_t<decltype(rhs)>(lhs) != rhs;
 }

template <size_t M, wide_integer_s S>
std::ostream& operator<<(std::ostream& out, wide_integer<M, S> const& n) {
    out << to_string(n);
    return out;
}

template <size_t M, wide_integer_s S>
std::wostream& operator<<(std::wostream& out, wide_integer<M, S> const& n) {
    out << to_wstring(n);
    return out;
}

template <size_t M, wide_integer_s S>
std::istream& operator>>(std::istream& in, wide_integer<M, S>& n) {
    std::string s;
    in >> s;
    wide_integer<M, S>::_impl::from_string(s.c_str(), n);
    return in;
}

template <size_t M, wide_integer_s S>
std::wistream& operator>>(std::wistream& in, wide_integer<M, S>& n) {
    std::wstring s;
    in >> s;
    wide_integer<M, S>::_impl::from_string(s.c_str(), n);
    return in;
}

template <size_t M, wide_integer_s S>
std::string to_string(wide_integer<M, S> const& n) {
    std::string res;
    if (wide_integer<M, S>::_impl::operator_eq(n, 0U))
        return "0";

    wide_integer<M, wide_integer_s::Unsigned> t;
    bool is_neg = wide_integer<M, S>::_impl::is_negative(n);
    t = is_neg ? wide_integer<M, S>::_impl::operator_unary_minus(n)
               : n;

    while (!wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_eq(t, 0U)) {
        res.insert(res.begin(), '0' + char(wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_percent(t, 10U)));
        t = wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_slash(t, 10U);
    }

    if (is_neg)
        res.insert(res.begin(), '-');

    return res;
}

template <size_t M, wide_integer_s S>
std::wstring to_wstring(const wide_integer<M, S>& n) {
    std::wstring res;
    if (wide_integer<M, S>::_impl::operator_eq(n, 0U))
        return L"0";

    wide_integer<M, wide_integer_s::Unsigned> t;
    bool is_neg = wide_integer<M, S>::_impl::is_negative(n);
    t = is_neg ? wide_integer<M, S>::_impl::operator_unary_minus(n)
               : n;

    while (!wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_eq(t, 0U)) {
        res.insert(res.begin(), '0' + wchar_t(wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_percent(t, 10U)));
        t = wide_integer<M, wide_integer_s::Unsigned>::_impl::operator_slash(t, 10U);
    }

    if (is_neg)
        res.insert(res.begin(), '-');

    return res;
}

template <size_t M, wide_integer_s S>
to_chars_result to_chars(char* first, char* last, const wide_integer<M, S>& value, int base) {
    if (base < 2 || base > 36)
        return {last, std::make_error_code(std::errc::invalid_argument)};
    if (first >= last)
        return {last, std::make_error_code(std::errc::invalid_argument)};

    if (value == 0) {
        *first = '0';
        *(++first) = '\0';
        return {++first, {}};
    }

    wide_integer<M, S> v = value;
    if (v < 0) {
        v = -v;
        *(first++) = '-';
    }

    char* cur = last;

    while (v != 0 && --cur >= first) {
        static const char ALPHA[] = "0123456789abcdefghijklmnopqrstuvwxyz";
        *cur = ALPHA[v % base];
        v /= base;
    }

    if (v && cur + 1 == first)
        return {nullptr, std::make_error_code(std::errc::value_too_large)};

    while (cur < last)
        *(first++) = *(cur++);

    if (first < last)
        *first = '\0';

    return {first, {}};
}

inline std::array<char, 256> genReverseAlpha() noexcept {
    static const char ALPHA[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    std::array<char, 256> res;
    res.fill(-1);
    for (size_t i = 0; i < sizeof(ALPHA); ++i)
        res[ALPHA[i]] = static_cast<char>(i);
    return res;
}

template <size_t M, wide_integer_s S>
from_chars_result from_chars(const char* first, const char* last, wide_integer<M, S>& value, int base) {
    if (base < 2 || base > 36)
        return {first, std::make_error_code(std::errc::invalid_argument)};

    if (first >= last)
        return {first, std::make_error_code(std::errc::invalid_argument)};

    bool is_negative = *first == '-';
    if (is_negative) {
        if (S == wide_integer_s::Unsigned)
            return {first, std::make_error_code(std::errc::result_out_of_range)};
        if (++first >= last)
            return {first, std::make_error_code(std::errc::invalid_argument)};
    }

    wide_integer<M, S> v = 0;
    const char* cur = first;

    do {
        static const std::array<char, 256> ALPHA = genReverseAlpha();
        char cv = ALPHA[*cur];
        if (cv >= base || cv == -1) {
            if (cur == first)
                return {cur, std::make_error_code(std::errc::result_out_of_range)};
             else {
                value = v;
                return {cur, {}};
            }
        }

        v *= base;
        v += cv;
    } while (++cur < last);

    value = is_negative ? -v : v;
    return {cur, {}};
}

inline std::int128_t operator"" _int128(const char* n) {
    std::int128_t r;
    std::int128_t::_impl::from_string(n, r);
    return r;
}
inline std::int256_t operator"" _int256(const char* n) {
    std::int256_t r;
    std::int256_t::_impl::from_string(n, r);
    return r;
}
inline std::int512_t operator"" _int512(const char* n) {
    std::int512_t r;
    std::int512_t::_impl::from_string(n, r);
    return r;
}
inline std::uint128_t operator"" _uint128(const char* n) {
    std::uint128_t r;
    std::uint128_t::_impl::from_string(n, r);
    return r;
}
inline std::uint256_t operator"" _uint256(const char* n) {
    std::uint256_t r;
    std::uint256_t::_impl::from_string(n, r);
    return r;
}
inline std::uint512_t operator"" _uint512(const char* n) {
    std::uint512_t r;
    std::uint512_t::_impl::from_string(n, r);
    return r;
}

template <size_t M, wide_integer_s S>
struct hash<wide_integer<M, S>> {
    std::size_t operator()(const wide_integer<M, S>& lhs) const {
        size_t res = 0;
        for (auto n : lhs.m_arr)
            res += n;
        return hash<size_t>()(res);
    }
};

template <size_t M, wide_integer_s S>
constexpr wide_integer<M, S> abs(wide_integer<M,S> const& val) noexcept {
    return val > 0 ? val : (val * -1);
}

template<>
struct is_signed<std::int128_t> {
    using type = std::int128_t;
};

template<>
struct is_signed<std::int256_t> {
    using type = std::int256_t;
};

template<>
struct make_signed<std::int128_t> {
    using type = std::int128_t;
};

template<>
struct make_signed<std::int256_t> {
    using type = std::int256_t;
};

template<>
struct make_signed<std::int512_t> {
    using type = std::int512_t;
};

template<>
struct make_signed<std::uint128_t> {
    using type = std::int128_t;
};

template<>
struct make_signed<std::uint256_t> {
    using type = std::int256_t;
};

template<>
struct make_signed<std::uint512_t> {
    using type = std::int512_t;
};

template<>
struct make_unsigned<std::int128_t> {
    using type = std::uint128_t;
};

template<>
struct make_unsigned<std::int256_t> {
    using type = std::uint256_t;
};

template<>
struct make_unsigned<std::int512_t> {
    using type = std::uint512_t;
};

template<>
struct make_unsigned<std::uint128_t> {
    using type = std::uint128_t;
};

template<>
struct make_unsigned<std::uint256_t> {
    using type = std::uint256_t;
};

template<>
struct make_unsigned<std::uint512_t> {
    using type = std::uint512_t;
};

template< class T >
struct is_wide_int final
     : std::integral_constant<
         bool,
         std::is_same<std::int128_t, typename std::remove_cv<T>::type>::value   ||
         std::is_same<std::int256_t, typename std::remove_cv<T>::type>::value   ||
         std::is_same<std::int512_t, typename std::remove_cv<T>::type>::value   ||
         std::is_same<std::uint128_t, typename std::remove_cv<T>::type>::value  ||
         std::is_same<std::uint256_t, typename std::remove_cv<T>::type>::value  ||
         std::is_same<std::uint512_t, typename std::remove_cv<T>::type>::value
     > {};

template<class T>
constexpr bool is_wide_int_v = is_wide_int<T>::value;

} // namespace std

#ifdef QT
#include <QDebug>
#include <QString>

template <size_t M, std::wide_integer_s S>
static inline QDebug operator<<(QDebug dbg, std::wide_integer<M,S>const& w) {
    return dbg.nospace()
        << "\"wide_integer<M,S>\": {"
        << "\"value\": " << QString::fromStdString(std::to_string(w))
        << "}";
}
#endif
