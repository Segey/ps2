/**
 * \file      ps2/ps2/cpp/db/postgresql/postgresql.h
	\brief		Все для работы с PostgreSQl
	\author		S.Panin <panin@ashmanov.com>
 * \copyright S.Panin, 2006 - 2017
	\version	v.1.03
	\date		Created on 29 January 2011 y., 23:33:44
	\TODO
*/
#pragma once
#include <string>
#include <vector>
#include <sstream>
#include <libpq-fe.h>

/** namespace ps */
namespace ps  {
	/** namespace ps::postgresql */
	namespace postgresql  {

	class db
	{
		typedef	db				class_name;
		typedef	std::string		string_type;

		PGconn* 					m_pConn;

		db(db const& rhs);
		db& operator=(db const& rhs);


		template<class T> inline T addQuotes ( T const& t) const {
			return t;
		}

		std::string addQuotes ( std::string const& s) const {
			std::string result;

			for(std::string::const_iterator it = s.begin(); it != s.end(); ++it) {
				if(*it == '\'') result.push_back('\'');
				result.push_back(*it);
			};
			return result;
		}

		template<unsigned MAX>
		bool transaction(const char* name) {
			for (unsigned i = 0; i!= MAX; ++i)  {
				PGresult* res = PQexec(m_pConn, name);

				if (PQresultStatus(res) == PGRES_COMMAND_OK) {
					PQclear(res);
					return true;
				}

				PQclear(res);
			}

			std::cerr << "Transaction is failed: " << PQerrorMessage(m_pConn) << std::endl;
			return false;
		}

	public:
		template<unsigned MAX>
		bool startTransaction()  	{ return transaction<MAX>("BEGIN;"); }

		template<unsigned MAX>
		bool endTransaction()  	{ return transaction<MAX>("END;"); }

		db(): m_pConn(NULL) { }
		~db() { PQfinish(m_pConn); }

		PGconn*  conn() const  { return m_pConn; }

		bool connect(string_type const& str) {
			m_pConn = PQconnectdb(str.c_str());
			return is_connect();
		}

		/** 
		 *  \brief Подключаемся к базе
		 *  \param {in: FCGX_Request} r  A structure Request
		 */
		bool connect(const char* host, const char* port, const char* db_name, const char* user, const char* pass, const char* timeout) {
			std::stringstream str;
				str << "hostaddr=\'"        << host     << "\' "
				<< "port=\'"            << port     << "\' "
				<< "dbname=\'"          << db_name 	<< "\' "
				<< "user=\'"            << user     << "\' "
				<< "password=\'"        << pass     << "\' "
				<< "connect_timeout=\'" << timeout  << "\'";

			return connect(str.str());
		}

        bool is_connect() const {
			return m_pConn != NULL && PQstatus(m_pConn) == CONNECTION_OK;
        }

		void printError(const char* err = "Error" ) const {
			std::cerr <<  err << " " << PQerrorMessage(m_pConn) << std::endl;
		}

		/** \brief Функция для выполнения обычных комманд (например: Удалить таблицу)
			\template {unsigned} MAX - кол-во попыток выполнения insert
			\param {in: const char*} command - команда
		*/
		template<unsigned MAX>
		bool execute(const char* command) { 
			for ( unsigned i = 0; i < MAX; ++i ) {
			PGresult* res = PQexec(m_pConn, command); 
			if (PQresultStatus(res) == PGRES_COMMAND_OK) {
					PQclear(res);
					return true;
				}
				PQclear(res);
			}
			return false;
		}

		/** \brief Функция добавления единичных данных в базу
			\template {unsigned} MAX - кол-во попыток выполнения insert
			\template {typename} T 	- Данные. Тип обычно простые типы std::string, int
			\param {in: const char*} dbTable - название таблицы 
			\param {in: const char*} columnName - название таблицы 
			\param {in: T } map_val - Данные тип смотри выше.
		*/
		template<unsigned MAX, typename T>
		bool insert(const char* dbTable, const char* column, T const& val) const {
			std::ostringstream stream;
			stream << "INSERT INTO " << dbTable << '(' << column <<") ";
			stream << "VALUES(\'" << val <<"\');";

			for ( unsigned i = 0; i < MAX; ++i ) {
				PGresult* res = PQexec(m_pConn, stream.str().c_str()); 
				if (PQresultStatus(res) == PGRES_COMMAND_OK) {
					PQclear(res);
					return true;
				}
				PQclear(res);
			}

			return false;
		}

		/** \brief Функция добавления данных в базу
			\template {unsigned} MAX - кол-во попыток выполнения insert
			\template {typename} T 	- названия колонок. Тип обычно std::vector, std::list c типом std::string
			\template {typename} U 	- Данные. Тип обычно std::vector<std::vector>, std::list<std::list> c типом std::string или int 
			\param {in: const char*} dbTable - название таблицы 
			\param {in: T } columns - Названия колонок тип смотри выше.
			\param {in: U } values - Данные тип смотри выше.
			\note Перечисления данных должны идти в том же порядке
		*/
		template<unsigned MAX, typename T, typename U>
		bool insert(const char* dbTable,  T const& columns, U const& values) const {
			if(columns.empty() || values.empty()) return false;
			if(columns.size() != values.begin()->size()) return false;

			std::ostringstream first;
			first << "INSERT INTO " << dbTable << '(' << columns.front();

			for ( typename T::const_iterator it = ++columns.begin(); it != columns.end(); ++it ) {
				first 	<< ", " << *it;
			}

			first << ")";

			std::ostringstream second;
			second << " VALUES";

			for( typename U::const_iterator it = values.begin() ; it != values.end() ; ++it) {
				if(it->empty()) continue;
				second << '(';

				for( typename U::value_type::const_iterator at = it->begin(); at != it->end(); ++at) {
					second << '\'' << addQuotes(*at) << '\'';
					if(at != it->end() - 1) second << ','; 
				}
				second << ')';
				if(it != values.end() - 1) second << ','; 
			}
			
			second << ";"; 
			if(second.str() == " VALUES;") return false;
//std::cout << std::string(first.str() + second.str()) << std::endl;
			for ( unsigned i = 0; i < MAX; ++i ) {
				PGresult* res = PQexec(m_pConn, std::string(first.str() + second.str()).c_str()); 
				if (PQresultStatus(res) == PGRES_COMMAND_OK) {
					PQclear(res);
					return true;
				}
				PQclear(res);
			}

			return false;
		}
	};

	} /* namespace ps::postgresql */
} /* namespace ps */
