/**
 * \file      ps2/ps2/cpp/trio.h
 * \brief     Trio class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 12(th), 2015, 20:37 MSK
 * \updated   May (the) 12(th), 2015, 20:37 MSK
 * \TODO
**/
#ifndef TrioH
#define TrioH

/** namespace ps2 */
namespace ps2 {

template<class T, class X, class Y>
struct trio  final {
    typedef T first_t;
    typedef X second_t;
    typedef Y third_t;

private:
    first_t m_first;
    second_t m_second;
    third_t m_third;

public:
    trio(first_t const& first = first_t(), second_t const& second = second_t()
            , third_t const& third = third_t())
        : m_first(first), m_second(second), m_third(third) {}
    void setFirst(first_t const& first) noexcept {
        m_first = first;
    }
    first_t const& first() const noexcept {
        return m_first;
    }
    void setSecond(second_t const& second) noexcept {
        m_second = second;
    }
    second_t const& second() const noexcept {
        return m_second;
    }
    void setThird(third_t const& third) noexcept {
        m_third = third;
    }
    third_t const& third() const noexcept {
        return m_third;
    }
};

} // namespace ps2
#endif

