/**
 * \file      ps2/ps2/cpp/quad.h
 * \brief     Quad class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 12(th), 2015, 20:39 MSK
 * \updated   May (the) 12(th), 2015, 20:39 MSK
 * \TODO
**/
#ifndef QuadH
#define QuadH

#ifdef CPPBUILDER2007
#define final 
#endif

/** namespace ps2 */
namespace ps2 {

template<class T, class X, class Y, class Z>
struct quad final {
    typedef T first_t;
    typedef X second_t;
    typedef Y third_t;
    typedef Z fourth_t;

private:
    T m_first;
    X m_second;
    Y m_third;
    Z m_fourth;

public:
    quad(first_t const& first = first_t(), second_t const& second = second_t()
            , third_t const& third = third_t(), fourth_t const& fourth = fourth_t()) 
        : m_first(first)
        , m_second(second)
        , m_third(third)
        , m_fourth(fourth) {
    }
    void setFirst(first_t const& first) {
        m_first = first;
    }
    first_t const& first() const {
        return m_first;
    }
    void setSecond(second_t const& second) {
        m_second = second;
    }
    second_t const& second() const {
        return m_second;
    }
    void setThird(third_t const& third) {
        m_third = third;
    }
    third_t const& third() const {
        return m_third;
    }
    void setFourth(fourth_t const& fourth) {
        m_fourth = fourth;
    }
    fourth_t const& fourth() const {
        return m_fourth;
    }
};

} // namespace ps2
#endif
