/**
 * \file      ps2/ps2/cpp/integer.h
 * \brief     Work with Integer
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   April     (the) 23(th), 2015, 17:12 MSK
 * \updated   April     (the) 25(th), 2017, 13:12 MSK
 * \TODO
**/
#pragma once
#include <cmath>

/** \namespace ps2 */
namespace ps2 {

static inline bool isOdd(int nInput) noexcept {
    if(nInput == 0) return false;
    return 2 * (nInput / 2) != nInput;
}
/**
 * \code
        auto const x = digitCount(15300); // 5
 * \endcode
**/
static inline int digitCount(int num) noexcept {
    return static_cast<int>(floor(log10(abs(num?num:1)) + 1));
}
/**
 * \code
        auto const x = getDigit(453,1); // 4
        auto const x = getDigit(453,2); // 5
 * \endcode
**/
template<class T>
static inline int getDigit(T number, T pos) noexcept {
    auto const cnt = digitCount(number);
    return static_cast<T>(number/pow(10,(cnt - pos))) % 10;
}

/**
 * \code
        auto const x = cutTail(453,1); // 45
        auto const x = cutTail(453,2); // 4
 * \endcode
**/
template<class T>
static inline int cutTail(T number, unsigned count) noexcept {
    while(count-- != 0u)
        number /= 10;
    return number;
}
/**
 * \code
        auto const x = cutHead(453,1); // 53
        auto const x = cutHead(453,2); // 3
 * \endcode
**/
template<class T>
static inline int cutLead(T number, unsigned count) noexcept {
    auto const cnt = digitCount(number);
    if(count > static_cast<uint>(cnt)) return 0;
    return number % static_cast<T>(pow(10, cnt - count));
}

/**
 * \code
        auto const x = concat(453,1); // 4531
        auto const x = concat(453,2); // 4532
 * \endcode
**/
template<class T>
static inline T concat(T _1, T _2) noexcept {
    return _1 * pow(10, static_cast<T>(log10(_2)+1)) + _2;
}

/**
 * \brief Checks only equial digit range numbers [4,5] or [11,55], [333,666]
 * \note [5,4] means from 5 to 14 inclusively, full range is uses, it extremely necessary
 * \code
        auto const x = checkInputRange(5,1,6);   // true
        auto const x = checkInputRange(55,72,62); // true
 * \endcode
**/
template<class T>
static inline bool checkInputRange(T bottom, T top, T val) noexcept {
    auto const off = pow(10, digitCount(bottom));
    if(bottom > top) top += off;
    if(val < bottom) val += off;
    return bottom <= val && val <= top;
}

/**
 * \brief Checks a result of operation is oveflowed?
 * \code
        auto const is = isOverflow(origin, result);
        auto const is = isOverflow(_1, _1 * _2);
 * \endcode
**/
template<class T>
static inline bool isOverflow(T origin, T result) noexcept {
    return result < origin;
}

} // end namespace ps2
