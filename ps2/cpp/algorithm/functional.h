/**
 * \file      c:/projects/perfect/projects/ps2/ps2/cpp/algorithm/functional.h 
 * \brief     The Functional class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   October   (the) 20(th), 2017, 00:28 MSK
 * \updated   October   (the) 20(th), 2017, 00:28 MSK
 * \TODO      
**/
#pragma once

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
    using map_t = std::map<QString, QString, ps2::StrGreater<QString>>;
 * \endcode
**/
template<class T>
struct StrGreater final {
    constexpr bool operator() (T const& lhs , T const& rhs) const noexcept {
         return lhs.size() == rhs.size()
             ? lhs < rhs : lhs.size() > rhs.size();
    }
};

} // end namespace ps2
