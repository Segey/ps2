/**
 * \file      ps2/ps2/cpp/algorithm/concat.h
 * \brief     The Concat class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   October  (the) 28(th), 2016, 14:27 MSK
 * \updated   November (the) 23(th), 2016, 14:21 MSK
 * \TODO
**/
#pragma once
#include <iterator>
#include <QVector>
#include <QString>

/** namespace ps2 */
namespace  ps2 {

/**
 * \return concats of values and all in [first, last), using fun
 * \code
    // #1
        std::vector<int> vec {1, 2, 3};
        auto&& result = ps2::concat(vec, ", "s, ""s, ""s, [](auto const& str, auto val){
            return str + std::to_string(val);
        });
        assertEquals(result, "1, 2, 3"s);

    // #2
    template<class T>
    inline QDebug operator<<(QDebug dbg, Type<T> const& vec) {
        return dbg.nospace() << ps2::concat(vec, QStringLiteral(", ")
                        , QStringLiteral("model::list::List: ["), QStringLiteral("]")
                                , [](auto const& str, auto val){
            return str + ps2::to_qstr(val);
        });
    }
    // #3
    return ps2::concat(list, QStringLiteral(", ")
           , QStringLiteral("SELECT ") , QStringLiteral(" FROM %1;"));

    // #4
    inline QDebug operator<<(QDebug dbg, Info const& vec) noexcept {
        return dbg.nospace()
            << ps2::concat(vec.items().begin(), vec.items().end()
           , QStringLiteral(", "), QStringLiteral("\"Info\": ["), QStringLiteral("]")
                                , [](auto const& str, auto const& val){
            return QStringLiteral("%1{\"%2\": \"%3\"}")
                    .arg(str, val.value.key(), val.value.value());
            });
    }

 * \enccode
**/
template<class T, class Y, class F>
inline Y concat(T first, T last, Y sep, Y beg, Y end, F fun) noexcept {
    if(first == last) return Y{(beg) + end};
    beg = fun(beg, *first++);
    if(first == last) return Y{(beg) + end};
    for (; first != last; ++first) {
        beg += sep;
        beg = fun(beg, *first);
    }
    return Y{(beg) + end};
}
template<class T, class Y, class F>
inline Y concat(T arr, Y sep, Y beg, Y end, F fun) noexcept {
    return concat(std::begin(arr),std::end(arr), sep, beg, end, fun);
}
template<class T, class F>
inline QString concat(T const& arr, QString const& sep
                      , QString const& beg, QString const& end, F fun) noexcept {
    return concat(std::begin(arr),std::end(arr), sep, beg, end, fun);
}
template<class T>
inline QString concat(T const& arr, QString const& sep
                      , QString const& beg, QString const& end) noexcept {
    return ps2::concat(arr.cbegin(), arr.cend(), sep, beg, end, [](auto const& str, auto const& val){
        return str + val;
    });
}
template<class T>
inline QString concat(T const& arr, QString const& sep) noexcept {
    return ps2::concat(arr.cbegin(), arr.cend(), sep, QString(), QString()
           , [](auto const& str, auto const& val){
        return str + val;
    });
}
inline std::string concat(QVector<std::string> const& arr, std::string const& sep
                          , std::string const& beg, std::string const& end) noexcept {
    return ps2::concat(arr.cbegin(), arr.cend(), sep, beg, end, [](auto const& str, auto const& val){
        return str + val;
    });
}

} // namespace ps2
