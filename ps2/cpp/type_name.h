/**
 * \file      ps2/ps2/cpp/type_name.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.2.1
 * \created   August (the) 11(th), 2016, 01:04 MSK
 * \updated   August (the) 11(th), 2016, 01:04 MSK
 * \TODO
**/
#pragma once
#include <cstring>

/** \namespace ps2 */
namespace ps2 {

/**
 * \code
        std::cout << ps2::type_name<decltype(keysList)>();
        << "\"RepsT<" << ps2::type_name<T>() << ">\": {"
 * \endcode
**/
template <class T>
char const* type_name() {
#if defined(_MSC_VER)
    static std::string s = __FUNCSIG__;
    auto const _1 = s.find('<');
    auto const _2 = s.find('>');
#else
    static std::string s = __PRETTY_FUNCTION__;
    auto const _1 = s.find("with T = ") + 8;
    auto const _2 = s.find(']');
#endif
    static auto p = s.substr(_1 + 1, _2 - _1 - 1);
    return p.c_str();
}

} // end namespace ps2
