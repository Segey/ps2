﻿/**
 * \file      ps2/ps2/cpp/encode.h
 * \brief     The main file "Encoding"
 * \author    S.Panin <panin@ashmanov.com>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.4
 * \date      Created on 01 February 2012 y., 15:28
 * \TODO		
 * \URL       http://www.webtoolkit.info/javascript-utf8.html
**/
#pragma once
#include <cstdlib>
#include <algorithm>
#include <sstream>
#include <vector>

namespace {

template<class T>
T to16x( T t1, T t2 ) {
    std::basic_string<T> s;
    s.push_back(t1);
    s.push_back(t2);

    std::wistringstream iss(s);
    unsigned res;
    iss >> std::hex >> res;
    return static_cast<T>(res);
}

template<class T>
void toUnicodeString ( std::basic_string<T> const& s, std::wstring& out ) {
    const size_t size  = s.size();
    unsigned cx  = 0;

    while(cx < size) {
        if(s[cx] == '%'){
            if(cx + 3 > size) return; 	// It'll print out if the length of string is uncorrect
            out.push_back(to16x(s[cx+1], s[cx+2]));	
            cx+=3;
        }
        else  out.push_back(s[cx++] == '+'? ' ' : s[cx -1]);
    }
}

} // end empty namespace

/** namespace ps2::encode */
namespace ps2 {
namespace encode {

typedef unsigned char u8;

/**
 * \brief	The function converts string data to utf8 wstring data 
 * \param {in:  std::string  const&} str  The string to be converted
 * \param {out: std::wstring const&} dest The wstring that a result of convert will be containt.
 * \result void
 * \code
 *     std::string  src;
 *     std::wstring dest;
 *     ps2::encode::utf8ToWString(src, dest);
 * \endcode
*/
static void utf8ToWString(std::string const& src, std::wstring& dest) {
    dest.clear();
    wchar_t w = 0;
    int bytes = 0;
    wchar_t err = L'�';
    for (size_t i = 0; i < src.size(); i++){
        unsigned char c = (unsigned char)src[i];
        if (c <= 0x7f){		//first byte
            if (bytes) {
                dest.push_back(err);
                bytes = 0;
            }
            dest.push_back((wchar_t)c);
        }
        else if (c <= 0xbf){	//second/third/etc byte
            if (bytes){
                w = ((w << 6)|(c & 0x3f));
                bytes--;
                if (bytes == 0)
                    dest.push_back(w);
            }
            else
                dest.push_back(err);
        }
        else if (c <= 0xdf){//2byte sequence start
            bytes = 1;
            w = c & 0x1f;
        }
        else if (c <= 0xef){//3byte sequence start
            bytes = 2;
            w = c & 0x0f;
        }
        else if (c <= 0xf7){//3byte sequence start
            bytes = 3;
            w = c & 0x07;
        }
        else{
            dest.push_back(err);
            bytes = 0;
        }
    }
    if (bytes)
        dest.push_back(err);
} 
/**
 * \brief  Convert from wstring to string	
 * \param  {in:  const wchar_t*} in  The source data
 * \result {out:  std::string& } out The result data
 * \code
 *     auto result = ps2::encode::wStringToString(L"hello");
 * \endcode
 * \warning If number of written byte equal to -1, then a convert is failed
**/
static std::string wStringToString(const wchar_t* str) {
	int Len = static_cast<int>(::wcslen(str) + 1);
	std::vector<char> Buffer(Len);
	::WideCharToMultiByte(CP_ACP, 0, str, -1, &Buffer.front(), Len, NULL, NULL);
	return &Buffer.front();
}
/**
 * \brief Convert from string to wstring	
 * \param  {in:  const char*} in The source data
 * \result The result data
 * \warning If number of written byte equal to -1, then a convert is failed
**/
static std::wstring stringToWString(const char* in) {
	int Len = static_cast<int>(::strlen(in) + 1);
	std::vector<wchar_t> Buffer(Len);
	::MultiByteToWideChar(CP_ACP, 0, in, -1, &Buffer.front(), Len);
	return &Buffer.front();
}
/**
 * \brief Coding throughout the equivalent javascript formula for a Get reguest
 * \param {in: std::basic_string<T>} text	The source data
 * \note  Note It's correct only for wchar_t(std::wstring)
**/
template<class T>
static std::basic_string<T> urlEncode (std::basic_string<T> const& s) {
	typedef T										char_type;
	typedef typename std::basic_string<char_type>	string_type;

	std::basic_ostringstream<char_type> out;
	out << std::hex;

	for(typename string_type::const_iterator it = s.begin(); it != s.end(); ++it) {
		char_type x = *it;
		const unsigned val = static_cast<unsigned>(x);
		if( (48 <= val && val <= 57)		// 0 ... 9
			|| (65 <= val && val <= 90)		// A ... Z
			|| (97 <= val && val <= 122)	// a ... z 
			|| T('~') == x || T('-') == x || T('.') == x || T('_') == x || T('!') == x || T('*') == x || T('(') == x || T(')') == x || T('\'') == x
			) out << x;
		else if(' ' == x) out << T('+');
		else {
			if(val < 128) out << T('%') << val;
			else if(127  < val && val < 2048) out << T('%') << ((val >> 6) | 192) << T('%')  << ((val & 63) | 128);
			else out << T('%') << ((val >> 12) | 224) <<  T('%')  << (((val >> 6) & 63) | 128) << T('%') << ((val & 63) | 128);
		}	
	}	
	return out.str();
}
/**
 * \brief The function checks opportunity converted 
 * \param  {in : std::wstring const&} str	The string to be checked
 * \result The result of opportunity convert.
 * \code
 *     std::string str;
 *     const bool b = ps2::encode::canConvertToUtf8(str);
 * \endcode
*/
static bool canConvertToUtf8(std::string const& str) {
		const UCHAR m1[] ={ (UCHAR)0, (UCHAR)~31, (UCHAR)~15, (UCHAR)~7, (UCHAR)~3 };
		const UCHAR m2[] ={ (UCHAR)(0), (UCHAR)(0xff & ~63), (UCHAR)(0xff& ~31), (UCHAR)(0xff &~15), (UCHAR)(0xff &~7) };

		UCHAR* start	= (UCHAR*)(str.c_str());
		UCHAR* end		= (UCHAR*)(start + str.length());

		INT ucnt = 0;
		UCHAR first = 0;
		ucnt = -1;

	START:
		while( start < end ){
			if ( *start < 128 ) ++start;
			else goto ENC;
		}
		return true;

	ENC:
		ucnt = 0; 
		first = *start;
		++start;
		while( start < end && (*start & 0xC0) == 0x80 ) ++ucnt, ++start;

		if (( ucnt == 0 || ucnt > 4 ) || ( (first & m1[ucnt]) != m2[ucnt])) return false;
	goto START;
}

/**
 * \brief Decoding throughout the equivalent javascript formula for a Get reguest
**/
static std::wstring urlDecode (std::wstring const& s) {
	typedef wchar_t	char_type;

	std::wostringstream out;
	std::wstring res;
	toUnicodeString(s, res);
	
	const size_t size = res.size();  
	unsigned cx = 0;	
	
	while(cx < size) {
		const unsigned num = static_cast<unsigned>(res[cx]);
		if(num < 128)  out << res[cx++];
		else if(num > 191 && num < 224) {
			const unsigned num2 = static_cast<unsigned>(res[cx+1]);
			out << static_cast<char_type>(((num & 31) << 6) | (num2 & 63));
			cx+=2;	
		}
		else {
			const unsigned num2 = static_cast<unsigned>(res[cx+1]);
			const unsigned num3 = static_cast<unsigned>(res[cx+2]);
			out << static_cast<char_type>(((num & 15) << 12) | ((num2 & 63) << 6) | (num3 & 63));
			cx+=3;	
		}
	}
	return out.str();
}
/**
 * \brief Decoding string for equal javascript get request function
*/
static std::string urlDecode (std::string const& s) 
{
	auto temp = stringToWString(s.c_str());
	return wStringToString(urlDecode(temp).c_str());
}

}} // end namespace ps2::encode
