/**
 * \file      ps2/ps2/cpp/fraction.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March (the) 01(th), 2016, 21:24 MSK
 * \updated   March (the) 01(th), 2016, 21:24 MSK
 * \TODO
**/
#pragma once
#include <cassert>
#include <climits>

/** namespace ps2 */
namespace  ps2 {

template<int Denominator>
class Fraction final {
public:
    using class_name = Fraction<Denominator>;

private:
    int m_whole = INT_MAX;
    int m_numerator = INT_MAX;

public:
    explicit Fraction() = default;
    constexpr explicit Fraction(int whole, int numerator) noexcept
        : m_whole(whole)
        , m_numerator(numerator) {
    }
    constexpr explicit Fraction(int numerator) noexcept
        : m_whole(0)
        , m_numerator(numerator) {
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) {
        return lhs.m_numerator == rhs.m_numerator
                && lhs.m_whole == rhs.m_whole
        ;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) {
        return !(lhs == rhs);
    }
    constexpr bool isNull() const noexcept {
        return m_whole == INT_MAX
            || m_numerator == INT_MAX;
    }
    constexpr bool isValid() const noexcept {
        return !isNull();
    }
    constexpr int numerator() const noexcept {
        return m_numerator;
    }
    void setNumerator(int numerator) noexcept {
        m_numerator = numerator;
    }
    constexpr int denominator() const noexcept {
        return Denominator;
    }
    constexpr int whole() const noexcept {
        return m_whole;
    }
    void setWhole(int whole) noexcept {
        m_whole = whole;
    }
    constexpr double toDouble() const noexcept {
        return static_cast<double>(m_whole * Denominator + m_numerator)
                / Denominator;
    }
    void swap(class_name& rhs) noexcept {
        std::swap(m_whole, rhs.m_whole);
        std::swap(m_numerator, rhs.m_numerator);
    }
};
template<int N>
constexpr void swap(Fraction<N>& lhs, Fraction<N>& rhs) noexcept {
    lhs.swap(rhs);
}
template<int N>
inline std::ostream& operator<<(std::ostream& out, Fraction<N> const& f) noexcept {
    return out << "\"Fraction\": { \"denominator\": " << f.denominator()
        << ", \"numerator\": " << f.numerator() << ", \"whole\": " << f.whole()
        << "}";
}

template<> class Fraction<0> {};
using Fraction2 = Fraction<2>;
using Fraction3 = Fraction<3>;
using Fraction5 = Fraction<5>;
using Fraction6 = Fraction<6>;
using Fraction7 = Fraction<7>;
using Fraction10 = Fraction<10>;
using Fraction100 = Fraction<100>;
using Fraction1000 = Fraction<1000>;

#ifdef QT
template<int N>
inline QDebug operator<<(QDebug dbg, Fraction<N> const& fraction) noexcept {
    return dbg.nospace()
            << "\"Fraction<" << fraction.denominator() << ">\": {"
            << "\"whole\":" << fraction.whole()
            << ", \"numerator\":" << fraction.numerator()
            << "}";
}
#endif

} // namespace ps2
