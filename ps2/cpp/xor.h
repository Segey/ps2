/**
 * \file      ps2/ps2/cpp/xor.h
 * \brief     Work with xor
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   April (the) 14(th), 2015, 18:43 MSK
 * \updated   April (the) 14(th), 2015, 18:43 MSK
 * \TODO
**/
#pragma once
#include <vector>

/**
 * \code
 *      std::vector<char> const& str = ps2::Xor::simple(s);
 * \endcode
**/
/** namespace ps2::Xor */
namespace ps2 {
namespace Xor {
    static inline char simple(char ch) {
        return ~ch;
    }
    static inline std::vector<char> simple(std::vector<char> const& s) {
        std::vector<char> result;
        result.reserve(s.size());

        std::transform(s.begin(), s.end(), std::back_inserter(result)
                       , (char(*)(char))simple);
        return result;
    }
}} // end namespace ps2::Xor
