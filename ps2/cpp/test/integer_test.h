/**
 * \file      ps2/ps2/cpp/test/integer_test.h
 * \brief     The Integer_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April     (the) 25(th), 2017, 13:16 MSK
 * \updated   April     (the) 25(th), 2017, 13:16 MSK
 * \TODO      
**/
#pragma once
#include <ps2/cpp/integer.h>

/** \namespace ps2 */
namespace ps2 {

class IntegerTest final {
public:
    using class_name = IntegerTest;

private:
    static inline void SimpleDigitCount() noexcept {
        assertEquals(digitCount(153), 3);
    }
    static inline void ComplexDigitCount() noexcept {
        assertEquals(digitCount(15300), 5);
    }
    static inline void InvalidMin() noexcept {
        assertEquals(getDigit(100, 0), 0);
    }
    static inline void InvalidMax() noexcept {
        assertEquals(getDigit(100, 4), 0);
    }
    static inline void FirstDigit() noexcept {
        assertEquals(getDigit(453, 1), 4);
    }
    static inline void SecondDigit() noexcept {
        assertEquals(getDigit(453, 2), 5);
    }
    static inline void ThirdDigit() noexcept {
        assertEquals(getDigit(453, 3), 3);
    }
    static inline void InvalidCutTailMin() noexcept {
        assertEquals(cutTail(453, 3), 0);
    }
    static inline void InvalidCutTailMax() noexcept {
        assertEquals(cutTail(453, 0), 453);
    }
    static inline void FirstCutTail() noexcept {
        assertEquals(cutTail(453, 1), 45);
    }
    static inline void SecondCutTail() noexcept {
        assertEquals(cutTail(453, 2), 4);
    }
    static inline void SimpleConcat() noexcept {
        assertEquals(concat(4, 2), 42);
    }
    static inline void ComplexConcat() noexcept {
        assertEquals(concat(453, 21), 45321);
    }
    static inline void ComplexConcat2() noexcept {
        assertEquals(concat(3, 2167), 32167);
    }
    static inline void CutLeadMin() noexcept {
        assertEquals(cutLead(453, 0), 453);
    }
    static inline void CutLeadMax() noexcept {
        assertEquals(cutLead(453, 4), 0);
    }
    static inline void FirstCutLead() noexcept {
        assertEquals(cutLead(4537, 1), 537);
    }
    static inline void SecondCutLead() noexcept {
        assertEquals(cutLead(453, 2), 3);
    }
    static inline void SimpleCheckInputRangeMinBorder() noexcept {
        assertTrue(checkInputRange(5, 1, 5));
        assertTrue(checkInputRange(55, 12, 55));
        assertFalse(checkInputRange(5, 1, 4));
        assertFalse(checkInputRange(55, 12, 54));
    }
    static inline void SimpleCheckInputRangeMaxBorder() noexcept {
        assertTrue(checkInputRange(5, 1, 1));
        assertFalse(checkInputRange(5, 1, 2));
        assertTrue(checkInputRange(59, 19, 19));
        assertFalse(checkInputRange(59, 19, 20));
    }
    static inline void SimpleCheckInputOneDigitRange() noexcept {
        assertTrue(checkInputRange(0, 0, 0));
        assertTrue(checkInputRange(1, 9, 4));
        assertTrue(checkInputRange(7, 9, 8));
        assertFalse(checkInputRange(5, 6, 4));
        assertFalse(checkInputRange(1, 4, 7));
    }
    static inline void ComplexCheckInputOneDigitRange() noexcept {
        assertTrue(checkInputRange(5, 1, 6));
        assertTrue(checkInputRange(9, 1, 0));
        assertTrue(checkInputRange(7, 2, 1));
        assertTrue(checkInputRange(3, 1, 9));
    }
    static inline void SimpleCheckInputTwoDigitRange() noexcept {
        assertTrue(checkInputRange(10, 10, 10));
        assertTrue(checkInputRange(10, 90, 41));
        assertFalse(checkInputRange(17, 19, 14));
    }
    static inline void ComplexCheckInputTwoDigitRange() noexcept {
        assertTrue(checkInputRange(50, 10, 66));
        assertTrue(checkInputRange(99, 10, 99));
        assertFalse(checkInputRange(77, 22, 76));
        assertFalse(checkInputRange(30, 19, 29));
    }
    static inline void NotOverflowed() noexcept {
        assertFalse(isOverflow(static_cast<std::uint8_t>(50)
                               , static_cast<std::uint8_t>(50 + 50)));
        assertFalse(isOverflow(static_cast<std::int16_t>(50)
                               , static_cast<std::int16_t>(50 + 50)));
        assertFalse(isOverflow(static_cast<std::int64_t>(50)
                               , static_cast<std::int64_t>(50 + 50)));
    }
    static inline void Overflowed() noexcept {
        assertTrue(isOverflow((unsigned char)(50), (unsigned char)(50 + 250)));
        assertFalse(isOverflow(static_cast<std::int16_t>(50)
                       , static_cast<std::int16_t>(16'000 + 10'000)));
    }

public:
    void operator()() noexcept {
        SimpleDigitCount();
        ComplexDigitCount();
        InvalidMin();
        InvalidMax();
        FirstDigit();
        SecondDigit();
        ThirdDigit();
        InvalidCutTailMin();
        InvalidCutTailMax();
        FirstCutTail();
        SecondCutTail();
        SimpleConcat();
        ComplexConcat();
        ComplexConcat2();
        CutLeadMin();
        CutLeadMax();
        FirstCutLead();
        SecondCutLead();
        SimpleCheckInputRangeMinBorder();
        SimpleCheckInputRangeMaxBorder();
        SimpleCheckInputOneDigitRange();
        ComplexCheckInputOneDigitRange();
        SimpleCheckInputTwoDigitRange();
        ComplexCheckInputTwoDigitRange();
        NotOverflowed();
        Overflowed();
    }
};

} // end namespace ps2
