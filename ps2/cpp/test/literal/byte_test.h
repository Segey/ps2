/**
 * \file      ps2/ps2/cpp/test/literal/byte_test.h
 * \brief     The Byte_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 14:16 MSK
 * \updated   January (the) 19(th), 2017, 14:16 MSK
 * \TODO      
**/
#pragma once
#include <QString>
#include <ps2/cpp/literal/byte.h>

/** \namespace ps2::cpp */
namespace ps2 {
namespace cpp {
    
class ByteLiteralTest {
public:
    using class_name = ByteLiteralTest;

private:
    static void ValidByteTest() noexcept {
        assertTrue(10_b == 10);
        assertTrue(32_b == 32);
    }
    static void ValidKiloByteTest() noexcept {
        assertTrue(10_Kb == 10240);
        assertTrue(33_Kb == 33792);
    }
    static void ValidMegaByteTest() noexcept {
        assertTrue(11_Mb == 11'534'336);
        assertTrue(44_Mb == 46'137'344);
    }
    static void ValidGigaByteTest() noexcept {
        assertTrue(0_Gb == 0);
        assertTrue(44_Gb == 47'244'640'256);
    }

public:
    void operator()() noexcept {
        ValidByteTest();
        ValidKiloByteTest();
        ValidMegaByteTest();
        ValidGigaByteTest();
    }
};

}} // end namespace ps2::cpp
