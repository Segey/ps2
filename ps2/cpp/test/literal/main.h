/**
 * \file      ps2/ps2/cpp/test/literal/main.h
 * \brief     The Main class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 14:00 MSK
 * \updated   January (the) 19(th), 2017, 14:00 MSK
 * \TODO      
**/
#pragma once
#include "byte_test.h"

/** \namespace ps2::cpp */
namespace ps2 {
namespace cpp {
    
class LiteralsMainTestClasses {
public:
    using class_name = LiteralsMainTestClasses;

public:
    void operator()() noexcept {
        TEST(ByteLiteralTest);
    }
};

}} // end namespace ps2::cpp
