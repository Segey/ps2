/**
 * \file      perfect/projects/ps2/ps2/cpp/test/bitpack_test.h
 * \brief     The Bitpack_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 30(th), 2019, 23:17 MSK
 * \updated   January   (the) 30(th), 2019, 23:17 MSK
 * \TODO
**/
#pragma once
#include <set>
#include <ps2/cpp/pack/base64pack.h>

/** \namespace ps2 */
namespace ps2 {

TEST_CASE("Base64pack testing", "[base64pack<Size>]" ) {
    Base64Pack<10> _1;
    REQUIRE(_1.size() == 10);

    SECTION("Testing copy constructors") {
        auto const& _2 = _1;

        REQUIRE(_1.size() == 10);
        REQUIRE(_2.size() == 10);
    }
    SECTION("Testing asign ctor constructors") {
        Base64Pack<10> _2;
        _2 = _1;

        REQUIRE(_1.size() == 10);
        REQUIRE(_2.size() == 10);
    }
    SECTION("Testing equal operators") {
        Base64Pack<10> _2;
        _2 = _1;

        REQUIRE(_1 == _2);
    }
    SECTION("Testing no equal operators") {
        Base64Pack<10> _2;
        _2.set(6, 36);

        REQUIRE(_1 != _2);
    }
    SECTION("Testing setters and getters") {
        Base64Pack<10> _2;
        REQUIRE(_2.get(5) == 0);

        _2.set(5, 45);
        REQUIRE(_2.get(5) == 45);
        REQUIRE(_2[5] == 45);
    }
    SECTION("Testing ptint function") {
        Base64Pack<10> _2 {{0,8,5,63,32,24}};
        REQUIRE(_2.print() == std::string("0856332240000"));
    }
    SECTION("Testing simple pack function") {
        Base64Pack<10> _2 {{0,8,5,63,32,24}};
        REQUIRE(_2.pack() == std::string("085-vg0000"));
    }
    SECTION("Testing full range of chars") {
        std::set<std::string> s;

        for(auto i = 0u; i != 64u; ++i) {
            Base64Pack<1> _1 {{static_cast<std::uint8_t>(i)}};
            auto const& line = _1.pack();
            auto const& _2 = Base64Pack<1>::unpack(line);
            s.insert(line);
            REQUIRE(_1 == _2);
        }
        REQUIRE(s.size() == 64u);
    }
    SECTION("Testing less or more string size") {
        Base64Pack<1> _1 {{10}};
        auto const& line = _1.pack();
        auto const& _2 = Base64Pack<1>::unpack(line + 'A');
        REQUIRE(_1 == _2);

        auto const& _3 = Base64Pack<1>::unpack("");
        REQUIRE(_1 != _3);
    }
}

} // end namespace ps2
