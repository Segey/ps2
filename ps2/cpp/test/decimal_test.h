/**
 * \file      /home/dix/projects/perfect/projects/ps2/ps2/cpp/test/decimal_test.h 
 * \brief     The Decimal_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 04(th), 2017, 11:36 MSK
 * \updated   June      (the) 04(th), 2017, 11:36 MSK
 * \TODO      
**/
#pragma once
#include <iomanip>
#include <iostream>
#include <typeinfo>
#include <type_traits>
#include <unordered_map>
#include <ps2/cpp/decimal.h>
#include <lib/locale.h>
#include <ps2/cpp/constexpr_math.h>
#include <ps2/cpp/literal/literals.h>

#ifdef QT
#include <QString>
#include <QLocale>
#endif

/** \namespace ps2 */
namespace ps2 {

class DecimalTest final {
public:
    using class_name = DecimalTest;
    using nano_t     = ps2::decimal<long long int, 9>;
    //using nano_t     = ps2::decimal<std::int256_t, 9>;
    using val_t      = typename nano_t::integral_t;
    using fra_t      = typename nano_t::fraction_t;
    using milli_t    = ps2::decimal<int, 3>;
    using lmilli_t   = ps2::decimal<long long int, 3>;
    using deci_t     = ps2::decimal<int, 1>;

private:
    static inline void ClassicNumericLimits() noexcept {
        assertEquals(std::numeric_limits<milli_t>::is_integer, true);
        assertEquals(std::numeric_limits<milli_t>::min()
                                     , std::numeric_limits<milli_t>::min());
        assertEquals(std::numeric_limits<milli_t>::max()
                                     , std::numeric_limits<milli_t>::max());
        assertFalse(ps2::is_null(milli_t(4,5)));
        assertTrue(ps2::is_valid(milli_t(4,5)));
    }
    static inline void NumericLimits() noexcept {
//        assertEquals(std::numeric_limits<nano_t>::is_integer, false);
//        assertEquals(std::numeric_limits<nano_t>::min()
//                                     , std::numeric_limits<nano_t>::min());
//        assertEquals(std::numeric_limits<nano_t>::max()
//                                     , std::numeric_limits<nano_t>::max());
//        assertFalse(ps2::is_null(nano_t(4,5)));
//        assertTrue(ps2::is_valid(nano_t(4,5)));
    }
    static inline void Swap() noexcept {
        nano_t _1(45,3);
        nano_t _2(155,39);
        qSwap(_1,_2);
        assertEquals(_1, nano_t(155,39));
        assertEquals(_2, nano_t(45,3));
    }
    static inline void Ctors() noexcept {
        deci_t d0;
        assertEquals(d0, deci_t(0));
        assertTrue(d0 == milli_t(0));

        nano_t a1;
        assertEquals(a1, nano_t(0));
        assertTrue(a1 == milli_t(0));

        nano_t a2(77);
        assertEquals(a2, nano_t(77));
        assertEquals(a2.integral(), val_t(77));
        assertEquals(a2.fractional(), fra_t(0));
        assertEquals(a2.value(), val_t(77000000000));

        nano_t a3(nano_t(889));
        assertEquals(a3, nano_t(889));

        nano_t a4(35023222, 7893402);
        assertEquals(a4.integral(), val_t(35023222));
        assertEquals(a4.fractional(), fra_t(7893402));

        nano_t a41(-350, 7893402);
        assertEquals(a41.integral(), val_t(-350));
        assertEquals(a41.fractional(), fra_t(7893402));

        nano_t a5(554.666);
        assertEquals(a5.integral(), val_t(554));
        assertEquals(a5.fractional(), fra_t(666000000));

        nano_t a6(55364.0004003);
        assertEquals(a6.integral(), val_t(55364));
        assertEquals(a6.fractional(), fra_t(400299));

        nano_t a7(55364.4f);
        assertEquals(a7.integral(), val_t(55364));
        assertEquals(a7.fractional(), fra_t(398437500));
    }
    static inline void LeftShifts() noexcept {
        nano_t a1{0};
        for (auto c = 0; c != 5 ; ++c) {
            a1 <<= c;
            assertEquals(a1, nano_t(0));
        }

        nano_t a2{1};
        for (auto c = 1; c != 5 ; ++c)
            assertEquals((a2 << c), nano_t(std::pow(2, c)));

        for (auto c = 1; c != 5 ; ++c) {
            nano_t a3{1};
            a3 <<= c;
            assertEquals(a3, nano_t(std::pow(2, c)));
        }
    }
    static inline void RightShifts() noexcept {
        nano_t a1{0};
        for (auto c = 0; c != 5 ; ++c) {
            a1 >>= c;
            assertEquals(a1, nano_t(0));
        }

        nano_t a2{0, 1};
        a2 >>= 2;
        assertEquals((a2 >> 1), nano_t(0));

        nano_t a3{0, 1};
        a3 >>= 2;
        assertEquals(a3, nano_t(0));
    }
    static inline void Assign() noexcept {
        nano_t a1 = 55;
        assertEquals(a1, nano_t(55));
        assertEquals(a1.integral(), val_t(55));
        assertEquals(a1.fractional(), fra_t(0));

        nano_t a2 = 155.44;
        assertEquals(a2.integral(), val_t(155));
        assertEquals(a2.fractional(), fra_t(439999999));

        nano_t a3 = nano_t(155.44L);
        assertEquals(a3.integral(), val_t(155));
        assertEquals2(a3.fractional(), fra_t(439999999), fra_t(440000000));
    }
    static inline void Cast() noexcept {
        assertEquals(int(nano_t(18, 55)), 18);
        assertEquals(std::uint64_t(nano_t(18, 55)), std::uint64_t(18));
        assertDoubleEquals(double(nano_t(1024, 660000000)), 1024.66L);
        assertDoubleEquals(static_cast<long double>(nano_t(1024, 66000000)), 1024.066L);
        assertDoubleEquals(float(nano_t(24, 700000000)), 24.7f);
        assertDoubleEquals(double(nano_t(-1024, 660000000)), -1024.66L);

        assertEquals(std::int128_t(nano_t(-1024)), std::int128_t(-1024));
        assertEquals(std::int256_t(nano_t(-1024)), std::int256_t(-1024));
        assertEquals(std::int512_t(nano_t(-1024)), std::int512_t(-1024));
        assertEquals(std::uint128_t(nano_t(-1024)), std::uint128_t(-1024));
        assertEquals(std::uint256_t(nano_t(-1024)), std::uint256_t(-1024));
        assertEquals(std::uint512_t(nano_t(-1024)), std::uint512_t(-1024));
    }
    static inline void NativeOperators() noexcept {
        auto a1 = ~nano_t(0, 0xff);
        assertEquals(a1.fractional(), fra_t(256));

        auto a2 = -nano_t(0x1);
        assertEquals(a2, nano_t(-0x1));

        auto a3 = -nano_t(-0x1);
        assertEquals(a3, nano_t(0x1));

        auto a4 = +nano_t(-0x1);
        assertEquals(a4, nano_t(-0x1));
    }
    static inline void ClassicOperatorMultiply() noexcept {
        milli_t a1 = 2;
        assertEquals(a1, milli_t(2, 0));

        a1 *= milli_t(1, 44);
        assertEquals(a1, milli_t(2, 88));

        a1 *= 1;
        assertEquals(a1, milli_t(2, 88));

        a1 *= 3;
        assertEquals(a1, milli_t(6, 264));

        a1 *= 1.5;
        assertEquals(a1, milli_t(9, 396));

        a1 *= static_cast<int>(milli_t(7, 784));
        assertEquals(a1, milli_t(65, 772));
    }
    static inline void ClassicDecimalOperatorMultiply() noexcept {
        milli_t a1(1, 44);
        assertEquals(a1, milli_t(1, 44));

        a1 *= milli_t(54, 33);
        assertEquals(a1, milli_t(56, 410));

        a1 *= lmilli_t(2, 16);
        assertEquals(a1, milli_t(113, 722));

        a1 *= deci_t(3, 5);
        assertEquals(a1, milli_t(398, 27));

        deci_t d1(0, 4);
        assertEquals(d1, deci_t(0, 4));

        d1 *= deci_t(1, 6);
        assertEquals(d1, deci_t(0, 6));

        d1 *= lmilli_t(2, 16);
        assertEquals(d1, deci_t(1, 2));

        d1 *= milli_t(3, 555);
        assertEquals(d1, deci_t(4, 2));
    }
    static inline void ClassicIntMultiply() noexcept {
        auto a1 = 2 * milli_t(0x1, 44);
        assertEquals(a1, milli_t(0x2, 88));

        a1 = 1 * milli_t(1, 44) * 1;
        assertEquals(a1, milli_t(1, 44));

        a1 = milli_t(0x1, 44) * 3;
        assertEquals(a1, milli_t(0x3, 132));

        a1 = milli_t(0x1, 44) * 35;
        assertEquals(a1, milli_t(36, 540));

        a1 = 5 * static_cast<double>(milli_t(7, 784)) * 4;
        assertEquals(a1, milli_t(155, 680));

        a1 = 5 * static_cast<int>(milli_t(7, 784)) * 4;
        assertEquals(a1, milli_t(140, 0));
    }
    static inline void ClassicFloatMultiply() noexcept {
        auto a1 = 1.5 * milli_t(1, 44);
        assertEquals(a1, milli_t(1, 566));

        a1 = 1. * milli_t(1, 44) * 1.;
        assertEquals(a1, milli_t(1, 44));

        a1 = milli_t(1, 44) * 3.1;
        assertEquals(a1, milli_t(3, 236));

        a1 = milli_t(0x1, 44) * 35.01;
        assertEquals(a1, milli_t(36, 550));

        a1 = 32 * milli_t(1, 4) * 3.1;
        assertEquals(a1, milli_t(99, 596));

        a1 = 1.2 * milli_t(7, 784) * -22.5;
        assertEquals(a1, milli_t(-210, 150));

        a1 = 1.2 * static_cast<double>(milli_t(7, 784)) * 22.5;
        assertEquals(a1, milli_t(210, 168));
    }
    static inline void ClassicDecimalMultiply() noexcept {
        auto a1 = milli_t(1, 44) * milli_t(54, 33);
        assertEquals(a1, milli_t(56, 410));

        auto a2 = milli_t(3, 105) * lmilli_t(4, 3);
        assertEquals(a2, lmilli_t(12, 429));

        a2 = lmilli_t(3, 10) * milli_t(4, 30);
        assertEquals(a2, lmilli_t(12, 130));

        auto a3 = milli_t(3, 105) * deci_t(4, 3);
        assertEquals(a3, milli_t(13, 351));
    }
    static inline void WideIntMultiply() noexcept {
        auto a1 = 2 * nano_t(0x1, 44);
        assertEquals(a1, nano_t(0x2, 88));

        auto a2 = nano_t(44, 500) * 2;
        assertEquals(a2, nano_t(88, 1000));

//        a2 *= nano_t(1);
//        assertEquals(a2, nano_t(88, 1000));

//        a2 *= nano_t(2, 5);
//        assertEquals(a2, nano_t(176, 2440));

//        a2 *= 3;
//        assertEquals(a2, nano_t(528, 7320));

//        a2 *= 2 * 4;
//        assertEquals(a2, nano_t(4224, 58560));

//        a2 *= 1.53;
//        assertEquals(a2, nano_t(6462, 720089596));
    }
    static inline void ClassicOperatorDivide() noexcept {
        milli_t a1 = 102.4;
        assertEquals(a1, milli_t(102, 400));

        a1 /= milli_t(1, 44);
        assertEquals(a1, milli_t(98, 84));

        a1 /= 1;
        assertEquals(a1, milli_t(98, 84));

        a1 /= 3;
        assertEquals(a1, milli_t(32, 694));

        a1 /= 1.5;
        assertEquals(a1, milli_t(21, 796));

        a1 /= static_cast<int>(milli_t(7, 784));
        assertEquals(a1, milli_t(3, 113));
    }
    static inline void ClassicDecimalOperatorDivide() noexcept {
        milli_t a1(301, 144);

        a1 /= milli_t(6, 1);
        assertEquals(a1, milli_t(50, 182));

        a1 /= lmilli_t(2, 16);
        assertEquals(a1, milli_t(24, 891));

        a1 /= deci_t(3, 5);
        assertEquals(a1, milli_t(7, 111));

        deci_t d1(549, 1);
        assertEquals(d1, deci_t(549, 1));

        d1 /= deci_t(1, 6);
        assertEquals(d1, deci_t(343, 1));

        d1 /= lmilli_t(2, 16);
        assertEquals(d1, deci_t(171, 5));

        d1 /= milli_t(3, 555);
        assertEquals(d1, deci_t(49, 0));
    }
    static inline void ClassicIntDivide() noexcept {
        auto a1 = 22 / milli_t(1, 44);
        assertEquals(a1, milli_t(21, 72));

        a1 = 1 * milli_t(1, 44) / 1;
        assertEquals(a1, milli_t(1, 44));

        a1 = milli_t(1, 44) / 3;
        assertEquals(a1, milli_t(0, 348));

        a1 = milli_t(100, 4) / 35;
        assertEquals(a1, milli_t(2, 857));

        a1 = 5 * static_cast<double>(milli_t(7, 784)) / 4;
        assertEquals(a1, milli_t(9, 730));

        a1 = 5 * static_cast<int>(milli_t(7, 784)) / 4;
        assertEquals(a1, milli_t(8, 0));
    }
    static inline void ClassicFloatDivide() noexcept {
        auto a1 = 1.5 / milli_t(1, 44);
        assertEquals(a1, milli_t(1, 436));

        a1 = 1. * milli_t(1, 44) / 1.;
        assertEquals(a1, milli_t(1, 44));

        a1 = milli_t(71, 44) / 3.1;
        assertEquals(a1, milli_t(22, 917));

        a1 = milli_t(681, 1) / 35.01;
        assertEquals(a1, milli_t(19, 451));

        a1 = 32 * milli_t(1, 4) / 3.1;
        assertEquals(a1, milli_t(10, 363));

        a1 = 1.2 * milli_t(7, 784) / -22.5;
        assertEquals(a1, -milli_t(0, 415));

        a1 = 1.2 * static_cast<double>(milli_t(7, 784)) / 22.5;
        assertEquals(a1, milli_t(0, 415));
    }
    static inline void ClassicDecimalDivide() noexcept {
        auto a1 = milli_t(89, 44) / milli_t(4, 33);
        assertEquals(a1, milli_t(22, 78));

        auto a2 = milli_t(3, 105) / lmilli_t(4, 3);
        assertEquals(a2, lmilli_t(0, 775));

        a2 = lmilli_t(33, 10) / milli_t(4, 30);
        assertEquals(a2, lmilli_t(8, 191));

        auto a3 = milli_t(3, 105) / deci_t(4, 3);
        assertEquals(a3, milli_t(0, 722));
    }
    static inline void Divide() noexcept {
//        auto a1 = 16 / nano_t(0x2);
//        assertEquals(a1, nano_t(0x8, 0));

//        auto a7 = 4 / nano_t(0x2);
//        assertEquals(a7, nano_t(0x2, 0));

//        auto a8 = 16 / nano_t(0x2);
//        assertEquals(a8, nano_t(0x8, 0));

//        auto a7 = 4 / nano_t(0, 5);
//        assertEquals(a7, nano_t(3, 0));

    }
    static inline void ClassicOperatorPlus() noexcept {
        milli_t a1 = 2;
        assertEquals(a1, milli_t(2, 0));

        a1 += milli_t(1, 44);
        assertEquals(a1, milli_t(3, 44));

        a1 += 1;
        assertEquals(a1, milli_t(4, 44));

        a1 += 3.5;
        assertEquals(a1, milli_t(7, 544));

        a1 += static_cast<int>(milli_t(11, 1));
        assertEquals(a1, milli_t(18, 544));
    }
    static inline void ClassicDecimalOperatorPlus() noexcept {
        milli_t a1(1, 44);
        assertEquals(a1, milli_t(1, 44));

        a1 += milli_t(54, 33);
        assertEquals(a1, milli_t(55, 77));

        a1 += lmilli_t(2, 16);
        assertEquals(a1, milli_t(57, 93));

        a1 += deci_t(3, 5);
        assertEquals(a1, milli_t(60, 593));

        deci_t d1(0, 4);
        assertEquals(d1, deci_t(0, 4));

        d1 += deci_t(1, 6);
        assertEquals(d1, deci_t(2, 0));

        d1 += lmilli_t(2, 160);
        assertEquals(d1, deci_t(4, 1));

        d1 += milli_t(3, 555);
        assertEquals(d1, deci_t(7, 6));
    }
    static inline void ClassicDecimalPlus() noexcept {
        auto a0 = milli_t(1, 44) + 1;
        assertEquals(a0, milli_t(2, 44));

        a0 = milli_t(1, 44) + 1.5;
        assertEquals(a0, milli_t(2, 544));

        auto a1 = milli_t(1, 44) + milli_t(54, 33);
        assertEquals(a1, milli_t(55, 77));

        auto a2 = milli_t(3, 105) + lmilli_t(4, 3);
        assertEquals(a2, lmilli_t(7, 108));

        a2 = lmilli_t(3, 10) + milli_t(4, 30);
        assertEquals(a2, lmilli_t(7, 40));

        auto a3 = milli_t(3, 105) + deci_t(4, 3);
        assertEquals(a3, milli_t(7, 405));
    }
    static inline void WideIntPlus() noexcept {
        auto a1 = 2 + nano_t(1, 44);
        assertEquals(a1, nano_t(3, 44));

        auto a3 = nano_t(44, 500) + -2;
        assertEquals(a3, nano_t(42, 500));

        auto a2 = nano_t(44, 500) + 2;
        assertEquals(a2, nano_t(46, 500));

        a2 += nano_t(2, 5);
        assertEquals(a2, nano_t(48, 505));

        a2 += 3;
        assertEquals(a2, nano_t(51, 505));

        a2 += 2 + 4;
        assertEquals(a2, nano_t(57, 505));

//        a2 += 1.53;
//        assertEquals(a2, nano_t(58, 530000505));

//        a2 += a1 + 1.534;
//        assertEquals(a2, nano_t(63, 64000549));

//        a2 = a1 + 16;
//        assertEquals(a2, nano_t(19, 44));
    }
    static inline void ClassicOperatorMinus() noexcept {
        milli_t a1 = 23.5;
        assertEquals(a1, milli_t(23, 500));

        a1 -= milli_t(1, 44);
        assertEquals(a1, milli_t(22, 456));

        a1 -= 1;
        assertEquals(a1, milli_t(21, 456));

        a1 -= 3.5;
        assertEquals(a1, milli_t(17, 956));

        a1 -= static_cast<int>(milli_t(11, 1));
        assertEquals(a1, milli_t(6, 956));
    }
    static inline void ClassicDecimalOperatorMinus() noexcept {
        milli_t a1(21, 1);
        assertEquals(a1, milli_t(21, 1));

        a1 -= milli_t(54, 33);
        assertEquals(a1, milli_t(-33, 32));

        a1 -= lmilli_t(2, 16);
        assertEquals(a1, milli_t(-35, 48));

        a1 -= deci_t(3, 5);
        assertEquals(a1, -milli_t(38, 548));

        deci_t d1(100, 4);
        assertEquals(d1, deci_t(100, 4));

        d1 -= deci_t(1, 6);
        assertEquals(d1, deci_t(98, 8));

        d1 -= lmilli_t(2, 160);
        assertEquals(d1, deci_t(96, 7));

        d1 -= milli_t(3, 555);
        assertEquals(d1, deci_t(93, 2));
    }
    static inline void WideIntMinus() noexcept {
        auto a1 = 2 - nano_t(1);
        assertEquals(a1, nano_t(1));

        auto a2 = nano_t(44, 500) - 2;
        assertEquals(a2, nano_t(42, 500));

        a2 -= nano_t(2, 5);
        assertEquals(a2, nano_t(40, 495));

        a2 -= 3;
        assertEquals(a2, nano_t(37, 495));

        a2 -= 2 - 4;
        assertEquals(a2, nano_t(39, 495));

        a2 -= 1.53;
        assertEquals(a2, nano_t(37, 470000495));

        a2 -= a1 - 1.534;
        assertEquals(a2, nano_t(38, 4000495));

        a2 = a1 - 16;
        assertEquals(a2, nano_t(-15));
    }
    static inline void ClassicDecimalMinus() noexcept {
        auto a0 = milli_t(1, 44) - 1;
        assertEquals(a0, milli_t(0, 44));

        a0 = milli_t(1, 44) - 1.5;
        assertEquals(a0, -milli_t(0, 456));

        auto a1 = milli_t(100, 44) - milli_t(54, 33);
        assertEquals(a1, milli_t(46, 11));

        auto a2 = milli_t(3, 105) - lmilli_t(4, 3);
        assertEquals(a2, -lmilli_t(0, 898));

        a2 = lmilli_t(3, 10) - milli_t(4, 30);
        assertEquals(a2, lmilli_t(-1, 20));

        auto a3 = milli_t(3, 105) - deci_t(4, 3);
        assertEquals(a3, milli_t(-1, 195));
    }
    static inline void ToStdString() noexcept {
        assertEquals(ps2::to_string(nano_t(123455)), "123455.0"_s);
        assertEquals(ps2::to_string(nano_t(-123455)), "-123455.0"_s);
        assertEquals(ps2::to_string(nano_t(12,1)), "12.000000001"_s);
        assertEquals(ps2::to_string(nano_t(-123455, 66)), "-123455.000000066"_s);
        assertEquals(ps2::to_string(nano_t(-123455, 5466)), "-123455.000005466"_s);
    }
    static inline void FromStdString() noexcept {
        nano_t res;
        ps2::from_string("", res);
        assertEquals(res, nano_t(0));

        ps2::from_string("12.1.5", res);
        assertEquals(res, nano_t(0));

        ps2::from_string("123455", res);
        assertEquals(res, nano_t(123455));

        ps2::from_string("12.", res);
        assertEquals(res, nano_t(12));

        ps2::from_string("12.1", res);
        assertEquals(res, nano_t(12, 100000000));

        ps2::from_string("12.013", res);
        assertEquals(res, nano_t(12, 13000000));

        ps2::from_string("56.00000039", res);
        assertEquals(res, nano_t(56, 390));

        ps2::from_string("6.000000007", res);
        assertEquals(res, nano_t(6, 7));
    }
    static inline void ToSimpleQStr() noexcept {
        assertEquals(ps2::to_sqstr(nano_t(8)) , "8"_utf8);
        assertEquals(ps2::to_sqstr(nano_t(123455)) , "123455"_utf8);
        assertEquals(ps2::to_sqstr(nano_t(12,1)) , "12.000000001"_utf8);
        assertEquals(ps2::to_sqstr(nano_t(-123455, 66))
                                     , "-123455.000000066"_utf8);
    }
    static inline void ToSQStr() noexcept {
#ifdef QT
        assertEquals(ps2::to_qstr(nano_t()) , "0"_utf8);
        assertEquals(ps2::to_qstr(nano_t(8)) , "8"_utf8);
        assertEquals2(the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(123455)))
                 , ("123 455"_utf8), ("123,455"_utf8));
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::ru())
                     , "12,000000001"_utf8);
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::en())
                     , "12.000000001"_utf8);
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::de())
                     , "12,000000001"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::ru()))
                 , "-123 455,000001266"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::en()))
                 , "-123,455.000001266"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::de()))
                 , "-123.455,000001266"_utf8);
#endif
    }
    static inline void ToQStr() noexcept {
#ifdef QT
        assertEquals(ps2::to_qstr(nano_t()) , "0"_utf8);
        assertEquals(ps2::to_qstr(nano_t(8)) , "8"_utf8);
        assertEquals2(the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(123455)))
                 , ("123 455"_utf8), ("123,455"_utf8));
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::ru())
                     , "12,000000001"_utf8);
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::en())
                     , "12.000000001"_utf8);
        assertEquals(ps2::to_qstr(nano_t(12,1), test::Locale::de())
                     , "12,000000001"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::ru()))
                 , "-123 455,000001266"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::en()))
                 , "-123,455.000001266"_utf8);
        assertEquals(
            the::Base::updateNBSpaceToSpace(ps2::to_qstr(nano_t(-123455, 1266), test::Locale::de()))
                 , "-123.455,000001266"_utf8);
#endif
    }
    static inline void FromSQStr() noexcept {
#ifdef QT
        nano_t res;
        ps2::from_sqstr(QString(), res);
        assertEquals(res, nano_t(0));

        ps2::from_sqstr("8"_utf8, res);
        assertEquals(res, nano_t(8, 0));

        assertFalse(ps2::from_sqstr("12.1.5"_utf8, res));
        assertFalse(ps2::from_sqstr("12,013"_utf8, res));
        assertFalse(ps2::from_sqstr("1 2.013"_utf8, res));
        assertFalse(ps2::from_sqstr("12,0cool13"_utf8, res));

        deci_t d;
        assertFalse(ps2::from_sqstr("12.013"_utf8, d));
        assertFalse(ps2::from_sqstr("12.01"_utf8, d));

        ps2::from_sqstr("123455"_utf8, res);
        assertEquals(res, nano_t(123455, 0));

        ps2::from_sqstr("12."_utf8, res);
        assertEquals(res, nano_t(12));

        ps2::from_sqstr("12.1"_utf8, res);
        assertEquals(res, nano_t(12, 100000000));

        ps2::from_sqstr("12.013"_utf8, res);
        assertEquals(res, nano_t(12, 13000000));

        ps2::from_sqstr("56.00000039"_utf8, res);
        assertEquals(res, nano_t(56, 390));

        ps2::from_sqstr("6.000000007"_utf8, res);
        assertEquals(res, nano_t(6, 7));
#endif
    }
    static inline void FromQStr() noexcept {
#ifdef QT
        nano_t res;
        ps2::from_qstr(QString(), res);
        assertEquals(res, nano_t(0));

        ps2::from_qstr("8"_utf8, res);
        assertEquals(res, nano_t(8, 0));

        assertFalse(ps2::from_qstr("12.1.5"_utf8, res));
        assertFalse(ps2::from_qstr("12,0cool13"_utf8, res));
        assertFalse(ps2::from_qstr("1 2,013"_utf8, res, test::Locale::en()));

        deci_t d;
        assertFalse(ps2::from_qstr("12.013"_utf8, d));
        assertFalse(ps2::from_qstr("12.01"_utf8, d));

        ps2::from_qstr("12,013"_utf8, res, test::Locale::en());
        assertEquals(res, nano_t(12013,0));

        ps2::from_qstr("12,013"_utf8, res, test::Locale::de());
        assertEquals(res, nano_t(12,13000000));

        ps2::from_qstr("12,013"_utf8, res, test::Locale::ru());
        assertEquals(res, nano_t(12,13000000));

        ps2::from_qstr("123455"_utf8, res);
        assertEquals(res, nano_t(123455, 0));

        ps2::from_qstr("12."_utf8, res, test::Locale::en());
        assertEquals(res, nano_t(12));

        ps2::from_qstr("12.1"_utf8, res, test::Locale::en());
        assertEquals(res, nano_t(12, 100000000));

        ps2::from_qstr("1,556.00000039"_utf8, res, test::Locale::en());
        assertEquals(res, nano_t(1556, 390));

        ps2::from_qstr("1.556,00000039"_utf8, res, test::Locale::de());
        assertEquals(res, nano_t(1556, 390));

        ps2::from_qstr("1 556,00000039"_utf8, res, test::Locale::ru());
        assertEquals(res, nano_t(1556, 390));
#endif
    }

public:
    void operator()() noexcept {
        ClassicNumericLimits();
        NumericLimits();
        Swap();
        Ctors();
        LeftShifts();
        RightShifts();
        Assign();
        Cast();
        NativeOperators();

        ClassicOperatorMultiply();
        ClassicDecimalOperatorMultiply();
        ClassicIntMultiply();
        ClassicFloatMultiply();
        ClassicDecimalMultiply();
        WideIntMultiply();

        ClassicOperatorDivide();
        ClassicDecimalOperatorDivide();
        ClassicIntDivide();
        ClassicFloatDivide();
        ClassicDecimalDivide();
        Divide();

        ClassicOperatorPlus();
        ClassicDecimalOperatorPlus();
        ClassicDecimalPlus();
        WideIntPlus();

        ClassicOperatorMinus();
        ClassicDecimalOperatorMinus();
        ClassicDecimalMinus();
        WideIntMinus();

        ToStdString();
        FromStdString();
        ToSQStr();
        ToQStr();
        FromSQStr();
        FromQStr();
    }
};

} // end namespace ps2
