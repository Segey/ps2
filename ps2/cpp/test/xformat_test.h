/**
 * \file      c:/projects/perfect/projects/ps2/ps2/cpp/test/xformat_test.h 
 * \brief     The Xformat_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   February  (the) 07(th), 2019, 18:08 MSK
 * \updated   February  (the) 07(th), 2019, 18:08 MSK
 * \TODO      
**/
#pragma once
#include <iostream>
#include <ps2/cpp/xformat.h>

/** \namespace ps2 */
namespace ps2 {

TEST_CASE("Xformat testing", "[xformat]" ) {
    auto const& str = ps2::xformat(L"hello super %i boy %s %b%%", 77u, L"Row", true);
    REQUIRE(str == L"hello super 77 boy Row 1%");

    SECTION("Simple string testing") {
        auto const& s = ps2::xformat(L"hello %s", L"Jhon");
        REQUIRE(s == L"hello Jhon");
    }
    SECTION("Complex string testing") {
        auto const& s = ps2::xformat(L"Hello %s %s.", L"Сергей", L"Александрович");
        REQUIRE(s == L"Hello Сергей Александрович.");
    }
    SECTION("Complex string testing") {
        auto const& s = ps2::xformat(L"Hello %s %s.", L"Сергей", L"Александрович");
        REQUIRE(s == L"Hello Сергей Александрович.");
    }
    SECTION("Invalid parameter passed") {
        REQUIRE_THROWS(ps2::xformat(L"Hello %s %s.%", L"Сергей", L"Александрович"));
    }
    SECTION("Added more parameters") {
        REQUIRE_THROWS(ps2::xformat(L"Hello %s", L"Сергей", L"Александрович"));
    }
}

} // end namespace ps2
