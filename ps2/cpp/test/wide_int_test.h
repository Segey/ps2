/**
 * \file      C:/projects/perfect/projects/ps2/ps2/cpp/test/u256_test.h 
 * \brief     The U256_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   May       (the) 30(th), 2017, 23:56 MSK
 * \updated   May       (the) 30(th), 2017, 23:56 MSK
 * \TODO      
**/
#pragma once
#include <iomanip>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <ps2/cpp/wide_int.h>
#include <ps2/cpp/literal/literals.h>

/** \namespace ps2 */
namespace ps2 {

class WideIntTest final {
public:
    using class_name = WideIntTest;

private:
    template <class T>
    static inline void checkFromChars(int64_t expect, int base, const std::string& str) noexcept {
        T num = 0;
        std::from_chars_result res = from_chars(str.data(), str.data() + str.size(), num, base);
        assertTrue(!res.ec);
        assertTrue(expect == num);
    }

private:
    static inline void Ctors() noexcept {
        int512_t a1 = 0;
        for (auto c : a1.m_arr)
            assertEquals(c, static_cast<uint8_t>(0));

        uint512_t b1 = 0;
        for (auto c : b1.m_arr)
            assertEquals(c, static_cast<uint8_t>(0));

        int512_t a2 = 13;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], static_cast<uint8_t>(0));

        assertEquals(a2.m_arr[63], static_cast<uint8_t>(13));

        uint512_t b2 = 13;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b2.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(b2.m_arr[63], static_cast<uint8_t>(13));

        int512_t a3 = -13;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a3.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(a3.m_arr[63], static_cast<uint8_t>(0xf3));

        uint512_t b3 = -13;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b3.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(b3.m_arr[63], static_cast<uint8_t>(0xf3));

        int512_t a4 = int128_t(13);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a4.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(a4.m_arr[63], static_cast<uint8_t>(13));

        uint512_t b4 = uint128_t(13);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b4.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(b4.m_arr[63], static_cast<uint8_t>(13));

        int512_t a5 = uint128_t(13);
        for (int idx = 0; idx < 7; ++idx)
            assertEquals(a5.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(a5.m_arr[63], static_cast<uint8_t>(13));

        uint512_t b5 = int128_t(13);
        for (int idx = 0; idx < 7; ++idx)
            assertEquals(b5.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(b5.m_arr[63], static_cast<uint8_t>(13));

        int512_t a6 = uint128_t(-13);
        for (int idx = 0; idx < 48; ++idx)
            assertEquals(a6.m_arr[idx], static_cast<uint8_t>(0));

        for (int idx = 48; idx < 63; ++idx)
            assertEquals(a6.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(a6.m_arr[63], static_cast<uint8_t>(0xf3));

        uint512_t b6 = int128_t(-13);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b6.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(b6.m_arr[63], static_cast<uint8_t>(0xf3));

        int512_t a7 = int128_t(-13);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a7.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(a7.m_arr[63], static_cast<uint8_t>(0xf3));

        uint512_t b7 = uint128_t(-13);
        for (int idx = 0; idx < 48; ++idx)
            assertEquals(b7.m_arr[idx], static_cast<uint8_t>(0));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b7.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(b7.m_arr[63], static_cast<uint8_t>(0xf3));

        int512_t a8 = -13.0;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a8.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(a8.m_arr[63], static_cast<uint8_t>(0xf3));

        uint512_t b8 = -13.0;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b8.m_arr[idx], static_cast<uint8_t>(0xff));
        assertEquals(b8.m_arr[63], static_cast<uint8_t>(0xf3));

        int512_t a9 = 13.0;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a9.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(a9.m_arr[63], static_cast<uint8_t>(13));

        uint512_t b9 = 13.0;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b9.m_arr[idx], static_cast<uint8_t>(0));
        assertEquals(b9.m_arr[63], static_cast<uint8_t>(13));
    }
    static inline void LeftShifts() noexcept {
        uint512_t a1 = 0;
        for (auto c : uint512_t::_impl::shift_left(a1, 0).m_arr)
            assertEquals(c, uint8_t(0));

        uint512_t a2 = 0;
        for (auto c : uint512_t::_impl::shift_left(a2, 15).m_arr)
            assertEquals(c, uint8_t(0));

        uint512_t a3 = 0;
        a3.m_arr[56] = 0xf1;
        a3.m_arr[57] = 0xf2;
        a3.m_arr[58] = 0xf3;
        a3.m_arr[59] = 0xf4;
        a3.m_arr[60] = 0xf5;
        a3.m_arr[61] = 0xf6;
        a3.m_arr[62] = 0xf7;
        a3.m_arr[63] = 0xf8;
        uint512_t a31 = uint512_t::_impl::shift_left(a3, 8);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a31.m_arr[idx], uint8_t(0));

        assertEquals(a31.m_arr[55], uint8_t(0xf1));
        assertEquals(a31.m_arr[56], uint8_t(0xf2));
        assertEquals(a31.m_arr[57], uint8_t(0xf3));
        assertEquals(a31.m_arr[58], uint8_t(0xf4));
        assertEquals(a31.m_arr[59], uint8_t(0xf5));
        assertEquals(a31.m_arr[60], uint8_t(0xf6));
        assertEquals(a31.m_arr[61], uint8_t(0xf7));
        assertEquals(a31.m_arr[62], uint8_t(0xf8));
        assertEquals(a31.m_arr[63], uint8_t(0x00));

        uint512_t a32 = uint512_t::_impl::shift_left(a3, 16);
        for (int idx = 0; idx < 54; ++idx)
            assertEquals(a32.m_arr[idx], uint8_t(0));

        assertEquals(a32.m_arr[54], uint8_t(0xf1));
        assertEquals(a32.m_arr[55], uint8_t(0xf2));
        assertEquals(a32.m_arr[56], uint8_t(0xf3));
        assertEquals(a32.m_arr[57], uint8_t(0xf4));
        assertEquals(a32.m_arr[58], uint8_t(0xf5));
        assertEquals(a32.m_arr[59], uint8_t(0xf6));
        assertEquals(a32.m_arr[60], uint8_t(0xf7));
        assertEquals(a32.m_arr[61], uint8_t(0xf8));
        assertEquals(a32.m_arr[62], uint8_t(0x00));
        assertEquals(a32.m_arr[63], uint8_t(0x00));

        uint512_t a33 = uint512_t::_impl::shift_left(a3, 4);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a33.m_arr[idx], uint8_t(0));

        assertEquals(a33.m_arr[55], uint8_t(0x0f));
        assertEquals(a33.m_arr[56], uint8_t(0x1f));
        assertEquals(a33.m_arr[57], uint8_t(0x2f));
        assertEquals(a33.m_arr[58], uint8_t(0x3f));
        assertEquals(a33.m_arr[59], uint8_t(0x4f));
        assertEquals(a33.m_arr[60], uint8_t(0x5f));
        assertEquals(a33.m_arr[61], uint8_t(0x6f));
        assertEquals(a33.m_arr[62], uint8_t(0x7f));
        assertEquals(a33.m_arr[63], uint8_t(0x80));

        uint512_t a34 = uint512_t::_impl::shift_left(a3, 64);
        for (int idx = 0; idx < 48; ++idx)
            assertEquals(a34.m_arr[idx], uint8_t(0));

        assertEquals(a34.m_arr[48], uint8_t(0xf1));
        assertEquals(a34.m_arr[49], uint8_t(0xf2));
        assertEquals(a34.m_arr[50], uint8_t(0xf3));
        assertEquals(a34.m_arr[51], uint8_t(0xf4));
        assertEquals(a34.m_arr[52], uint8_t(0xf5));
        assertEquals(a34.m_arr[53], uint8_t(0xf6));
        assertEquals(a34.m_arr[54], uint8_t(0xf7));
        assertEquals(a34.m_arr[55], uint8_t(0xf8));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a34.m_arr[idx], uint8_t(0));

        uint512_t a35 = uint512_t::_impl::shift_left(a3, 256);
        for (int idx = 0; idx < 24; ++idx)
            assertEquals(a35.m_arr[idx], uint8_t(0));

        assertEquals(a35.m_arr[24], uint8_t(0xf1));
        assertEquals(a35.m_arr[25], uint8_t(0xf2));
        assertEquals(a35.m_arr[26], uint8_t(0xf3));
        assertEquals(a35.m_arr[27], uint8_t(0xf4));
        assertEquals(a35.m_arr[28], uint8_t(0xf5));
        assertEquals(a35.m_arr[29], uint8_t(0xf6));
        assertEquals(a35.m_arr[30], uint8_t(0xf7));
        assertEquals(a35.m_arr[31], uint8_t(0xf8));
        for (int idx = 32; idx < 63; ++idx)
            assertEquals(a35.m_arr[idx], uint8_t(0));

        uint512_t a36 = uint512_t::_impl::shift_left(a3, 260);
        for (int idx = 0; idx < 23; ++idx)
            assertEquals(a36.m_arr[idx], uint8_t(0));
        assertEquals(a36.m_arr[23], uint8_t(0x0f));
        assertEquals(a36.m_arr[24], uint8_t(0x1f));
        assertEquals(a36.m_arr[25], uint8_t(0x2f));
        assertEquals(a36.m_arr[26], uint8_t(0x3f));
        assertEquals(a36.m_arr[27], uint8_t(0x4f));
        assertEquals(a36.m_arr[28], uint8_t(0x5f));
        assertEquals(a36.m_arr[29], uint8_t(0x6f));
        assertEquals(a36.m_arr[30], uint8_t(0x7f));
        assertEquals(a36.m_arr[31], uint8_t(0x80));

        for (int idx = 32; idx < 63; ++idx)
            assertEquals(a36.m_arr[idx], uint8_t(0));
    }
    static inline void RightShifts() noexcept {
        uint512_t a4 = 0;
        for (auto c : uint512_t::_impl::shift_right(a4, 0).m_arr)
            assertEquals(c, uint8_t(0));

        uint512_t a5 = 0;
        for (auto c : uint512_t::_impl::shift_right(a5, 15).m_arr)
            assertEquals(c, uint8_t(0));

        uint512_t a6 = 0;
        a6.m_arr[56] = 0xf1;
        a6.m_arr[57] = 0xf2;
        a6.m_arr[58] = 0xf3;
        a6.m_arr[59] = 0xf4;
        a6.m_arr[60] = 0xf5;
        a6.m_arr[61] = 0xf6;
        a6.m_arr[62] = 0xf7;
        a6.m_arr[63] = 0xf8;
        a6.m_arr[0] = 0xf1;
        a6.m_arr[1] = 0xf2;
        a6.m_arr[2] = 0xf3;
        a6.m_arr[3] = 0xf4;
        a6.m_arr[4] = 0xf5;
        a6.m_arr[5] = 0xf6;
        a6.m_arr[6] = 0xf7;
        a6.m_arr[7] = 0xf8;
        uint512_t a61 = uint512_t::_impl::shift_right(a6, 8);
        assertEquals(a61.m_arr[0], uint8_t(0));
        assertEquals(a61.m_arr[1], uint8_t(0xf1));
        assertEquals(a61.m_arr[2], uint8_t(0xf2));
        assertEquals(a61.m_arr[3], uint8_t(0xf3));
        assertEquals(a61.m_arr[4], uint8_t(0xf4));
        assertEquals(a61.m_arr[5], uint8_t(0xf5));
        assertEquals(a61.m_arr[6], uint8_t(0xf6));
        assertEquals(a61.m_arr[7], uint8_t(0xf7));
        assertEquals(a61.m_arr[8], uint8_t(0xf8));
        for (int idx = 9; idx < 57; ++idx)
            assertEquals(a61.m_arr[idx], uint8_t(0));

        assertEquals(a61.m_arr[57], uint8_t(0xf1));
        assertEquals(a61.m_arr[58], uint8_t(0xf2));
        assertEquals(a61.m_arr[59], uint8_t(0xf3));
        assertEquals(a61.m_arr[60], uint8_t(0xf4));
        assertEquals(a61.m_arr[61], uint8_t(0xf5));
        assertEquals(a61.m_arr[62], uint8_t(0xf6));
        assertEquals(a61.m_arr[63], uint8_t(0xf7));

        uint512_t a62 = uint512_t::_impl::shift_right(a6, 16);
        assertEquals(a62.m_arr[0], uint8_t(0));
        assertEquals(a62.m_arr[1], uint8_t(0));
        assertEquals(a62.m_arr[2], uint8_t(0xf1));
        assertEquals(a62.m_arr[3], uint8_t(0xf2));
        assertEquals(a62.m_arr[4], uint8_t(0xf3));
        assertEquals(a62.m_arr[5], uint8_t(0xf4));
        assertEquals(a62.m_arr[6], uint8_t(0xf5));
        assertEquals(a62.m_arr[7], uint8_t(0xf6));
        assertEquals(a62.m_arr[8], uint8_t(0xf7));
        assertEquals(a62.m_arr[9], uint8_t(0xf8));

        for (int idx = 10; idx < 58; ++idx)
            assertEquals(a62.m_arr[idx], uint8_t(0));
        assertEquals(a62.m_arr[58], uint8_t(0xf1));
        assertEquals(a62.m_arr[59], uint8_t(0xf2));
        assertEquals(a62.m_arr[60], uint8_t(0xf3));
        assertEquals(a62.m_arr[61], uint8_t(0xf4));
        assertEquals(a62.m_arr[62], uint8_t(0xf5));
        assertEquals(a62.m_arr[63], uint8_t(0xf6));

       uint512_t a63 = uint512_t::_impl::shift_right(a6, 4);
        assertEquals(a63.m_arr[0], uint8_t(0x0f));
        assertEquals(a63.m_arr[1], uint8_t(0x1f));
        assertEquals(a63.m_arr[2], uint8_t(0x2f));
        assertEquals(a63.m_arr[3], uint8_t(0x3f));
        assertEquals(a63.m_arr[4], uint8_t(0x4f));
        assertEquals(a63.m_arr[5], uint8_t(0x5f));
        assertEquals(a63.m_arr[6], uint8_t(0x6f));
        assertEquals(a63.m_arr[7], uint8_t(0x7f));
        assertEquals(a63.m_arr[8], uint8_t(0x80));

        for (int idx = 9; idx < 56; ++idx)
            assertEquals(a63.m_arr[idx], uint8_t(0));

        assertEquals(a63.m_arr[56], uint8_t(0x0f));
        assertEquals(a63.m_arr[57], uint8_t(0x1f));
        assertEquals(a63.m_arr[58], uint8_t(0x2f));
        assertEquals(a63.m_arr[59], uint8_t(0x3f));
        assertEquals(a63.m_arr[60], uint8_t(0x4f));
        assertEquals(a63.m_arr[61], uint8_t(0x5f));
        assertEquals(a63.m_arr[62], uint8_t(0x6f));
        assertEquals(a63.m_arr[63], uint8_t(0x7f));

        uint512_t a64 = uint512_t::_impl::shift_right(a6, 64);
        for (int idx = 0; idx < 8; ++idx)
            assertEquals(a64.m_arr[idx], uint8_t(0));

        assertEquals(a64.m_arr[8], uint8_t(0xf1));
        assertEquals(a64.m_arr[9], uint8_t(0xf2));
        assertEquals(a64.m_arr[10], uint8_t(0xf3));
        assertEquals(a64.m_arr[11], uint8_t(0xf4));
        assertEquals(a64.m_arr[12], uint8_t(0xf5));
        assertEquals(a64.m_arr[13], uint8_t(0xf6));
        assertEquals(a64.m_arr[14], uint8_t(0xf7));
        assertEquals(a64.m_arr[15], uint8_t(0xf8));

        for (int idx = 16; idx < 63; ++idx)
            assertEquals(a64.m_arr[idx], uint8_t(0));

        uint512_t a65 = uint512_t::_impl::shift_right(a6, 256);
        for (int idx = 0; idx < 32; ++idx)
            assertEquals(a65.m_arr[idx], uint8_t(0));

        assertEquals(a65.m_arr[32], uint8_t(0xf1));
        assertEquals(a65.m_arr[33], uint8_t(0xf2));
        assertEquals(a65.m_arr[34], uint8_t(0xf3));
        assertEquals(a65.m_arr[35], uint8_t(0xf4));
        assertEquals(a65.m_arr[36], uint8_t(0xf5));
        assertEquals(a65.m_arr[37], uint8_t(0xf6));
        assertEquals(a65.m_arr[38], uint8_t(0xf7));
        assertEquals(a65.m_arr[39], uint8_t(0xf8));

        for (int idx = 40; idx < 63; ++idx)
            assertEquals(a65.m_arr[idx], uint8_t(0));

        uint512_t a66 = uint512_t::_impl::shift_right(a6, 260);
        for (int idx = 0; idx < 32; ++idx)
            assertEquals(a66.m_arr[idx], uint8_t(0));

        assertEquals(a66.m_arr[32], uint8_t(0x0f));
        assertEquals(a66.m_arr[33], uint8_t(0x1f));
        assertEquals(a66.m_arr[34], uint8_t(0x2f));
        assertEquals(a66.m_arr[35], uint8_t(0x3f));
        assertEquals(a66.m_arr[36], uint8_t(0x4f));
        assertEquals(a66.m_arr[37], uint8_t(0x5f));
        assertEquals(a66.m_arr[38], uint8_t(0x6f));
        assertEquals(a66.m_arr[39], uint8_t(0x7f));
        assertEquals(a66.m_arr[40], uint8_t(0x80));

        for (int idx = 41; idx < 63; ++idx)
            assertEquals(a66.m_arr[idx], uint8_t(0));
    }
    static inline void RightShiftsMoreZero() noexcept {
        int512_t b4 = 0;
        for (auto c : int512_t::_impl::shift_right(b4, 0).m_arr)
            assertEquals(c, uint8_t(0));

        int512_t b5 = 0;
        for (auto c : int512_t::_impl::shift_right(b5, 15).m_arr)
            assertEquals(c, uint8_t(0));

        int512_t b6 = 0;
        b6.m_arr[0] = 0x71;
        b6.m_arr[1] = 0xf2;
        b6.m_arr[2] = 0xf3;
        b6.m_arr[3] = 0xf4;
        b6.m_arr[4] = 0xf5;
        b6.m_arr[5] = 0xf6;
        b6.m_arr[6] = 0xf7;
        b6.m_arr[7] = 0xf8;
        b6.m_arr[56] = 0xf1;
        b6.m_arr[57] = 0xf2;
        b6.m_arr[58] = 0xf3;
        b6.m_arr[59] = 0xf4;
        b6.m_arr[60] = 0xf5;
        b6.m_arr[61] = 0xf6;
        b6.m_arr[62] = 0xf7;
        b6.m_arr[63] = 0xf8;

        int512_t b61 = int512_t::_impl::shift_right(b6, 8);
        assertEquals(b61.m_arr[0], uint8_t(0));
        assertEquals(b61.m_arr[1], uint8_t(0x71));
        assertEquals(b61.m_arr[2], uint8_t(0xf2));
        assertEquals(b61.m_arr[3], uint8_t(0xf3));
        assertEquals(b61.m_arr[4], uint8_t(0xf4));
        assertEquals(b61.m_arr[5], uint8_t(0xf5));
        assertEquals(b61.m_arr[6], uint8_t(0xf6));
        assertEquals(b61.m_arr[7], uint8_t(0xf7));
        assertEquals(b61.m_arr[8], uint8_t(0xf8));

        for (int idx = 9; idx < 57; ++idx)
            assertEquals(b61.m_arr[idx], uint8_t(0));

        assertEquals(b61.m_arr[57], uint8_t(0xf1));
        assertEquals(b61.m_arr[58], uint8_t(0xf2));
        assertEquals(b61.m_arr[59], uint8_t(0xf3));
        assertEquals(b61.m_arr[60], uint8_t(0xf4));
        assertEquals(b61.m_arr[61], uint8_t(0xf5));
        assertEquals(b61.m_arr[62], uint8_t(0xf6));
        assertEquals(b61.m_arr[63], uint8_t(0xf7));

        int512_t b62 = int512_t::_impl::shift_right(b6, 16);
        assertEquals(b62.m_arr[0], uint8_t(0));
        assertEquals(b62.m_arr[1], uint8_t(0));
        assertEquals(b62.m_arr[2], uint8_t(0x71));
        assertEquals(b62.m_arr[3], uint8_t(0xf2));
        assertEquals(b62.m_arr[4], uint8_t(0xf3));
        assertEquals(b62.m_arr[5], uint8_t(0xf4));
        assertEquals(b62.m_arr[6], uint8_t(0xf5));
        assertEquals(b62.m_arr[7], uint8_t(0xf6));
        assertEquals(b62.m_arr[8], uint8_t(0xf7));
        assertEquals(b62.m_arr[9], uint8_t(0xf8));

        for (int idx = 10; idx < 58; ++idx)
            assertEquals(b62.m_arr[idx], uint8_t(0));

        assertEquals(b62.m_arr[58], uint8_t(0xf1));
        assertEquals(b62.m_arr[59], uint8_t(0xf2));
        assertEquals(b62.m_arr[60], uint8_t(0xf3));
        assertEquals(b62.m_arr[61], uint8_t(0xf4));
        assertEquals(b62.m_arr[62], uint8_t(0xf5));
        assertEquals(b62.m_arr[63], uint8_t(0xf6));

        int512_t b63 = int512_t::_impl::shift_right(b6, 4);
        assertEquals(b63.m_arr[0], uint8_t(0x07));
        assertEquals(b63.m_arr[1], uint8_t(0x1f));
        assertEquals(b63.m_arr[2], uint8_t(0x2f));
        assertEquals(b63.m_arr[3], uint8_t(0x3f));
        assertEquals(b63.m_arr[4], uint8_t(0x4f));
        assertEquals(b63.m_arr[5], uint8_t(0x5f));
        assertEquals(b63.m_arr[6], uint8_t(0x6f));
        assertEquals(b63.m_arr[7], uint8_t(0x7f));
        assertEquals(b63.m_arr[8], uint8_t(0x80));

        for (int idx = 9; idx < 56; ++idx)
            assertEquals(b63.m_arr[idx], uint8_t(0));

        assertEquals(b63.m_arr[56], uint8_t(0x0f));
        assertEquals(b63.m_arr[57], uint8_t(0x1f));
        assertEquals(b63.m_arr[58], uint8_t(0x2f));
        assertEquals(b63.m_arr[59], uint8_t(0x3f));
        assertEquals(b63.m_arr[60], uint8_t(0x4f));
        assertEquals(b63.m_arr[61], uint8_t(0x5f));
        assertEquals(b63.m_arr[62], uint8_t(0x6f));
        assertEquals(b63.m_arr[63], uint8_t(0x7f));

        int512_t b64 = int512_t::_impl::shift_right(b6, 64);
        assertEquals(b64.m_arr[8], uint8_t(0x71));
        assertEquals(b64.m_arr[9], uint8_t(0xf2));
        assertEquals(b64.m_arr[10], uint8_t(0xf3));
        assertEquals(b64.m_arr[11], uint8_t(0xf4));
        assertEquals(b64.m_arr[12], uint8_t(0xf5));
        assertEquals(b64.m_arr[13], uint8_t(0xf6));
        assertEquals(b64.m_arr[14], uint8_t(0xf7));
        assertEquals(b64.m_arr[15], uint8_t(0xf8));

        for (int idx = 16; idx < 63; ++idx)
            assertEquals(b64.m_arr[idx], uint8_t(0));

        int512_t b65 = int512_t::_impl::shift_right(b6, 256);
        for (int idx = 0; idx < 32; ++idx)
            assertEquals(b65.m_arr[idx], uint8_t(0));

        assertEquals(b65.m_arr[32], uint8_t(0x71));
        assertEquals(b65.m_arr[33], uint8_t(0xf2));
        assertEquals(b65.m_arr[34], uint8_t(0xf3));
        assertEquals(b65.m_arr[35], uint8_t(0xf4));
        assertEquals(b65.m_arr[36], uint8_t(0xf5));
        assertEquals(b65.m_arr[37], uint8_t(0xf6));
        assertEquals(b65.m_arr[38], uint8_t(0xf7));
        assertEquals(b65.m_arr[39], uint8_t(0xf8));

        for (int idx = 40; idx < 63; ++idx)
            assertEquals(b65.m_arr[idx], uint8_t(0));

        int512_t b66 = int512_t::_impl::shift_right(b6, 260);
        for (int idx = 0; idx < 32; ++idx)
            assertEquals(b66.m_arr[idx], uint8_t(0));

        assertEquals(b66.m_arr[32], uint8_t(0x07));
        assertEquals(b66.m_arr[33], uint8_t(0x1f));
        assertEquals(b66.m_arr[34], uint8_t(0x2f));
        assertEquals(b66.m_arr[35], uint8_t(0x3f));
        assertEquals(b66.m_arr[36], uint8_t(0x4f));
        assertEquals(b66.m_arr[37], uint8_t(0x5f));
        assertEquals(b66.m_arr[38], uint8_t(0x6f));
        assertEquals(b66.m_arr[39], uint8_t(0x7f));
        assertEquals(b66.m_arr[40], uint8_t(0x80));

        for (int idx = 41; idx < 63; ++idx)
            assertEquals(b66.m_arr[idx], uint8_t(0));
    }
    static inline void RightShiftsLessZero() noexcept {
        int512_t b7 = 0;
        b7.m_arr[0] = 0xf1;
        b7.m_arr[1] = 0xf2;
        b7.m_arr[2] = 0xf3;
        b7.m_arr[3] = 0xf4;
        b7.m_arr[4] = 0xf5;
        b7.m_arr[5] = 0xf6;
        b7.m_arr[6] = 0xf7;
        b7.m_arr[7] = 0xf8;
        b7.m_arr[56] = 0xf1;
        b7.m_arr[57] = 0xf2;
        b7.m_arr[58] = 0xf3;
        b7.m_arr[59] = 0xf4;
        b7.m_arr[60] = 0xf5;
        b7.m_arr[61] = 0xf6;
        b7.m_arr[62] = 0xf7;
        b7.m_arr[63] = 0xf8;
        int512_t b71 = int512_t::_impl::shift_right(b7, 8);
        assertEquals(b71.m_arr[0], uint8_t(0xff));
        assertEquals(b71.m_arr[1], uint8_t(0xf1));
        assertEquals(b71.m_arr[2], uint8_t(0xf2));
        assertEquals(b71.m_arr[3], uint8_t(0xf3));
        assertEquals(b71.m_arr[4], uint8_t(0xf4));
        assertEquals(b71.m_arr[5], uint8_t(0xf5));
        assertEquals(b71.m_arr[6], uint8_t(0xf6));
        assertEquals(b71.m_arr[7], uint8_t(0xf7));
        assertEquals(b71.m_arr[8], uint8_t(0xf8));

        for (int idx = 9; idx < 57; ++idx)
            assertEquals(b71.m_arr[idx], uint8_t(0));

        assertEquals(b71.m_arr[57], uint8_t(0xf1));
        assertEquals(b71.m_arr[58], uint8_t(0xf2));
        assertEquals(b71.m_arr[59], uint8_t(0xf3));
        assertEquals(b71.m_arr[60], uint8_t(0xf4));
        assertEquals(b71.m_arr[61], uint8_t(0xf5));
        assertEquals(b71.m_arr[62], uint8_t(0xf6));
        assertEquals(b71.m_arr[63], uint8_t(0xf7));

        int512_t b72 = int512_t::_impl::shift_right(b7, 16);
        assertEquals(b72.m_arr[0], uint8_t(0xff));
        assertEquals(b72.m_arr[1], uint8_t(0xff));
        assertEquals(b72.m_arr[2], uint8_t(0xf1));
        assertEquals(b72.m_arr[3], uint8_t(0xf2));
        assertEquals(b72.m_arr[4], uint8_t(0xf3));
        assertEquals(b72.m_arr[5], uint8_t(0xf4));
        assertEquals(b72.m_arr[6], uint8_t(0xf5));
        assertEquals(b72.m_arr[7], uint8_t(0xf6));
        assertEquals(b72.m_arr[8], uint8_t(0xf7));
        assertEquals(b72.m_arr[9], uint8_t(0xf8));

        for (int idx = 10; idx < 58; ++idx)
            assertEquals(b72.m_arr[idx], uint8_t(0));

        assertEquals(b72.m_arr[58], uint8_t(0xf1));
        assertEquals(b72.m_arr[59], uint8_t(0xf2));
        assertEquals(b72.m_arr[60], uint8_t(0xf3));
        assertEquals(b72.m_arr[61], uint8_t(0xf4));
        assertEquals(b72.m_arr[62], uint8_t(0xf5));
        assertEquals(b72.m_arr[63], uint8_t(0xf6));

        int512_t b73 = int512_t::_impl::shift_right(b7, 4);
        assertEquals(b73.m_arr[0], uint8_t(0xff));
        assertEquals(b73.m_arr[1], uint8_t(0x1f));
        assertEquals(b73.m_arr[2], uint8_t(0x2f));
        assertEquals(b73.m_arr[3], uint8_t(0x3f));
        assertEquals(b73.m_arr[4], uint8_t(0x4f));
        assertEquals(b73.m_arr[5], uint8_t(0x5f));
        assertEquals(b73.m_arr[6], uint8_t(0x6f));
        assertEquals(b73.m_arr[7], uint8_t(0x7f));
        assertEquals(b73.m_arr[8], uint8_t(0x80));

        for (int idx = 9; idx < 56; ++idx)
            assertEquals(b73.m_arr[idx], uint8_t(0));

        assertEquals(b73.m_arr[56], uint8_t(0x0f));
        assertEquals(b73.m_arr[57], uint8_t(0x1f));
        assertEquals(b73.m_arr[58], uint8_t(0x2f));
        assertEquals(b73.m_arr[59], uint8_t(0x3f));
        assertEquals(b73.m_arr[60], uint8_t(0x4f));
        assertEquals(b73.m_arr[61], uint8_t(0x5f));
        assertEquals(b73.m_arr[62], uint8_t(0x6f));
        assertEquals(b73.m_arr[63], uint8_t(0x7f));

        int512_t b74 = int512_t::_impl::shift_right(b7, 64);
        for (int idx = 0; idx < 7; ++idx)
            assertEquals(b74.m_arr[idx], uint8_t(0xff));

        assertEquals(b74.m_arr[8], uint8_t(0xf1));
        assertEquals(b74.m_arr[9], uint8_t(0xf2));
        assertEquals(b74.m_arr[10], uint8_t(0xf3));
        assertEquals(b74.m_arr[11], uint8_t(0xf4));
        assertEquals(b74.m_arr[12], uint8_t(0xf5));
        assertEquals(b74.m_arr[13], uint8_t(0xf6));
        assertEquals(b74.m_arr[14], uint8_t(0xf7));
        assertEquals(b74.m_arr[15], uint8_t(0xf8));

        for (int idx = 16; idx < 64; ++idx)
            assertEquals(b74.m_arr[idx], uint8_t(0));

        int512_t b75 = int512_t::_impl::shift_right(b7, 256);
        for (int idx = 0; idx < 32; ++idx)
            assertEquals(b75.m_arr[idx], uint8_t(0xff));

        assertEquals(b75.m_arr[32], uint8_t(0xf1));
        assertEquals(b75.m_arr[33], uint8_t(0xf2));
        assertEquals(b75.m_arr[34], uint8_t(0xf3));
        assertEquals(b75.m_arr[35], uint8_t(0xf4));
        assertEquals(b75.m_arr[36], uint8_t(0xf5));
        assertEquals(b75.m_arr[37], uint8_t(0xf6));
        assertEquals(b75.m_arr[38], uint8_t(0xf7));
        assertEquals(b75.m_arr[39], uint8_t(0xf8));

        for (int idx = 40; idx < 63; ++idx)
            assertEquals(b75.m_arr[idx], uint8_t(0));

        int512_t b76 = int512_t::_impl::shift_right(b7, 260);
        for (int idx = 0; idx < 33; ++idx)
            assertEquals(b76.m_arr[idx], uint8_t(0xff));

        assertEquals(b76.m_arr[33], uint8_t(0x1f));
        assertEquals(b76.m_arr[34], uint8_t(0x2f));
        assertEquals(b76.m_arr[35], uint8_t(0x3f));
        assertEquals(b76.m_arr[36], uint8_t(0x4f));
        assertEquals(b76.m_arr[37], uint8_t(0x5f));
        assertEquals(b76.m_arr[38], uint8_t(0x6f));
        assertEquals(b76.m_arr[39], uint8_t(0x7f));
        assertEquals(b76.m_arr[40], uint8_t(0x80));

        for (int idx = 41; idx < 63; ++idx)
            assertEquals(b76.m_arr[idx], uint8_t(0));

        int512_t b8 = -13;
        int512_t b81 = int512_t::_impl::shift_right(b8, 1);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b81.m_arr[idx], uint8_t(0xff));
        assertEquals(b81.m_arr[63], uint8_t(0xf9));
    }
    static inline void Assign() noexcept {
        uint512_t a1 = 0;
        int512_t b1 = 0;

        a1 = 4;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[63], uint8_t(4));

        b1 = 4;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0));
        assertEquals(b1.m_arr[63], uint8_t(4));

        a1 = -4;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0xff));
        assertEquals(a1.m_arr[63], uint8_t(0xfc));

        b1 = -4;
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0xff));
        assertEquals(b1.m_arr[63], uint8_t(0xfc));

        a1 = uint128_t(4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[63], uint8_t(4));

        b1 = int128_t(4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0));
        assertEquals(b1.m_arr[63], uint8_t(4));

        a1 = uint128_t(-4);
        for (int idx = 0; idx < 48; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));

        for (int idx = 48; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0xff));
        assertEquals(a1.m_arr[63], uint8_t(0xfc));

        b1 = int128_t(-4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0xff));
        assertEquals(b1.m_arr[63], uint8_t(0xfc));

        a1 = int128_t(4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[63], uint8_t(4));

        b1 = uint128_t(4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0));
        assertEquals(b1.m_arr[63], uint8_t(4));

        a1 = int128_t(-4);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0xff));
        assertEquals(a1.m_arr[63], uint8_t(0xfc));

        b1 = uint128_t(-4);
        for (int idx = 0; idx < 48; ++idx)
            assertEquals(b1.m_arr[idx], uint8_t(0));

        for (int idx = 48; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0xff));
        assertEquals(b1.m_arr[63], uint8_t(0xfc));
    }
    static inline void OperatorPlus() noexcept {
        uint512_t a1 = 3;
        a1.m_arr[55] = 1;

        uint512_t a11 = uint512_t::_impl::operator_plus_T(a1, 5);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[63], uint8_t(8));

        uint512_t a12 = uint512_t::_impl::operator_plus_T(a1, 0);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));
        assertEquals(a12.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));
        assertEquals(a12.m_arr[63], uint8_t(3));

        uint512_t a14 = uint512_t::_impl::operator_plus_T(a1, std::numeric_limits<uint64_t>::max());
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a14.m_arr[idx], uint8_t(0));
        assertEquals(a14.m_arr[55], uint8_t(2));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a14.m_arr[idx], uint8_t(0));
        assertEquals(a14.m_arr[63], uint8_t(2));

        int512_t b1 = 3;
        b1.m_arr[55] = 1;

        int512_t b11 = int512_t::_impl::operator_plus_T(b1, 5);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b11.m_arr[idx], uint8_t(0));
        assertEquals(b11.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b11.m_arr[idx], uint8_t(0));
        assertEquals(b11.m_arr[63], uint8_t(8));

        int512_t b12 = int512_t::_impl::operator_plus_T(b1, 0);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b12.m_arr[idx], uint8_t(0));
        assertEquals(b12.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b12.m_arr[idx], uint8_t(0));
        assertEquals(b12.m_arr[63], uint8_t(3));

        int512_t b2 = -3;
        b2.m_arr[55] = -2;

        int512_t b22 = int512_t::_impl::operator_plus_T(b2, 0);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b22.m_arr[idx], uint8_t(0xff));
        assertEquals(b22.m_arr[55], uint8_t(0xfe));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b22.m_arr[idx], uint8_t(0xff));
        assertEquals(b22.m_arr[63], uint8_t(0xfd));

        int512_t b23 = int512_t::_impl::operator_plus_T(b2, uint16_t(5));
        for (int idx = 0; idx < 56; ++idx)
            assertEquals(b23.m_arr[idx], uint8_t(0xff));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b23.m_arr[idx], uint8_t(0));
        assertEquals(b23.m_arr[63], uint8_t(2));

        int512_t b3 = -3;

        int512_t b31 = int512_t::_impl::operator_plus_T(b3, 5);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b31.m_arr[idx], uint8_t(0));
        assertEquals(static_cast<int64_t>(b31.m_arr[63]), int64_t(2));

        int512_t b4 = -1;
        b4.m_arr[55] = -2;
        b4.m_arr[63] = 3;

        int512_t b41 = int512_t::_impl::operator_plus_T(b4, 5);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b41.m_arr[idx], uint8_t(0xff));
        assertEquals(b41.m_arr[55], uint8_t(0xfe));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b41.m_arr[idx], uint8_t(0xff));
        assertEquals(b41.m_arr[63], uint8_t(8));
    }
    static inline void OperatorTilda() noexcept {
        uint512_t a1 = 250;
        a1.m_arr[3] = 17;

        uint512_t a2 = uint512_t::_impl::operator_unary_tilda(a1);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0xff));
        assertEquals(a2.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0xff));
        assertEquals(a2.m_arr[63], uint8_t(0x05));

        int512_t b1 = 250;
        b1.m_arr[3] = 17;

        int512_t b2 = int512_t::_impl::operator_unary_tilda(b1);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(b2.m_arr[idx], uint8_t(0xff));
        assertEquals(b2.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(b2.m_arr[idx], uint8_t(0xff));
        assertEquals(b2.m_arr[63], uint8_t(0x05));

        int512_t b3 = -250;
        b3.m_arr[3] = 17;

        int512_t b4 = int512_t::_impl::operator_unary_tilda(b3);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(b4.m_arr[idx], uint8_t(0));
        assertEquals(b4.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(b4.m_arr[idx], uint8_t(0));
        assertEquals(b4.m_arr[63], uint8_t(0xf9));
    }
    static inline void OperatorUnaryMinus() noexcept {
        uint512_t a1 = 250;
        a1.m_arr[3] = 17;

        uint512_t a2 = uint512_t::_impl::operator_unary_minus(a1);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0xff));
        assertEquals(a2.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0xff));
        assertEquals(a2.m_arr[63], uint8_t(0x06));

        int512_t b1 = 250;
        b1.m_arr[3] = 17;

        int512_t b2 = int512_t::_impl::operator_unary_minus(b1);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(b2.m_arr[idx], uint8_t(0xff));
        assertEquals(b2.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(b2.m_arr[idx], uint8_t(0xff));
        assertEquals(b2.m_arr[63], uint8_t(0x06));

        int512_t b3 = -250;
        b3.m_arr[3] = 17;

        int512_t b4 = int512_t::_impl::operator_unary_minus(b3);
        for (int idx = 0; idx < 3; ++idx)
            assertEquals(b4.m_arr[idx], uint8_t(0));
        assertEquals(b4.m_arr[3], uint8_t(0xee));

        for (int idx = 4; idx < 63; ++idx)
            assertEquals(b4.m_arr[idx], uint8_t(0));
        assertEquals(b4.m_arr[63], uint8_t(0xfa));
    }
    static inline void OperatorPlusWide() noexcept {
        uint512_t a1 = 3;
        a1.m_arr[55] = 1;

        uint512_t a11 = uint512_t::_impl::operator_plus(a1, a1);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[55], uint8_t(2));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[63], uint8_t(6));

        uint512_t a12 = uint512_t::_impl::operator_plus(a1, uint256_t(0));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));
        assertEquals(a12.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));
        assertEquals(a12.m_arr[63], uint8_t(3));

        uint512_t a13 = uint512_t::_impl::operator_minus(a1, uint128_t(5));
        for (int idx = 0; idx < 56; ++idx)
            assertEquals(a13.m_arr[idx], uint8_t(0));

        for (int idx = 57; idx < 63; ++idx)
            assertEquals(a13.m_arr[idx], uint8_t(0xff));
        assertEquals(a13.m_arr[63], uint8_t(0xfe));

        uint512_t a14 = uint512_t::_impl::operator_plus(a1, uint512_t::_impl::operator_plus_T(a1, std::numeric_limits<uint64_t>::max()));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a14.m_arr[idx], uint8_t(0));
        assertEquals(a14.m_arr[55], uint8_t(3));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a14.m_arr[idx], uint8_t(0));
        assertEquals(a14.m_arr[63], uint8_t(5));

        uint512_t a15 = uint512_t::_impl::operator_minus(a1, uint256_t(0));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a15.m_arr[idx], uint8_t(0));
        assertEquals(a15.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a15.m_arr[idx], uint8_t(0));
        assertEquals(a15.m_arr[63], uint8_t(3));

        int512_t b1 = 3;
        b1.m_arr[55] = 1;

        int512_t b11 = int512_t::_impl::operator_plus(b1, uint128_t(5));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b11.m_arr[idx], uint8_t(0));
        assertEquals(b11.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b11.m_arr[idx], uint8_t(0));
        assertEquals(b11.m_arr[63], uint8_t(8));

        int512_t b12 = int512_t::_impl::operator_plus(b1, uint512_t(0));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b12.m_arr[idx], uint8_t(0));
        assertEquals(b12.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b12.m_arr[idx], uint8_t(0));
        assertEquals(b12.m_arr[63], uint8_t(3));

        int512_t b13 = int512_t::_impl::operator_minus(b1, uint128_t(5));
        for (int idx = 0; idx < 56; ++idx)
            assertEquals(b13.m_arr[idx], uint8_t(0));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b13.m_arr[idx], uint8_t(0xff));
        assertEquals(b13.m_arr[63], uint8_t(0xfe));

        int512_t b14 = int512_t::_impl::operator_minus(b1, int128_t(0));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b14.m_arr[idx], uint8_t(0));
        assertEquals(b14.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b14.m_arr[idx], uint8_t(0));
        assertEquals(b14.m_arr[63], uint8_t(3));

        int512_t b2 = -3;
        b2.m_arr[55] = -2;

        int512_t b21 = int512_t::_impl::operator_minus(b2, uint128_t(5));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b21.m_arr[idx], uint8_t(0xff));
        assertEquals(b21.m_arr[55], uint8_t(0xfe));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b21.m_arr[idx], uint8_t(0xff));
        assertEquals(static_cast<int8_t>(b21.m_arr[63]), int8_t(-8));

        int512_t b22 = int512_t::_impl::operator_plus(b2, int256_t(0));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b22.m_arr[idx], uint8_t(0xff));
        assertEquals(b22.m_arr[55], uint8_t(0xfe));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b22.m_arr[idx], uint8_t(0xff));
        assertEquals(static_cast<int8_t>(b22.m_arr[63]), int8_t(-3));

        int512_t b23 = int512_t::_impl::operator_plus(b2, uint128_t(5));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b23.m_arr[idx], uint8_t(0xff));
        assertEquals(b23.m_arr[55], uint8_t(0xff));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b23.m_arr[idx], uint8_t(0));
        assertEquals(b23.m_arr[63], uint8_t(2));

        int512_t b3 = -3;

        int512_t b31 = int512_t::_impl::operator_plus(b3, int128_t(5));
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(b31.m_arr[idx], uint8_t(0));
        assertEquals(static_cast<int8_t>(b31.m_arr[63]), int8_t(2));

        int512_t b4 = -1;
        b4.m_arr[55] = -2;
        b4.m_arr[63] = 3;

        int512_t b41 = int512_t::_impl::operator_plus(b4, uint256_t(5));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(b41.m_arr[idx], uint8_t(0xff));
        assertEquals(b41.m_arr[55], uint8_t(0xfe));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(b41.m_arr[idx], uint8_t(0xff));
        assertEquals(static_cast<int8_t>(b41.m_arr[63]), int8_t(8));
    }
    static inline void OperatorMinus() noexcept {
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  uint128_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  uint256_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    int128_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    int256_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  int128_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  int256_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_plus(uint512_t(0)
                        ,  int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    uint128_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    uint256_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_plus(int512_t(0)
                        ,    uint512_t(3))));

        assertTrue(typeid(uint512_t) == typeid(uint128_t::_impl::operator_plus(uint128_t(0)
                        ,  uint512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint256_t::_impl::operator_plus(uint256_t(0)
                        ,  uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int128_t::_impl::operator_plus(int128_t(0)
                        ,    int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int256_t::_impl::operator_plus(int256_t(0)
                        ,    int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint128_t::_impl::operator_plus(uint128_t(0)
                        ,  int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint256_t::_impl::operator_plus(uint256_t(0)
                        ,  int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int128_t::_impl::operator_plus(int128_t(0)
                        ,    uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int256_t::_impl::operator_plus(int256_t(0)
                        ,    uint512_t(3))));

        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , uint128_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , uint256_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   int128_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   int256_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , int128_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , int256_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint512_t::_impl::operator_minus(uint512_t(0)
                        , int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   uint128_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   uint256_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int512_t::_impl::operator_minus(int512_t(0)
                        ,   uint512_t(3))));

        assertTrue(typeid(uint512_t) == typeid(uint128_t::_impl::operator_minus(uint128_t(0)
                        , uint512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint256_t::_impl::operator_minus(uint256_t(0)
                        , uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int128_t::_impl::operator_minus(int128_t(0)
                        ,   int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int256_t::_impl::operator_minus(int256_t(0)
                        ,   int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint128_t::_impl::operator_minus(uint128_t(0)
                        , int512_t(3))));
        assertTrue(typeid(uint512_t) == typeid(uint256_t::_impl::operator_minus(uint256_t(0)
                        , int512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int128_t::_impl::operator_minus(int128_t(0)
                        ,   uint512_t(3))));
        assertTrue(typeid(int512_t)  == typeid(int256_t::_impl::operator_minus(int256_t(0)
                        ,   uint512_t(3))));
    }
    static inline void OperatorStar() noexcept {
        uint32_t b = 10;
        b *= 2.35;
        assertEquals(b, uint32_t(23));

        uint512_t a = 10;
        a *= 2.5;
        assertEquals(a, uint512_t(20));

        uint512_t a1 = 256_uint512;
        uint512_t a11 = uint512_t::_impl::operator_star(a1, uint512_t(1));
        for (int idx = 0; idx < 62; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[62], uint8_t(1));
        assertEquals(a11.m_arr[63], uint8_t(0));

        uint512_t a12 = uint512_t::_impl::operator_star(a1, uint512_t(0));
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));

        uint512_t a13 = uint512_t::_impl::operator_star(a1, uint512_t(2));
        for (int idx = 0; idx < 62; ++idx)
            assertEquals(a13.m_arr[idx], uint8_t(0));
        assertEquals(a13.m_arr[62], uint8_t(2));
        assertEquals(a13.m_arr[63], uint8_t(0));

        uint512_t a2 = std::numeric_limits<uint64_t>::max();
        uint512_t a24 = uint512_t::_impl::operator_star(a2, uint512_t(2));
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a24.m_arr[idx], uint8_t(0));
        assertEquals(a24.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a24.m_arr[idx], uint8_t(0xff));
        assertEquals(a24.m_arr[63], uint8_t(0xfe));

        uint512_t a3 = 256;
        uint512_t a31 = uint512_t::_impl::operator_star(a3, 1);
        for (int idx = 0; idx < 62; ++idx)
            assertEquals(a31.m_arr[idx], uint8_t(0));
        assertEquals(a31.m_arr[62], uint8_t(1));
        assertEquals(a31.m_arr[63], uint8_t(0));

        uint512_t a32 = uint512_t::_impl::operator_star(a3, 0);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a32.m_arr[idx], uint8_t(0));

        uint512_t a33 = uint512_t::_impl::operator_star(a3, 2);
        for (int idx = 0; idx < 62; ++idx)
            assertEquals(a33.m_arr[idx], uint8_t(0));
        assertEquals(a33.m_arr[62], uint8_t(2));
        assertEquals(a33.m_arr[63], uint8_t(0));

        uint512_t a4 = std::numeric_limits<uint64_t>::max();
        uint512_t a44 = uint512_t::_impl::operator_star(a4, 2);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a44.m_arr[idx], uint8_t(0));
        assertEquals(a44.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 63; ++idx)
            assertEquals(a44.m_arr[idx], uint8_t(0xff));
        assertEquals(a44.m_arr[63], uint8_t(0xfe));
    }
    static inline void OperatorMore() noexcept {
        assertTrue(int512_t::_impl::operator_more(15, -18));
        assertTrue(!uint512_t::_impl::operator_more(15, 18U));
        assertTrue(uint512_t::_impl::operator_more(18, 15U));
        assertTrue(!uint512_t::_impl::operator_more(15, 18U));
        assertTrue(int512_t::_impl::operator_more(-15, -18));
        assertTrue(!int512_t::_impl::operator_more(-18, -15));
    }
    static inline void OperatorEq() noexcept {
        assertTrue(uint512_t::_impl::operator_eq(uint512_t(12), int64_t(12)));
        assertTrue(uint512_t::_impl::operator_eq(12, 12));
        assertTrue(uint512_t::_impl::operator_eq(0, 0));
    }
    static inline void OperatorPipe() noexcept {
        uint512_t a1 = 0x0102030405060708;
        for (int i = 0; i < 7; ++i)
            a1.m_arr[i] = 0x01;

        uint512_t a2 = uint512_t::_impl::operator_pipe(a1, 0xf0f0f0f0f0f0f0f0);

        for (int i = 0; i < 7; ++i)
            assertEquals(a2.m_arr[i], uint8_t(0x01));

        for (int idx = 8; idx < 56; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0));

        assertEquals(a2.m_arr[56], uint8_t(0xf1));
        assertEquals(a2.m_arr[57], uint8_t(0xf2));
        assertEquals(a2.m_arr[58], uint8_t(0xf3));
        assertEquals(a2.m_arr[59], uint8_t(0xf4));
        assertEquals(a2.m_arr[60], uint8_t(0xf5));
        assertEquals(a2.m_arr[61], uint8_t(0xf6));
        assertEquals(a2.m_arr[62], uint8_t(0xf7));
        assertEquals(a2.m_arr[63], uint8_t(0xf8));

        uint512_t a3 = 0xf0f0f0f0f0f0f0f0;
        for (int i = 0; i < 7; ++i)
            a3.m_arr[i] = 0xf0;

        uint512_t a4 = uint512_t::_impl::operator_pipe(a1, a3);

        for (int i = 0; i < 7; ++i)
            assertEquals(a4.m_arr[i], uint8_t(0xf1));

        for (int idx = 8; idx < 56; ++idx)
            assertEquals(a4.m_arr[idx], uint8_t(0));

        assertEquals(a4.m_arr[56], uint8_t(0xf1));
        assertEquals(a4.m_arr[57], uint8_t(0xf2));
        assertEquals(a4.m_arr[58], uint8_t(0xf3));
        assertEquals(a4.m_arr[59], uint8_t(0xf4));
        assertEquals(a4.m_arr[60], uint8_t(0xf5));
        assertEquals(a4.m_arr[61], uint8_t(0xf6));
        assertEquals(a4.m_arr[62], uint8_t(0xf7));
        assertEquals(a4.m_arr[63], uint8_t(0xf8));
    }
    static inline void OperatorAmp() noexcept {
        uint512_t a1 = 0x0102030405060708;
        for (int i = 0; i < 7; ++i)
            a1.m_arr[i] = 0x01;

        uint512_t a2 = uint512_t::_impl::operator_amp(a1, 0xf0f0f0f0f0f0f0f0);

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0));

        uint512_t a3 = 0xffffffffffffffff;
        for (int i = 0; i < 7; ++i) {
            a2.m_arr[i] = 0xf0;
        }

        uint512_t a4 = uint512_t::_impl::operator_amp(a1, a3);

        for (int idx = 0; idx < 56; ++idx)
            assertEquals(a4.m_arr[idx], uint8_t(0));

        assertEquals(a4.m_arr[56], uint8_t(0x01));
        assertEquals(a4.m_arr[57], uint8_t(0x02));
        assertEquals(a4.m_arr[58], uint8_t(0x03));
        assertEquals(a4.m_arr[59], uint8_t(0x04));
        assertEquals(a4.m_arr[60], uint8_t(0x05));
        assertEquals(a4.m_arr[61], uint8_t(0x06));
        assertEquals(a4.m_arr[62], uint8_t(0x07));
        assertEquals(a4.m_arr[63], uint8_t(0x08));
    }
    static inline void OperatorSlash() noexcept {
        uint512_t a1 = uint512_t::_impl::operator_slash(15, 3);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[63], uint8_t(5));

        uint512_t a2 = uint512_t::_impl::operator_slash(17, 3);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0));
        assertEquals(a2.m_arr[63], uint8_t(5));

        uint512_t a3 = uint512_t::_impl::operator_slash(0, 3);
        for (int idx = 0; idx < 64; ++idx)
            assertEquals(a3.m_arr[idx], uint8_t(0));

        uint512_t a4 = 2048;
        a4.m_arr[55] = 2;
        uint512_t a41 = uint512_t::_impl::operator_slash(a4, 2);
        for (int idx = 0; idx < 55; ++idx)
            assertEquals(a41.m_arr[idx], uint8_t(0));
        assertEquals(a41.m_arr[55], uint8_t(1));

        for (int idx = 56; idx < 62; ++idx)
            assertEquals(a41.m_arr[idx], uint8_t(0));
        assertEquals(a41.m_arr[62], uint8_t(4));
        assertEquals(a41.m_arr[63], uint8_t(0));

        assertEquals((int256_t(-6)/int256_t(-2)), int256_t(3));
    }
    static inline void OperatorPercent() noexcept {
        uint512_t a1 = uint512_t::_impl::operator_percent(15, 3);
        for (int idx = 0; idx < 64; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));

        uint512_t a2 = uint512_t::_impl::operator_percent(17, 3);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0));
        assertEquals(a2.m_arr[63], uint8_t(2));

        uint512_t a3 = uint512_t::_impl::operator_percent(0, 3);
        for (int idx = 0; idx < 64; ++idx)
            assertEquals(a3.m_arr[idx], uint8_t(0));

        uint512_t a4 = 2049;
        a4.m_arr[55] = 2;
        uint512_t a41 = uint512_t::_impl::operator_percent(a4, 2);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a41.m_arr[idx], uint8_t(0));
        assertEquals(a41.m_arr[63], uint8_t(1));
        assertEquals(int256_t(-14)%int256_t(10), int256_t(-4));
    }
    static inline void Circumflex() noexcept {
        uint512_t a1 = 0xff;

        uint512_t a2 = uint512_t::_impl::operator_circumflex(a1, 0xf);
        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0));
        assertEquals(a2.m_arr[63], uint8_t(0xf0));
    }
    static inline void ToString() noexcept {
        assertEquals(std::to_string(uint512_t(123455)), "123455"_s);
        assertEquals(std::to_string(int512_t(-123455)), "-123455"_s);
        //std::cout << int512_t(-123455) << std::endl;
        //std::wcout << 123000999_int128 << std::endl;
    }
    static inline void Cast() noexcept {
        assertEquals(int(uint512_t(18)), 18);
        assertDoubleEquals(double(uint512_t(1024)), 1024.);

        uint128_t a1 = uint512_t(18);
        for (int idx = 0; idx < 15; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[15], uint8_t(18));

        uint512_t a2 = 1024;
        assertEquals(a2, uint512_t(1024));

        uint32_t a3 = a2;
        assertEquals(a3, uint32_t(1024));

        assertEquals(int(int512_t(-1024)), int(-1024));
        assertDoubleEquals(double(int512_t(-1024)), -1024.);
        assertDoubleEquals(double(-int512_t(1024)), -1024.);
        assertDoubleEquals(-double(int512_t(1024)), -1024.);
    }
    static inline void FromString() noexcept {
        uint512_t a1;
        uint512_t::_impl::from_string("1234567", a1);
        assertEquals(typeid(uint512_t), typeid(a1));
        assertEquals(std::to_string(a1), "1234567"_s);

        for (int idx = 0; idx < 61; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0));
        assertEquals(a1.m_arr[61], uint8_t(0x12));
        assertEquals(a1.m_arr[62], uint8_t(0xd6));
        assertEquals(a1.m_arr[63], uint8_t(0x87));

        int256_t a;
        std::string s = "1000000000000000000";
        std::istringstream in(s);
        in >> a;
        assertEquals(a, 1000000000000000000_int256);
    }
    static inline void NativeOperators() noexcept {
        auto a1 = ~uint512_t(0xff);
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a1.m_arr[idx], uint8_t(0xff));
        assertEquals(a1.m_arr[63], uint8_t(0x00));

        auto a2 = -uint512_t(0x1);
        assertEquals(typeid(uint512_t), typeid(a2));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a2.m_arr[idx], uint8_t(0xff));

        auto a3 = 2 * uint512_t(0x1);
        assertEquals(typeid(uint512_t), typeid(a3));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a3.m_arr[idx], uint8_t(0));
        assertEquals(a3.m_arr[63], uint8_t(2));

        auto a4 = uint512_t(0x1) * 2;
        assertEquals(typeid(uint512_t), typeid(a4));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a4.m_arr[idx], uint8_t(0));
        assertEquals(a4.m_arr[63], uint8_t(2));

        auto a5 = 4 / uint512_t(0x2);
        assertEquals(typeid(uint512_t), typeid(a5));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a5.m_arr[idx], uint8_t(0));
        assertEquals(a5.m_arr[63], uint8_t(2));

        auto a6 = uint512_t(0x4) / 2;
        assertEquals(typeid(uint512_t),typeid(a6));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a6.m_arr[idx], uint8_t(0));
        assertEquals(a6.m_arr[63], uint8_t(2));

        auto a7 = 8 % uint512_t(0x5);
        assertEquals(typeid(uint512_t), typeid(a7));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a7.m_arr[idx], uint8_t(0));
        assertEquals(a7.m_arr[63], uint8_t(3));

        auto a8 = uint512_t(0x8) % 5;
        assertEquals(typeid(uint512_t), typeid(a8));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a8.m_arr[idx], uint8_t(0));
        assertEquals(a8.m_arr[63], uint8_t(3));

        auto a9 = 8 + uint512_t(0x5);
        assertEquals(typeid(uint512_t), typeid(a9));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a9.m_arr[idx], uint8_t(0));
        assertEquals(a9.m_arr[63], uint8_t(13));

        auto a10 = uint512_t(0x8) + 5;
        assertEquals(typeid(uint512_t), typeid(a10));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a10.m_arr[idx], uint8_t(0));
        assertEquals(a10.m_arr[63], uint8_t(13));

        auto a11 = 8 - uint512_t(0x5);
        assertEquals(typeid(uint512_t), typeid(a11));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a11.m_arr[idx], uint8_t(0));
        assertEquals(a11.m_arr[63], uint8_t(3));

        auto a12 = uint512_t(0x8) - 5;
        assertEquals(typeid(uint512_t), typeid(a12));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a12.m_arr[idx], uint8_t(0));
        assertEquals(a12.m_arr[63], uint8_t(3));

        assertFalse(uint512_t(150) < 20U);
        assertTrue(uint512_t(15) < 20U);
        assertFalse(uint512_t(20) > 25U);
        assertTrue(uint512_t(20) > 15U);
        assertTrue(uint512_t(15) <= 20U);
        assertFalse(uint512_t(20) >= 25U);
        assertTrue(uint512_t(20) >= 15U);
        assertFalse(uint512_t(25) <= 20U);
        assertTrue(uint512_t(20) <= 20U);
        assertFalse(uint512_t(20) >= 22U);
        assertTrue(uint512_t(20) >= 20U);
        assertFalse(uint512_t(20) == 21U);
        assertTrue(uint512_t(20) == 20U);
        assertFalse(uint512_t(20) != 20U);
        assertTrue(uint512_t(20) != 21U);

        assertFalse(150U < uint512_t(20));
        assertTrue(15U < uint512_t(20));
        assertFalse(20U > uint512_t(25));
        assertTrue(20U > uint512_t(15));
        assertTrue(15U <= uint512_t(20));
        assertFalse(20U >= uint512_t(55));
        assertTrue(20U >= uint512_t(15));
        assertFalse(25U <= uint512_t(20));
        assertTrue(20U <= uint512_t(20));
        assertFalse(20U >= uint512_t(24));
        assertTrue(20U >= uint512_t(20));
        assertFalse(20U == uint512_t(21));
        assertTrue(20U == uint512_t(20));
        assertFalse(20U != uint512_t(20));
        assertTrue(20U != uint512_t(21));

        assertTrue(uint512_t(15) < uint128_t(20));
        assertTrue(uint512_t(20) > uint128_t(15));
        assertTrue(uint512_t(15) <= uint128_t(20));
        assertTrue(uint512_t(20) >= uint128_t(15));
        assertTrue(uint512_t(20) <= uint128_t(20));
        assertTrue(uint512_t(20) >= uint128_t(20));
        assertTrue(uint512_t(20) == uint128_t(20));

        assertTrue(uint128_t(15) < uint512_t(20));
        assertTrue(uint128_t(20) > uint512_t(15));
        assertTrue(uint128_t(15) <= uint512_t(20));
        assertTrue(uint128_t(20) >= uint512_t(15));
        assertTrue(uint128_t(20) <= uint512_t(20));
        assertTrue(uint128_t(20) >= uint512_t(20));
        assertTrue(uint128_t(20) == uint512_t(20));

        assertFalse(uint128_t(0) && uint512_t(20));
        assertTrue(uint128_t(20) && uint512_t(20));

        assertFalse(uint128_t(0) || uint512_t(0));
        assertTrue(uint128_t(20) || uint512_t(20));
        assertTrue(uint128_t(20) || uint512_t(20));

        auto a100 = uint512_t(5);
        a100++;
        assertEquals(uint512_t(6), a100);
        ++a100;
        assertEquals(uint512_t(7), a100);

        a100--;
        assertEquals(uint512_t(6), a100);
        --a100;
        assertEquals(uint512_t(5), a100);

        auto a13 = 9 & uint512_t(5);
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a13.m_arr[idx], uint8_t(0));
        assertEquals(a13.m_arr[63], uint8_t(1));

        auto a14 = uint512_t(9) & 5;
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a14.m_arr[idx], uint8_t(0));
        assertEquals(a14.m_arr[63], uint8_t(1));

        auto a15 = 9 | uint512_t(5);
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a15.m_arr[idx], uint8_t(0));
        assertEquals(a15.m_arr[63], uint8_t(13));

        auto a16 = uint512_t(9) | 5;
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a16.m_arr[idx], uint8_t(0));
        assertEquals(a16.m_arr[63], uint8_t(13));

        auto a17 = 4 ^ uint512_t(5);
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a17.m_arr[idx], uint8_t(0));
        assertEquals(a17.m_arr[63], uint8_t(1));

        auto a18 = uint512_t(4) ^ 5;
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a18.m_arr[idx], uint8_t(0));
        assertEquals(a18.m_arr[63], uint8_t(1));

        auto a19 = uint512_t(8) << 1;
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a19.m_arr[idx], uint8_t(0));
        assertEquals(a19.m_arr[63], uint8_t(16));

        auto a20 = uint512_t(8) >> 1;
        assertEquals(typeid(uint512_t), typeid(a1));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a20.m_arr[idx], uint8_t(0));
        assertEquals(a20.m_arr[63], uint8_t(4));

        assertTrue(!uint256_t(0));
        assertFalse(!uint256_t(-2));
        assertFalse(!uint256_t(1));
        assertTrue(!!uint256_t(1));
        assertTrue(bool(uint256_t(1)));
        assertFalse(!uint256_t(-1));
        assertTrue(bool(uint256_t(-1)));
        assertTrue(bool(uint256_t(-1)));

        auto a21 = +uint512_t(0x1);
        assertEquals(typeid(uint512_t), typeid(a21));

        for (int idx = 0; idx < 63; ++idx)
            assertEquals(a21.m_arr[idx], uint8_t(0));
        assertEquals(a21.m_arr[63], uint8_t(1));

        assertEquals(1024_int128 * 2.0, int128_t(2048));
        assertEquals(2.0 * 1024_int128, int128_t(2048));

        int128_t b = -123;
        assertTrue(b < 0);
        assertFalse(b > 0);
    }
    static inline void NativeOperatorsAssign() noexcept {
        uint512_t a1 = 18;

        a1 *= 2U;
        assertEquals(a1, uint512_t(36U));

        a1 /= 2U;
        assertEquals(a1, uint512_t(18U));

        a1 %= 5U;
        assertEquals(a1, uint512_t(3U));

        a1 += 2U;
        assertEquals(a1, uint512_t(5U));

        a1 -= 1U;
        assertEquals(a1, uint512_t(4U));

        a1 &= 3U;
        assertEquals(a1, uint512_t(0U));

        a1 |= 255U;
        assertEquals(a1, uint512_t(255U));

        a1 ^= 0xffffffffffffffff;
        assertEquals(a1, uint512_t(0xffffffffffffff00));

        a1 <<= 4;
        assertEquals(a1, uint512_t(0xffffffffffffff000_uint128));

        a1 >>= 8;
        assertEquals(a1, uint512_t(0x0ffffffffffffff0));
    }
    static inline void Constexpr() noexcept {
        constexpr uint256_t a0{};
        assertEquals(a0, uint256_t(0));
        assertTrue(a0 != 1);
        assertTrue(1 != a0);
        assertTrue(a0 >= 0U);
        assertTrue(a0 < 1U);
        assertTrue(a0 <= 1U);

        uint512_t a1 = 7;
        assertTrue(a1 > 0U);
        assertTrue(a1 == 7);
        assertTrue(a1 + 12 == 19);
        assertTrue(12 + a1 == 19);

        int128_t a2 = a1;
        assertTrue(a2 == 7);
        assertTrue(a2 - 3 == 4);
        assertTrue(11 - a2 == 4);

        assertTrue(a1 / 2 == 3);
        assertTrue(a2 * 2 == 14);
        assertTrue(a1 % 2 == 1);

        assertTrue(~a2 == -8);
        assertTrue(-a2 == -7);

        assertTrue((a1 | 0xff) == 0xff);
        assertTrue((a1 & 0xff) == 7);
        assertTrue((a2 ^ 0xff) == 0xf8);

        assertTrue(0xff_int128 == 255);
        assertTrue(255_int128 == 255);

        assertTrue((a1 >> 1) == 3);
        assertTrue((a1 << 1) == 14);

        uint256_t b = std::numeric_limits<uint256_t>::min();
        assertTrue(b == 0);

        int128_t c = std::numeric_limits<int128_t>::min();
        assertTrue(c == 0x80000000000000000000000000000000_uint128);
        assertTrue(!!0x80000000000000000000000000000000_uint128);
    }
    static inline void Etc() noexcept {
        std::unordered_map<uint256_t, bool> m;
        assertTrue(m.empty());

        assertTrue((34562625464547567_uint512 * 234623412124100_uint256) == 8109201118459457025988957064700_uint256);
        assertTrue((34562625464547567_uint512 / 234623412124100_uint256) == 147);
        assertTrue((34562625464547567_uint512 + 234623412124100_uint256) == 34797248876671667_uint256);
        assertTrue((34562625464547567_uint512 - 234623412124100_uint256) == 34328002052423467_uint256);
        assertTrue((34562625464547567_uint512 % 234623412124100_uint256) == 72983882304867_uint256);
        assertTrue((34562625464547567_uint512 & 234623412124100_uint512) == 211114906400964_uint256);
        assertTrue((34562625464547567_uint512 | 234623412124100_uint256) == 34586133970270703_uint256);

        assertTrue((34562625464547567_uint512 << 3) == 276501003716380536_uint512);
        assertTrue((34562625464547567_uint512 >> 3) == 4320328183068445_uint512);

        assertTrue(234623412124100_uint256 < 34562625464547567_uint512);
        assertTrue(34562625464547567_uint512 > 234623412124100_uint256);
        assertTrue(234623412124100_uint256 <= 34562625464547567_uint512);
        assertTrue(34562625464547567_uint512 >= 234623412124100_uint256);
        assertTrue(34562625464547567_uint512 == 34562625464547567_uint256);
        assertTrue(34562625464547567_uint512 != 234623412124100_uint256);
    }
    static inline void ToChars() noexcept {
        uint128_t a = 65535;
        int128_t b = -65535;
        uint128_t c = 0;

        char arr1[1];
        std::to_chars_result res = to_chars(arr1, arr1 + sizeof(arr1), a, 1);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        res = to_chars(arr1, arr1 + sizeof(arr1), a, 50);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        res = to_chars(arr1 + sizeof(arr1), arr1, a);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        res = to_chars(arr1, arr1 + sizeof(arr1), a);
        assertEquals(res.ec.value(), (int)std::errc::value_too_large);

        char arr2[5];
        res = to_chars(arr2, arr2 + sizeof(arr2), a);
        assertTrue(!res.ec);

        res = to_chars(arr2, arr2 + sizeof(arr2), b);
        assertEquals(res.ec.value(), (int)std::errc::value_too_large);

        auto check = [](auto num, int base, const std::string& str) {
            char arr3[24];
            std::to_chars_result res = to_chars(arr3, arr3 + sizeof(arr3), num, base);
            assertTrue(!res.ec);
            assertTrue(str == std::string(arr3));
        };
        check(a, 2, "1111111111111111");
        check(a, 3, "10022220020");
        check(a, 4, "33333333");
        check(a, 5, "4044120");
        check(a, 6, "1223223");
        check(a, 7, "362031");
        check(a, 8, "177777");
        check(a, 9, "108806");
        check(a, 10, "65535");
        check(a, 11, "45268");
        check(a, 12, "31b13");
        check(a, 13, "23aa2");
        check(a, 14, "19c51");
        check(a, 15, "14640");
        check(a, 16, "ffff");
        check(a, 17, "d5d0");
        check(a, 18, "b44f");
        check(a, 19, "9aa4");
        check(a, 20, "83gf");
        check(a, 21, "71cf");
        check(a, 22, "638j");
        check(a, 23, "58k8");
        check(a, 24, "4hif");
        check(a, 25, "44la");
        check(a, 26, "3iof");
        check(a, 27, "38o6");
        check(a, 28, "2rgf");
        check(a, 29, "2jqo");
        check(a, 30, "2cof");
        check(a, 31, "2661");
        check(a, 32, "1vvv");
        check(a, 33, "1r5u");
        check(a, 34, "1mnh");
        check(a, 35, "1ihf");
        check(a, 36, "1ekf");

        check(b, 2, "-1111111111111111");
        check(b, 3, "-10022220020");
        check(b, 4, "-33333333");
        check(b, 5, "-4044120");
        check(b, 6, "-1223223");
        check(b, 7, "-362031");
        check(b, 8, "-177777");
        check(b, 9, "-108806");
        check(b, 10, "-65535");
        check(b, 11, "-45268");
        check(b, 12, "-31b13");
        check(b, 13, "-23aa2");
        check(b, 14, "-19c51");
        check(b, 15, "-14640");
        check(b, 16, "-ffff");
        check(b, 17, "-d5d0");
        check(b, 18, "-b44f");
        check(b, 19, "-9aa4");
        check(b, 20, "-83gf");
        check(b, 21, "-71cf");
        check(b, 22, "-638j");
        check(b, 23, "-58k8");
        check(b, 24, "-4hif");
        check(b, 25, "-44la");
        check(b, 26, "-3iof");
        check(b, 27, "-38o6");
        check(b, 28, "-2rgf");
        check(b, 29, "-2jqo");
        check(b, 30, "-2cof");
        check(b, 31, "-2661");
        check(b, 32, "-1vvv");
        check(b, 33, "-1r5u");
        check(b, 34, "-1mnh");
        check(b, 35, "-1ihf");
        check(b, 36, "-1ekf");

        for (int i = 2; i <= 36; ++i)
            check(c, i, "0");
    }
    static inline void FromChars() noexcept {
        uint128_t a = 0;
        int128_t b = 0;

        char arr1[] = "65535";
        std::from_chars_result res = from_chars(arr1 - 1, arr1, a, 1);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        res = from_chars(arr1, arr1 + sizeof(arr1), a, 50);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        res = from_chars(arr1 + sizeof(arr1), arr1, a);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        char arr2[] = "-65535";
        res = from_chars(arr2, arr2 + sizeof(arr2), a);
        assertEquals(res.ec.value(), (int)std::errc::result_out_of_range);

        char arr3[] = "-";
        res = from_chars(arr3, arr3 + 1, b);
        assertEquals(res.ec.value(), (int)std::errc::invalid_argument);

        checkFromChars<uint128_t>(65535, 2, "1111111111111111");
        checkFromChars<uint128_t>(65535, 3, "10022220020");
        checkFromChars<uint128_t>(65535, 4, "33333333");
        checkFromChars<uint128_t>(65535, 5, "4044120");
        checkFromChars<uint128_t>(65535, 6, "1223223");
        checkFromChars<uint128_t>(65535, 7, "362031");
        checkFromChars<uint128_t>(65535, 8, "177777");
        checkFromChars<uint128_t>(65535, 9, "108806");
        checkFromChars<uint128_t>(65535, 10, "65535");
        checkFromChars<uint128_t>(65535, 11, "45268");
        checkFromChars<uint128_t>(65535, 12, "31b13");
        checkFromChars<uint128_t>(65535, 13, "23aa2");
        checkFromChars<uint128_t>(65535, 14, "19c51");
        checkFromChars<uint128_t>(65535, 15, "14640");
        checkFromChars<uint128_t>(65535, 16, "ffff");
        checkFromChars<uint128_t>(65535, 17, "d5d0");
        checkFromChars<uint128_t>(65535, 18, "b44f");
        checkFromChars<uint128_t>(65535, 19, "9aa4");
        checkFromChars<uint128_t>(65535, 20, "83gf");
        checkFromChars<uint128_t>(65535, 21, "71cf");
        checkFromChars<uint128_t>(65535, 22, "638j");
        checkFromChars<uint128_t>(65535, 23, "58k8");
        checkFromChars<uint128_t>(65535, 24, "4hif");
        checkFromChars<uint128_t>(65535, 25, "44la");
        checkFromChars<uint128_t>(65535, 26, "3iof");
        checkFromChars<uint128_t>(65535, 27, "38o6");
        checkFromChars<uint128_t>(65535, 28, "2rgf");
        checkFromChars<uint128_t>(65535, 29, "2jqo");
        checkFromChars<uint128_t>(65535, 30, "2cof");
        checkFromChars<uint128_t>(65535, 31, "2661");
        checkFromChars<uint128_t>(65535, 32, "1vvv");
        checkFromChars<uint128_t>(65535, 33, "1r5u");
        checkFromChars<uint128_t>(65535, 34, "1mnh");
        checkFromChars<uint128_t>(65535, 35, "1ihf");
        checkFromChars<uint128_t>(65535, 36, "1ekf");

        checkFromChars<int128_t>(-65535, 2, "-1111111111111111");
        checkFromChars<int128_t>(-65535, 3, "-10022220020");
        checkFromChars<int128_t>(-65535, 4, "-33333333");
        checkFromChars<int128_t>(-65535, 5, "-4044120");
        checkFromChars<int128_t>(-65535, 6, "-1223223");
        checkFromChars<int128_t>(-65535, 7, "-362031");
        checkFromChars<int128_t>(-65535, 8, "-177777");
        checkFromChars<int128_t>(-65535, 9, "-108806");
        checkFromChars<int128_t>(-65535, 10, "-65535");
        checkFromChars<int128_t>(-65535, 11, "-45268");
        checkFromChars<int128_t>(-65535, 12, "-31b13");
        checkFromChars<int128_t>(-65535, 13, "-23aa2");
        checkFromChars<int128_t>(-65535, 14, "-19c51");
        checkFromChars<int128_t>(-65535, 15, "-14640");
        checkFromChars<int128_t>(-65535, 16, "-ffff");
        checkFromChars<int128_t>(-65535, 17, "-d5d0");
        checkFromChars<int128_t>(-65535, 18, "-b44f");
        checkFromChars<int128_t>(-65535, 19, "-9aa4");
        checkFromChars<int128_t>(-65535, 20, "-83gf");
        checkFromChars<int128_t>(-65535, 21, "-71cf");
        checkFromChars<int128_t>(-65535, 22, "-638j");
        checkFromChars<int128_t>(-65535, 23, "-58k8");
        checkFromChars<int128_t>(-65535, 24, "-4hif");
        checkFromChars<int128_t>(-65535, 25, "-44la");
        checkFromChars<int128_t>(-65535, 26, "-3iof");
        checkFromChars<int128_t>(-65535, 27, "-38o6");
        checkFromChars<int128_t>(-65535, 28, "-2rgf");
        checkFromChars<int128_t>(-65535, 29, "-2jqo");
        checkFromChars<int128_t>(-65535, 30, "-2cof");
        checkFromChars<int128_t>(-65535, 31, "-2661");
        checkFromChars<int128_t>(-65535, 32, "-1vvv");
        checkFromChars<int128_t>(-65535, 33, "-1r5u");
        checkFromChars<int128_t>(-65535, 34, "-1mnh");
        checkFromChars<int128_t>(-65535, 35, "-1ihf");
        checkFromChars<int128_t>(-65535, 36, "-1ekf");

        for (int i = 2; i <= 36; ++i)
            checkFromChars<uint128_t>(0, i, "0");
    }
    static inline void Negative() noexcept {
        assertTrue(int256_t(-3) * int256_t(-2) == (-3) * (-2));
        assertTrue(int256_t( 3) * int256_t(-2) == ( 3) * (-2));
        assertTrue(int256_t(-3) * int256_t( 2) == (-3) * ( 2));
        assertTrue(int256_t( 3) * int256_t( 2) == ( 3) * ( 2));

        assertTrue(int256_t(-3) / int256_t(-2) == (-3) / (-2));
        assertTrue(int256_t( 3) / int256_t(-2) == ( 3) / (-2));
        assertTrue(int256_t(-3) / int256_t( 2) == (-3) / ( 2));
        assertTrue(int256_t( 3) / int256_t( 2) == ( 3) / ( 2));

        assertTrue(int256_t(-3) % int256_t(-2) == (-3) % (-2));
        assertTrue(int256_t( 3) % int256_t(-2) == ( 3) % (-2));
        assertTrue(int256_t(-3) % int256_t( 2) == (-3) % ( 2));
        assertTrue(int256_t( 3) % int256_t( 2) == ( 3) % ( 2));

        assertTrue(int256_t(-3) + int256_t(-2) == (-3) + (-2));
        assertTrue(int256_t( 3) + int256_t(-2) == ( 3) + (-2));
        assertTrue(int256_t(-3) + int256_t( 2) == (-3) + ( 2));
        assertTrue(int256_t( 3) + int256_t( 2) == ( 3) + ( 2));

        assertTrue(int256_t(-3) - int256_t(-2) == (-3) - (-2));
        assertTrue(int256_t( 3) - int256_t(-2) == ( 3) - (-2));
        assertTrue(int256_t(-3) - int256_t( 2) == (-3) - ( 2));
        assertTrue(int256_t( 3) - int256_t( 2) == ( 3) - ( 2));

        assertTrue( (int256_t(-3) == int256_t(-2)) == ((-3) == (-2)));
        assertTrue( (int256_t( 3) == int256_t(-2)) == (( 3) == (-2)));
        assertTrue( (int256_t(-3) == int256_t( 2)) == ((-3) == ( 2)));
        assertTrue( (int256_t( 3) == int256_t( 2)) == (( 3) == ( 2)));

        assertTrue( (int256_t(-2) == int256_t(-2)) == ((-2) == (-2)));
        assertTrue( (int256_t( 2) == int256_t(-2)) == (( 2) == (-2)));
        assertTrue( (int256_t(-2) == int256_t( 2)) == ((-2) == ( 2)));
        assertTrue( (int256_t( 2) == int256_t( 2)) == (( 2) == ( 2)));

        assertTrue( (-int256_t(-2) == -int256_t(-2)) == (-(-2) == -(-2)));
        assertTrue( (-int256_t( 2) == -int256_t(-2)) == (-( 2) == -(-2)));
        assertTrue( (-int256_t(-2) == -int256_t( 2)) == (-(-2) == -( 2)));
        assertTrue( (-int256_t( 2) == -int256_t( 2)) == (-( 2) == -( 2)));

        assertTrue( (-int256_t(0) ==  int256_t(0)) == (-(0) ==  (0)));
        assertTrue( ( int256_t(0) == -int256_t(0)) == ( (0) == -(0)));

        assertTrue( (int256_t(-3) <= int256_t(-2)) == ((-3) <= (-2)));
        assertTrue( (int256_t( 3) <= int256_t(-2)) == (( 3) <= (-2)));
        assertTrue( (int256_t(-3) <= int256_t( 2)) == ((-3) <= ( 2)));
        assertTrue( (int256_t( 3) <= int256_t( 2)) == (( 3) <= ( 2)));

        assertTrue( (int256_t(-2) <= int256_t(-2)) == ((-2) <= (-2)));
        assertTrue( (int256_t( 2) <= int256_t(-2)) == (( 2) <= (-2)));
        assertTrue( (int256_t(-2) <= int256_t( 2)) == ((-2) <= ( 2)));
        assertTrue( (int256_t( 2) <= int256_t( 2)) == (( 2) <= ( 2)));

        assertTrue( (int256_t(-3) < int256_t(-2)) == ((-3) < (-2)));
        assertTrue( (int256_t( 3) < int256_t(-2)) == (( 3) < (-2)));
        assertTrue( (int256_t(-3) < int256_t( 2)) == ((-3) < ( 2)));
        assertTrue( (int256_t( 3) < int256_t( 2)) == (( 3) < ( 2)));

        assertTrue( (int256_t(-2) < int256_t(-2)) == ((-2) < (-2)));
        assertTrue( (int256_t( 2) < int256_t(-2)) == (( 2) < (-2)));
        assertTrue( (int256_t(-2) < int256_t( 2)) == ((-2) < ( 2)));
        assertTrue( (int256_t( 2) < int256_t( 2)) == (( 2) < ( 2)));

        assertTrue( (int256_t(-3) >= int256_t(-2)) == ((-3) >= (-2)));
        assertTrue( (int256_t( 3) >= int256_t(-2)) == (( 3) >= (-2)));
        assertTrue( (int256_t(-3) >= int256_t( 2)) == ((-3) >= ( 2)));
        assertTrue( (int256_t( 3) >= int256_t( 2)) == (( 3) >= ( 2)));

        assertTrue( (int256_t(-2) >= int256_t(-2)) == ((-2) >= (-2)));
        assertTrue( (int256_t( 2) >= int256_t(-2)) == (( 2) >= (-2)));
        assertTrue( (int256_t(-2) >= int256_t( 2)) == ((-2) >= ( 2)));
        assertTrue( (int256_t( 2) >= int256_t( 2)) == (( 2) >= ( 2)));

        assertTrue( (int256_t(-3) > int256_t(-2)) == ((-3) > (-2)));
        assertTrue( (int256_t( 3) > int256_t(-2)) == (( 3) > (-2)));
        assertTrue( (int256_t(-3) > int256_t( 2)) == ((-3) > ( 2)));
        assertTrue( (int256_t( 3) > int256_t( 2)) == (( 3) > ( 2)));

        assertTrue( (int256_t(-2) > int256_t(-2)) == ((-2) > (-2)));
        assertTrue( (int256_t( 2) > int256_t(-2)) == (( 2) > (-2)));
        assertTrue( (int256_t(-2) > int256_t( 2)) == ((-2) > ( 2)));
        assertTrue( (int256_t( 2) > int256_t( 2)) == (( 2) > ( 2)));
    }
    static inline void NumbericLimits() noexcept {
        assertTrue(std::is_pod<int512_t>::value);
        assertTrue(std::is_pod<uint512_t>::value);
        assertTrue(std::is_standard_layout<int512_t>::value);
        assertTrue(std::is_standard_layout<uint512_t>::value);
    }
    static inline void UnaryIncOrDecr() noexcept {
         int512_t a1 = 0;
         assertTrue(++a1 == 1);
         assertTrue(a1 == 1);

         assertTrue(a1++ == 1);
         assertTrue(a1 == 2);

         assertTrue(--a1 == 1);
         assertTrue(a1 == 1);

         assertTrue(a1-- == 1);
         assertTrue(a1 == 0);

         uint512_t b1 = 0;
         assertTrue(++b1 == 1);
         assertTrue(b1 == 1);

         assertTrue(b1++ == 1);
         assertTrue(b1 == 2);

         assertTrue(--b1 == 1);
         assertTrue(b1 == 1);

         assertTrue(b1-- == 1);
         assertTrue(b1 == 0);
    }
    static inline void Make() noexcept {
        assertEquals(typeid(std::make_signed<int>::type)
                     , typeid(int));
        assertEquals(typeid(std::make_signed<unsigned int>::type)
                     , typeid(int));
        assertEquals(typeid(std::make_signed<std::int128_t>::type)
                     , typeid(std::int128_t));
        assertEquals(typeid(std::make_signed<std::int256_t>::type)
                     , typeid(std::int256_t));
        assertEquals(typeid(std::make_signed<std::int512_t>::type)
                     , typeid(std::int512_t));
        assertEquals(typeid(std::make_signed<std::uint128_t>::type)
                     , typeid(std::int128_t));
        assertEquals(typeid(std::make_signed<std::uint256_t>::type)
                     , typeid(std::int256_t));
        assertEquals(typeid(std::make_signed<std::uint512_t>::type)
                     , typeid(std::int512_t));

        assertEquals(typeid(std::make_unsigned<std::int128_t>::type)
                     , typeid(std::uint128_t));
        assertEquals(typeid(std::make_unsigned<std::int256_t>::type)
                     , typeid(std::uint256_t));
        assertEquals(typeid(std::make_unsigned<std::int512_t>::type)
                     , typeid(std::uint512_t));
        assertEquals(typeid(std::make_unsigned<std::uint128_t>::type)
                     , typeid(std::uint128_t));
        assertEquals(typeid(std::make_unsigned<std::uint256_t>::type)
                     , typeid(std::uint256_t));
        assertEquals(typeid(std::make_unsigned<std::uint512_t>::type)
                     , typeid(std::uint512_t));
    }
    static inline void IsWideInt() noexcept {
        assertFalse(std::is_wide_int<int>::value);
        assertFalse(std::is_wide_int<long>::value);

        assertTrue(std::is_wide_int<std::int128_t>::value);
        assertTrue(std::is_wide_int<std::int256_t>::value);
        assertTrue(std::is_wide_int<std::int512_t>::value);
        assertTrue(std::is_wide_int<std::uint128_t>::value);
        assertTrue(std::is_wide_int<std::uint256_t>::value);
        assertTrue(std::is_wide_int<std::uint512_t>::value);
    }
    static inline void Utils() noexcept {
        assertEquals(std::abs(std::int128_t(-1)), std::int128_t(1));
        assertEquals(std::abs(std::uint128_t(1)), std::uint128_t(1));
        assertEquals(std::abs(std::int256_t(1)),  std::int256_t(1));
    }
    static void NativeOperatorsAssignFloat() {
//         std::uint512_t a1 = 18; //XXX

//         a1 *= 2.5;
//         assertEquals(a1, std::uint512_t(45));

//         a1 = a1 * 2.5;
//         assertEquals(a1, std::uint512_t(112));

//         a1 = 1.5 * a1;
//         assertEquals(a1, std::uint512_t(168));

//         a1 /= 2.5;
//         assertEquals(a1, std::uint512_t(67));

//         a1 += 2.9;
//         assertEquals(a1, std::uint512_t(69));

//         a1 -= 1.9;
//         assertEquals(a1, std::uint512_t(68));
    }
    static void NumericLimits() noexcept {
        assertTrue(
          std::numeric_limits<std::uint128_t>::max() ==
              340282366920938463463374607431768211455_uint128);
    }

public:
    void operator()() noexcept {
        Ctors();
        LeftShifts();
        RightShifts();
        RightShiftsMoreZero();
        RightShiftsLessZero();
        Assign();
        OperatorPlus();
        OperatorTilda();
        OperatorUnaryMinus();
        OperatorPlusWide();
        OperatorMinus();
        OperatorStar();
        OperatorMore();
        OperatorEq();
        OperatorPipe();
        OperatorAmp();
        OperatorSlash();
        OperatorPercent();
        Circumflex();
        ToString();
        Cast();
        FromString();
        NativeOperators();
        NativeOperatorsAssign();
        Constexpr();
        Etc();
        ToChars();
        FromChars();
        Negative();
        NumbericLimits();
        UnaryIncOrDecr();
        Make();
        IsWideInt();
        Utils();
        NativeOperatorsAssignFloat();
        NumericLimits();
    }
};

} // end namespace ps2
