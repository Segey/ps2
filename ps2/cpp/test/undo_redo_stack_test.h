/**
 * \file      ps2/ps2/cpp/test/undo_redo_stack_test.h
 * \brief     UndoRedoStackTest class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   May      (the) 31(th), 2015, 19:46 MSK
 * \updated   December (the) 16(th), 2016, 14:40 MSK
 * \TODO
**/
#pragma once
#include <ps2/cpp/undo_redo_stack.h>

class UndoRedoStackTest final {
public:
    using class_name = UndoRedoStackTest;
    using tested     = ps2::UndoRedoStack<int,6u>;

private:
    static void EmptyAfterDefaultInit() noexcept {
        tested cls;
        assertTrue(cls.isEmpty());
    }
    static void InValidFirstUndo() noexcept {
        tested cls;
        cls.push_back(15);

        assertEquals(*cls.current(), 15);
        assertEquals(cls.redoSize(), static_cast<size_t>(0u));
        assertEquals(cls.undoSize(), static_cast<size_t>(0u));
        assertFalse(cls.hasUndo());
        assertFalse(cls.hasRedo());
    }
    static void ValidPtrAfterInit() noexcept {
        tested cls{7,0,4};

        assertEquals(*cls.current(), 4);
    }
    static void ValidPtrAfterPushBack() noexcept {
        tested cls{3,2,4};
        cls.push_back(4);

        assertEquals(*cls.current(), 4);
        assertEquals(cls.redoSize(), static_cast<size_t>(0u));
        assertEquals(cls.undoSize(), static_cast<size_t>(3u));
    }
    static void ValidAfterUndo() noexcept {
        tested cls{3,2,5,7};
        cls.undo();

        assertEquals(*cls.current(), 5);
        assertEquals(cls.redoSize(), static_cast<size_t>(1u));
        assertEquals(cls.undoSize(), static_cast<size_t>(2u));
    }
    static void ValidAfterRedo() noexcept {
        tested cls{5,3,2};
        cls.push_back(10);
        cls.push_back(5);
        cls.redo();

        assertEquals(*cls.current(), 5);
        assertEquals(cls.redoSize(), static_cast<size_t>(0u));
        assertEquals(cls.undoSize(), static_cast<size_t>(4u));
    }
    static void ValidFirstUndo() noexcept {
        tested cls;
        cls.push_back(15);
        cls.push_back(1);

        assertEquals(*cls.current(), 1);
        assertTrue(cls.hasUndo());
        assertFalse(cls.hasRedo());
    }
    static void ValidFirstUndoAction() noexcept {
        tested cls;
        cls.push_back(15);
        cls.push_back(1);
        cls.undo();

        assertEquals(*cls.current(), 15);
        assertFalse(cls.hasUndo());
        assertTrue(cls.hasRedo());
    }
    static void ValidNavigation() noexcept {
        tested cls{5,3,2,6};
        cls.undo();
        cls.undo();
        cls.undo();
        cls.redo();
        cls.push_back(15);
        cls.undo();
        cls.undo();

        assertEquals(*cls.current(), 5);
        assertEquals(cls.redoSize(), static_cast<size_t>(2u));
        assertEquals(cls.undoSize(), static_cast<size_t>(0u));
    }
    static void ValidBorder() noexcept {
        tested cls{5,3,2,6,9};
        cls.push_back(1);
        cls.push_back(111);

        assertEquals(*cls.current(), 111);
        assertEquals(cls.redoSize(), static_cast<size_t>(0u));
        assertEquals(cls.undoSize(), static_cast<size_t>(5u));
    }
    static void ValidComplexBorder() noexcept {
        tested cls{5,3,2,6,9};
        cls.push_back(1);
        cls.push_back(111);
        cls.undo();
        cls.undo();
        cls.push_back(12);

        assertEquals(*cls.current(), 12);
        assertEquals(cls.redoSize(), static_cast<size_t>(0u));
        assertEquals(cls.undoSize(), static_cast<size_t>(4u));
    }

public:
    void operator()() noexcept {
        EmptyAfterDefaultInit();
        InValidFirstUndo();
        ValidPtrAfterInit();
        ValidPtrAfterPushBack();
        ValidAfterUndo();
        ValidAfterRedo();
        ValidNavigation();
        ValidBorder();
        ValidFirstUndo();
        ValidFirstUndoAction();
        ValidComplexBorder();
    }
};

