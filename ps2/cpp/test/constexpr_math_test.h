/**
 * \file      /home/dix/projects/perfect/projects/ps2/ps2/cpp/test/decimal_test.h 
 * \brief     The Decimal_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 04(th), 2017, 11:36 MSK
 * \updated   June      (the) 04(th), 2017, 11:36 MSK
 * \TODO      
**/
#pragma once
#include <iomanip>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <ps2/cpp/constexpr_math.h>
#include <ps2/cpp/literal/literals.h>

/** \namespace ps2 */
namespace ps2 {

class ConstexprMathTest final {
public:
    using class_name = ConstexprMathTest;

private:
    static inline void Ipow2() noexcept {
        assertEquals((ps2::ipow<2, 0>()), 1);
        assertEquals((ps2::ipow<2, 1>()), 2);
        assertEquals((ps2::ipow<2, 2>()), 4);
        assertEquals((ps2::ipow<2, 3>()), 8);
        assertEquals((ps2::ipow<2, 5>()), 32);
    }
    static inline void Ipow10() noexcept {
        assertEquals((ps2::ipow<10, 0>()), 1);
        assertEquals((ps2::ipow<10, 1>()), 10);
        assertEquals((ps2::ipow<10, 2>()), 100);
        assertEquals((ps2::ipow<10, 3>()), 1000);
        assertEquals((ps2::ipow<10, 5>()), 100000);
    }

public:
    void operator()() noexcept {
        Ipow2();
        Ipow10();
    }
};

} // end namespace ps2
