/**
 * \file      ps2/ps2/cpp/test/main.h
 * \brief     Main ps2 cpp test
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.10
 * \created   April     (the) 14(th), 2015, 18:45 MSK
 * \updated   May       (the) 31(th), 2017, 15:00 MSK
 * \TODO
**/
#pragma once
#include "xor_test.h"
#include "undo_redo_stack_test.h"
#include "fraction_test.h"
#include "concat_test.h"
#include "integer_test.h"
#include "string_algo_test.h"
#include "wide_int_test.h"
#include "constexpr_math_test.h"
#include "decimal_test.h"
#include "bitpack_test.h"
#include "literal/main.h"

/** \namespace ps2 */
namespace ps2 {

class MainCppTestClasses final {
public:
    using class_name = MainCppTestClasses;

public:
    void operator()() noexcept {
        TEST(ConstexprMathTest);
        TEST(XorTest);
        TEST(UndoRedoStackTest);
        TEST(FractionTest);
        TEST(ConcatTest);
        TEST(ps2::IntegerTest);
        TEST(ps2::StringTest);
        TEST(ps2::WideIntTest);
        TEST(ps2::DecimalTest);
        TEST(ps2::BitpackTest);
        TEST_CLASSES(cpp::LiteralsMainTestClasses);
    }
};

} // end namespace ps2
