/**
 * \file      ps2/ps2/cpp/test/xor_test.h
 * \brief     iSet IRoNDooM Workout test class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   April (the) 14(th), 2015, 18:47 MSK
 * \updated   May   (the) 18(th), 2015, 15:09 MSK
 * \TODO
**/
#pragma once
#include <vector>
#include <ps2/cpp/xor.h>

class XorTest final {
public:
    using class_name = XorTest;
    using vec_t      = std::vector<char>;

private:
    static void simpleCharDecode() noexcept {
        assertEquals(ps2::Xor::simple('I'), char(-74));
    }
    static void simpleCharEncode() noexcept {
        assertEquals(ps2::Xor::simple(-74), 'I');
    }
    static void simpleStringDecode() noexcept {
        vec_t result{ -74,-83, -69};
        vec_t data{'I', 'R', 'D'};

        assertEquals(ps2::Xor::simple(data), result);
    }
    static void simpleStringEncode() noexcept {
        vec_t str{ -74,-83, -69};
        vec_t result{'I', 'R', 'D'};

        assertEquals(ps2::Xor::simple(str), result);
    }

public:
    void operator()() noexcept {
         simpleCharDecode();
         simpleCharEncode();
         simpleStringDecode();
         simpleStringEncode();
    }
};
