/**
 * \file      ps2/ps2/cpp/test/fraction_test.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   Март (the) 01(th), 2016, 21:54 MSK
 * \updated   Март (the) 01(th), 2016, 21:54 MSK
 * \TODO
**/
#pragma once
#include <ps2/cpp/fraction.h>

/** \namespace ps2 */
namespace ps2 {

class FractionTest final {
public:
    using class_name = FractionTest;

private:
    static void Simple() noexcept {
        constexpr ps2::Fraction<2> cls{1};
        assertEquals(cls.toDouble(), 0.5);
        assertEquals(cls.denominator(), 2);
        assertFalse(cls.isNull());
        assertTrue(cls.isValid());
    }
    static void Complex() noexcept {
        constexpr ps2::Fraction<2> cls{1,1};
        assertEquals(cls.toDouble(), 1.5);
    }
    static void Fraction10() noexcept {
        constexpr ps2::Fraction10 cls{3};
        assertEquals(cls.toDouble(), .3);
    }
    static void IsEqual() noexcept {
        constexpr ps2::Fraction10 _1{3};
        constexpr ps2::Fraction10 _2{3};

        assertEquals(_1, _2);
    }
    static void IsNotEqual() noexcept {
        constexpr ps2::Fraction5 _1{3};
        constexpr ps2::Fraction5 _2{5};

        assertNotEquals(_1, _2);
    }
    static void Swap() noexcept {
        ps2::Fraction5 _1{3};
        ps2::Fraction5 _2{5};

        swap(_1, _2);
        assertEquals(_1, ps2::Fraction5{5});
        assertEquals(_2, ps2::Fraction5{3});
    }

public:
    void operator()() noexcept {
         Simple();
         Complex();
         Fraction10();
         IsEqual();
         IsNotEqual();
         Swap();
    }
};
} // end namespace ps2
