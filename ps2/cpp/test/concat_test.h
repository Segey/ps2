/**
 * \file      ps2/ps2/cpp/test/concat_test.h
 * \brief     The Concat_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   October (the) 28(th), 2016, 14:28 MSK
 * \updated   October (the) 28(th), 2016, 14:28 MSK
 * \TODO
**/
#pragma once
#include <string>
#include <vector>
#include <ps2/cpp/algorithm/concat.h>

class ConcatTest {
public:
    using class_name = ConcatTest;

private:
    static void Empty() noexcept {
        std::vector<int> vec;
        auto&& result = ps2::concat(vec, ","s, ""s, ""s, [](auto const& str, auto val){
            return str + std::to_string(val);
        });
        assertEquals(result, std::string());
    }
    static void OneElement() noexcept {
        std::vector<int> vec {1};
        auto&& result = ps2::concat(vec, ","s, ""s,""s,[](auto const& str, auto val){
            return str + std::to_string(val);
        });
        assertEquals(result, "1"s);
    }
    static void MoreElements() noexcept {
        std::vector<int> vec {1, 2, 3};
        auto&& result = ps2::concat(vec, ", "s, ""s,""s,[](auto const& str, auto val){
            return str + std::to_string(val);
        });
        assertEquals(result, "1, 2, 3"s);
    }

public:
    void operator()() noexcept {
         Empty();
         OneElement();
         MoreElements();
    }
};
