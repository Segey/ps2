/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/test/fixed_point_test.h 
 * \brief     The Fixed_point_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 02(th), 2017, 11:50 MSK
 * \updated   June      (the) 02(th), 2017, 11:50 MSK
 * \TODO      
**/
#pragma once
#include <iomanip>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <ps2/cpp/fixed_point.h>
#include <ps2/cpp/elastic_fixed_point.h>
#include <ps2/cpp/literal/literals.h>

/** \namespace ps2 */
namespace ps2 {

class FixedPointTest final {
public:
    using class_name = FixedPointTest;

private:
    static inline float averageInteger(float input1, float input2) noexcept {
        auto fixed1 = static_cast<int32_t>(input1 * 65536.f);
        auto fixed2 = static_cast<int32_t>(input2 * 65536.f);

        auto const sum = int64_t{fixed1} + fixed2;
        auto const avg = sum / 2;
        return avg / 65536.f;
    }
    static inline float averageElasticInteger(float input1, float input2) {
        using namespace sg14::literals;

        auto fixed1 = sg14::elastic_integer<31>{input1 * 65536.f};
        auto fixed2 = sg14::elastic_integer<31>{input2 * 65536.f};

        auto const sum = fixed1 + fixed2;
        auto const avg = sum / 2_c;
        return static_cast<float>(avg) / 65536.f;
    }
    static inline float averageFixedPoint(float input1, float input2) {
        auto fixed1 = sg14::fixed_point<int32_t, -16>{input1};
        auto fixed2 = sg14::fixed_point<int32_t, -16>{input2};

        auto sum = sg14::fixed_point<int64_t, -16>{fixed1} + fixed2;
        auto avg = sg14::divide(sum, 2);
        return static_cast<float>(avg);
    }
    static inline float averageElasticPoint(float input1, float input2) {
        using namespace sg14::literals;

        auto fixed1 = sg14::elastic_fixed_point<15, 16>{input1};
        auto fixed2 = sg14::elastic_fixed_point<15, 16>{input2};

        auto sum = fixed1 + fixed2;
        //auto avg = sum / 2_elastic;
        auto avg = sg14::divide(sum, 2_elastic);
        return static_cast<float>(avg);
    }

private:
    static inline void IsIntegerOrFloat() noexcept {
        using sg14::_impl::is_integer_or_float;
        static_assert(is_integer_or_float<uint8_t>::value
             , "sg14::_integer_impl::is_integer_or_float test failed");
        static_assert(is_integer_or_float<float>::value
              , "sg14::_integer_impl::is_integer_or_float test failed");
        static_assert(!is_integer_or_float<void>::value
              , "sg14::_integer_impl::is_integer_or_float test failed");
        static_assert(!is_integer_or_float<int*>::value
              , "sg14::_integer_impl::is_integer_or_float test failed");
        static_assert(!is_integer_or_float<std::string>::value
              , "sg14::_integer_impl::is_integer_or_float test failed");
    }
    static inline void ZeroCostAverage() noexcept {
         assertDoubleEquals(averageInteger(32000.125, 27805.75)
                            , 29902.9375f);
         assertDoubleEquals(averageInteger(30000, 0.125)
                            , 15000.0625f);

         assertDoubleEquals(averageElasticInteger(32000.125, 27805.75)
                            , 29902.9375f);
         assertDoubleEquals(averageElasticInteger(30000, 0.125)
                            , 15000.0625f);

         assertDoubleEquals(averageFixedPoint(30000, 0.125)
                            , 15000.0625f);
         assertDoubleEquals(averageFixedPoint(32000.125, 27805.75)
                            , 29902.9375f);

         assertDoubleEquals(averageElasticPoint(30000, 0.125)
                            , 15000.0625f);
         assertDoubleEquals(averageElasticPoint(32000.125, 27805.75)
                            , 29902.9375f);
    }
    static inline void Ctors() noexcept {
    }
    static inline void UtilsSin() noexcept {
        auto x = 45.678;
        auto y = static_cast<int>(x * 1000);
        std::cout <<  (y / 1000)  << " " << (y % 1000) << std::endl;
        std::cout << sg14::fixed_point<int64_t, -4>{x} << std::endl;
        std::cout << sg14::fixed_point<int32_t, -5>{x} << std::endl;
        std::cout << sg14::fixed_point<int32_t, -6>{x} << std::endl;

//        std::cout << sg14::fixed_point<int32_t, -2>{50.0422} << std::endl;
//        std::cout << sg14::fixed_point<int32_t, -3>{50.04222} << std::endl;
//        std::cout << sg14::fixed_point<int32_t, 1>{50.04333}  << std::endl;

        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::uint8_t, -6>(0))
                           , 0.);
        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::int16_t, -13>(3.1415926))
                           , 0.f);
        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::uint16_t, -14>(3.1415926/2))
                           , 1.);
        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::int32_t, -24>(3.1415926*7./2.))
                           , -1.);
        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::int32_t, -28>(3.1415926/4))
                           , .707106769f);
        assertDoubleEquals(sg14::sin(sg14::fixed_point<std::int16_t, -10>(-3.1415926/3))
                           , -.865234375);
    }
    static inline void UtilsCos() noexcept {
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::uint8_t, -6>(0))
                           , 1.f);
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::int16_t, -13>(3.1415926))
                           , -1.);
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::uint16_t, -14>(3.1415926/2))
                           , 0.L);
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::int32_t, -20>(3.1415926*7./2.))
                           , 0.f);
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::int32_t, -28>(3.1415926/4))
                           , .707106829f);
        assertDoubleEquals(sg14::cos(sg14::fixed_point<std::int16_t, -10>(-3.1415926/3))
                           , .5L);
    }
    static inline void Abs() noexcept {
        static_assert(sg14::abs(sg14::make_fixed<7, 0>(66))==66
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_fixed<7, 0>(-123))==123
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_fixed<63, 0>(9223372036854775807))==9223372036854775807LL
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_fixed<63, 0>(-9223372036854775807))==9223372036854775807LL
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_fixed<7, 8>(-5))==5
                      , "sg14::abs test failed");

        static_assert(sg14::abs(sg14::make_ufixed<8, 0>(66))==66
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_ufixed<8, 0>(123))==123
                      , "sg14::abs test failed");
        static_assert(sg14::abs(sg14::make_ufixed<8, 8>(5))==5
                      , "sg14::abs test failed");
//        static_assert((sg14::make_ufixed<56, 8>(1003006)*sg14::make_ufixed<56, 8>(7))==7021042
//                      , "sg14::fixed_point test failed");
//        static_assert(static_cast<int>((sg14::fixed_point<uint64_t, -8>(65535)/sg14::fixed_point<uint64_t, -8>(256)))==255
//                      , "sg14::fixed_point test failed");
//        static_assert(sqrt(sg14::make_fixed<63, 0>(9223372036854775807))==3037000499LL
//                      , "sg14::sqrt test failed");
    }

public:
    void operator()() noexcept {
        IsIntegerOrFloat();
        ZeroCostAverage();
        Ctors();
        UtilsSin();
        UtilsCos();
        Abs();
    }
};

} // end namespace ps2
