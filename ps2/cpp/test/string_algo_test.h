/**
 * \file      C:/projects/perfect/projects/ps2/ps2/cpp/test/string_test.h 
 * \brief     The String_test class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 12(th), 2017, 00:55 MSK
 * \updated   June      (the) 12(th), 2017, 00:55 MSK
 * \TODO      
**/
#pragma once
#include <string>
#include <ps2/cpp/string_algo.h>

/** \namespace ps2 */
namespace ps2 {

class StringTest final {
public:
    using class_name = StringTest;

private:
    static inline void LeftJustified() noexcept {
        assertEquals(ps2::leftJustified("cool"_s, 3, '0'), "cool"_s);
        assertEquals(ps2::leftJustified("cool"_s, 5, '0'), "0cool"_s);
        assertEquals(ps2::leftJustified("cool"_s, 9, '0'), "00000cool"_s);
    }

public:
    void operator()() noexcept {
        LeftJustified();
    }
};

} // end namespace ps2

