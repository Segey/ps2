/**
 * \file      perfect/projects/ps2/ps2/cpp/test/bitpack_test.h
 * \brief     The Bitpack_test class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 30(th), 2019, 23:17 MSK
 * \updated   January   (the) 30(th), 2019, 23:17 MSK
 * \TODO
**/
#pragma once
#include <set>
#include <ps2/cpp/pack/bitpack.h>

/** \namespace ps2 */
namespace ps2 {

// namespace <empty>
namespace  {

ps2::Bitpack<6, 6> create(size_t n) {
    ps2::Bitpack<6> res;
    for (auto i = 0u; i < 6u; ++i) {
        if(n & (1 << i))
            res.set(i);
    }
    return res;

}

} // <empty> namespace

TEST_CASE("Bitpack testing", "[bitpack<Size, Len>]" ) {
    SECTION("Testing constructors") {
        Bitpack<7> _1;

        REQUIRE(_1.size() == 12);
    }
    SECTION("Testing copy constructors") {
        Bitpack<8, 4> _1;
        auto const& _2 = _1;

        REQUIRE(_1.size() == 8);
        REQUIRE(_2.size() == 8);
    }
    SECTION("Testing asign ctor constructors") {
        Bitpack<8> _1;
        Bitpack<8> _2;
        _1 = _2;

        REQUIRE(_1.size() == 12);
        REQUIRE(_2.size() == 12);
    }
    SECTION("Testing equal operators") {
        Bitpack<8> _1;
        Bitpack<8> _2;
        _1 = _2;

        REQUIRE(_1 == _2);
    }
    SECTION("Testing no equal operators") {
        Bitpack<8> _1;
        Bitpack<8> _2;
        _2[6] = true;

        REQUIRE(_1 != _2);
    }
    SECTION("Testing setters and getters") {
        Bitpack<8> _1;
        REQUIRE(_1.get(5) == false);

        _1.set(5);
        REQUIRE(_1.get(5));

        _1.unset(5);
        REQUIRE(_1.get(5) == false);
    }
    SECTION("Testing setters and getters") {
        Bitpack<16> _1;
        REQUIRE(_1[11] == false);

        _1[11] = true;
        REQUIRE(_1[11]);

        _1[11] = false;
        REQUIRE(_1[11] == false);
    }
    SECTION("Testing ptint function") {
        Bitpack<6> _1 {{0,1,1,0,1,0}};
        REQUIRE(_1.print() == std::string("011010"));

        Bitpack<4, 4> _2 {{1,1,1,0,1,0}};
        REQUIRE(_2.print() == std::string("1110"));
    }
    SECTION("Testing simple pack function") {
        Bitpack<12> _1 {{0,1,1,0,1,0,1,0,1}};
        REQUIRE(_1.pack() == std::string("d5"));

        Bitpack<8, 4> _2 {{1,1,1,0,1,0,0,1,1}};
        REQUIRE(_2.pack() == std::string("79"));
    }
    SECTION("Testing full range of chars") {
        std::set<std::string> s;

        for(auto i = 0u; i != 64u; ++i) {
            Bitpack<6> _1 = create(i);
            auto const& l = _1.pack();
            auto const& _2 = Bitpack<6>::unpack(l);
            s.insert(l);
            REQUIRE(_1 == _2);
        }
        REQUIRE(s.size() == 64u);
    }
    SECTION("Testing even numbers of 2 chars") {
        Bitpack<12> _1;

        for(auto i = 0u; i != 12u; ++i) {
            _1.set(i);
            auto const& l = _1.pack();
            auto const& _2 = Bitpack<12>::unpack(l);
            REQUIRE(_1 == _2);
            REQUIRE(l == _2.pack());
        }
    }
}

} // end namespace ps2
