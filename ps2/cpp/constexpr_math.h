/**
 * \file      /home/dix/projects/perfect/projects/ps2/ps2/cpp/constexpr_math.h
 * \brief     The Constexpr_math class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   June      (the) 04(th), 2017, 21:06 MSK
 * \updated   June      (the) 04(th), 2017, 21:06 MSK
 * \TODO
**/
#include <type_traits>

#pragma once
/** \namespace ps2 */
namespace ps2 {

/** namespace hide */
namespace hide {
template<int I>
constexpr inline int ipow(int i) noexcept {
    return i;
}
template<int I, class... Args>
constexpr inline int ipow(int i, int, Args&&... args) noexcept {
    return ipow<I>(i * I, std::forward<Args>(args)...);
}

template<int I, int... Is>
constexpr inline int ipow(int i, std::integer_sequence<int, Is...>) noexcept {
    return ipow<I>(i,Is...);
}
} // end namespace hide

template<int I, int N>
constexpr inline int ipow() noexcept {
    return hide::ipow<I>(1, std::make_integer_sequence<int, N>{} );
}


} // end namespace ps2
