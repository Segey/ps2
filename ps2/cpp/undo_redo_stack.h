/**
 * \file      ps2/ps2/cpp/undo_redo_stack.h
 * \brief     RedoUndoStack class contains a Redo and an Undo operations
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 29(th), 2015, 01:40 MSK
 * \updated   May (the) 29(th), 2015, 01:40 MSK
 * \TODO
**/
#pragma once
#include <vector>
/** \namespace ps2 */

namespace ps2 {
/**
 * \code
 *      UndoRedoStack<int> stack;
 *      stack.push_back(15);
 *      stack.push_back(16);
 *      stack.undo();
 *      stack.redo();
 *      auto const& val = stack.current();
 * \endcode
**/
template<typename T, uint MAX = 100u>
class UndoRedoStack {
public:
    typedef T                                value_t;
    typedef UndoRedoStack<T, MAX>            class_name;
    typedef std::vector<value_t>             stack_t;
    typedef typename stack_t::iterator       iterator;
    typedef typename stack_t::const_iterator const_iterator;

public:
    stack_t m_undo;
    stack_t m_redo;

public:
    UndoRedoStack() { }
    UndoRedoStack(std::initializer_list<value_t> const& list)
        : m_undo(list) {
    }
    const_iterator current() const {
        return m_undo.empty() ? m_undo.end() : m_undo.end() - 1;
    }
    void undo() {
        if(!hasUndo()) return;
        m_redo.push_back(m_undo.back());
        m_undo.pop_back();
    }
    void redo() {
        if(!hasRedo()) return;
        m_undo.push_back(m_redo.back());
        m_redo.pop_back();
    }
    bool hasUndo() const {
        return m_undo.size() > 1u;
    }
    bool hasRedo() const {
        return !m_redo.empty();
    }
    size_t undoSize() const {
        return m_undo.size() - 1u;
    }
    size_t redoSize() const {
        return m_redo.size();
    }
    void push_back(value_t const& val) {
        m_redo.clear();
        m_undo.push_back(val);
        if(isFull()) m_undo.erase(m_undo.begin());
    }
    void push_back(value_t&& val) {
        m_redo.clear();
        m_undo.push_back(val);
        if(isFull()) m_undo.erase(m_undo.begin());
    }
    bool isFull() const {
        return m_redo.size() + m_undo.size() > MAX;
    }
    bool isEmpty() const noexcept {
        return m_redo.size() + m_undo.size() == 0u;
    }
};

} // end namespace ps2
