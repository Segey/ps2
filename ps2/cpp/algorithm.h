/**
 * \file      C:/projects/perfect/projects/ps2/ps2/cpp/algorithm.h
 * \brief     The Algorithm class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   June      (the) 28(th), 2017, 01:54 MSK
 * \updated   June      (the) 28(th), 2017, 01:54 MSK
 * \TODO
**/
#pragma once
#include <limits>
#include <memory>
#include "typedefs.h"

/** \namespace ps2 */
namespace ps2 {

template<class T>
struct tune_up_t {
    using type = T;
    constexpr static inline T null() noexcept {
        return std::numeric_limits<T>::max();
    }
    constexpr static inline T zero() noexcept {
        return T{0};
    }
    constexpr static inline T one() noexcept {
        return T{1};
    }
    constexpr static inline T new_() noexcept {
        return true;
    }
};

template<class... Args>
constexpr auto null() noexcept -> decltype(tune_up_t<Args...>::null()) {
    return tune_up_t<Args...>::null();
};
template<class... Args>
constexpr auto zero() noexcept -> decltype(tune_up_t<Args...>::zero()) {
    return tune_up_t<Args...>::zero();
};
template<class... Args>
constexpr auto one() noexcept -> decltype(tune_up_t<Args...>::one()) {
    return tune_up_t<Args...>::one();
};

template<class T>
bool is_null(T val) noexcept {
    return val == tune_up_t<T>::null();
};
template<class T>
bool is_null(std::shared_ptr<T> const& val) noexcept {
    return val == nullptr;
};

template<class T>
constexpr bool is_zero(T val) noexcept {
    return val == tune_up_t<T>::zero();
};
template<class T>
constexpr bool is_one(T val) noexcept {
    return val == tune_up_t<T>::one();
};

template<class T, typename std::enable_if<!std::is_enum<T>::value, int>::type = 0>
constexpr static bool is_valid(T val) noexcept {
    return !is_null(val);
};
template<class T, typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
constexpr static bool is_valid(T val) noexcept {
    return val != T::Invalid;
};

template<class T>
constexpr static bool is_new(T val) noexcept {
    return val == tune_up_t<T>::new_();
};

constexpr static inline bool is_valid(int val) noexcept {
    return val != std::numeric_limits<int>::max()
        && val != std::numeric_limits<int>::min()
    ;
}
constexpr static inline bool is_valid(double val) noexcept {
    return val != std::numeric_limits<double>::max()
        && val != std::numeric_limits<double>::min()
    ;
}

/**
 * \code
        ps2::conditional_value<(S1 > S2), S1, S2>::value
        [[deprecated]]
 * \endcode
**/
template<bool is, std::size_t _1, std::size_t _2>
struct conditional_value final {
   static constexpr std::size_t value = _2;
};

template<std::size_t _1, std::size_t _2>
struct conditional_value<true, _1, _2> final {
   static constexpr std::size_t value = _1;
};

template<class T>
constexpr static inline T abs(T val) noexcept {
    val *= val >= 0 ? 1 : -1;
    return val;
}

} // end namespace ps2
