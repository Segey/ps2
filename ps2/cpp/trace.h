/**
 * \file      ps2/ps2/cpp/trace.h
 * \brief     The class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 18(th), 2016, 01:02 MSK
 * \updated   May (the) 18(th), 2016, 01:02 MSK
 * \TODO
**/
#pragma once
#define ASSERT _ASSERTE

#ifdef _DEBUG
#define VERIFY ASSERT
#else
#define VERIFY(expression) (expression)
#endif

/** \namespace ps2 */
namespace ps2 {

#include <stdio.h>
#include <crtdbg.h>
#include <windows.h>

#ifdef _DEBUG

static inline void Trace(wchar_t const * format, ...) {
    va_list args;
    va_start(args, format);

    wchar_t buffer [256];
    ASSERT(-1 != _vsnwprintf_s(buffer, _countof(buffer) -1, format, args));
    va_end(args);
    OutputDebugString(buffer);
}

class Tracer final {
public:
    using Tracer = class_name;

private:
    char const* m_filename = nullptr;
    unsigned m_line        = 0u;

public:
    Tracer(char const* filename, unsigned line) noexcept
        : m_filename { filename }
        , m_line { line } {
    }
    template <typename... Args>
    void operator()(wchar_t const* format, Args&&... args) const {
        wchar_t buffer [256];

        auto const count = swprintf_s(buffer, L"%S(%d): ", m_filename, m_line);
        ASSERT(-1 != count);
        ASSERT(-1 != _snwprintf_s(buffer + count, _countof(buffer) - count, _countof(buffer) - count - 1, format, std::forward<args>&&...));
        OutputDebugString(buffer);
    }
};

#endif
} // end namespace ps2

#ifdef _DEBUG
#define TRACE ps2::Tracer(__FILE__, __LINE__)
#else
#define TRACE __noop
#endif
