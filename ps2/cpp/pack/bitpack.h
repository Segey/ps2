/**
 * \file      projects/ps2/ps2/cpp/bitpack.h
 * \brief     The Bitpack class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 30(th), 2019, 15:40 MSK
 * \updated   January   (the) 30(th), 2019, 15:40 MSK
 * \TODO
**/
#pragma once
#include <deque>
#include <array>
#include <string>
#include <cassert>
#include <ps2/cpp/algorithm.h>
#include "base64_table.h"

/** \namespace ps2 */
namespace ps2 {

/**
 * \brief The Bitpack pack represents a line with bool value. During compression each char collects <Size> bits.
          Size - number of pack size.
 * \brief Size - numer of bits, Len - compress number per char[2-6]
 * \code
      Bitpack<12, 6> pack {{1,0,1,0,1,1,1,1,1,0,0,1}}; // 101011111001
      auto const& str = pack.pack();                   // KR
      auto const& p1 = Bitpack<12, 6>::unpack(str);    // 101011111001
      p1.print();                                      // 101011111001
 * \endcode
 **/
template<size_t Size, size_t Len = 6u>
class Bitpack final {
 public:
    using value_t = bool;
    using class_name = Bitpack<Size, Len>;

 private:
    static inline constexpr size_t len() noexcept {
        static_assert(1 < Len && Len <= 6);
        return Size % Len ? (Size / Len + 1) * Len : Size;
    }

 private:
    std::deque<value_t> m_vec = std::deque<value_t>(len());

 public:
    Bitpack() = default;
    Bitpack(std::vector<value_t> const& lis) noexcept {
        auto const min = std::min(lis.size(), size());
        for(auto i = 0u; i != min; ++i)
            if(lis[i])
                set(i);
    }
    Bitpack(class_name const&) = default;
    Bitpack& operator=(class_name const&) = default;
    size_t size() const noexcept {
        return m_vec.size();
    }
    static constexpr size_t linePackSize() noexcept {
        return len()/Len;
    }
    value_t operator[](size_t n) const noexcept {
        return m_vec[n];
    }
    value_t& operator[](size_t n) noexcept {
        return m_vec[n];
    }
    value_t get(size_t n) const {
        assert(n < size());
        return m_vec[n];
    }
    void set(size_t n) {
        assert(n < size());
        m_vec[n] = true;
    }
    void unset(size_t n) {
        assert(n < size());
        m_vec[n] = false;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_vec == rhs.m_vec;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs.m_vec == rhs.m_vec);
    }
    std::string pack() const noexcept {
        std::string result;
        for (auto i = 0u; i != m_vec.size(); i += Len) {
            auto s = 0u;
            for (auto u = 0u; u != Len; ++u) {
                if(get(i+u))
                    s += 1 << u;
            }
            result += Base64Table::character(s);
        }
        return result;
    }
    static class_name unpack(std::string const& val) noexcept {
        assert(val.size() == linePackSize());

        class_name result;
        for (auto x = 0; x != linePackSize(); ++x) {
            auto const ch = Base64Table::number(val[x]);
            for (auto i = 0; i != Len; ++i) {
                if ((ch) & (1 << (i)))
                    result.set(x*Len + i);
            }
        }
        return result;
    }
    std::string print() const noexcept {
        std::string result;
        for (auto x: m_vec)
            result.push_back(x ? '1' : '0');
        return result;
    }
    static std::string pack(class_name const& val) noexcept {
        return val.pack();
    }
};

template<size_t Size, std::uint8_t Len = 6u>
std::ostream& operator<<(std::ostream& out, Bitpack<Size, Len> const& n) noexcept {
    out << n.print();
    return out;
}

}  // end namespace ps2
