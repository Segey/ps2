/**
 * \file      projects/ps2/ps2/cpp/pack/base64pack.h
 * \brief     The Base64pack class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   February  (the) 04(th), 2019, 16:36 MSK
 * \updated   February  (the) 04(th), 2019, 16:36 MSK
 * \TODO
**/
#pragma once
#include <array>
#include <vector>
#include <string>
#include <cassert>
#include "base64_table.h"

/** \namespace ps2 */
namespace ps2 {

/**
 * \brief The Base64 pack represents a line with 64 value per char. Size - number of pack size.
 * \code
      Base64Pack<10> pack {{0,8,5,63,32,24}};           // 085-vg0000
      auto const& str = pack.pack();                    // 085-vg0000
      auto const& p1 = Base64pack<12, 6>::unpack(str);  // 085-vg0000
      p1.print();                                       // 0856332240000
 * \endcode
 **/
template<size_t Size>
class Base64Pack final {
 public:
    using value_t    = char;
    using class_name = Base64Pack<Size>;

 private:
    std::vector<value_t> m_vec = std::vector<value_t>(Size);

 public:
    explicit Base64Pack() = default;
    Base64Pack(std::array<std::uint8_t, Size> const& lis) noexcept {
        auto const min = std::min(lis.size(), size());
        for(auto i = 0u; i != min; ++i)
            set(i, lis[i]);
    }
    Base64Pack(class_name const&) = default;
    Base64Pack& operator=(class_name const&) = default;
    size_t size() const noexcept {
        return m_vec.size();
    }
	size_t operator[](size_t n) const noexcept {
        auto x = m_vec[n];
        return Base64Table::number(m_vec[n]);
    }
    size_t get(size_t n) const {
        assert(n < size());
        return Base64Table::number(m_vec[n]);
    }
    void set(size_t pos, size_t val) {
        assert(pos < size());
        m_vec[pos] = Base64Table::character(val);
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) noexcept {
        return lhs.m_vec == rhs.m_vec;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) noexcept {
        return !(lhs.m_vec == rhs.m_vec);
    }
    std::string pack() const noexcept {
        std::string result;
        std::copy(m_vec.begin(), m_vec.end(), std::back_inserter(result));
        return result;
    }
    static class_name unpack(std::string const& val) noexcept {
        class_name cls;
        auto const min = std::min(val.size(), cls.size());
        for(auto i = 0u; i != min; ++i)
            cls.m_vec[i] = val[i];
        return cls;
    }
    std::string print() const noexcept {
        std::string result;
        std::for_each(m_vec.begin(), m_vec.end(), [&] (char ch) {
            result += std::to_string(Base64Table::number(ch));
        });
        return result;
    }
    static std::string pack(class_name const& val) noexcept {
        return val.pack();
    }
};

template<size_t Size>
std::ostream& operator<<(std::ostream& out, Base64Pack<Size> const& n) noexcept {
    out << n.print();
    return out;
}

}  // end namespace ps2
