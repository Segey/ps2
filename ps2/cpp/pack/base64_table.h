/**
 * \file      projects/ps2/ps2/cpp/pack/base64_table.h
 * \brief     The Base64_table class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   February  (the) 04(th), 2019, 16:42 MSK
 * \updated   February  (the) 04(th), 2019, 16:42 MSK
 * \TODO
**/
#pragma once
#include <unordered_map>

/** \namespace ps2 */
namespace ps2 {

class Base64Table final {
 public:
    using class_name = Base64Table;

 public:
    static inline std::uint8_t number(char c) {
        return std::unordered_map<char, std::uint8_t> {
            {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}
          , {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9}
          , {'q', 10}, {'w', 11}, {'e', 12}, {'r', 13}, {'t', 14}
          , {'y', 15}, {'u', 16}, {'i', 17}, {'o', 18}, {'p', 19}
          , {'a', 20}, {'s', 21}, {'d', 22}, {'f', 23}, {'g', 24}
          , {'h', 25}, {'j', 26}, {'k', 27}, {'l', 28}, {'z', 29}
          , {'x', 30}, {'c', 31}, {'v', 32}, {'b', 33}, {'n', 34}
          , {'m', 35}, {'Q', 36}, {'W', 37}, {'E', 38}, {'R', 39}
          , {'T', 40}, {'Y', 41}, {'U', 42}, {'I', 43}, {'O', 44}
          , {'P', 45}, {'A', 46}, {'S', 47}, {'D', 48}, {'F', 49}
          , {'G', 50}, {'H', 51}, {'J', 52}, {'K', 53}, {'L', 54}
          , {'Z', 55}, {'X', 56}, {'C', 57}, {'V', 58}, {'B', 59}
          , {'N', 60}, {'M', 61}, {'+', 62}, {'-', 63}
         }[c];
    }
    static inline constexpr char character(std::uint8_t i) {
        char m[] = {"0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM+-"};
        return m[i];
    }
};

}  // end namespace ps2
