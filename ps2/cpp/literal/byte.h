/**
 * \file      ps2/ps2/cpp/literal/byte.h
 * \brief     The Byte class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 14:19 MSK
 * \updated   January (the) 19(th), 2017, 14:19 MSK
 * \TODO      
**/
#pragma once

/** \namespace literal */
inline namespace literal {

constexpr unsigned long long operator "" _b(unsigned long long byte) noexcept {
    return byte;
}
constexpr unsigned long long operator "" _Kb(unsigned long long byte) noexcept {
    return byte * 1024;
}
constexpr unsigned long long operator "" _Mb(unsigned long long byte) noexcept {
    return byte * 1024 * 1024;
}
constexpr unsigned long long operator "" _Gb(unsigned long long byte) noexcept {
    return byte * 1024 * 1024 * 1024;
}

}  // end inline namespace literal
