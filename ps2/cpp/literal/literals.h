/**
 * \file      ps2/ps2/cpp/literal/literals.h
 * \brief     The Literals class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   January (the) 19(th), 2017, 15:00 MSK
 * \updated   January (the) 19(th), 2017, 15:00 MSK
 * \TODO
**/
#pragma once
#include <string>
#include "byte.h"
//#include <ps2/cpp/u128.h>
//#include <ps2/cpp/u256.h>

//constexpr ps2::Uint128 operator "" _u128(unsigned long long val) noexcept {
//    return ps2::Uint128(val);
//}
//constexpr ps2::Uint256 operator "" _u256(unsigned long long val) noexcept {
//    return ps2::Uint256(val);
//}
static inline std::string operator "" _s(char const* str, size_t len) noexcept {
    return std::string(str, len);
}
