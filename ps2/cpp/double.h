/**
 * \file      ps2/ps2/cpp/double.h
 * \brief     Work with double
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   April (the) 17(th), 2015, 14:31 MSK
 * \updated   April (the) 17(th), 2015, 14:31 MSK
 * \TODO
**/
#pragma once

#define PS2_EPSILON_MM 1.0e-3

#ifdef CPPBUILDER2007
#define constexpr
#define noexcept
#define PS2_EPSILON 1.0e-12
#else
#define PS2_EPSILON std::numeric_limits<double>::epsilon()
#endif

/** namespace ps2 */
namespace ps2 {

constexpr inline bool isZero(double a, double aEpsilon = PS2_EPSILON) noexcept {
    return (-aEpsilon < a && a < aEpsilon);
}
constexpr inline bool isEqual(double firstValue, double secondValue
                       , const double aEpsilon = PS2_EPSILON) noexcept {
    return isZero(firstValue - secondValue, aEpsilon);
}
constexpr inline bool isGreater(double firstValue, double secondValue
                     , const double aEpsilon = PS2_EPSILON) noexcept {
    return firstValue > secondValue + aEpsilon;
}
constexpr inline bool isLess(double firstValue, double secondValue
                          , const double aEpsilon = PS2_EPSILON) noexcept {
    return firstValue < secondValue - aEpsilon;
}

} // end namespace ps2

