/**
 * \file      ps2/ps2/cpp/smart_ptr/smart_ptr.h
 * \brief     smart_ptr class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.1
 * \created   May (the) 17(th), 2015, 03:15 MSK
 * \updated   May (the) 17(th), 2015, 03:15 MSK
 * \TODO
**/
#ifndef SmartPtrH
#define SmartPtrH

/** namespace ps2 */
namespace ps2 {

template<class T>
class smart_ptr {
public:
	typedef smart_ptr<T> class_name;
	typedef T            type_name;

private:
	type_name*  m_ptr;

public:
	explicit smart_ptr(type_name* ptr = NULL)
		: m_ptr(ptr) {
	}
	explicit smart_ptr(class_name const& rhs)  {
		m_ptr = new type_name(*rhs.get());
	}
	~smart_ptr()  {
		delete m_ptr;
		m_ptr = NULL;
	}
	class_name& operator=(T* rhs) {
		class_name(rhs).swap(*this);
		return *this;
	}
	class_name& operator=(class_name const& rhs) {
		if(rhs.get()!= get()) {
			delete m_ptr;
			m_ptr = new type_name(*rhs.get());
        }
        return *this;
	}
	class_name& operator=(std::auto_ptr<type_name>& r) {
		class_name(rhs).swap(*this);
		return *this;
	}
	friend bool operator==(class_name const& lhs, class_name const& rhs) {
		return lhs.get() == rhs.get();
	}
	friend bool operator!=(class_name const& lhs, class_name const& rhs) {
		return lhs.get() != rhs.get();
	}
	void reset(type_name* ptr = NULL) {
		class_name(ptr).swap(*this);
	}
	void swap(class_name& rhs) {
		std::swap(m_ptr, rhs.m_ptr);
	}
	T const * get() const {
		return m_ptr;
	}
	T* get() {
		return m_ptr;
	}
	T* operator->() {
		return m_ptr;
	}
	T const * operator->() const {
		return m_ptr;
	}
};

} // namespace ps2
#endif

