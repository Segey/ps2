/**
 * \file      ps2/ps2/cpp/smart_ptr/ptr_vector.h
 * \brief     ptr_vector class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.3
 * \created   May (the) 17(th), 2015, 03:14 MSK
 * \updated   May (the) 17(th), 2015, 03:14 MSK
 * \TODO
**/
#ifndef PtrVectorH
#define PtrVectorH

#include <vector>
#include "shared_ptr.h"

/** namespace ps2 */
namespace ps2 {

template<class T>
class ptr_vector {
public:
    typedef T                               type_t;
    typedef ptr_vector<type_t>              class_name;
    typedef shared_ptr<type_t>              shared_t;
    typedef std::vector<shared_t>           vec_t;
    typedef typename vec_t::reference       reference;
    typedef typename vec_t::const_reference const_reference;
    typedef typename vec_t::value_type      value_type;
    typedef typename vec_t::iterator        iterator;
    typedef typename vec_t::const_iterator  const_iterator;
    typedef typename vec_t::pointer         pointer;

friend class ptr_vector_test;
private:
    vec_t m_vec;

public:
    friend bool operator==(class_name const& lhs, class_name const& rhs) {
        return lhs.m_vec == rhs.m_vec;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) {
        return lhs.m_vec != rhs.m_vec;
    }
    void push_back(value_type const& ptr) {
        m_vec.push_back(ptr);
    }
    void push_back(type_t* ptr) {
        m_vec.push_back(shared_t(ptr));
    }
    void insert(const_iterator pos, value_type const& ptr) {
        m_vec.insert(pos, ptr);
    }
    void insert(iterator pos, value_type const& ptr) {
        m_vec.insert(pos, ptr);
    }
    void insert(const_iterator pos, type_t* ptr) {
        m_vec.insert(pos, shared_t(ptr));
    }
    void insert(iterator pos, type_t* ptr) {
        m_vec.insert(pos, shared_t(ptr));
    }
    value_type const& operator[](size_t index) const {
        return m_vec[index];
    }
    void resize(size_t size) {
        return m_vec.resize(size);
    }
    value_type& operator[](size_t index) {
        return m_vec[index];
    }
    size_t size() const {
        return m_vec.size();
    }
    bool empty() const {
        return m_vec.empty();
    }
    iterator erase(iterator pos) {
        return m_vec.erase(pos);
    }
    void clear() {
        m_vec.clear();
    }
    const_iterator cbegin() const {
        return m_vec.begin();
    }
    const_iterator cend() const {
        return m_vec.end();
    }
    iterator begin() {
        return m_vec.begin();
    }
    iterator end() {
        return m_vec.end();
    }
};

} // namespace ps2
#endif

