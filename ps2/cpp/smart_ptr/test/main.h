/**
 * \file      ps2/ps2/cpp/smart_ptr/test/main.h
 * \brief     smart_ptr ps2 cpp test
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 18(th), 2015, 15:03 MSK
 * \updated   May (the) 18(th), 2015, 15:03 MSK
 * \TODO
**/
#pragma once
#include "ptr_vector_test_test.h"

class MainSmartPtrTest {
public:
    typedef MainSmartPtrTest class_name;

public:
    void operator()() {
        TEST(ptr_vector_test);
    }
};
