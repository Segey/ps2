/**
 * \file      ps2/ps2/cpp/smart_ptr/test/ptr_vector_test_test.h
 * \brief     ptr_vector test class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   May (the) 18(th), 2015, 15:01 MSK
 * \updated   May (the) 18(th), 2015, 15:01 MSK
 * \TODO
**/
#pragma once
#include "../ptr_vector.h"

class ptr_vector_test {
public:
    typedef ptr_vector_test      class_name;
    typedef ps2::ptr_vector<int> tested;

private:
    static tested initSimple() {
        tested cls;
        cls.push_back(new int(5));
        return cls;
    }

private:
    static void EmptyAfterInit() {
        tested cls;
        assertTrue(cls.empty());
    }
    static void OneValueyAfterAddValue() {
        tested const& cls = initSimple();
        assertEquals(cls.size(), static_cast<size_t>(1));
    }
    static void EqualValueAfterAddOne() {
        tested cls = initSimple();

        assertEquals(*(cls[0].get()), 5);
    }
    static void NoneEqualAfterInitOnlyOneVector() {
        tested const& _1 = initSimple();

        tested _2;
        assertTrue(_1 != _2);
    }
    static void EqualVectorsAfterCopies() {
        tested const& _1 = initSimple();

        tested _2;
        std::copy(_1.cbegin(), _1.cend(), std::back_inserter(_2));
        assertTrue(_1 == _2);
    }

public:
    void operator()() noexcept {
        EmptyAfterInit();
        OneValueyAfterAddValue();
        EqualValueAfterAddOne();
        NoneEqualAfterInitOnlyOneVector();
        EqualVectorsAfterCopies();
    }
};
