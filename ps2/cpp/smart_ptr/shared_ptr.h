/**
 * \file      ps2/ps2/cpp/smart_ptr/shared_ptr.h
 * \brief     shared_ptr class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   May (the) 18(th), 2015, 00:13 MSK
 * \updated   May (the) 18(th), 2015, 00:13 MSK
 * \TODO
**/
#ifndef SharedPtrH
#define SharedPtrH

/** namespace ps2 */
namespace ps2 {

template<class T>
class shared_ptr {
public:
    typedef shared_ptr<T> class_name;
    typedef T             type_name;

private:
    class rep {
        typedef rep class_name;

    friend class shared_ptr;
    private:
        type_name* m_ptr;
        int        m_count;

    public:
        rep(type_name* ptr = NULL)
            : m_ptr(ptr), m_count(1) {
        }
        ~rep() {
            delete m_ptr;
        }
    };

private:
    rep* m_rep;

public:
    explicit shared_ptr(type_name* ptr = NULL)
        : m_rep( new rep(ptr) ) {
    }
    shared_ptr(class_name const& rhs)  {
        m_rep = rhs.m_rep;
        ++m_rep->m_count;
    }
    ~shared_ptr()  {
        if(--m_rep->m_count <= 0) {
            delete m_rep;
            m_rep = 0;
        }
    }
    class_name& operator=(class_name const& rhs) {
        if(rhs.m_rep == m_rep) return *this;

        ++rhs.m_rep->m_count;
        class_name(rhs).swap(*this);

        return *this;
    }
    class_name& operator=(T* ptr) {
        class_name(ptr).swap(*this);
        return *this;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) {
        return lhs.get() == rhs.get();
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) {
        return lhs.get() != rhs.get();
    }
    void reset(type_name* ptr = NULL) {
        class_name(ptr).swap(*this);
    }
    void swap(class_name& rhs) {
        std::swap(m_rep, rhs.m_rep);
    }
    T const* get() const {
        return m_rep->m_ptr;
    }
    T* get() {
        return m_rep->m_ptr;
    }
    T* operator->() {
        return m_rep->m_ptr;
    }
    T const* operator->() const {
        return m_rep->m_ptr;
    }
    bool isNull() const {
        return m_rep == NULL;
    }
};

} // namespace ps2
#endif

