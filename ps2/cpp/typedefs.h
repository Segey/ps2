/**
 * \file      ps2/ps2/cpp/typedefs.h
 * \brief     The Typedefs class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   March     (the) 29(th), 2017, 18:07 MSK
 * \updated   March     (the) 29(th), 2017, 18:07 MSK
 * \TODO      
**/
#pragma once
#include <numeric>
#include <limits.h>

#ifndef DBL_MAX
#define DBL_MAX (std::numeric_limits<double>::max())
#endif

#ifndef DBL_MIN
#define DBL_MIN (std::numeric_limits<double>::min())
#endif

#ifndef INT_MAX
#define INT_MAX (std::numeric_limits<int>::max())
#endif

#ifndef UINT_MAX
#define UINT_MAX (std::numeric_limits<unsigned int>::max())
#endif

#ifndef UINT64_MAX
#define UINT64_MAX (std::numeric_limits<std::uint_64_t>::max())
#endif

#if (__cplusplus>=201402L)
#define CONSTEXPR constexpr
#else
#define CONSTEXPR
#endif
