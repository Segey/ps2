/**
 * \file      ps2/ps2/cpp/converts.h
 * \brief     The main file "Converts"
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.2
 * \created   February (the) 01(th), 2012, 13:39 MSK
 * \updated   April    (the) 17(th), 2015, 14:31 MSK
 * \TODO
**/
#pragma once

#include <string>
#include <sstream>

/** namespace ps2 */
namespace ps2 {

/**
 * \brief  This function converts a value from a string
 * \param  {out: class 1} T          = The type is a result of conversion from a string
 * \param  {in:  class 2} C          = The type is a type of string
 * \param  {in:  sdtd::string const&} s = The source string
 * \result A result value from conversion
**/
template<class T, class C>
static T fromString(std::basic_string<C> const& s) noexcept {
    T res;
    std::basic_istringstream<C> iss(s);
    iss >> res;
    return res;
}

/** \brief Convert to String */
template <class T>
static std::string toString(T val) noexcept {
    std::ostringstream oss;
    oss << val;
    return oss.str();
}

/** \brief Convert to WString */
template <class T>
static std::wstring toWString(T val) noexcept {
    std::wostringstream oss;
    oss << val;
    return oss.str();
}

} // namespace ps2
