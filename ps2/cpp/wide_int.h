/**
 * \file      D:/Projects/Perfect/projects/ps2/ps2/cpp/wide_int.h 
 * \brief     The Wide_int class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017 
 * \version   v.1.0
 * \created   June      (the) 01(th), 2017, 17:18 MSK
 * \updated   June      (the) 01(th), 2017, 17:18 MSK
 * \TODO      
 * \NOTE      https://github.com/cerevra/int  
**/
#pragma once
#include <cmath>
#include <cstdint>
#include <iomanip>
#include <limits>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <stdint.h>
#include <system_error>
#include <type_traits>
#include <climits> // CHAR_BIT

namespace ps2 {
    class WideIntTest;
}

namespace std {
enum class wide_integer_s {
    Unsigned = 0,
    Signed = 1
};

template <size_t M, wide_integer_s S>
class wide_integer;

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
struct common_type<wide_integer<M, S>, wide_integer<M2, S2>>;

template <size_t M, wide_integer_s S, class A>
struct common_type<wide_integer<M, S>, A>;

template <class A, size_t M, wide_integer_s S>
struct common_type<A, wide_integer<M, S>>;

template <size_t M, wide_integer_s S>
class wide_integer final {
public:
    using base_type = uint8_t;
    using signed_base_type = int8_t;
    using class_name = wide_integer<M, S>;

public:
    constexpr wide_integer() = default;

    template <class T, typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
    constexpr wide_integer(T rhs) noexcept;

    template <size_t M2, wide_integer_s S2>
    wide_integer(const wide_integer<M2, S2>& rhs) noexcept;

    template <size_t M2, wide_integer_s S2>
    class_name& operator=(wide_integer<M2, S2> const& rhs) noexcept;

    template <class A>
    class_name& operator=(A rhs) noexcept;

    template <class A>
    class_name& operator*=(const A& rhs);

    template <class A>
    class_name& operator/=(const A& rhs);

    template <class A>
    class_name& operator+=(const A& rhs) noexcept(S == wide_integer_s::Unsigned);

    template <class A>
    class_name& operator-=(const A& rhs) noexcept(S == wide_integer_s::Unsigned);

    template <class Integral>
    class_name& operator%=(const Integral& rhs);

    template <class Integral>
    class_name& operator&=(const Integral& rhs) noexcept;

    template <class Integral>
    class_name& operator|=(const Integral& rhs) noexcept;

    template <class Integral>
    class_name& operator^=(const Integral& rhs) noexcept;

    class_name& operator<<=(int n);
    class_name& operator>>=(int n) noexcept;

    class_name& operator++() noexcept(S == wide_integer_s::Unsigned);
    class_name operator++(int) noexcept(S == wide_integer_s::Unsigned);

    class_name& operator--() noexcept(S == wide_integer_s::Unsigned);
    class_name operator--(int) noexcept(S == wide_integer_s::Unsigned);

    explicit operator bool() const noexcept;

    template <class T>
    using __integral_not_wide_integer_class = typename std::enable_if<std::is_arithmetic<T>::value, T>::type;

    template <class T, class = __integral_not_wide_integer_class<T>>
    operator T() const noexcept;

    operator long double() const noexcept;
    operator double() const noexcept;
    operator float() const noexcept;

    struct _impl;

private:
    friend class ps2::WideIntTest;

    template <size_t M2, wide_integer_s S2>
    friend class wide_integer;

    friend struct numeric_limits<wide_integer<M, wide_integer_s::Signed>>;
    friend struct numeric_limits<wide_integer<M, wide_integer_s::Unsigned>>;

    base_type m_arr[_impl::arr_size];
};

template <class T>
static bool ArithmeticConcept() noexcept;
template <class T1, class T2>
using __only_arithmetic = typename std::enable_if<ArithmeticConcept<T1>() && ArithmeticConcept<T2>()>::type;

template <class T>
static bool IntegralConcept() noexcept;
template <class T, class T2>
using __only_integer = typename std::enable_if<IntegralConcept<T>() && IntegralConcept<T2>()>::type;

// Unary operators
template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed> operator~(const wide_integer<M, Signed>& lhs) noexcept;

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed> operator-(const wide_integer<M, Signed>& lhs) noexcept(Signed == wide_integer_s::Unsigned);

template <size_t M, wide_integer_s Signed>
wide_integer<M, Signed> operator+(wide_integer<M, Signed> lhs) noexcept(Signed == wide_integer_s::Unsigned);

// Binary operators
template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>  operator*(const wide_integer<M1, S1>& lhs, const wide_integer<M2, S2>& rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator* (wide_integer<M, S> lhs, T rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator* (T lhs, wide_integer<M, S> rhs);

template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>> operator/ (wide_integer<M1, S1> const& lhs, wide_integer<M2, S2> const& rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator/ (wide_integer<M, S> lhs, T rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator/ (T lhs, wide_integer<M, S> const& rhs); 

template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M1, S1>, wide_integer<M2, S2>>  operator%(const wide_integer<M1, S1>& lhs, const wide_integer<M2, S2>& rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator% (wide_integer<M, S> lhs, T rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator% (T lhs, wide_integer<M, S> const& rhs);

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator+(wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator+ (wide_integer<M, S> lhs, T rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator+ (T lhs, wide_integer<M, S> rhs);

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator-(wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs);
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator- (wide_integer<M, S> lhs, T rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator- (T lhs, wide_integer<M, S> const& rhs); 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator&(wide_integer<M, S> const& lhs, const wide_integer<M2, S2>& rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator& (wide_integer<M, S> lhs, T rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator& (T lhs, wide_integer<M, S> const& rhs); 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator|(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator| (wide_integer<M, S> lhs, T rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator| (T lhs, wide_integer<M, S> const& rhs); 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
std::common_type_t<wide_integer<M, S>, wide_integer<M2, S2>>  operator^(wide_integer<M, S> const& lhs , wide_integer<M2, S2> const& rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator^ (wide_integer<M, S> lhs, T rhs); 
template<size_t M, wide_integer_s S, class T>
std::common_type_t<wide_integer<M, S>, wide_integer<M, S>> operator^ (T lhs, wide_integer<M, S> const& rhs); 

template <size_t M, wide_integer_s S>
 wide_integer<M, S> operator<<(wide_integer<M, S> const& lhs, int n) noexcept; 
template <size_t M, wide_integer_s S>
 wide_integer<M, S> operator>>(wide_integer<M, S> const& lhs, int n) noexcept; 

template <size_t M1, wide_integer_s S1, size_t M2, wide_integer_s S2>
bool operator< (wide_integer<M1, S1> const& lhs, wide_integer<M2, S2> const& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator< (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator< (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template<size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator> (wide_integer<M, S> const& lhs, wide_integer<M2, S2> const& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator> (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator> (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template<size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator<= (const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator<= (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator<= (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator>= (const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator>= (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator>= (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator==(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator== (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator== (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template <size_t M, wide_integer_s S, size_t M2, wide_integer_s S2>
bool operator!=(const wide_integer<M, S>& lhs, const wide_integer<M2, S2>& rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator!= (wide_integer<M, S> const& lhs, T rhs) noexcept; 
template<size_t M, wide_integer_s S, class T>
bool operator!= (T lhs, wide_integer<M, S> const& rhs) noexcept; 

template <size_t M, wide_integer_s S>
std::ostream& operator<<(std::ostream& out, wide_integer<M, S> const& n); 

template <size_t M, wide_integer_s S>
std::wostream& operator<<(std::wostream& out, wide_integer<M, S> const& n); 

template <size_t M, wide_integer_s S>
std::istream& operator>>(std::istream& in, wide_integer<M, S>& n); 

template <size_t M, wide_integer_s S>
std::wistream& operator>>(std::wistream& in, wide_integer<M, S>& n); 

template <size_t M, wide_integer_s S>
std::string to_string(wide_integer<M, S> const& n); 

template <size_t M, wide_integer_s S>
std::wstring to_wstring(const wide_integer<M, S>& n); 

struct to_chars_result {
    char* ptr;
    std::error_code ec;
};

struct from_chars_result {
    const char* ptr;
    std::error_code ec;
};

template <size_t M, wide_integer_s Signed>
to_chars_result to_chars(char* first, char* last, const wide_integer<M, Signed>& value, int base = 10);

template <size_t M, wide_integer_s Signed>
from_chars_result from_chars(const char* first, const char* last, wide_integer<M, Signed>& value, int base = 10);

inline namespace literals {
inline namespace wide_integer_literals {
template <size_t M>
using wide_int = wide_integer<M, wide_integer_s::Signed>;

template <size_t M>
using wide_uint = wide_integer<M, wide_integer_s::Unsigned>;

using int128_t = wide_int<128 / CHAR_BIT / sizeof(long)>;
using uint128_t = wide_uint<128 / CHAR_BIT / sizeof(long)>;

using int256_t = wide_int<256 / CHAR_BIT / sizeof(long)>;
using uint256_t = wide_uint<256 / CHAR_BIT / sizeof(long)>;

using int512_t = wide_int<512 / CHAR_BIT / sizeof(long)>;
using uint512_t = wide_uint<512 / CHAR_BIT / sizeof(long)>;
} // namespace wide_integer_literals
} // namespace literals

std::int128_t operator"" _int128(const char* n);
std::int256_t operator"" _int256(const char* n);
std::int512_t operator"" _int512(const char* n);
std::uint128_t operator"" _uint128(const char* n);
std::uint256_t operator"" _uint256(const char* n);
std::uint512_t operator"" _uint512(const char* n);

// numeric limits
template <size_t M, wide_integer_s Signed>
struct numeric_limits<wide_integer<M, Signed>>;

template <size_t M, wide_integer_s Signed>
struct hash<wide_integer<M, Signed>>;

} // namespace std

#include "impl/wide_int/wide_int_impl.h"
