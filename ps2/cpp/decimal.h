/**
 * \file      /home/dix/projects/perfect/projects/ps2/ps2/cpp/decimal.h
 * \brief     The Decimal class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2017
 * \version   v.1.0
 * \created   June      (the) 04(th), 2017, 10:38 MSK
 * \updated   June      (the) 04(th), 2017, 10:38 MSK
 * \TODO
**/
#pragma once
#include <cmath>
#include <cstdint>
#include <iomanip>
#include <limits>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <stdint.h>
#include <system_error>
#include <type_traits>
#include <climits>
#include <ps2/cpp/string_algo.h>
#include <ps2/cpp/algorithm.h>
#include "wide_int.h"

namespace ps2 {

template <class T, size_t S>
class decimal final {
 //public: decimal<T, S>;

friend struct std::numeric_limits<decimal<T, S>>;

 public:
    using value_t    = T;
    using integral_t = T;
    using fraction_t = typename std::make_unsigned<T>::type;
    using class_name = decimal<T, S>;

 private:
    value_t m_value{0};

 private:
    constexpr static inline int ipow() noexcept {
        return std::pow(10, S);
    }

   template<class T1, class T2, std::size_t S1, std::size_t S2>
   constexpr static inline auto cxpow() noexcept
    -> typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type {
        return S1 > S2 ? static_cast<T1>(std::pow(10, S1 - S2))
                       : static_cast<T2>(std::pow(10, S2 - S1))
        ;
    }
    template<class I
      , typename std::enable_if<std::is_arithmetic<I>::value, int>::type = 0>
    constexpr static inline value_t createVal(I _1, fraction_t _2) noexcept {
        auto const& val = value_t(ps2::abs(_1)) * ipow() + _2;
        return _1 >= 0 ? val : -val;
    }

    template<class I
      , typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    constexpr static inline value_t createVal(I const& _1, fraction_t const& _2) noexcept {
        auto const& val = value_t(ps2::abs(_1)) * ipow() + _2;
        return _1 >= 0 ? val : -val;
    }

    static inline value_t mod(long double val) noexcept {
        long double z;
        auto x = std::modf(val, &z);
        return createVal(z, value_t(ipow() * x));
    }

    template<class T1, std::size_t S1>
    constexpr static inline value_t normalize(decimal<T1, S1> const& val) noexcept {
         if(S == S1)
             return val.value();

         auto const i = cxpow<T,T1,S,S1>();
         value_t result(val.value());
         if(S > S1)
             result *= i;
         else
             result /= i;
         return result;
    }

public:
    constexpr decimal() = default;
    template <class I
      , typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    constexpr decimal(I val)  noexcept(std::is_nothrow_constructible<T, I>::value)
        :  m_value(mod(val)) {
    }
    template <class I
      , typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    constexpr decimal(I rhs) noexcept(std::is_nothrow_constructible<T, I>::value)
        : m_value(createVal(rhs, 0)) {
    }
    template <class T1, class T2>
    constexpr decimal(T1 integral, T2 fraction)
        noexcept(std::is_nothrow_constructible<T, T1>::value &&
                 std::is_nothrow_constructible<T, T2>::value)
        : m_value(createVal(integral, fraction)) {
    }
    constexpr decimal(decimal<T, S> const& rhs) noexcept
        : m_value(rhs.value()) {
    }
    template<class T1>
    constexpr decimal(decimal<T1, S> const& rhs)
        noexcept(std::is_nothrow_constructible<decimal<T1,S>>::value)
        : m_value(rhs.value()) {
    }
    template <class T1, size_t S1>
    constexpr decimal(decimal<T1, S1> const& rhs)
        noexcept(std::is_nothrow_constructible<decimal<T1,S1>>::value)
        : m_value(normalize(rhs)) {
    }
    friend class_name operator~(class_name lhs) noexcept {
        lhs.m_value =~lhs.m_value;
        return lhs;
    }
    friend class_name operator-(class_name lhs) noexcept {
        lhs.m_value =-lhs.m_value;
        return lhs;
    }
    friend class_name operator+(class_name lhs) noexcept {
        return lhs;
    }
    template <class T2, size_t S2>
    class_name& operator=(decimal<T2, S2> const& rhs) noexcept {
        m_value = rhs;
        return *this;
    }
    template <class I
      , typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    class_name& operator=(I rhs)
        noexcept(std::is_nothrow_assignable<T, I>::value) {
        m_value = mod(rhs);
        return *this;
    }

    template <class I
      , typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    class_name& operator=(I rhs)
        noexcept(std::is_nothrow_assignable<T, I>::value) {
        m_value = createVal(rhs, 0);
        return *this;
    }

    template <class T2, size_t S2>
    constexpr class_name& operator*=(decimal<T2,S2> const& rhs) noexcept;

    template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    constexpr class_name& operator*=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    constexpr class_name& operator*=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    constexpr class_name& operator*=(I const& rhs) noexcept;

    template<class T1, std::size_t S1, class T2, std::size_t S2>
    friend constexpr auto operator*(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
        -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
        std::max(S1, S2)>;

    template <class T2, size_t S2>
    constexpr class_name& operator/=(decimal<T2,S2> const& rhs);

    template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    constexpr class_name& operator/=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    constexpr class_name& operator/=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    constexpr class_name& operator/=(I const& rhs) noexcept;

    template <class T2, size_t S2>
    constexpr class_name& operator+=(decimal<T2,S2> const& rhs) noexcept;

    template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    constexpr class_name& operator+=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    constexpr class_name& operator+=(I rhs) noexcept;

    template<class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    constexpr class_name& operator+=(I const& rhs) noexcept;

    template<class T1, std::size_t S1, class T2, std::size_t S2>
    friend constexpr auto operator+(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
        -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
        std::max(S1, S2)>;


    template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    constexpr class_name& operator-=(I rhs) noexcept;
    template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    constexpr class_name& operator-=(I rhs) noexcept;
    template<class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    constexpr class_name& operator-=(I const& rhs) noexcept;
    template <class T2, size_t S2>
    constexpr class_name& operator-=(decimal<T2,S2> const& rhs) noexcept;

    template<class T1, std::size_t S1, class T2, std::size_t S2>
    friend constexpr auto operator-(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
        -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
        std::max(S1, S2)>;

    constexpr class_name& operator<<=(int n);

    friend constexpr class_name operator<<(class_name lhs, int n) {
        lhs <<= n;
        return lhs;
    }

    friend constexpr std::istream& operator>>(std::istream& in, class_name& n) {
        in >> n.m_value;
        return in;
    }

    constexpr class_name& operator>>=(int n) noexcept;

    friend constexpr class_name operator>>(class_name lhs, int n) noexcept {
        lhs >>= n;
        return lhs;
    }

    constexpr class_name& operator++() noexcept;
    constexpr class_name operator++(int) noexcept;

    constexpr class_name& operator--() noexcept;
    constexpr class_name operator--(int) noexcept;

    value_t const& value() const noexcept {
        return m_value;
    }
    integral_t integral() const noexcept {
        return m_value / ipow();
    }

    fraction_t fractional() const noexcept {
        return ps2::abs(m_value) % ipow();
    }

    template <class I
      , typename std::enable_if<std::is_floating_point<I>::value, int>::type = 0>
    operator I() const noexcept {
        I value = static_cast<I>(integral());
        value += static_cast<I>(fractional()) / ipow()
                    * (m_value >= 0 ? 1 : -1);
        return value;
    }

    template <class I
      , typename std::enable_if<std::is_integral<I>::value, int>::type = 0>
    operator I() const noexcept {
        return static_cast<I>(integral());
    }

    template <class I
      , typename std::enable_if<std::is_wide_int<I>::value, int>::type = 0>
    operator I() const noexcept {
        return static_cast<I>(integral());
    }

    explicit constexpr operator bool() const noexcept {
        return static_cast<bool>(m_value);
    }

    void swap(class_name& rhs) noexcept {
        std::swap(m_value, rhs.m_value);
    }
};

template <class T, size_t S>
static inline void swap(decimal<T,S>& lhs, decimal<T,S>& rhs) noexcept {
    lhs.swap(rhs);
}


template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator==(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() == rhs.value())) {
    return lhs.value() == rhs.value();
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator!=(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() != rhs.value())) {
    return lhs.value() != rhs.value();
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator>(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() > rhs.value())) {
    return lhs.value() > rhs.value();
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator>=(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() >= rhs.value())) {
    return lhs.value() >= rhs.value();
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator<(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() < rhs.value())) {
    return lhs.value() < rhs.value();
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr inline bool operator<=(decimal<T1,S1> const& lhs , decimal<T2,S2> const& rhs)
        noexcept(noexcept(lhs.value() <= rhs.value())) {
    return lhs.value() <= rhs.value();
}

//!< operator*=
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator*=(I rhs) noexcept {
    m_value *= value_t(rhs);
    return *this;
}
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator*=(I rhs) noexcept {
    m_value = static_cast<I>(m_value) * rhs;
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator*=(I const& rhs) noexcept {
    m_value *= value_t(rhs);
    return *this;
}
template<class T1, std::size_t S1>
template<class T2, std::size_t S2>
constexpr decimal<T1, S1>& decimal<T1, S1>::operator*=(decimal<T2,S2> const& rhs) noexcept {
    m_value *= decimal<T1,S1>::normalize(rhs);
    m_value /= ipow();
    return *this;
}

//!< operator*
template<class T, std::size_t S, class I>
decimal<T,S> operator*(decimal<T, S> lhs, I rhs) {
     lhs *= rhs;
     return lhs;
}
template<class T, std::size_t S, class I>
decimal<T, S> operator* (I lhs, decimal<T, S> rhs) {
     rhs *= lhs;
     return rhs;
}
template<class T, std::size_t S>
decimal<T, S> operator*(decimal<T, S> lhs, decimal<T, S> const& rhs) noexcept {
     lhs *= rhs;
     return lhs;
}
template<class T1, class T2, std::size_t S>
auto operator*(decimal<T1, S> const& lhs , decimal<T2, S> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>
{
     using result_t = decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>;
     result_t result(lhs);
     result *= rhs;
     return result;
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr auto operator*(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
    std::max(S1, S2)>
{
    constexpr std::size_t M = std::max(S1, S2);
    using R = typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type;
    decimal<R,M> result(lhs);
    result *= rhs;
    return result;
}

//!< operator/=
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator/=(I rhs) noexcept {
    m_value /= value_t(rhs);
    return *this;
}
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator/=(I rhs) noexcept {
    m_value = static_cast<I>(m_value) / rhs;
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator/=(I const& rhs) noexcept {
    m_value /= value_t(rhs);
    return *this;
}
template<class T1, std::size_t S1>
template<class T2, std::size_t S2>
constexpr decimal<T1, S1>& decimal<T1, S1>::operator/=(decimal<T2,S2> const& rhs) {
    auto const val = decimal<T1,S1>::normalize(rhs);
    auto const _1 = static_cast<long double>(m_value)
            / static_cast<long double>(val);
    m_value = mod(_1);
    return *this;
}

//!< operator/
template<class T, std::size_t S, class I>
decimal<T,S> operator/(decimal<T, S> lhs, I rhs) {
     lhs /= rhs;
     return lhs;
}
template<class T, std::size_t S, class I>
decimal<T, S> operator/ (I lhs, decimal<T, S> const& rhs) {
    decimal<T, S> result(lhs);
    result /= rhs;
    return result;
}
template<class T, std::size_t S>
decimal<T, S> operator/(decimal<T, S> lhs, decimal<T, S> const& rhs) noexcept {
     lhs /= rhs;
     return lhs;
}
template<class T1, class T2, std::size_t S>
auto operator/(decimal<T1, S> const& lhs , decimal<T2, S> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>
{
     using result_t = decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>;
     result_t result(lhs);
     result /= rhs;
     return result;
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr auto operator/(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
    std::max(S1, S2)>
{
    constexpr std::size_t M = std::max(S1, S2);
    using R = typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type;
    decimal<R,M> result(lhs);
    result /= rhs;
    return result;
}

//!< operator+=
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator+=(I rhs) noexcept{
    m_value += createVal(rhs,0);
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if
          <std::is_floating_point<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator+=(I rhs) noexcept{
    m_value += mod(rhs);
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator+=(I const& rhs) noexcept {
    m_value += createVal(rhs,0);
    return *this;
}
template<class T1, std::size_t S1>
template<class T2, std::size_t S2>
constexpr decimal<T1, S1>& decimal<T1, S1>::operator+=(decimal<T2,S2> const& rhs) noexcept {
    m_value += decimal<T1,S1>::normalize(rhs);
    return *this;
}

//!< operator+
template<class T, std::size_t S, class I>
decimal<T,S> operator+(decimal<T, S> lhs, I rhs) noexcept {
     lhs += decimal<T,S>(rhs);
     return lhs;
}
template<class T, std::size_t S, class I>
decimal<T, S> operator+ (I lhs, decimal<T, S> rhs) noexcept {
     rhs += lhs;
     return rhs;
}
template<class T, std::size_t S>
decimal<T, S> operator+(decimal<T, S> lhs, decimal<T, S> const& rhs) noexcept {
     lhs += rhs;
     return lhs;
}
template<class T1, class T2, std::size_t S>
auto operator+(decimal<T1, S> const& lhs , decimal<T2, S> const& rhs) noexcept
        -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>
{
     using result_t = decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>;
     result_t result(lhs);
     result += rhs;
     return result;
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr auto operator+(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
    std::max(S1, S2)>
{
    constexpr std::size_t M = std::max(S1, S2);
    using R = typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type;
    decimal<R,M> result(lhs);
    result += rhs;
    return result;
}

//!< operator-=
template<class T, std::size_t S>
template<class I, typename std::enable_if<std::is_integral<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator-=(I rhs) noexcept {
    m_value -= createVal(rhs,0);
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if<std::is_floating_point<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator-=(I rhs) noexcept {
    m_value -= mod(rhs);
    return *this;
}
template<class T, std::size_t S>
template <class I, typename std::enable_if<std::is_wide_int<I>::value, int>::type>
constexpr decimal<T, S>& decimal<T, S>::operator-=(I const& rhs) noexcept {
    m_value -= decimal<T,S>::normalize(rhs);
    return *this;
}
template<class T1, std::size_t S1>
template<class T2, std::size_t S2>
constexpr decimal<T1, S1>& decimal<T1, S1>::operator-=(decimal<T2,S2> const& rhs) noexcept {
    m_value -= decimal<T1,S1>::normalize(rhs);
    return *this;
}

//!< operator-
template<class T, std::size_t S, class I>
decimal<T,S> operator-(decimal<T, S> lhs, I rhs) noexcept {
     lhs -= decimal<T,S>(rhs);
     return lhs;
}
template<class T, std::size_t S, class I>
decimal<T, S> operator- (I lhs, decimal<T, S> const& rhs) noexcept {
     return decimal<T,S>(lhs) - rhs;
}
template<class T, std::size_t S>
decimal<T, S> operator-(decimal<T, S> lhs, decimal<T, S> const& rhs) noexcept {
     lhs -=rhs;
     return lhs;
}
template<class T1, class T2, std::size_t S>
auto operator-(decimal<T1, S> const& lhs , decimal<T2, S> const& rhs) noexcept
        -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>
{
     using result_t = decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type, S>;
     result_t result(lhs);
     result -= rhs;
     return result;
}
template<class T1, std::size_t S1, class T2, std::size_t S2>
constexpr auto operator-(decimal<T1, S1> const& lhs
                          , decimal<T2, S2> const& rhs) noexcept
    -> decimal<typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type,
    std::max(S1, S2)>
{
    constexpr std::size_t M = std::max(S1, S2);
    using R = typename std::conditional<sizeof(T1) >= sizeof(T2), T1, T2>::type;
    decimal<R,M> result(lhs);
    result -= rhs;
    return result;
}

//!< operator <<=
template<class T, std::size_t S>
constexpr decimal<T,S>& decimal<T,S>::operator<<=(int n) {
    m_value <<= n;
    return *this;
}

template<class T, std::size_t S>
constexpr decimal<T,S>& decimal<T,S>::operator>>=(int n) noexcept {
    m_value >>= n;
    return *this;
}

template<class T, std::size_t S>
constexpr decimal<T, S>& decimal<T, S>::operator++() noexcept {
    ++m_value;
        return *this;
    }

template<class T, std::size_t S>
constexpr decimal<T, S> decimal<T, S>::operator++(int) noexcept {
    class_name temp(*this);
    ++*this;
    return temp;
}

template<class T, std::size_t S>
constexpr decimal<T, S>& decimal<T, S>::operator--() noexcept {
    --m_value;
    return *this;
}

template<class T, std::size_t S>
constexpr decimal<T, S> decimal<T, S>::operator--(int) noexcept {
    class_name temp(*this);
    --*this;
    return temp;
}

template<class T, std::size_t S>
std::wostream& operator<<(std::wostream& out, decimal<T, S> const& n) {
    out << n.value();
    return out;
}

template<class T, std::size_t S>
std::ostream& operator<<(std::ostream& out, decimal<T, S> const& n) {
    out << n.value();
    return out;
}

template <class T, std::size_t S>
std::wistream& operator>>(std::wistream& in, decimal<T, S>& n) {
    in >> n.m_value;
    return in;
}

template <class T, std::size_t S>
static std::string to_string(decimal<T,S> const& n) {
    return std::to_string(n.integral()) + '.'
           + (n.fractional() == 0 ?  "0"
            : ps2::leftJustified(std::to_string(n.fractional()), S, '0'));
}

template <class T, std::size_t S
    , typename std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
static bool from_string(std::string const& s, decimal<T,S>& val) {
    using result_t = decimal<T, S>;

    for(auto ch : s)
        if(!(std::isdigit(ch) || ch == '.'))
            return false;

    if(s.size() < 1 || std::count(s.begin(), s.end(), '.') > 1)
        return false;

    try {
        auto pos = s.find('.');
        if(pos == std::string::npos)
            pos = s.size();
        else
            if(s.size() - pos > S + 1)
                return false;

        auto const v1 = std::stoll(std::string(&s[0], &s[pos]));
        int v2 = 0;
        if(pos < s.size() - 1)
            v2 = std::stoi(std::string(&s[pos+1], &s[s.size()]));

        val = result_t(v1, v2 * std::pow(10,S - (s.size() - pos - 1)));
        return true;
    } catch(...) {
		return false;
    }
    return false;
}

template <class T, std::size_t S
    , typename std::enable_if_t<std::is_wide_int_v<T>, int> = 0>
static bool from_string(std::string const& s, decimal<T,S>& val) {
    using result_t = decimal<T, S>;

    for(auto ch : s)
        if(!(std::isdigit(ch) || ch == '.'))
            return false;

    if(s.size() < 1 || std::count(s.begin(), s.end(), '.') > 1)
        return false;

    auto pos = s.find('.');
    if(pos == std::string::npos)
        pos = s.size();
    else
        if(s.size() - pos > S + 1)
            return false;

    T v1;
    std::from_chars(&s[0], &s[pos], v1, 10);

    T v2 = 0;
    if(pos < s.size() - 1)
        std::from_chars(&s[pos+1], &s[s.size()], v2, 10);

    val = result_t(v1, v2 * std::pow(10,S - (s.size() - pos - 1)));
    return true;
}

#ifdef QT
#include <QString>
#include <QLocale>

template <class T, std::size_t S>
static QString to_qstr(decimal<T,S> const& n, QLocale const& loc = {}) {
    auto const& i = loc.toString(static_cast<long long int>(n.integral()));

    if(n.fractional() == 0) return i;

    auto const& f
            = ps2::leftJustified(std::to_string(n.fractional()), S, '0');
    return  i + loc.decimalPoint() + QString::fromStdString(f);
}

template <class T, std::size_t S>
static QString to_sqstr(decimal<T,S> const& n) {
    auto const& i = QString::fromStdString(std::to_string(n.integral()));

    if(n.fractional() == 0) return i;

    auto const& f
            = ps2::leftJustified(std::to_string(n.fractional()), S, '0');
    return  i + QChar::fromLatin1('.') + QString::fromStdString(f);
}

template <class T, std::size_t S>
static bool from_qstr(QString s, decimal<T,S>& val, QLocale const& loc = {}) {
    if(loc.language() == QLocale::Language::Russian) {
        s.replace(QChar::fromLatin1(' '), QString());
        if(s.indexOf(QChar::fromLatin1('.')) != -1)
            return false;
    }

    s.replace(loc.groupSeparator(), QString());
    s.replace(loc.decimalPoint(), QChar::fromLatin1('.'));
    return from_string(s.toStdString(), val);
}

template <class T, std::size_t S>
static bool from_sqstr(QString s, decimal<T,S>& val) {
    return from_string(s.toStdString(), val);
}

template <class T, std::size_t S>
constexpr bool is_null(decimal<T,S> const& val) noexcept {
    return val == tune_up_t<decimal<T,S>>::null();
};

template <class T, std::size_t S>
static bool is_valid(decimal<T,S> const& val) noexcept {
    return !is_null(val);
}

template <class T, std::size_t S>
static inline QDebug operator<<(QDebug dbg, decimal<T,S> const& d) {
    return dbg.nospace()
        << "\"decimal\": {"
        << "\"value\": " << d.value()
        << ", \"integral\": " << d.integral()
        << ", \"fractional\": " << d.fractional()
        << "}";
}
#endif
} // namespace ps2

namespace  std {

template <class T, size_t S>
class numeric_limits<ps2::decimal<T, S>> {
public:
    using decimal_t  = ps2::decimal<T, S>;
    using class_name = numeric_limits<decimal_t>;

public:
    static constexpr bool is_specialized = std::numeric_limits<T>::is_specialized;
    static constexpr bool is_signed = std::numeric_limits<T>::is_signed;
    static constexpr bool is_integer = std::numeric_limits<T>::is_integer;
    static constexpr bool is_exact = std::numeric_limits<T>::is_exact;
    static constexpr bool has_infinity = std::numeric_limits<T>::has_infinity;
    static constexpr bool has_quiet_NaN = std::numeric_limits<T>::has_quiet_NaN;
    static constexpr bool has_signaling_NaN = std::numeric_limits<T>::has_signaling_NaN;
    static constexpr std::float_denorm_style has_denorm = std::numeric_limits<T>::has_denorm;
    static constexpr bool has_denorm_loss = std::numeric_limits<T>::has_denorm_loss;
    static constexpr std::float_round_style round_style = std::numeric_limits<T>::round_style;
    static constexpr bool is_iec559 = std::numeric_limits<T>::is_iec559;
    static constexpr bool is_bounded = std::numeric_limits<T>::is_bounded;
    static constexpr bool is_modulo = std::numeric_limits<T>::is_modulo;
    static constexpr int digits = std::numeric_limits<T>::digits;
    static constexpr int digits10 = std::numeric_limits<T>::digits10;
    static constexpr int max_digits10 = std::numeric_limits<T>::max_digits10;
    static constexpr int radix = std::numeric_limits<T>::radix;
    static constexpr int min_exponent = std::numeric_limits<T>::min_exponent;
    static constexpr int min_exponent10 = std::numeric_limits<T>::min_exponent10;
    static constexpr int max_exponent = std::numeric_limits<T>::max_exponent;
    static constexpr int max_exponent10 = std::numeric_limits<T>::max_exponent10;
    static constexpr bool traps = std::numeric_limits<T>::traps;
    static constexpr bool tinyness_before = std::numeric_limits<T>::tinyness_before;

    static decimal_t min() noexcept {
        return std::numeric_limits<T>::min();
    }
    static decimal_t lowest() noexcept {
        return std::numeric_limits<T>::lowest();
    }
    static decimal_t max() noexcept {
        return std::numeric_limits<T>::max();
    }
    constexpr static decimal_t epsilon() noexcept {
        return std::numeric_limits<T>::epsilon();
    }
    constexpr static decimal_t round_error() noexcept {
        return std::numeric_limits<T>::round_error();
    }
    constexpr static decimal_t infinity() noexcept {
        return std::numeric_limits<T>::infinity();
    }
    constexpr static decimal_t quiet_NaN() noexcept {
        return std::numeric_limits<T>::quiet_NaN();
    }
    constexpr static decimal_t signaling_NaN() noexcept {
        return std::numeric_limits<T>::signaling_NaN();
    }
    constexpr static decimal_t denorm_min() noexcept {
        return std::numeric_limits<T>::denorm_min();
    }
};

} // namespace std

