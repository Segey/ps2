/**
 * \file      C:/Users/spanin/Portables/git/bin/git_irondoom/projects/Editor/ICE (Irondoom Cryptoghaphy Editor)/file.h
 * \brief     The File class provides
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019
 * \version   v.1.0
 * \created   January   (the) 11(th), 2019, 15:37 MSK
 * \updated   January   (the) 11(th), 2019, 15:37 MSK
 * \TODO
**/
#pragma once
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>

/** \namespace ps2 */
namespace ps2 {

template<class T>
class File final {
 public:
    using char_t     = T;
    using str_t      = std::basic_string<T>;
    using class_name = File<T>;

 private:
    str_t m_file;

 public:
    explicit File(str_t const& file = str_t()) noexcept
        : m_file(file) {
    }
    void setFileName(str_t const& file) noexcept {
        m_file = file;
    }
    str_t const& fileName() const noexcept {
        return m_file;
    }
    friend bool operator==(class_name const& lhs, class_name const& rhs) {
        return lhs.m_file == rhs.m_file;
    }
    friend bool operator!=(class_name const& lhs, class_name const& rhs) {
        return !(lhs == rhs);
    }
    std::uint64_t size() const noexcept {
        return class_name::size(m_file);
    }
    void compareFile(str_t const& _1) noexcept {
		class_name::compareFiles(_1, m_file);
	}
	bool copyFile(str_t const& from) noexcept {
		return class_name::copyFile(from, m_file);
    }
    static std::uint64_t size(str_t const& file) noexcept {
		std::basic_ifstream<char_t> f(file.c_str(), ios::in);

        auto size = f.tellg();
        static_assert(sizeof(size) >= sizeof(std::uint64_t), "TOO big size");

        f.seekg(0, ios::end);
        size = f.tellg() - size;
        f.close();
        return size;
    }
	static bool compareFiles(str_t const& _1, str_t const& _2) noexcept {
		std::basic_ifstream<char_t> f1(_1, std::ifstream::binary|std::ifstream::ate);
		std::basic_ifstream<char_t> f2(_2, std::ifstream::binary|std::ifstream::ate);

		if (f1.fail() || f2.fail())
            return false;

        if (f1.tellg() != f2.tellg())
            return false;

        //seek back to beginning and use std::equal to compare contents
        f1.seekg(0, std::ifstream::beg);
        f2.seekg(0, std::ifstream::beg);

		return std::equal(std::istreambuf_iterator<char_t>(f1.rdbuf()),
						  std::istreambuf_iterator<char_t>(),
                          std::istreambuf_iterator<char_t>(f2.rdbuf()));
	}
	static bool copyFile(str_t const& from, str_t const& to) noexcept {
		std::basic_ifstream<T>  src{from,  std::ios::binary};
        std::basic_ofstream<T>  dst{to, std::ios::binary};
		dst << src.rdbuf();
		return dst.good();
	};
};
} // end namespace ps2

